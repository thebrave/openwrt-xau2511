# 
# Copyright (C) 2006 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

KDIR:=$(BUILD_DIR)/linux-$(KERNEL)-$(BOARD)
MODULE_NAME:=$(MODELNAME)
FW_VERSION:=V1.1.2.6
VALID_REGION:=0

ifndef FIRMWARE_REGION
  FW_REGION:=""
  VALID_REGION:=1
else
  ifeq ($(FIRMWARE_REGION),WW)
    FW_REGION:=""
    VALID_REGION:=1
  endif
  ifeq ($(FIRMWARE_REGION),NA)
    FW_REGION:="NA"
    VALID_REGION:=1
  endif
  ifeq ($(FIRMWARE_REGION),CE)
    FW_REGION:="CE"
    VALID_REGION:=1
  endif
endif

ifeq ($(VALID_REGION),0)
  FW_REGION:=""
  VALID_REGION:=1
endif

ifeq ($(FW_REGION),"")
  HDR_REGION:="WW"
else
  HDR_REGION:=$(FW_REGION)
endif

KERNEL_IMAGE_NAME=vmlinux.img
KIMAGE_PADDED=vmlinux.burn
RIMAGE_PADDED=root.bin
INFO_HEAD_LENGTH:=128
HEAD_LENGTH:=126
H_VERSION:=WNR2000
FLASH_BLOCK="\x12\x32"
KERNEL_LENGTH=1048576
ROOTFS_LENGTH=2359296
#ROOTFS_LENGTH=2097152

#############################################
# RTL8196C plafform temporality used
#############################################

ifeq ("$(BOARD)", "rtl8196c")
  MODULE_NAME=XAU2511
  FW_VERSION=V0.2.1.7_GPL
  H_VERSION=XAU2511
  KERNEL_LENGTH=1048576
  ROOTFS_LENGTH=2162688
endif
#############################################

KERNEL_ENTRY=`readelf -a $(BIN_DIR)/vmlinux.$(BOARD)|grep "Entry"|cut -d":" -f 2`

ifneq ($(CONFIG_BIG_ENDIAN),y)
JFFS2OPTS     :=  --pad --little-endian --squash
SQUASHFS_OPTS :=  -le
else
JFFS2OPTS     :=  --pad --big-endian --squash
SQUASHFS_OPTS :=  -be
endif

ifneq ($(CONFIG_TARGET_ROOTFS_INITRAMFS),y)
  ifeq ($(CONFIG_TARGET_ROOTFS_JFFS2),y)
    define Image/mkfs/jffs2
		rm -rf $(BUILD_DIR)/root/jffs	
		#$(STAGING_DIR)/bin/mkfs.jffs2 $(JFFS2OPTS) -e 0x10000 -o $(KDIR)/root.jffs2-64k -d $(BUILD_DIR)/root
		ln -sf /var/resolv.conf $(BUILD_DIR)/root/etc/resolv.conf
		$(STAGING_DIR)/bin/mkfs.jffs2 $(JFFS2OPTS) -e 0x20000 -o $(KDIR)/root.jffs2-128k -d $(BUILD_DIR)/root -D device_table.txt

		# add End-of-Filesystem markers
		#echo -ne '\xde\xad\xc0\xde' >> $(KDIR)/root.jffs2-64k
		#echo -ne '\xde\xad\xc0\xde' >> $(KDIR)/root.jffs2-128k
	
		$(call Image/Build,jffs2-64k)
		$(call Image/Build,jffs2-128k)
    endef
  endif
    
  ifeq ($(CONFIG_TARGET_ROOTFS_SQUASHFS),y)
    define Image/mkfs/squashfs
		@mkdir -p $(BUILD_DIR)/root/jffs
		rm -fr $(BUILD_DIR)/root/jffs $(BUILD_DIR)/root/rom 
		ln -sf /var/resolv.conf $(BUILD_DIR)/root/etc/resolv.conf
		cd $(BUILD_DIR)/root/etc; rm -fr config hostapd.conf host hotplug.d profile protocols sysctl.conf banner
		rm -fr $(BUILD_DIR)/root/lib/config $(BUILD_DIR)/root/lib/network $(BUILD_DIR)/root/sbin/hotplug $(BUILD_DIR)/root/sbin/ifup $(BUILD_DIR)/root/sbin/ifdown $(BUILD_DIR)/root/usr/lib/common.awk $(BUILD_DIR)/root/ipkg $(BUILD_DIR)/root/usr/lib/ipkg
		echo "$(MODULE_NAME)" > $(BUILD_DIR)/root/module_name
		echo "$(FW_VERSION)" > $(BUILD_DIR)/root/firmware_version
		echo "$(HDR_REGION)" > $(BUILD_DIR)/root/firmware_region
		echo "$(H_VERSION)" > $(BUILD_DIR)/root/hardware_version
		sudo mknod $(BUILD_DIR)/root/dev/console c 5 1
		sudo mknod $(BUILD_DIR)/root/dev/null  c 1 3
		sudo mknod $(BUILD_DIR)/root/dev/zero  c 1 5
		sudo mknod $(BUILD_DIR)/root/dev/urandom  c 1 9
		sudo mknod $(BUILD_DIR)/root/dev/wd  c 10 130
		sudo mknod $(BUILD_DIR)/root/dev/ttyS0 c 4 64
#modified by dvd.chen, for DNI_MTD_PARTITION
		sudo mknod $(BUILD_DIR)/root/dev/mtdblock0 b 31 0
		sudo mknod $(BUILD_DIR)/root/dev/pot b 31 1
		sudo mknod $(BUILD_DIR)/root/dev/pppoe b 31 2
		sudo mknod $(BUILD_DIR)/root/dev/pib b 31 3
		sudo mknod $(BUILD_DIR)/root/dev/nvram b 31 4
		sudo mknod $(BUILD_DIR)/root/dev/mtdblock5 b 31 5
		sudo mknod $(BUILD_DIR)/root/dev/mtdblock6 b 31 6
		mkdir $(BUILD_DIR)/root/dev/mtd
		sudo mknod $(BUILD_DIR)/root/dev/mtd/0 c 90 0
		sudo mknod $(BUILD_DIR)/root/dev/mtd/0ro c 90 1
		sudo mknod $(BUILD_DIR)/root/dev/mtd/1 c 90 2
		sudo mknod $(BUILD_DIR)/root/dev/mtd/1ro c 90 3
		sudo mknod $(BUILD_DIR)/root/dev/mtd/2 c 90 4
		sudo mknod $(BUILD_DIR)/root/dev/mtd/2ro c 90 5
		sudo mknod $(BUILD_DIR)/root/dev/mtd/3 c 90 6
		sudo mknod $(BUILD_DIR)/root/dev/mtd/3ro c 90 7
		sudo mknod $(BUILD_DIR)/root/dev/mtd/4 c 90 8
		sudo mknod $(BUILD_DIR)/root/dev/mtd/4ro c 90 9
		sudo mknod $(BUILD_DIR)/root/dev/mtd/5 c 90 10
		sudo mknod $(BUILD_DIR)/root/dev/mtd/5ro c 90 11
		sudo mknod $(BUILD_DIR)/root/dev/mtd/6 c 90 12
		sudo mknod $(BUILD_DIR)/root/dev/mtd/6ro c 90 13
		sudo mknod $(BUILD_DIR)/root/dev/ppp c 108 0
		sudo mknod $(BUILD_DIR)/root/dev/tty c 5 0
		#sudo chmod 666 $(BUILD_DIR)/root/dev/tty
		sudo mknod $(BUILD_DIR)/root/dev/ptmx c 5 2 
		#sudo chmod 666 $(BUILD_DIR)/root/dev/ptmx
		mkdir $(BUILD_DIR)/root/dev/pts
		sudo mknod $(BUILD_DIR)/root/dev/pts/0 c 136 0
		sudo mknod $(BUILD_DIR)/root/dev/pts/1 c 136 1
		sudo mknod $(BUILD_DIR)/root/dev/sda b 8 0
		sudo mknod $(BUILD_DIR)/root/dev/sda1 b 8 1
		sudo mknod $(BUILD_DIR)/root/dev/sda2 b 8 2
		sudo mknod $(BUILD_DIR)/root/dev/sda3 b 8 3
		sudo mknod $(BUILD_DIR)/root/dev/sda4 b 8 4
		sudo mknod $(BUILD_DIR)/root/dev/sda5 b 8 5
		sudo mknod $(BUILD_DIR)/root/dev/sda6 b 8 6
		sudo mknod $(BUILD_DIR)/root/dev/sda7 b 8 7
		sudo mknod $(BUILD_DIR)/root/dev/sda8 b 8 8	
		sudo mknod $(BUILD_DIR)/root/dev/sdb b 8 16
		sudo mknod $(BUILD_DIR)/root/dev/sdb1 b 8 17
		sudo mknod $(BUILD_DIR)/root/dev/sdb2 b 8 18
		sudo mknod $(BUILD_DIR)/root/dev/sdb3 b 8 19
		sudo mknod $(BUILD_DIR)/root/dev/sdb4 b 8 20
		sudo mknod $(BUILD_DIR)/root/dev/sdb5 b 8 21
		sudo mknod $(BUILD_DIR)/root/dev/sdb6 b 8 22
		sudo mknod $(BUILD_DIR)/root/dev/sdb7 b 8 23
		sudo mknod $(BUILD_DIR)/root/dev/sdb8 b 8 24					
		sudo mknod $(BUILD_DIR)/root/dev/sdc b 8 32
		sudo mknod $(BUILD_DIR)/root/dev/sdc1 b 8 33
		sudo mknod $(BUILD_DIR)/root/dev/sdc2 b 8 34
		sudo mknod $(BUILD_DIR)/root/dev/sdc3 b 8 35
		sudo mknod $(BUILD_DIR)/root/dev/sdc4 b 8 36
		sudo mknod $(BUILD_DIR)/root/dev/sdc5 b 8 37
		sudo mknod $(BUILD_DIR)/root/dev/sdc6 b 8 38
		sudo mknod $(BUILD_DIR)/root/dev/sdc7 b 8 39
		sudo mknod $(BUILD_DIR)/root/dev/sdc8 b 8 40					
		sudo mknod $(BUILD_DIR)/root/dev/sdd b 8 48
		sudo mknod $(BUILD_DIR)/root/dev/sdd1 b 8 49
		sudo mknod $(BUILD_DIR)/root/dev/sdd2 b 8 50
		sudo mknod $(BUILD_DIR)/root/dev/sdd3 b 8 51
		sudo mknod $(BUILD_DIR)/root/dev/sdd4 b 8 52
		sudo mknod $(BUILD_DIR)/root/dev/sdd5 b 8 53
		sudo mknod $(BUILD_DIR)/root/dev/sdd6 b 8 54
		sudo mknod $(BUILD_DIR)/root/dev/sdd7 b 8 55
		sudo mknod $(BUILD_DIR)/root/dev/sdd8 b 8 56	
		sudo mknod $(BUILD_DIR)/root/dev/sde b 8 64
		sudo mknod $(BUILD_DIR)/root/dev/sde1 b 8 65
		sudo mknod $(BUILD_DIR)/root/dev/sde2 b 8 66
		sudo mknod $(BUILD_DIR)/root/dev/sde3 b 8 67
		sudo mknod $(BUILD_DIR)/root/dev/sde4 b 8 68
		sudo mknod $(BUILD_DIR)/root/dev/sde5 b 8 69
		sudo mknod $(BUILD_DIR)/root/dev/sde6 b 8 70
		sudo mknod $(BUILD_DIR)/root/dev/sde7 b 8 71
		sudo mknod $(BUILD_DIR)/root/dev/sde8 b 8 72	
		sudo mknod $(BUILD_DIR)/root/dev/sdf b 8 80
		sudo mknod $(BUILD_DIR)/root/dev/sdf1 b 8 81
		sudo mknod $(BUILD_DIR)/root/dev/sdf2 b 8 82
		sudo mknod $(BUILD_DIR)/root/dev/sdf3 b 8 83
		sudo mknod $(BUILD_DIR)/root/dev/sdf4 b 8 84
		sudo mknod $(BUILD_DIR)/root/dev/sdf5 b 8 85
		sudo mknod $(BUILD_DIR)/root/dev/sdf6 b 8 86
		sudo mknod $(BUILD_DIR)/root/dev/sdf7 b 8 87
		sudo mknod $(BUILD_DIR)/root/dev/sdf8 b 8 88	
		sudo mknod $(BUILD_DIR)/root/dev/sdg b 8 96
		sudo mknod $(BUILD_DIR)/root/dev/sdg1 b 8 97
		sudo mknod $(BUILD_DIR)/root/dev/sdg2 b 8 98
		sudo mknod $(BUILD_DIR)/root/dev/sdg3 b 8 99
		sudo mknod $(BUILD_DIR)/root/dev/sdg4 b 8 100
		sudo mknod $(BUILD_DIR)/root/dev/sdg5 b 8 101
		sudo mknod $(BUILD_DIR)/root/dev/sdg6 b 8 102
		sudo mknod $(BUILD_DIR)/root/dev/sdg7 b 8 103
		sudo mknod $(BUILD_DIR)/root/dev/sdg8 b 8 104	
		sudo mknod $(BUILD_DIR)/root/dev/sdh b 8 112
		sudo mknod $(BUILD_DIR)/root/dev/sdh1 b 8 113
		sudo mknod $(BUILD_DIR)/root/dev/sdh2 b 8 114
		sudo mknod $(BUILD_DIR)/root/dev/sdh3 b 8 115
		sudo mknod $(BUILD_DIR)/root/dev/sdh4 b 8 116
		sudo mknod $(BUILD_DIR)/root/dev/sdh5 b 8 117
		sudo mknod $(BUILD_DIR)/root/dev/sdh6 b 8 118
		sudo mknod $(BUILD_DIR)/root/dev/sdh7 b 8 119
		sudo mknod $(BUILD_DIR)/root/dev/sdh8 b 8 120													
		sudo mknod $(BUILD_DIR)/root/dev/sg0 b 21 0
		sudo mknod $(BUILD_DIR)/root/dev/sg1 b 21 1		
		sudo mknod $(BUILD_DIR)/root/dev/fuse c 10 229
		mkdir $(BUILD_DIR)/root/dev/misc
		cd $(BUILD_DIR)/root/dev/misc; ln -sf ../fuse fuse;	
		sudo mknod $(BUILD_DIR)/root/dev/dsp -m 666 c 14 3
		sudo mknod $(BUILD_DIR)/root/dev/mixer -m 666 c 14 0
		mkdir $(BUILD_DIR)/root/dev/net
		sudo mknod $(BUILD_DIR)/root/dev/net/tun -m 700 c 10 200
#added by dvd.chen for miniigd
		ln -s /var/linuxigd $(BUILD_DIR)/root/etc/
		ln -s /var/wps $(BUILD_DIR)/root/etc/simplecfg
#		rm $(BUILD_DIR)/root/usr/sbin/pppd
#		rm $(BUILD_DIR)/root/usr/sbin/ez-ipupdate
#		rm $(BUILD_DIR)/root/usr/sbin/mini_upnpd
#		rm $(BUILD_DIR)/root/usr/sbin/miniigd
#		rm $(BUILD_DIR)/root/bin/snarf
		cd $(BUILD_DIR)/root/bin; sudo ln -sf ../usr/sbin/nvram config;
		rm -rf $(BUILD_DIR)/root/mnt
		cd $(BUILD_DIR)/root; ln -sf /tmp/mnt mnt;
		cd $(BUILD_DIR)/root/dev; ln -sf /tmp/log/log log;
		#cd $(BUILD_DIR)/root/etc; rm -rf shadow; ln -sf /tmp/config/shadow shadow;
		rm -f $(KDIR)/root.squashfs
#		$(STAGING_DIR)/bin/mksquashfs-lzma-rtk $(BUILD_DIR)/root $(KDIR)/root.squashfs $(SQUASHFS_OPTS)
		$(TOOL_BUILD_DIR)/rtl-tools/mksquashfs $(BUILD_DIR)/root $(KDIR)/root.squashfs -comp lzma -always-use-fragments
#modified by dvd.chen, for DNI_MTD_PARTITION, our root-fs started from 768K (realtek is 896k)
#		cd $(BUILD_DIR); $(BUILD_DIR)/linux/rtkload/cvimg root linux-2.4-rtl8196b/root.squashfs ../bin/$(RIMAGE_PADDED) 120000 E0000
#		cd $(BUILD_DIR); cvimg root $(KDIR)/root.squashfs ../bin/$(RIMAGE_PADDED) 330000 C0000
		cd $(BUILD_DIR); cvimg root ./linux-$(KERNEL)-$(BOARD)/root.squashfs ../bin/$(RIMAGE_PADDED) 330000 D0000
		cp $(KDIR)/linux-2.6.30/rtkload/linux.bin $(BIN_DIR)
		$(call Image/Build,squashfs)
		dd bs=$(HEAD_LENGTH) if=/dev/zero count=1 of=$(BIN_DIR)/head.pad
		dd bs=512K if=/dev/zero count=1 of=$(BIN_DIR)/pad.img
		cat $(BIN_DIR)/pad.img | head -c 496 > $(BIN_DIR)/pad_t.img
		cat $(BIN_DIR)/pad_t.img $(BIN_DIR)/$(RIMAGE_PADDED) > $(BIN_DIR)/root_t.burn
		dd if=$(BIN_DIR)/root_t.burn of=$(BIN_DIR)/root.burn skip=1
		rm -rf $(BIN_DIR)/root_t.burn
		echo "device:$(MODULE_NAME)" > $(BIN_DIR)/head_info.pad
		echo "version:$(FW_VERSION)" >> $(BIN_DIR)/head_info.pad
		echo "region:$(HDR_REGION)" >> $(BIN_DIR)/head_info.pad
		echo "KernelSize:$(KERNEL_LENGTH)" >> $(BIN_DIR)/head_info.pad
#		echo "RootfsSize:$(ROOTFS_LENGTH)" >> $(BIN_DIR)/head_info.pad
		ls -l $(BIN_DIR)/root.burn | awk -F ' ' '{printf "%d\n",$$$$5}' > $(BIN_DIR)/root.size
		echo -n "RootfsSize:" >> $(BIN_DIR)/head_info.pad
		cat $(BIN_DIR)/root.size >> $(BIN_DIR)/head_info.pad
		echo "InfoHeadSize:$(INFO_HEAD_LENGTH)" >> $(BIN_DIR)/head_info.pad
		cat $(BIN_DIR)/head_info.pad $(BIN_DIR)/head.pad | head -c $(HEAD_LENGTH) > $(BIN_DIR)/info.pad
		echo -n -e $(FLASH_BLOCK) >> $(BIN_DIR)/info.pad
		rm -rf $(BIN_DIR)/head_info.pad $(BIN_DIR)/head.pad $(BIN_DIR)/root.size
		cat $(BIN_DIR)/linux.bin $(BIN_DIR)/pad.img | head -c $(KERNEL_LENGTH) > $(BIN_DIR)/$(KIMAGE_PADDED)
		cat $(BIN_DIR)/info.pad $(BIN_DIR)/$(KIMAGE_PADDED)  $(BIN_DIR)/root.burn > $(BIN_DIR)/$(MODULE_NAME)-no-crc.img
#		cat $(BIN_DIR)/$(KIMAGE_PADDED)  $(BIN_DIR)/root.burn > $(BIN_DIR)/$(MODULE_NAME)-no-crc.img
		$(STAGING_DIR)/../tools/appendsum $(BIN_DIR)/$(MODULE_NAME)-no-crc.img $(BIN_DIR)/$(MODULE_NAME)-$(FW_VERSION)$(FW_REGION).img
		rm -rf $(BIN_DIR)/info.pad $(BIN_DIR)/pad.img $(BIN_DIR)/pad_t.img $(BIN_DIR)/root.burn $(BIN_DIR)/$(KIMAGE_PADDED) $(BIN_DIR)/$(MODULE_NAME)-no-crc.img
    endef
  endif

  ifeq ($(CONFIG_TARGET_ROOTFS_CRAMFS),y)
    define Image/mkfs/cramfs
	ln -sf /var/resolv.conf $(BUILD_DIR)/root/etc/resolv.conf
	make -C $(BUILD_DIR)/linux/scripts/cramfs mkcramfs
	$(BUILD_DIR)/linux/scripts/cramfs/mkcramfs -u 0 -g 0 $(BUILD_DIR)/root $(BUILD_DIR)/../bin/target.cramfs
	cp -f $(BUILD_DIR)/linux/arch/mips/brcm-boards/bcm947xx/compressed/vmlinuz $(BUILD_DIR)/../bin/
	#cp -f $(LINUX_SRCDIR)/pre-drivers/vmlinuz $(BUILD_DIR)/../bin/
	trx -o $(BUILD_DIR)/../bin/linux.trx $(BUILD_DIR)/../bin/vmlinuz $(BUILD_DIR)/../bin/target.cramfs
	addpattern -i $(BUILD_DIR)/../bin/linux.trx -o $(BUILD_DIR)/../bin/linux_lsys.bin
	# Pad self-booting Linux to a 64 KB boundary
	cp -f $(BUILD_DIR)/linux/arch/mips/brcm-boards/bcm947xx/compressed/zImage $(BUILD_DIR)/../bin/
    endef
  endif
    
  ifeq ($(CONFIG_TARGET_ROOTFS_TGZ),y)
    define Image/mkfs/tgz
		tar -zcf $(BIN_DIR)/openwrt-$(BOARD)-$(KERNEL)-rootfs.tgz --owner=root --group=root -C $(BUILD_DIR)/root/ .
    endef
  endif
  
  
endif


ifeq ($(CONFIG_TARGET_ROOTFS_EXT2FS),y)
  E2SIZE=$(shell echo $$(($(CONFIG_TARGET_ROOTFS_FSPART)*1024)))
  
  define Image/mkfs/ext2
		$(STAGING_DIR)/bin/genext2fs -q -b $(E2SIZE) -I 1500 -d $(BUILD_DIR)/root/ $(KDIR)/root.ext2
		$(call Image/Build,ext2)
  endef
endif


define Image/mkfs/prepare/default
	find $(BUILD_DIR)/root -type f -not -perm +0100 -not -name 'ssh_host*' | xargs chmod 0644
	find $(BUILD_DIR)/root -type f -perm +0100 | xargs chmod 0755
	find $(BUILD_DIR)/root -type d | xargs chmod 0755
	mkdir -p $(BUILD_DIR)/root/tmp
	chmod 0777 $(BUILD_DIR)/root/tmp
	ln -sf /var/web $(BUILD_DIR)/root/web
endef

define Image/mkfs/prepare
	$(call Image/mkfs/prepare/default)
endef

define BuildImage
compile:
	$(call Build/Compile)

install:
	$(call Image/Prepare)
	$(call Image/mkfs/prepare)
	$(call Image/BuildKernel)
#	$(call Image/mkfs/jffs2)
	$(call Image/mkfs/squashfs)
	$(call Image/mkfs/cramfs)
	$(call Image/mkfs/tgz)
	$(call Image/mkfs/ext2)
	
clean:
	$(call Build/Clean)
endef

compile-targets:
install-targets:
clean-targets:

download:
prepare:
compile: compile-targets
install: compile install-targets
clean: clean-targets
