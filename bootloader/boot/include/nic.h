 /*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 */

#ifndef	NIC_H
#define NIC_H

/*
 *	Structure returned from eth_probe and passed to other driver
 *	functions.
 */

struct nic
{
	char		*packet;
	unsigned int	packetlen;
};
#ifdef DNI_PATCH
#define MAX_RX_BUFF 64 

typedef struct MAC_RX_BUFF {
                    unsigned char packet[1600];
                    int packet_size;
                   } RX_BUFF_Queue ; /* add by alfa */

extern int buff_wr_index;  /* add by alfa */
extern int buff_rd_index; /* add by alfa */
extern int buff_bz_num; /* add by alfa */
extern RX_BUFF_Queue mac_rx_buff[MAX_RX_BUFF];
extern int polling_rx(void); /* add by alfa */
#endif
#endif	/* NIC_H */
