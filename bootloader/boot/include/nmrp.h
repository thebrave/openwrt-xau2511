#ifndef	__NMRP_H__
#define __NMRP_H__

#define bool int
#define false 0
#define true 1

typedef struct {
        unsigned short  type;
        unsigned short len;
#if 0  //dvd.chen, for new NMRP
        unsigned int value; 
#else
	 char value[1];
#endif
} NMRP_OPT;

typedef struct {
        unsigned short reserved;
        unsigned char code;
        unsigned char id;
        unsigned short length;

        NMRP_OPT opt;
} NMRP_MSG;


#define ETHER_NMRP              0x0912

#define NMRP_HDR_LEN            6//(sizeof(NMRP_MSG) - sizeof(NMRP_OPT)-2)
#define NMRP_MIN_OPT_LEN        4 //(sizeof(NMRP_OPT) - 1)
#if 1  //dvd.chen, for new NMRP
#define NMRP_OPT_HEADER_LEN	(NMRP_MIN_OPT_LEN)	
#define FIRMWARE_FILENAME "firmware"
#define FIRMWARE_FILENAME_LEN 8
#define FIRMWARE_FILENAME_OPT_LEN (NMRP_OPT_HEADER_LEN + FIRMWARE_FILENAME_LEN)
#define STRING_TABLE_FILENAME_PREFIX "string table "
#define STRING_TABLE_FILENAME_PREFIX_LEN 13
#define STRING_TABLE_FILENAME_EXAMPLE "string table 01"
#define STRING_TABLE_FILENAME_LEN (STRING_TABLE_FILENAME_PREFIX_LEN + 2)
#define STRING_TABLE_FILENAME_OPT_LEN (NMRP_OPT_HEADER_LEN + STRING_TABLE_FILENAME_LEN)
#ifdef DNI_NMRP_STRING_TABLE_WRITE_SUPPORT
#define STRING_TABLE1_ADDRESS		0x3C0000
#define STRING_TABLE2_ADDRESS		0x3E0000
#endif
#define STRING_TABLE_SIZE	(128*1024)
#if SUPPORT_MORE_STRING_TABLES
#define MAX_STRING_TABLE_NUM				1
#define STRING_TABLE_ADDRESS_BASE		0x3C0000
#define STRING_TABLE_SIZE				0x20000   //128k for each string table
#define STRING_TABLE_LIST_BEGINE_ADDR	STRING_TABLE_ADDRESS_BASE
#define STRING_TABLE_LIST_END_ADDR		STRING_TABLE_LIST_BEGINE_ADDR+MAX_STRING_TABLE_NUM*STRING_TABLE_SIZE
#endif
#endif

#define NMRP_MAGIC_NO   "\x4E\x54\x47\x52"
#define NMRP_MAGIC_NO_LEN       (sizeof(NMRP_MAGIC_NO) - 1)


/* NMRP codes */
enum _nmrp_codes_ {
        NMRP_CODE_ADVERTISE             = 0x01,
        NMRP_CODE_CONF_REQ              = 0x02,
        NMRP_CODE_CONF_ACK              = 0x03,
        NMRP_CODE_CLOSE_REQ             = 0x04,
        NMRP_CODE_CLOSE_ACK             = 0x05,
        NMRP_CODE_KEEP_ALIVE_REQ    = 0x06,
        NMRP_CODE_KEEP_ALIVE_ACK    = 0x07,
        NMRP_CODE_TFTP_UL_REQ   = 0x10
};

/* NMRP option types */
enum _nmrp_option_types_ {
        NMRP_OPT_MAGIC_NO       = 0x0001,
        NMRP_OPT_DEV_IP         = 0x0002,
        NMRP_OPT_DEV_REGION = 0x0004,
        NMRP_OPT_FW_UP          = 0x0101,
        NMRP_OPT_ST_UP	= 0x0102,
        NMRP_OPT_FILE_NAME	=0x0181
};

/* NMRP Region value */
enum _nmrp_region_values_ {
	NMRP_REGION_NA = 0x0001,
	NMRP_REGION_WW = 0x0002,
	NMRP_REGION_GR = 0x0003,
	NMRP_REGION_PR = 0x0004,
	NMRP_REGION_RU = 0x0005,
	NMRP_REGION_BZ = 0x0006,
	NMRP_REGION_IN = 0x0007,
	NMRP_REGION_KO = 0x0008,
	NMRP_REGION_JP = 0x0009
};
/* NMRP REQ max retries */
enum _nmrp_req_max_retries_ {
        NMRP_MAX_RETRY_CONF             = 5,
        NMRP_MAX_RETRY_CLOSE    = 2,
        NMRP_MAX_RETRY_TFTP_UL  = 4
};

/* NMRP timeouts */
enum _nmrp_timeouts_ {
        NMRP_TIMEOUT_REQ                =   500, /* 0.5 sec */
        NMRP_TIMEOUT_LISTEN             =  3000, /* 3 sec */
        NMRP_TIMEOUT_ACTIVE             = 60000, /* 1 minute */
        NMRP_TIMEOUT_CLOSE              =  6000, /* 6 sec */
        NMRP_TIMEOUT_ADVERTISE  =   500  /* 0.5 sec */
};

/*============== (1) Ptorocol related definitions (End) ===============*/


/*============== (2) Implementation related definitions ===============*/

#define NMRP_MAX_OPT_PER_MSG    (6)


enum _nmrp_errors_ {
        NMRP_ERR_NONE = 0,
        NMRP_ERR_MSG_INVALID_LEN,
        NMRP_ERR_MSG_TOO_LONG,
        NMRP_ERR_MSG_TOO_MANY_OPT,
        NMRP_ERR_MSG_UNKNOWN_OPT,
        NMRP_ERR_MSG_INVALID_OPT,
        NMRP_ERR_NO_BUF,
        NMRP_ERR_GENERAL
};

typedef struct {
        unsigned short type;
        unsigned short len;

        union {
                unsigned char byteVal;
                unsigned short  wordVal;
                unsigned int dwordVal;
                char *streamVal;
        } value;
} NMRP_PARSED_OPT;
typedef struct {
        unsigned char code;
        unsigned char id;
        unsigned short length;

        int numOptions;
        NMRP_PARSED_OPT options[NMRP_MAX_OPT_PER_MSG];
} NMRP_PARSED_MSG;

typedef unsigned char enet_addr_t[6];
typedef unsigned char ip_addr_t[4];

/*
 * Ethernet header.
 */
typedef struct {
    enet_addr_t   destination;
    enet_addr_t   source;
    unsigned short          type;
#define ETH_TYPE_IP   0x800
#define ETH_TYPE_ARP  0x806
#define ETH_TYPE_RARP 0x8053
} eth_header_t;

/*
 * A IP<->ethernet address mapping.
 */
typedef struct {
    ip_addr_t    ip_addr;
    enet_addr_t  enet_addr;
} ip_route_t;

int NMRP_MsgParsing(unsigned char *pkt, int pktLen, NMRP_PARSED_MSG *msg);
NMRP_PARSED_OPT *NMRP_MsgGetOpt(NMRP_PARSED_MSG *msg, int optType);

#endif