
#include <linux/interrupt.h>
#include <asm/system.h>

#include "monitor.h"
#include "etherboot.h"
#include "nic.h"


#if defined(KLD)
#include "../init/rtk.h"
#endif

#if defined(KLD)
#if defined(CONFIG_BOOT_DEBUG_ENABLE)
#undef CONFIG_BOOT_DEBUG_ENABLE
#endif
#endif

#include <asm/mipsregs.h>	//wei add
#if CONFIG_SPI_FLASH
#include "spi_flash.h"
#endif


#if defined(RTL8196B)
#include <asm/rtl8196x.h>
#endif

#if defined(RTL8198)
#include <asm/rtl8198.h>
#endif

/*
#ifdef RTL8196B
#include <asm/rtl8196.h>
#endif
#if defined(RTL8198)
#include <asm/rtl8198.h>
#endif
*/

#ifndef DNI_BOOTCODE_DOWNSIZE
#define SYS_BASE 0xb8000000
#define SYS_INI_STATUS (SYS_BASE +0x04)
#define SYS_HW_STRAP (SYS_BASE +0x08)
#define SYS_CLKMANAGE (SYS_BASE +0x10)
//hw strap
#define ST_SYNC_OCP_OFFSET 9
#define CK_M2X_FREQ_SEL_OFFSET 10
#define ST_CPU_FREQ_SEL_OFFSET 13
#define ST_CPU_FREQDIV_SEL_OFFSET 19
#define ST_BOOTPINSEL (1<<0)
#define ST_DRAMTYPE (1<<1)
#define ST_BOOTSEL (1<<2)
#define ST_PHYID (0x3<<3) //11b 
#define ST_EN_EXT_RST (1<<8)
#define ST_SYNC_OCP (1<<9)
#define CK_M2X_FREQ_SEL (0x7 <<10)
#define ST_CPU_FREQ_SEL (0xf<<13)
#define ST_NRFRST_TYPE (1<<17)
#define SYNC_LX (1<<18)
#define ST_CPU_FREQDIV_SEL (0x7<<19)
#define ST_EVER_REBOOT_ONCE (1<<23)
#define ST_SYS_DBG_SEL  (0x3f<<24)
#define ST_PINBUS_DBG_SEL (3<<30)
#define SPEED_IRQ_NO 29
#define SPEED_IRR_NO 3
#define SPEED_IRR_OFFSET 20
#endif
extern unsigned int	_end;
#ifndef DNI_BOOTCODE_DOWNSIZE
extern unsigned char 	ethfile[20];
#endif
extern struct arptable_t	arptable[MAX_ARP];
#ifdef DNI_PATCH
#include "../init/rtk.h"
#define MAIN_PROMPT						"<" MODEL_NAME ">"
#else
#define MAIN_PROMPT						"<RealTek>"
#endif
#define putchar(x)	serial_outc(x)
#define IPTOUL(a,b,c,d)	((a << 24)| (b << 16) | (c << 8) | d )

#ifndef DNI_BOOTCODE_DOWNSIZE
int YesOrNo(void);
#endif
int CmdHelp( int argc, char* argv[] );


#if defined(CONFIG_BOOT_DEBUG_ENABLE)
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdDumpWord( int argc, char* argv[] );
#endif
int CmdDumpByte( int argc, char* argv[] ); //wei add
int CmdWriteWord( int argc, char* argv[] );
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdWriteByte( int argc, char* argv[] );
int CmdWriteHword( int argc, char* argv[] );
int CmdWriteAll( int argc, char* argv[] );
int CmdCmp(int argc, char* argv[]);
int CmdIp(int argc, char* argv[]);
int CmdAuto(int argc, char* argv[]);
int CmdLoad(int argc, char* argv[]);
#endif
#endif
//int CmdEDl(int argc, char* argv[]);
//int CmdEUl(int argc, char* argv[]);
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdCfn(int argc, char* argv[]);
#endif
#ifdef DNI_PATCH
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
int CmdRegion(int argc, char* argv[]);
#ifdef DNI_NMRP_V2
void NmrpSetRegion(uint16 region_number);
#endif
int CmdPinCode(int argc, char* argv[]);
int CmdMac(int argc, char* argv[]);
int CmdSerialNumber(int argc, char* argv[]);
int CmdHwVersion(int argc, char* argv[]);
#endif
int CmdDownload(int argc, char* argv[]);
#ifdef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
int CmdDefaultHWSettings(int argc, char* argv[]);
#endif
int CmdReboot(int argc, char* argv[]);
#endif
//int CmdFle(int argc, char* argv[]);
#if defined(KLD)
#ifndef CONFIG_SPI_FLASH
int CmdFlw(int argc, char* argv[]);
#endif
int CmdFlr(int argc, char* argv[]);
#else
#ifndef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdFlw(int argc, char* argv[]);
int CmdFlr(int argc, char* argv[]);
#endif
#endif
#endif
#ifdef SUPPORT_NAND
int CmdNFlr(int argc, char* argv[]);
int CmdNFlw(int argc, char* argv[]);
#endif
#if defined(KLD)
int CmdFlw_sig(int argc, char* argv[]);
int CmdFlr_sig(int argc, char* argv[]);
#endif

//int CmdTimer(int argc, char* argv[]);
//int CmdMTC0SR(int argc, char* argv[]);  //wei add
//int CmdMFC0SR(int argc, char* argv[]);  //wei add
//int CmdTFTP(int argc, char* argv[]);  //wei add
#ifndef DNI_BOOTCODE_DOWNSIZE
#if defined(CONFIG_BOOT_DEBUG_ENABLE)
#ifdef REMOVED_UNUSED
int CmdTFTP_TX(int argc, char* argv[]);  //wei add
#endif
#endif
#endif
#ifdef CONFIG_RTL8198_TAROKO
int CmdIMEM98TEST(int argc, char* argv[]);
int CmdWBMG(int argc, char* argv[]);
#endif

#ifdef RTL8198
int CmdEEEPatch(int argc, char* argv[]); 
#endif

//Ziv
#ifdef WRAPPER
#ifndef CONFIG_SPI_FLASH
//write bootcode to flash from my content
int CmdWB(int argc, char* argv[]);

#endif
#ifdef CONFIG_SPI_FLASH
int CmdSWB(int argc, char* argv[]);
#endif
extern char _bootimg_start, _bootimg_end;
#endif

#ifdef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdSFlw(int argc, char* argv[]);
     //JSW: Auto-memory test program @20070916 for SIO/MIO
#if SPI_DBG_MESSAGE
extern void auto_spi_memtest_8198(unsigned long DRAM_starting_addr, unsigned int spi_clock_div_num);
#endif
#endif
#endif


#ifdef  CONFIG_DRAM_TEST
	void Dram_test(int argc, char* argv[]);
#endif

#ifdef  CONFIG_NOR_TEST
	int CmdNTEST(int argc, char* argv[]);	
	extern void auto_nor_memtest( unsigned long dram_test_starting_addr   );
#endif


#ifdef  CONFIG_SPI_TEST
	int CmdSTEST(int argc, char* argv[]);               //JSW: add for SPI/SDRAM auto-memory-test program
#endif


#ifdef CONFIG_CPUsleep_PowerManagement_TEST
	int CmdCPUSleep(int argc, char* argv[]);
	void CmdCPUSleepIMEM(void);
#endif


#if defined(CONFIG_PCIE_MODULE) 
int CmdTestPCIE(int argc, char* argv[]);
#endif
#if defined(CONFIG_R8198EP_HOST) || defined(CONFIG_R8198EP_DEVICE)
int CmdTestSlavePCIE(int argc, char* argv[]);
#endif



#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdPHYregR(int argc, char* argv[]);
int CmdPHYregW(int argc, char* argv[]);
#endif

/*Cyrus Tsai*/
/*move to ehterboot.h
#define TFTP_SERVER 0
#define TFTP_CLIENT 1
*/
extern struct arptable_t  arptable_tftp[3];
/*Cyrus Tsai*/

extern int flasherase(unsigned long src, unsigned int length);
extern int flashwrite(unsigned long dst, unsigned long src, unsigned long length);
extern int flashread (unsigned long dst, unsigned long src, unsigned long length);

extern int write_data(unsigned long dst, unsigned long length, unsigned char *target);
extern int read_data (unsigned long src, unsigned long length, unsigned char *target);

/*Cyrus Tsai*/
extern unsigned long file_length_to_server;
extern unsigned long file_length_to_client;
extern unsigned long image_address; 
/*this is the file length, should extern to flash driver*/
/*Cyrus Tsai*/

#if defined(RTL8196B) || defined(RTL8198)
#define WRITE_MEM32(addr, val)   (*(volatile unsigned int *) (addr)) = (val)
#define WRITE_MEM16(addr, val)   (*(volatile unsigned short *) (addr)) = (val)
#define READ_MEM32(addr)         (*(volatile unsigned int *) (addr))

#define SWCORE_BASE      0xBB800000        
#define PCRAM_BASE       (0x4100+SWCORE_BASE)
#define PITCR                  (0x000+PCRAM_BASE)       /* Port Interface Type Control Register */
#define PCRP0                 (0x004+PCRAM_BASE)       /* Port Configuration Register of Port 0 */
#define PCRP1                 (0x008+PCRAM_BASE)       /* Port Configuration Register of Port 1 */
#define PCRP2                 (0x00C+PCRAM_BASE)       /* Port Configuration Register of Port 2 */
#define PCRP3                 (0x010+PCRAM_BASE)       /* Port Configuration Register of Port 3 */
#define PCRP4                 (0x014+PCRAM_BASE)       /* Port Configuration Register of Port 4 */
#define EnablePHYIf        (1<<0)                           /* Enable PHY interface.                    */
#endif

#ifdef DNI_PATCH
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT

#define REGION_CMD "REGION"

#ifndef DNI_REGION_STRING_SUPPORT
#define REGION_CMD_HELP "REGION <1|2|3|4|5|...>"
#else
#define REGION_CMD_HELP "REGION <NA|CE>"

#define REGION_LIST_MAX_INDEX 9
char RegionList[REGION_LIST_MAX_INDEX][EXTRA_SETTINGS_REGION_LEN+1]={
				"NA",				//North America
#ifdef DNI_PLC_SUPPORT
				"CE",
				"CE",
				"CE",
				"CE",
				"CE",
				"CE",
				"CE",
				"CE"
#else
				"WW",
				"GR",
				"PR",
				"RU",
				"BZ",
				"IN",
				"KO",
				"JP"
#endif //DNI_PLC_SUPPORT
				};
int IsRegionValid(char region[])
{
	int i;
	
	for(i=0;i<REGION_LIST_MAX_INDEX;i++)
	{
		if(!strcmp(region,RegionList[i]))
			break;
	}
	if(i<REGION_LIST_MAX_INDEX)
		return 1;
	return 0;
}
#endif // DNI_REGION_STRING_SUPPORT
#endif // DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
#endif // DNI_PATCH
 
COMMAND_TABLE	MainCmdTable[] =
{
	{ "?"	  ,0, CmdHelp			, "HELP (?)				    : Print this help message"					},
	{ "HELP"  ,0, CmdHelp			, NULL					},		
#if defined(CONFIG_BOOT_DEBUG_ENABLE)												
#ifndef DNI_BOOTCODE_DOWNSIZE									
	{ "D"	  ,2, CmdDumpWord		, "D <Address> <Len>"},    
#endif
	{ "DB"	  ,2, CmdDumpByte		, "DB <Address> <Len>"}, //wei add	
#ifndef DNI_BOOTCODE_DOWNSIZE		
	{ "DW"	  ,2, CmdDumpWord		, "DW <Address> <Len>"},  //same command with ICE, easy use
	{ "EW",2, CmdWriteWord, "EW <Address> <Value1> <Value2>..."},
#ifdef REMOVED_UNUSED
	{ "EH",2, CmdWriteHword, "EH <Address> <Value1> <Value2>..."},
#endif
	{ "EB",2, CmdWriteByte, "EB <Address> <Value1> <Value2>..."},
#ifdef REMOVED_UNUSED
	{ "EC",2, CmdWriteAll, "EC <Address> <Value1> <Length>..."},
#endif	
	{ "CMP",3, CmdCmp, "CMP: CMP <dst><src><length>"},
	{ "IPCONFIG",2, CmdIp, "IPCONFIG:<TargetAddress>"},
#else
	{ "EW",2, CmdWriteWord, "EW <Address> <Value1> <Value2>..."},
#endif	
#ifdef DNI_PATCH
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
	{ REGION_CMD,2, CmdRegion, REGION_CMD_HELP},
#ifndef DNI_LAN_PORT_ONLY	
	{ "PINCODE",2, CmdPinCode, "PINCODE <XXXXXXXX>"},
#endif
	{ "MAC",2, CmdMac, "MAC <XX:XX:XX:XX:XX:XX>"}, 
	{ "SN",2, CmdSerialNumber, "SN <XXXXXXXXXXXXX>"},
	{ "HW",2, CmdHwVersion, "HW <A1~Z9>"},	
#endif
	{ "DOWNLOAD",2, CmdDownload, "DOWNLOAD (Download image to target)"},
#ifdef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT	
	{ "DEFHW",2, CmdDefaultHWSettings, "DEFHW (Return to default HW settings)"},
#endif
	{ "REBOOT",2, CmdReboot, "REBOOT"},
#endif
#ifndef RTL8197B
#ifndef DNI_BOOTCODE_DOWNSIZE
	{ "AUTOBURN"   ,1, CmdAuto			, "AUTOBURN: 0/1" },
#endif
#endif
#ifndef DNI_BOOTCODE_DOWNSIZE
	{ "LOADADDR"   ,1, CmdLoad			, "LOADADDR: <Load Address>"					},
#endif
#endif
#ifndef DNI_BOOTCODE_DOWNSIZE
	{ "J"  ,1, CmdCfn			, "J: Jump to <TargetAddress>"											},
#endif
#ifndef RTL8197B
#if defined(KLD)
#ifndef CONFIG_SPI_FLASH
	{ "FLW"   ,3, CmdFlw			, "FLW: FLW <dst><src><length>"					},
#endif
	{ "FLR"   ,3, CmdFlr			, "FLR: FLR <dst><src><length>"					},	
#else
#ifndef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
	{ "FLW"   ,3, CmdFlw			, "FLW: FLW <dst><src><length>"					},
	{ "FLR"   ,3, CmdFlr			, "FLR: FLR <dst><src><length>"					},	
#endif
#endif
#endif
#endif
#ifndef DNI_BOOTCODE_DOWNSIZE
#if defined(CONFIG_BOOT_DEBUG_ENABLE)
#ifdef REMOVED_UNUSED	
	{ "TFTPTX"   ,0, CmdTFTP_TX			, "TFTPTX: Start TFTP Send "					}, 	//wei add
#endif
#endif
#endif
#ifdef CONFIG_RTL8198_TAROKO
	{ "IMEMTEST"   ,0, CmdIMEM98TEST			, "IMEMTEST "					},
	{ "WBMG"	,0, CmdWBMG			, "WBMF: write buffer merge"				},
#endif
#if defined(KLD)
	{ "SIG"   ,2, CmdFlw_sig			, "SIG: SIG <type><sig>"					},
	{ "SIGR"   ,0, CmdFlr_sig			,"SIGR: Display signature and flag"			},
	
#endif

	//{ "TIMER"   ,1, CmdTimer			, "TIMER: Timer Test" },	
//	{ "MTC0SR"   ,1, CmdMTC0SR			, "MTC0SR: MTC0SR <Value>>"					}, //wei add
//	{ "MFC0SR"   ,0, CmdMFC0SR			, "MFC0SR: MFC0SR "					}, //wei add	

#ifdef SUPPORT_NAND
	{ "NFLR",3, CmdNFlr, "NFLR: NFLR <dst><src><length>"},
	{ "NFLW",3, CmdNFlw, "NFLW: NFLW <dst><src><length>"},
#endif

#ifdef WRAPPER
#ifndef CONFIG_SPI_FLASH
	{ "WB", 0, CmdWB, "WB: WB"},
#endif
#ifdef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
	{ "SWB", 1, CmdSWB, "SWB <SPI cnt#> (<0>=1st_chip,<1>=2nd_chip): SPI Flash WriteBack (for MXIC/Spansion)"}, 	//JSW	
#endif
#endif
#endif

#ifdef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
	{ "FLW",4, CmdSFlw, "FLW <dst_ROM_offset><src_RAM_addr><length_Byte> <SPI cnt#>: Write offset-data to SPI from RAM"},	 //JSW
#endif
#endif


#ifdef CONFIG_DRAM_TEST
	{ "DRAMTEST",2,Dram_test , "DRAMTEST <R/W> <enable_random_delay> <PowerManagementMode>" },
#endif

#ifdef CONFIG_NOR_TEST
	{ "NTEST",2, CmdNTEST, "NTEST <test_count> <HEX_DRAM_test_starting_addr>: Auto test NOR and DRAM"    }, //JSW
#endif

#ifdef CONFIG_SPI_TEST
	{ "STEST",3, CmdSTEST, "STEST <test_count><HEX_DRAM_test_starting_addr><spi_clock_div_num>: Auto test SPI and DRAM "},	//JSW
#endif

#ifdef CONFIG_CPUsleep_PowerManagement_TEST
  { "SLEEP"   ,0, CmdCPUSleep          , "Sleep  : Test CPU sleep "                 },
#endif

#if defined(CONFIG_PCIE_MODULE) 
	{ "PCIE",0, CmdTestPCIE, "PCIE: Test Host PCI-E"},
	
#endif
#if defined(CONFIG_R8198EP_HOST) || defined(CONFIG_R8198EP_DEVICE)
	{ "SPE",0, CmdTestSlavePCIE, "SPE: Test Slave PCI-E"},	
#endif
#ifndef DNI_BOOTCODE_DOWNSIZE
#if defined(CONFIG_BOOT_DEBUG_ENABLE)
  { "PHYR",2, CmdPHYregR, "PHYR: PHYR <PHYID><reg>"},
  { "PHYW",3, CmdPHYregW, "PHYW: PHYW <PHYID><reg><data>"},
#endif  
#endif
#ifdef RTL8198
	{ "EEE"   ,1, CmdEEEPatch			, "EEE :Set EEE Pathch "}, 
#endif 
};

#ifdef DNI_PATCH
int power_led_blink_slow = 0; /* add by alfa */
int power_led_blink_quick = 0; /* add by alfa */
int power_led_toggle = 0;/* add by alfa */ 
#define Get_GPIO_SW_IN() (!(REG32(PABCDDAT_REG) & (1<<DNI_RESET_BUTTON)) )
#endif

#ifdef KLD 
int ps_led_blink = 0;
int ps_led_toggle = 0;
#define RESET_LED_PIN 6
#define Set_GPIO_LED_ON()	(REG32(PABCDDAT_REG) =  REG32(PABCDDAT_REG)  & (~(1<<RESET_LED_PIN)) )
#define Set_GPIO_LED_OFF()	(REG32(PABCDDAT_REG) =  REG32(PABCDDAT_REG)  | (1<<RESET_LED_PIN))
#endif

static unsigned long	CurrentDumpAddress;
static unsigned long   sys_ipaddress;
//------------------------------------------------------------------------------
/********   caculate CPU clock   ************/
int check_cpu_speed(void);
void timer_init(unsigned long lexra_clock);
void timer_stop(void);
static void timer_interrupt(int num, void *ptr, struct pt_regs * reg);
struct irqaction irq_timer = {timer_interrupt, 0, 8, "timer", NULL, NULL};                                   
static volatile unsigned int jiffies=0;
#ifdef DNI_PATCH
int reset_button_time = 0;
int cmd_mode = 0;

void reset_default_settings(void)
{
	prom_printf("Router Configuration Reset To Default \n");
#if defined(SUPPORT_SPI_MIO_8198_8196C) && defined(CONFIG_SPI_FLASH)	
    	spi_pio_init_8198();	//JSW: Must add
#ifdef DNI_PLC_SUPPORT    		
	spi_flw_image_mio_8198(0, CONFIG_SFLASH_PLC_PIB_OFFSET, 0x80800000 , CONFIG_SFLASH_PLC_PIB_SIZE, 1);
#endif
#if defined(CONFIG_SFLASH_4M)||defined(CONFIG_SFLASH_8M)
	spi_flw_image_mio_8198(0, CONFIG_SFLASH_NVRAM_OFFSET, 0x80800000 , CONFIG_SFLASH_NVRAM_SIZE, 1);
#endif
#endif         	
	autoreboot();	
}
#endif
static void timer_interrupt(int num, void *ptr, struct pt_regs * reg)
{
	//dprintf("jiffies=%x\r\n",jiffies);
	//flush_WBcache();
	rtl_outl(TCIR,rtl_inl(TCIR));
	jiffies++;
#ifdef KLD
	if (ps_led_blink) {
		
		if ((jiffies % 100) == 0) {
			
			if (ps_led_toggle == 0) {
				ps_led_toggle = 1;
				Set_GPIO_LED_OFF();
			}
			else if (ps_led_toggle == 1) {
				ps_led_toggle = 2;
			}
			else {
				ps_led_toggle = 0;
				Set_GPIO_LED_ON();
			}
		}
	}
#endif

#ifdef DNI_PATCH
    if( Get_GPIO_SW_IN() )
    {
        ++reset_button_time;
        if( reset_button_time > 100 )
        {
        	if (!cmd_mode)
        		power_led_blink_slow = 1;
        	else
        	{	
        		if (power_led_blink_slow)
        			power_led_blink_quick=1;
        	}
        }
        if( reset_button_time > DEFAULT_RESET_BUTTON_TIME)
        {
        	power_led_blink_quick = 1;	
        }        
    }
    else
      {
       	if(reset_button_time >  DEFAULT_RESET_BUTTON_TIME)
       	{
		if (cmd_mode)
			reset_default_settings();
           	}      	
       	else if(( reset_button_time > 100 )&&( reset_button_time < DEFAULT_RESET_BUTTON_TIME))
       	{
       		if (!cmd_mode)
       			power_led_blink_slow = 1;
		reset_default_settings();
           	}
       } 
   
	if (power_led_blink_slow) {
		if ((jiffies % 50) == 0) {
			if (power_led_toggle == 0) {
				power_led_toggle = 1;
				Set_POWER_LED_OFF();
			}
			else {
				power_led_toggle = 0;
				Set_POWER_LED_ON();
			}
		}
	}
	if (power_led_blink_quick) {
		if ((jiffies % 10) == 0) {
			if (power_led_toggle == 0) {
				power_led_toggle = 1;
				Set_POWER_LED_OFF();
			}
			else {
				power_led_toggle = 0;
				Set_POWER_LED_ON();
			}
		}
	}
#endif


}
volatile int get_timer_jiffies(void)
{

	return jiffies;
};



//------------------------------------------------------------------------------
void timer_init(unsigned long lexra_clock)
{
    /* Set timer mode and Enable timer */
    REG32(TCCNR_REG) = (0<<31) | (0<<30);	//using time0
    //REG32(TCCNR_REG) = (1<<31) | (0<<30);	//using counter0

	#define DIVISOR     0xE
	#define DIVF_OFFSET                         16		
    REG32(CDBR_REG) = (DIVISOR) << DIVF_OFFSET;
    
    /* Set timeout per msec */
#ifdef CONFIG_FPGA_PLATFORM
    //int SysClkRate = 33860000;	 /* 33.86 MHz */
    int SysClkRate = 27000000;	 /* 27MHz */
#else
	int SysClkRate = lexra_clock;	 /* CPU 200MHz */
#endif

	#define TICK_10MS_FREQ  100 /* 100 Hz */
	#define TICK_100MS_FREQ 1000 /* 1000 Hz */
	#define TICK_FREQ       TICK_10MS_FREQ	
    #ifdef CONFIG_RTL8196C_REVISION_B
	if (REG32(REVR) == RTL8196C_REVISION_B)
    		REG32(TC0DATA_REG) = (((SysClkRate / DIVISOR) / TICK_FREQ) + 1) <<4;
    	else
    #endif
    		REG32(TC0DATA_REG) = (((SysClkRate / DIVISOR) / TICK_FREQ) + 1) <<8;		//set 10msec
   
       
    /* Set timer mode and Enable timer */
    REG32(TCCNR_REG) = (1<<31) | (1<<30);	//using time0
    /* We must wait n cycles for timer to re-latch the new value of TC1DATA. */
	int c;	
	for( c = 0; c < DIVISOR; c++ );
	

      /* Set interrupt mask register */
    //REG32(GIMR_REG) |= (1<<8);	//request_irq() will set 

    /* Set interrupt routing register */
#ifdef RTL8196C
    REG32(IRR1_REG) = (4<<24);  // time0:IRQ4
#else  //RTL8196B, RTL8198
    REG32(IRR1_REG) = 0x00050004;  //uart:IRQ5,  time0:IRQ4
#endif    
    
    /* Enable timer interrupt */
    REG32(TCIR_REG) = (1<<31);
}
//------------------------------------------------------------------------------
void timer_stop(void)
{
	// disable timer interrupt
	rtl_outl(TCCNR,0);
	rtl_outl(TCIR,0);
}


//------------------------------------------------------------------------------

__inline__ void
__delay(unsigned long loops)
{
	__asm__ __volatile__ (
		".set\tnoreorder\n"
		"1:\tbnez\t%0,1b\n\t"
		"subu\t%0,1\n\t"
		".set\treorder"
		:"=r" (loops)
		:"0" (loops));
}

/*
80007988 <__delay>:                                             
80007988:	1480ffff 	bnez	a0,80007988 <__delay>           
8000798c:	2484ffff 	addiu	a0,a0,-1                        
80007990:	03e00008 	jr	ra                                  
*/

//---------------------------------------------------------------------------
static unsigned long loops_per_jiffy = (1<<12);
#define LPS_PREC 8
#define HZ 100
int check_cpu_speed(void)
{
	unsigned long ticks, loopbit;
	int lps_precision = LPS_PREC;
      
#ifdef RTL8196C
  	request_IRQ(14, &irq_timer, NULL); 
#else  //RTL8196B, RTL8198
  	request_IRQ(8, &irq_timer, NULL); 
#endif
	extern long glexra_clock;
       timer_init(glexra_clock);	

	loops_per_jiffy = (1<<12);
	while (loops_per_jiffy <<= 1) {
		/* wait for "start of" clock tick */
		ticks = jiffies;
		while (ticks == jiffies)
			/* nothing */;
		/* Go .. */
		ticks = jiffies;
		__delay(loops_per_jiffy);
		ticks = jiffies - ticks;
		if (ticks)
			break;
	}
/* Do a binary approximation to get loops_per_jiffy set to equal one clock
   (up to lps_precision bits) */
	loops_per_jiffy >>= 1;
	loopbit = loops_per_jiffy;
	while ( lps_precision-- && (loopbit >>= 1) ) 
	{
		loops_per_jiffy |= loopbit;
		ticks = jiffies;
		while (ticks == jiffies);
		ticks = jiffies;
		__delay(loops_per_jiffy);
		if (jiffies != ticks)	/* longer than 1 tick */
			loops_per_jiffy &= ~loopbit;
	}
	
	//timer_stop();	//wei del, because not close timer
	//free_IRQ(8);
/* Round the value and print it */	
	//prom_printf("cpu run %d.%d MIPS\n", loops_per_jiffy/(500000/HZ),      (loops_per_jiffy/(5000/HZ)) % 100);
	return ((loops_per_jiffy/(500000/HZ))+1);
	
}
//---------------------------------------------------------------------------


/*
---------------------------------------------------------------------------
;				Monitor
---------------------------------------------------------------------------
*/
extern char** GetArgv(const char* string);

void monitor(void)
{
	char		buffer[ MAX_MONITOR_BUFFER +1 ];
	int		argc ;
	char**		argv ;
	int		i, retval ;
	
//	i = &_end;
//	i = (i & (~4095)) + 4096;
	//printf("Free Mem Start=%X\n", i);
	while(1)
	{	
		printf( "%s", MAIN_PROMPT );
		memset( buffer, 0, MAX_MONITOR_BUFFER );
		GetLine( buffer, MAX_MONITOR_BUFFER,1);
		printf( "\n" );
		argc = GetArgc( (const char *)buffer );
		argv = GetArgv( (const char *)buffer );
		if( argc < 1 ) continue ;
		StrUpr( argv[0] );
		for( i=0 ; i < (sizeof(MainCmdTable) / sizeof(COMMAND_TABLE)) ; i++ )
		{
			
			if( ! strcmp( argv[0], MainCmdTable[i].cmd ) )
			{
#if 0
				if (MainCmdTable[i].n_arg != (argc - 1))
					printf("%s\n", MainCmdTable[i].msg);
				else
					retval = MainCmdTable[i].func( argc - 1 , argv+1 );
#endif
				retval = MainCmdTable[i].func( argc - 1 , argv+1 );
				memset(argv[0],0,sizeof(argv[0]));
				break;
			}
		}
		if(i==sizeof(MainCmdTable) / sizeof(COMMAND_TABLE)) printf("Unknown command !\r\n");
	}
}


//---------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------
#ifdef DNI_PATCH
extern char eth0_mac[6];
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT

int CmdRegion(int argc, char* argv[])
{
	unsigned char  *ptr;
	unsigned int i;
#ifdef DNI_REGION_STRING_SUPPORT
	unsigned char region[EXTRA_SETTINGS_REGION_LEN+1], pre_region[EXTRA_SETTINGS_REGION_LEN+1];
#else
	unsigned char curr_region[EXTRA_SETTINGS_REGION_LEN+1];
#endif
    	unsigned char DATA[EXTRA_SETTINGS_LEN];
#ifndef DNI_REGION_STRING_SUPPORT		
	uint16	curr_region_num=0, new_region_num=0;
	int idx;
#endif	
	for(i=0;i<EXTRA_SETTINGS_LEN;i++)
		DATA[i]=0;    	
	
#ifdef DNI_REGION_STRING_SUPPORT	
	for(i=0;i<=EXTRA_SETTINGS_REGION_LEN;i++)
		region[i]=0;
	
	// Backup SN data
	flashread(DATA+EXTRA_SETTINGS_REGION_LEN, MIB_EXTRA_SETTINGS_SN,EXTRA_SETTINGS_SN_LEN);
	// Backup Hardware version data
	flashread(DATA+MIB_EXTRA_SETTINGS_HWVER-MIB_EXTRA_SETTINGS, MIB_EXTRA_SETTINGS_HWVER,EXTRA_SETTINGS_HWVER_LEN);
	flashread(region, MIB_EXTRA_SETTINGS_REGION,EXTRA_SETTINGS_REGION_LEN);
#else
	// Backup all EXTRA_SETTINGS data
	flashread(DATA, MIB_EXTRA_SETTINGS, EXTRA_SETTINGS_LEN);
	flashread(curr_region, MIB_EXTRA_SETTINGS_REGION, EXTRA_SETTINGS_REGION_LEN);
	memcpy(&curr_region_num, curr_region, EXTRA_SETTINGS_REGION_LEN);
#endif
	if (argc==0)  //show current region, don't check if valid, for adding more regions in the future by netgear SPEC
	{
#ifndef DNI_REGION_STRING_SUPPORT
		curr_region_num = ntohs(curr_region_num); //the flash recorded  region_num was in network order
	#if 0
		for(i=EXTRA_SETTINGS_REGION_LEN; i>0; i--)
		{
			region_num += curr_region[i-1] << 8*(i-1);
		}
	#endif
		prom_printf("Region: %d\n", curr_region_num);
#else
		if (IsRegionValid(region))
			prom_printf("Region :%s\n",region);
		else
		{
			prom_printf("Warning!! Region is invaild!!\n");
			argc=1;
			argv[0]=DEFAULT_HW_SETTING_REGION;
			CmdRegion(argc,argv);
		}
#endif
		return;	 
	}			
	
	ptr = argv[0];

#ifndef DNI_REGION_STRING_SUPPORT
	//try to parse the string format decimal valure
	idx = strlen(ptr)-1;
	i=0;
	while( idx >= 0 )
	{
		//make sure it's decimal
		if( !((ptr[idx] >= '0') && (ptr[idx] <= '9'))  )
		{
			prom_printf("Region fail(not a decimal), value[%s]\n", ptr);	
			return;
		}
		
		switch(i)
		{
			case 0:
				new_region_num += ((uint16)ptr[idx] -'0');
				break;
			case 1:
				new_region_num += ((uint16)ptr[idx] - '0') * 10;
				break;
			case 2:
				new_region_num += ((uint16)ptr[idx] - '0') * 100;
				break;
			case 3:
				new_region_num += ((uint16)ptr[idx] - '0') * 1000;
				break;
			case 4:
				new_region_num += ((uint16)ptr[idx] - '0') * 10000;
				break;
		}
		idx--;
		i++;
	}

	prom_printf("curr_region[%d], new_region[%d]\n", ntohs(curr_region_num), new_region_num);
	
	new_region_num = htons(new_region_num); //stored region num as network order

	if( new_region_num !=  curr_region_num)
	{
		memcpy(DATA, &new_region_num, EXTRA_SETTINGS_REGION_LEN);
#else
	 if(strlen(ptr) != EXTRA_SETTINGS_REGION_LEN)
	  {
	  	prom_printf("Region fail \n");
	  	return;
	  }
 
    memcpy(region,ptr,EXTRA_SETTINGS_REGION_LEN);
    
    	StrUpr( region );
    	
    	if (!IsRegionValid(region))
	  {
	  	prom_printf("Region fail \n");
	  	return;
	  }  

	prom_printf("Region :%s\n",region);
	
	flashread(pre_region, MIB_EXTRA_SETTINGS_REGION,EXTRA_SETTINGS_REGION_LEN);
	for(i=0;i<EXTRA_SETTINGS_REGION_LEN;i++)
	{
		if(pre_region[i]!=region[i])
			break;
	}
	if (i!=EXTRA_SETTINGS_REGION_LEN)
	{
		memcpy(DATA,region,EXTRA_SETTINGS_REGION_LEN);
#endif
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
#ifdef DNI_PLC_SUPPORT
// Once the region be changed in bootloader, the DUT's PLC PIB will be returned to default settings.
			spi_flw_image_mio_8198(0, CONFIG_SFLASH_PLC_PIB_OFFSET, 0x80800000 , CONFIG_SFLASH_PLC_PIB_SIZE, 1);
#endif
			spi_flw_image_mio_8198(0,MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN, 0);
		#else
#ifdef DNI_PLC_SUPPORT    		
			spi_flw_image(0, CONFIG_SFLASH_PLC_PIB_OFFSET, 0x80800000 , CONFIG_SFLASH_PLC_PIB_SIZE);
#endif		
			spi_flw_image(0,MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN);
		#endif
#else
	flashwrite(MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN);
#endif  
	}
}

#ifdef DNI_NMRP_V2
void NmrpSetRegion(uint16 new_region_num)
{
	int argc;
	char* argv[2];
#ifndef DNI_REGION_STRING_SUPPORT
	char region_num[10];
#endif
    	//printf(">>>new_region_num =%d<<<<", new_region_num);
    	argc=1;
#ifdef DNI_REGION_STRING_SUPPORT
	argv[0]=RegionList[(new_region_num%REGION_LIST_MAX_INDEX) - 1];
#else
	SprintF(region_num,"%d", new_region_num);
	argv[0]=region_num;	
#endif
	CmdRegion(argc,argv);
	return;
}
#endif

int CmdSerialNumber(int argc, char* argv[])
{
	unsigned char  *ptr;
	unsigned int i;
	unsigned char SN[EXTRA_SETTINGS_SN_LEN+1];
    	unsigned char DATA[EXTRA_SETTINGS_LEN];
	
	for(i=0;i<EXTRA_SETTINGS_LEN;i++)
		DATA[i]=0;
		
	for(i=0;i<=EXTRA_SETTINGS_SN_LEN;i++)
		SN[i]=0;
	
#if 0 //just backup all extra settings
	// Backup Region data
	flashread(DATA, MIB_EXTRA_SETTINGS_REGION,EXTRA_SETTINGS_REGION_LEN);
	// Backup Hardware version data
	flashread(DATA+MIB_EXTRA_SETTINGS_HWVER-MIB_EXTRA_SETTINGS, MIB_EXTRA_SETTINGS_HWVER,EXTRA_SETTINGS_HWVER_LEN);
#else
	// Backup all EXTRA_SETTINGS data
	flashread(DATA, MIB_EXTRA_SETTINGS, EXTRA_SETTINGS_LEN);
#endif
	flashread(SN, MIB_EXTRA_SETTINGS_SN,EXTRA_SETTINGS_SN_LEN);
	
	if (argc==0)
	{
		for(i=0;i<EXTRA_SETTINGS_SN_LEN;i++)
		{
			if ((SN[i]>='A')&&(SN[i]<='Z'))
				continue;
			//if ((SN[i]>='a')&&(SN[i]<='z'))
			//	continue;
			if ((SN[i]>='0')&&(SN[i]<='9'))
				continue;
			break;
		}
		if (i==EXTRA_SETTINGS_SN_LEN)
			prom_printf("Serial Number :%s\n",SN);
		else
		{
			prom_printf("Warning!! Serial Number is invaild!!\n");
			argc=1;
			argv[0]=DEFAULT_HW_SETTING_SN;
			CmdSerialNumber(argc,argv);
		}
		return;	 
	}			
	
	ptr = argv[0];
	
	if(strlen(ptr) != EXTRA_SETTINGS_SN_LEN)
	{
		prom_printf("Serial Number fail \n");
	  	return;
	}

    	memcpy(SN,ptr,EXTRA_SETTINGS_SN_LEN);
    	StrUpr( SN );

	if (argc==2)
	{
		prom_printf("Skip the error checking of serial number \n");
	}
	else
	{
		for(i=0;i<EXTRA_SETTINGS_SN_LEN;i++)
		{	
			if ((SN[i]>='A')&&(SN[i]<='Z'))
				continue;
			if ((SN[i]>='0')&&(SN[i]<='9'))
				continue;
			break;
		}
		if (i!=EXTRA_SETTINGS_SN_LEN)
		{
			prom_printf("Serial Number fail \n");
	  		return;
	  	}
	}
	prom_printf("Serial Number :%s\n",SN);
	
	memcpy(DATA+EXTRA_SETTINGS_REGION_LEN,SN,EXTRA_SETTINGS_SN_LEN);

#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			spi_flw_image_mio_8198(0,MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN, 0);
		#else
			spi_flw_image(0,MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN);
		#endif
#else
	flashwrite(MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN);
#endif   
}

int CmdHwVersion(int argc, char* argv[])
{
	unsigned char  *ptr;
	unsigned int i;
	unsigned char HwVer[EXTRA_SETTINGS_HWVER_LEN+1];
    	unsigned char DATA[EXTRA_SETTINGS_LEN];
	
	for(i=0;i<EXTRA_SETTINGS_LEN;i++)
		DATA[i]=0;
		
	for(i=0;i<=EXTRA_SETTINGS_HWVER_LEN;i++)
		HwVer[i]=0;
	
	flashread(DATA, MIB_EXTRA_SETTINGS,EXTRA_SETTINGS_REGION_LEN+EXTRA_SETTINGS_SN_LEN);
	flashread(HwVer, MIB_EXTRA_SETTINGS_HWVER,EXTRA_SETTINGS_HWVER_LEN);
	
	if (argc==0)
	{
		for(i=0;i<EXTRA_SETTINGS_HWVER_LEN;i++)
		{
			if ((HwVer[i]>='A')&&(HwVer[i]<='Z'))
				continue;
			if ((HwVer[i]>='0')&&(HwVer[i]<='9'))
				continue;
			break;
		}
		if (i==EXTRA_SETTINGS_HWVER_LEN)
			prom_printf("Hardware Version :%s\n",HwVer);
		else
		{
			prom_printf("Warning!! Hardware Version is invaild!!\n");
		}
		return;	 
	}			
	
	ptr = argv[0];
	
	if(strlen(ptr) != EXTRA_SETTINGS_HWVER_LEN)
	{
		prom_printf("Hardware Version fail \n");
	  	return;
	}

    	memcpy(HwVer,ptr,EXTRA_SETTINGS_HWVER_LEN);
    	StrUpr( HwVer );
	/*if (argc==2)
	{
		prom_printf("Skip the error checking of Hardware Version \n");
	}
	else
	{*/
		for(i=0;i<EXTRA_SETTINGS_HWVER_LEN;i++)
		{	
			if ((HwVer[i]>='A')&&(HwVer[i]<='Z'))
				continue;
			if ((HwVer[i]>='0')&&(HwVer[i]<='9'))
				continue;
			break;
		}
		if (i!=EXTRA_SETTINGS_HWVER_LEN)
		{
			prom_printf("Hardware Version fail \n");
	  		return;
	  	}
	//}
	prom_printf("Hardware Version :%s\n",HwVer);
	
	memcpy(DATA+EXTRA_SETTINGS_REGION_LEN+EXTRA_SETTINGS_SN_LEN,HwVer,EXTRA_SETTINGS_HWVER_LEN);

#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			spi_flw_image_mio_8198(0,MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN, 0);
		#else
			spi_flw_image(0,MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN);
		#endif
#else
	flashwrite(MIB_EXTRA_SETTINGS, DATA, EXTRA_SETTINGS_LEN);
#endif   
}

#ifndef DNI_LAN_PORT_ONLY
int CmdPinCode(int argc, char* argv[])
{
	unsigned char  *ptr;
	unsigned int i;
	unsigned char tmpbuf[CONFIG_MAXIMUM_SIZE];
    unsigned short len;
	unsigned char sum=0;
	
	flashread(tmpbuf, HW_SETTING_OFFSET,CONFIG_MAXIMUM_SIZE);
	memcpy(&len, &tmpbuf[4], 2);

	 if (len>(8*1024))
	 {
	 	//prom_printf(">>>The length of HW settings error!(len=%d)<<<<\n",len);
	 	return;
	 }
	 		
	if (argc==0)
	{	
		prom_printf("WPS PinCode :%s\n",&tmpbuf[MIB_WSC_PIN]);
		return;	 
	}			
	
	ptr = argv[0];
	
	 if(strlen(ptr) != WSC_PIN_LEN)
	  {
	  	prom_printf("PinCode number fail \n");
	  	return;
	  }
 
    memcpy(&tmpbuf[MIB_WSC_PIN],ptr,WSC_PIN_LEN);
    //tmpbuf[MIB_WSC_PIN+WSC_PIN_LEN+1] = NULL;
    tmpbuf[MIB_WSC_PIN+WSC_PIN_LEN] = NULL;
   
    for (i=4+2; i<4+2+len-1; i++) 
				sum += tmpbuf[i];
	sum = 0x100 - sum;
	tmpbuf[len+4+2-1] = sum;
	
	prom_printf("WPS PinCode :%s\n",&tmpbuf[MIB_WSC_PIN]);

#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			spi_flw_image_mio_8198(0,HW_SETTING_OFFSET, tmpbuf, len+4+2, 0);
		#else
			spi_flw_image(0,HW_SETTING_OFFSET, tmpbuf, len+4+2);
		#endif
#else
	flashwrite(HW_SETTING_OFFSET, tmpbuf, len+4+2);
#endif
	
    
}
#endif		

int strToMac(unsigned char *pMac, char *pStr) /* add by alfa */
{
    unsigned char *ptr;
    unsigned int i,k;
    

    bzero(pMac,6);
    ptr = pStr;
    for(k=0;*ptr;ptr++)
    {
        if( (*ptr==':') || (*ptr=='-') )
            k++;
        else if( ('0'<=*ptr) && (*ptr<='9') )
            pMac[k]=(pMac[k]<<4)+(*ptr-'0');
        else if( ('a'<=*ptr) && (*ptr<='f') )
            pMac[k]=(pMac[k]<<4)+(*ptr-'a'+10);
        else if( ('A'<=*ptr) && (*ptr<='F') )
            pMac[k]=(pMac[k]<<4)+(*ptr-'A'+10);
        else
            break;
    }
    
    if(k!=5)
        return -1;
    
    return 0;
}
#endif
extern void tftpd_entry(void);
int CmdDownload(int argc, char* argv[])/* add by alfa */
{
	prom_printf("Waitting for TFTP Client ...\n");
 	tftpd_entry();
 }
 
 int CmdReboot(int argc, char* argv[])
{
	autoreboot();
 }
 
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
unsigned char str[3];

unsigned char *val2hex(unsigned char val)
{
	unsigned char value;
	unsigned char i;
	
	for(i=0;i<3;i++)
		str[i]=0;
	value=(val&0xf0)>>4;
	if (value >= 0x0a)
	{
		str[0]=value+'A'-0x0a;
	}
	else
	{
		str[0]=value+'0';
	}
	value=val&0x0f;
	if (value >= 0x0a)
	{
		str[1]=value+'A'-0x0a;
	}
	else
	{
		str[1]=value+'0';
	}

	//prom_printf("\n###str = %s###\n", str);
	return &str;
}

int CmdMac(int argc, char* argv[])/* add by alfa */
{
	unsigned char tmpbuf[CONFIG_MAXIMUM_SIZE];
    unsigned short len;
	unsigned char sum=0;
	int i;
	unsigned char mac[6];
	char *ptr;
     
     flashread(tmpbuf, HW_SETTING_OFFSET,CONFIG_MAXIMUM_SIZE);
	 memcpy(&len, &tmpbuf[4], 2);

	 if (len>(8*1024))
	 {
	 	//prom_printf(">>>The length of HW settings error!(len=%d)<<<<\n",len);
	 	return;
	 }
	 
	 if (len != 0 && len <= CONFIG_MAXIMUM_SIZE) {					
			for (i=4+2; i<4+2+len; i++) 
				sum += tmpbuf[i];
		}
		else
			sum = 1;
	
     if (sum == 0 && memcmp(&tmpbuf[4+2+1], "\x0\x0\x0\x0\x0\x0", 6)) {
			memcpy(arptable_tftp[0].node, &tmpbuf[4+2+1], 6);
			memcpy(eth0_mac, &tmpbuf[4+2+1], 6);
			memcpy(mac, &tmpbuf[4+2+1+6], 6);
		}
	
	
	if (argc==0)
	{	
#ifdef DNI_PLC_SUPPORT
		prom_printf("PLC Mac address to ");
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[0]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[1]));	
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[2]));
		if ((eth0_mac[5])!=0)
		{
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]));
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[4]));
			prom_printf("%s \n",val2hex((unsigned char)eth0_mac[5]-1));
		}
		else 	if ((eth0_mac[4])!=0)
		{
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]));
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[4]-1));
			prom_printf("FF \n");
		}
		else 	if ((eth0_mac[3])!=0)
		{
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]-1));
			prom_printf("FF:");
			prom_printf("FF \n");
		}		
#endif
#ifdef DNI_AP_MODE_ONLY
		prom_printf("AP  Mac address is ");
#else
		prom_printf("Lan Mac address is ");
#endif
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[0]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[1]));	
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[2]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[4]));
		prom_printf("%s \n",val2hex((unsigned char)eth0_mac[5]));
#if !defined DNI_AP_MODE_ONLY &&  !defined DNI_LAN_PORT_ONLY
		prom_printf("Wan Mac address is ");
		prom_printf("%s:",val2hex((unsigned char)mac[0]));
		prom_printf("%s:",val2hex((unsigned char)mac[1]));	
		prom_printf("%s:",val2hex((unsigned char)mac[2]));
		prom_printf("%s:",val2hex((unsigned char)mac[3]));
		prom_printf("%s:",val2hex((unsigned char)mac[4]));
		prom_printf("%s \n",val2hex((unsigned char)mac[5]));
#endif		
		return;	 
	}			
	
	ptr = argv[0];
	
	if (strToMac(mac, ptr) == 0)
	 {
	    memcpy(&tmpbuf[4+2+1],mac,6);
	    
	    if( mac[5] == 0xff )
          {
           	mac[5] = 0x00;
                  	
           	if( mac[4] == 0xff )
           	  {
           	  	mac[4] = 0x00;
           	  	mac[3] = mac[3] + 1;
           	  }
            else
               mac[4] = mac[4] + 1;
          }
        else
          mac[5] = mac[5] + 1;
           
	    memcpy(&tmpbuf[4+2+1+6],mac,6);
	    
	    for (i=4+2; i<4+2+len-1; i++) 
				sum += tmpbuf[i];
	    sum = 0x100 - sum;
	    tmpbuf[len+4+2-1] = sum;
	    
	    memcpy(eth0_mac, &tmpbuf[4+2+1], 6);
	    memcpy(mac, &tmpbuf[4+2+1+6], 6);
#ifdef DNI_PLC_SUPPORT
		prom_printf("Change PLC Mac address to ");
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[0]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[1]));	
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[2]));
		if ((eth0_mac[5])!=0)
		{
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]));
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[4]));
			prom_printf("%s \n",val2hex((unsigned char)eth0_mac[5]-1));
		}
		else 	if ((eth0_mac[4])!=0)
		{
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]));
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[4]-1));
			prom_printf("FF \n");
		}
		else 	if ((eth0_mac[3])!=0)
		{
			prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]-1));
			prom_printf("FF:");
			prom_printf("FF \n");
		}		
#endif
#ifdef DNI_AP_MODE_ONLY
		prom_printf("Change AP  Mac address to ");
#else
		prom_printf("Change Lan Mac address to ");
#endif		
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[0]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[1]));	
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[2]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[3]));
		prom_printf("%s:",val2hex((unsigned char)eth0_mac[4]));
		prom_printf("%s \n",val2hex((unsigned char)eth0_mac[5]));
#if !defined DNI_AP_MODE_ONLY &&  !defined DNI_LAN_PORT_ONLY
		prom_printf("Change Wan Mac address to ");
		prom_printf("%s:",val2hex((unsigned char)mac[0]));
		prom_printf("%s:",val2hex((unsigned char)mac[1]));	
		prom_printf("%s:",val2hex((unsigned char)mac[2]));
		prom_printf("%s:",val2hex((unsigned char)mac[3]));
		prom_printf("%s:",val2hex((unsigned char)mac[4]));
		prom_printf("%s \n",val2hex((unsigned char)mac[5]));
#endif
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
#ifdef DNI_PLC_SUPPORT    		
			spi_flw_image_mio_8198(0, CONFIG_SFLASH_PLC_PIB_OFFSET, 0x80800000 , CONFIG_SFLASH_PLC_PIB_SIZE, 1);
#endif		
			spi_flw_image_mio_8198(0,HW_SETTING_OFFSET, tmpbuf, len+4+2, 0);
		#else
#ifdef DNI_PLC_SUPPORT    		
			spi_flw_image(0, CONFIG_SFLASH_PLC_PIB_OFFSET, 0x80800000 , CONFIG_SFLASH_PLC_PIB_SIZE);
#endif		
			spi_flw_image(0,HW_SETTING_OFFSET, tmpbuf, len+4+2);
		#endif
#else
	flashwrite(HW_SETTING_OFFSET, tmpbuf, len+4+2);
#endif
	   	   
	    memcpy(arptable_tftp[0].node, &tmpbuf[4+2+1], 6);
	        
	 }
	else
	 printf("MAC address fail \n");   
		
}

void tftp_filename_cmds_set(char filename[])
{
	int i, LineCount ;
	char cmd[65];
	char *s,*m,*e;
		
	s=strstr(filename,"#");
	m=strstr(filename,"=");
	e=strstr(filename,"&");
	if ((s==0)||(m==0)||(e==0))
		return;
	if ((s>m)||(s>e))
		return;
	if (m>e)
		return;
		
	if ((s+1)==(m-1))
		return;
		
	if ((m+1)==(e-1))
		return;
		
	for(i=0;i<65;i++)
		cmd[i]=0;
	memcpy(cmd, s+1, m-s-1);
	StrUpr( cmd );
	for( i=0, LineCount = 0 ; i < (sizeof(MainCmdTable) / sizeof(COMMAND_TABLE)) ; i++ )
	{
		if( ! strcmp( cmd, MainCmdTable[i].cmd ) )
		{
			int j, argc;
			char* argv[2];
			
			//prom_printf("\n## TFTP filename Command => %s ", cmd);
			for(j=0;j<65;j++)
				cmd[j]=0;
			memcpy(cmd, m+1, e-m-1);
			StrUpr( cmd );
			//prom_printf("%s ##\n", cmd);
			argc=1;
			argv[0]=cmd;
			MainCmdTable[i].func(argc, argv);
			return;
		}
	}
}

void tftp_filename_ini_cmds_set(char filename[], unsigned long image_address, unsigned long file_length_to_client)
{
	unsigned char *product_string = MODEL_NAME;
	unsigned char *file_tag = ".INI";
	unsigned char tmpbuf[TEMP_MAXIMUM_SIZE], tmpdata[TEMP_MAXIMUM_SIZE],data[TEMP_MAXIMUM_SIZE];
	char *s,*e;
	int i, j, k, save_data;

	if(file_length_to_client>TEMP_MAXIMUM_SIZE)
		return;
	if(file_length_to_client<=0)
		return;		
	
	StrUpr( filename );
	if((strstr(filename,product_string)==0) || (strstr(filename,file_tag)==0))
		return;
	
	for(i=0;i<TEMP_MAXIMUM_SIZE;i++)
		tmpbuf[i]=0;
	memcpy(tmpbuf, image_address, file_length_to_client);
	
	s=strstr(tmpbuf,"BEGIN;");
	e=strstr(tmpbuf,"END;");
	
	if ((s==0)||(e==0))
		return;
	if (s>e)
		return;

	for(i=0;i<TEMP_MAXIMUM_SIZE;i++)
		tmpdata[i]=0;
	memcpy(tmpdata, s+5, e-(s+5)-1);
	
	for(i=0;i<TEMP_MAXIMUM_SIZE;i++)
		data[i]=0;
	
	save_data=0;
	for(i=0;i<(e-(s+5)-1);i++)
	{
		if(!save_data)
		{
			if(tmpdata[i]==';')
			{
				save_data=1;
				for(j=0;j<TEMP_MAXIMUM_SIZE;j++)
					data[j]=0;
				j=0;
			}
		}
		else
		{
			if(tmpdata[i]==';')
			{
				if(strlen(data)!=0)
					tftp_filename_cmds_set(data);
				save_data=0;
				i--;
			}
			else
			{
				data[j]=tmpdata[i];
				j++;
			}
		}
	}
	
	return;	
}

void extra_hw_settings_cmd(int cmd)
{
	int argc;
	char* argv[2];
	
	argc=cmd;
	argv[0]=DEFAULT_HW_SETTING_REGION;
	CmdRegion(argc,argv);
	argv[0]=DEFAULT_HW_SETTING_SN;
	CmdSerialNumber(argc,argv);
	argv[0]=DEFAULT_HW_SETTING_MAC;
	CmdMac(argc,argv);
#ifndef DNI_LAN_PORT_ONLY
	argv[0]=DEFAULT_HW_SETTING_PINCODE;
	CmdPinCode(argc,argv);
#endif	
}
#endif

#ifdef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
#define DEFAULT_HW_SETTINGS_LEN 1172
int CmdDefaultHWSettings(int argc, char* argv[])
{
    	unsigned char DATA[DEFAULT_HW_SETTINGS_LEN]={0x48,0x36,0x30,0x31,0x04,0x8e,0x01,0x00,0x30,0xab,0x00,0x00,0x01,0x00,0x30,0xab,
    			            0x00,0x00,0x02,0x00,0xe0,0x4c,0x81,0x96,0xc1,0x00,0xe0,0x4c,0x81,0x96,0xc2,0x00,
    			            0xe0,0x4c,0x81,0x96,0xc3,0x00,0xe0,0x4c,0x81,0x96,0xc4,0x00,0xe0,0x4c,0x81,0x96,
    			            0xc5,0x00,0xe0,0x4c,0x81,0x96,0xc6,0x00,0xe0,0x4c,0x81,0x96,0xc7,0x00,0xe0,0x4c,
    			            0x81,0x96,0xc8,0x29,0x29,0x29,0x29,0x27,0x27,0x27,0x27,0x27,0x26,0x26,0x26,0x26,
    			            0x26,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x27,
    			            0x27,0x27,0x27,0x26,0x26,0x26,0x26,0x26,0x26,0x26,0x26,0x26,0x26,0x2f,0x2f,0x2f,
    			            0x2f,0x2f,0x2f,0x2f,0x2f,0x2f,0x30,0x30,0x30,0x30,0x30,0x01,0x01,0x01,0x01,0x02,
    			            0x02,0x02,0x02,0x02,0x01,0x01,0x01,0x01,0x01,0xfe,0xfe,0xfe,0xfe,0xfe,0xfe,0xfe,
    			            0xfe,0xfe,0x0e,0x0e,0x0e,0x0e,0x0e,0x46,0x46,0x46,0x46,0x45,0x45,0x45,0x45,0x45,
    			            0x35,0x35,0x35,0x35,0x35,0x01,0x0a,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    			        
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,            			    

    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    			        
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    			        
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    			        
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    			        
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,            			    

    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,    			        
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    			            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,            			    
            			    
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            			    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x31,0x32,0x33,0x34,0x35,0x36,
            			    0x37,0x30,0x00,0x39                			    
    			            };
    			            
	prom_printf("\n\nReturn to default HW settings(HW_SETTING_OFFSET=0x%x)!!\n", HW_SETTING_OFFSET);
	
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			spi_flw_image_mio_8198(0,HW_SETTING_OFFSET, DATA, DEFAULT_HW_SETTINGS_LEN, 0);
		#else
			spi_flw_image(0,HW_SETTING_OFFSET, DATA, DEFAULT_HW_SETTINGS_LEN);
		#endif
#else
	flashwrite(HW_SETTING_OFFSET, DATA, DEFAULT_HW_SETTINGS_LEN);
#endif
}
#endif

#endif
#ifdef WRAPPER
#ifndef CONFIG_SPI_FLASH
int CmdWB(int argc, char* argv[])
{
       char* start = &_bootimg_start;
	char* end  = &_bootimg_end;
        
	   
	unsigned int length = end - start;
	
	printf("Flash wille write %X length of embedded boot code at %X to %X\n", length, start, end);
	printf("(Y)es, (N)o->");
	if (YesOrNo())
		if (flashwrite(0, (unsigned long)start, length))
			printf("Flash Write Successed!\n");
		else
			printf("Flash Write Failed!\n");
	else
		printf("Abort!\n");

}
#endif

#ifdef CONFIG_SPI_FLASH
extern char _bootimg_start, _bootimg_end;
//SPI Write-Back
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdSWB(int argc, char* argv[])
{
	unsigned short auto_spi_clock_div_num;//0~7
	unsigned int  cnt=strtoul((const char*)(argv[0]), (char **)NULL, 16);	//JSW check
	char* start = &_bootimg_start;
	char* end  = &_bootimg_end;	   
	unsigned int length = end - start;		
	printf("SPI Flash #%d will write 0x%X length of embedded boot code from 0x%X to 0x%X\n", cnt+1,length, start, end);
	printf("(Y)es, (N)o->");
	if (YesOrNo())
	{
		 #ifdef SUPPORT_SPI_MIO_8198_8196C
		 	spi_pio_init_8198();	//JSW: Must add
#ifdef DNI_PATCH
			spi_flw_image_mio_8198(cnt, 0, start , length, 0);
#else
			spi_flw_image_mio_8198(cnt, 0, start , length);	
#endif
	  	#else			
		spi_pio_init();	//JSW: Must add
		spi_flw_image(cnt, 0, start , length);
		#endif
		printf("SPI Flash Burn OK!\n");
	}	
	else 
	{
        	printf("Abort!\n");	
	}	
  }
#endif




#endif
#endif
/*/
---------------------------------------------------------------------------
; Ethernet Download
---------------------------------------------------------------------------
*/



#ifndef DNI_BOOTCODE_DOWNSIZE
extern unsigned long ETH0_ADD;

int CmdCfn(int argc, char* argv[])
{
	unsigned long		Address;
	void	(*jump)(void);
	if( argc > 0 )
	{
		if(!Hex2Val( argv[0], &Address ))
		{
			printf(" Invalid Address(HEX) value.\n");
			return FALSE ;
		}
	}

	dprintf("---Jump to address=%X\n",Address);
	jump = (void *)(Address);
	outl(0,GIMR0); // mask all interrupt
	cli(); 
#if defined(RTL8196B) || defined(RTL8198)
#ifndef CONFIG_FPGA_PLATFORM
      /* if the jump-Address is BFC00000, then do watchdog reset */
      if(Address==0xBFC00000)
      	{
      	   *(volatile unsigned long *)(0xB800311c)=0; /*this is to enable 865xc watch dog reset*/
          for( ; ; );
      	}
     else /*else disable PHY to prevent from ethernet disturb Linux kernel booting */
     	{
           WRITE_MEM32(PCRP0, (READ_MEM32(PCRP0)&(~EnablePHYIf )) ); 
           WRITE_MEM32(PCRP1, (READ_MEM32(PCRP1)&(~EnablePHYIf )) ); 
           WRITE_MEM32(PCRP2, (READ_MEM32(PCRP2)&(~EnablePHYIf )) ); 
           WRITE_MEM32(PCRP3, (READ_MEM32(PCRP3)&(~EnablePHYIf )) ); 
           WRITE_MEM32(PCRP4, (READ_MEM32(PCRP4)&(~EnablePHYIf )) ); 
	flush_cache();
     	}
#endif
#endif
	jump();	
	
}
#endif
/*int CmdEDl( int argc, char* argv[] )
{

	int			address;
	unsigned char 		iChar[2];
	int 	bytecount=0;
	
//	address = strtoul((const char*)(argv[1]), (char **)NULL, 16);	
//	prom_printf("Ethernet download %s to addr=%X\n? (Y/y/N/n)", argv[0], address);
	
		
//	GetLine( iChar, 2,1); 
//	if ((iChar[0] == 'Y') || (iChar[0] == 'y'))
//		printf("download! \n");
//	else
		printf("abort! \n");
	return TRUE;

}*/

/*int CmdEUl(int argc, char* argv[])
{

//	unsigned long		address;
//	unsigned long		bytecount;

//	unsigned char 		iChar[2];





//	address = strtoul((const char*)(argv[1]), (char **)NULL, 16);		
//	bytecount= strtoul((const char*)(argv[2]), (char **)NULL, 16);		

	
//	prom_printf("Ethernet upload %s from addr=%X with bytecount:%X\n? (Y/y/N/n)", 
//	argv[0], address,bytecount);	
	
//	GetLine( iChar, 2,1); 
//	if ((iChar[0] == 'Y') || (iChar[0] == 'y'))
//	        printf("\nUpload !\n");
//	else
	printf("abort! \n");
	
	
	return TRUE;
	
	
}*/

//---------------------------------------------------------------------------
#if defined(CONFIG_BOOT_DEBUG_ENABLE)	
//---------------------------------------------------------------------------
/* This command can be used to configure host ip and target ip	*/
#ifdef DNI_PATCH
extern char eth0_mac[6];
#endif
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdIp(int argc, char* argv[])
{
	unsigned char  *ptr;
	unsigned int i;
	int  ip[4];
	
	if (argc==0)
	{	
		printf(" Target Address=%d.%d.%d.%d\n",
		arptable_tftp[TFTP_SERVER].ipaddr.ip[0], arptable_tftp[TFTP_SERVER].ipaddr.ip[1], 
		arptable_tftp[TFTP_SERVER].ipaddr.ip[2], arptable_tftp[TFTP_SERVER].ipaddr.ip[3]);
#ifdef HTTP_SERVER
		printf("   Http Address=%d.%d.%d.%d\n",
		arptable_tftp[HTTPD_ARPENTRY].ipaddr.ip[0], arptable_tftp[HTTPD_ARPENTRY].ipaddr.ip[1], 
		arptable_tftp[HTTPD_ARPENTRY].ipaddr.ip[2], arptable_tftp[HTTPD_ARPENTRY].ipaddr.ip[3]);
#endif
		return;	 
	}			
	
	ptr = argv[0];

	for(i=0; i< 4; i++)
	{
		ip[i]=strtol((const char *)ptr,(char **)NULL, 10);		
		ptr = strchr(ptr, '.');
		ptr++;
	}
	arptable_tftp[TFTP_SERVER].ipaddr.ip[0]=ip[0];
	arptable_tftp[TFTP_SERVER].ipaddr.ip[1]=ip[1];
	arptable_tftp[TFTP_SERVER].ipaddr.ip[2]=ip[2];
	arptable_tftp[TFTP_SERVER].ipaddr.ip[3]=ip[3];
/*replace the MAC address middle 4 bytes.*/
#ifndef DNI_PATCH
	eth0_mac[1]=ip[0];
	eth0_mac[2]=ip[1];
	eth0_mac[3]=ip[2];
	eth0_mac[4]=ip[3];
#endif
	prom_printf("Now your Target IP is %d.%d.%d.%d\n", ip[0],ip[1],ip[2],ip[3]);
#if 0	
	ptr = argv[1];
	//prom_printf("You want to setup Host new ip as %s \n", ptr);	
	
	for(i=0; i< 4; i++)
	{
		ip[i]=strtol((const char *)ptr,(char **)NULL, 10);		
		ptr = strchr(ptr, '.');
		ptr++;
	}
	arptable[ARP_SERVER].ipaddr.ip[0]=ip[0];
	arptable[ARP_SERVER].ipaddr.ip[1]=ip[1];
	arptable[ARP_SERVER].ipaddr.ip[2]=ip[2];
	arptable[ARP_SERVER].ipaddr.ip[3]=ip[3];
	prom_printf("Now your Host IP is %d.%d.%d.%d\n", ip[0],ip[1],ip[2],ip[3]);
#endif	
		
}	
int CmdDumpWord( int argc, char* argv[] )
{
	
	unsigned long src;
	unsigned int len,i;

	if(argc<1)
	{	dprintf("Wrong argument number!\r\n");
		return;
	}
	
	if(argv[0])	
	{	src = strtoul((const char*)(argv[0]), (char **)NULL, 16);
		if(src <0x80000000)
			src|=0x80000000;
	}
	else
	{	dprintf("Wrong argument number!\r\n");
		return;		
	}
				
	if(!argv[1])
		len = 1;
	else
	len= strtoul((const char*)(argv[1]), (char **)NULL, 10);			
	while ( (src) & 0x03)
		src++;

	for(i=0; i< len ; i+=4,src+=16)
	{	
		dprintf("%08X:	%08X	%08X	%08X	%08X\n",
		src, *(unsigned long *)(src), *(unsigned long *)(src+4), 
		*(unsigned long *)(src+8), *(unsigned long *)(src+12));
	}

}
#endif

//---------------------------------------------------------------------------
int CmdDumpByte( int argc, char* argv[] )
{
	
	unsigned long src;
	unsigned int len,i;

	if(argc<1)
	{	dprintf("Wrong argument number!\r\n");
		return;
	}
	
	src = strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	if(!argv[1])
		len = 16;
	else
	len= strtoul((const char*)(argv[1]), (char **)NULL, 10);			


	ddump((unsigned char *)src,len);
}

//---------------------------------------------------------------------------
int CmdWriteWord( int argc, char* argv[] )
{
	
	unsigned long src;
	unsigned int value,i;
	
	src = strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	while ( (src) & 0x03)
		src++;

	for(i=0;i<argc-1;i++,src+=4)
	{
		value= strtoul((const char*)(argv[i+1]), (char **)NULL, 16);	
		*(volatile unsigned int *)(src) = value;
	}
	
}
//---------------------------------------------------------------------------
#ifndef DNI_BOOTCODE_DOWNSIZE
#ifdef REMOVED_UNUSED
int CmdWriteAll( int argc, char* argv[] )
{
	
	unsigned long src;
	unsigned int value,i;
	unsigned int length;
	
	src = strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	while ( (src) & 0x03)
		src++;

	i = 0;
	value = strtoul((const char*)(argv[1]), (char **)NULL, 16);	
	length = strtoul((const char*)(argv[2]), (char **)NULL, 16);	
	printf("Write %x to %x for length %d\n",value,src,length);
	for(i=0;i<length;i+=4,src+=4)
	{
		*(volatile unsigned int *)(src) = value;
	}
	
}
//---------------------------------------------------------------------------

int CmdWriteHword( int argc, char* argv[] )
{
	
	unsigned long src;
	unsigned short value,i;
	
	src = strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	
	src &= 0xfffffffe;	

	for(i=0;i<argc-1;i++,src+=2)
	{
		value= strtoul((const char*)(argv[i+1]), (char **)NULL, 16);	
		*(volatile unsigned short *)(src) = value;
	}
	
}
#endif
//---------------------------------------------------------------------------
int CmdWriteByte( int argc, char* argv[] )
{
	
	unsigned long src;
	unsigned char value,i;
	
	src = strtoul((const char*)(argv[0]), (char **)NULL, 16);		


	for(i=0;i<argc-1;i++,src++)
	{
		value= strtoul((const char*)(argv[i+1]), (char **)NULL, 16);	
		*(volatile unsigned char *)(src) = value;
	}
	
}
int CmdCmp(int argc, char* argv[])
{
	int i;
	unsigned long dst,src;
	unsigned long dst_value, src_value;
	unsigned int length;
	unsigned long error;

	if(argc < 3) {
		printf("Parameters not enough!\n");
		return 1;
	}
	dst = strtoul((const char*)(argv[0]), (char **)NULL, 16);
	src = strtoul((const char*)(argv[1]), (char **)NULL, 16);
	length= strtoul((const char*)(argv[2]), (char **)NULL, 16);		
	error = 0;
	for(i=0;i<length;i+=4) {
		dst_value = *(volatile unsigned int *)(dst+i);
		src_value = *(volatile unsigned int *)(src+i);
		if(dst_value != src_value) {		
			printf("%dth data(%x %x) error\n",i, dst_value, src_value);
			error = 1;
		}
	}
	if(!error)
		printf("No error found\n");

}
#endif
//---------------------------------------------------------------------------
#ifndef RTL8197B
#ifndef DNI_BOOTCODE_DOWNSIZE
extern int autoBurn;
int CmdAuto(int argc, char* argv[])
{
	unsigned long addr;


	if(argv[0][0] == '0')
		autoBurn = 0 ;
	else
		autoBurn = 1 ;
	printf("AutoBurning=%d\n",autoBurn);
}
#endif
#endif


//---------------------------------------------------------------------------
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdLoad(int argc, char* argv[])
{
	unsigned long addr;


	image_address= strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	printf("Set TFTP Load Addr 0x%x\n",image_address);
}
#endif
#endif
/*
--------------------------------------------------------------------------
Flash Utility
--------------------------------------------------------------------------
*/
#if defined(KLD) || !defined(CONFIG_SPI_FLASH)
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdFlr(int argc, char* argv[])
{
	int i;
	unsigned long dst,src;
	unsigned int length;
	//unsigned char TARGET;
//#define  FLASH_READ_BYTE	4096

	dst = strtoul((const char*)(argv[0]), (char **)NULL, 16);
	src = strtoul((const char*)(argv[1]), (char **)NULL, 16);
	length= strtoul((const char*)(argv[2]), (char **)NULL, 16);		
	//length= (length + (FLASH_READ_BYTE - 1)) & FLASH_READ_BYTE;

/*Cyrus Tsai*/
/*file_length_to_server;*/
//length=file_length_to_client;
//length=length & (~0xffff)+0x10000;
//dst=image_address;
file_length_to_client=length;
/*Cyrus Tsai*/

	printf("Flash read from %X to %X with %X bytes	?\n",src,dst,length);
	printf("(Y)es , (N)o ? --> ");

	if (YesOrNo())
	        //for(i=0;i<length;i++)
	        //   {
		//    if ( flashread(&TARGET, src+i,1) )
		//	printf("Flash Read Successed!, target %X\n",TARGET);
		//    else
		//	printf("Flash Read Failed!\n");
		//  }	
		    if (flashread(dst, src, length))
			printf("Flash Read Successed!\n");
		    else
			printf("Flash Read Failed!\n");
	else
		printf("Abort!\n");
//#undef	FLASH_READ_BYTE		4096

}
#endif
#endif
#ifndef RTL8197B
/* Setting image header */
#if defined(KLD)
#define HW_SETTING_11N_RESERVED7_OFFSET 0xB2
#define HW_SETTING_11N_RESERVED8_OFFSET 0xB3
#define HW_SETTING_11N_RESERVED9_OFFSET 0xB4
#define HW_SETTING_11N_RESERVED10_OFFSET 0xB5
#define HW_SETTING_OFFSET 0x6000
typedef struct _setting_header_ {
	unsigned char  Tag[2];
	unsigned char  Version[2];
	unsigned short len;
} SETTING_HEADER_T, *SETTING_HEADER_Tp;

unsigned char CHECKSUM(unsigned char *data, int len)
{	
	int i;
	unsigned char sum=0;	
	for (i=0; i<len; i++)		
		sum += data[i];	
	sum = ~sum + 1;	
	return sum;
}
int CmdFlw_sig(int argc, char* argv[])
{
	unsigned long dst,src;
	unsigned long length;
	unsigned char checksum;
	SETTING_HEADER_T hw_setting;
	unsigned int offset=0;
	unsigned long option=0;
	unsigned char option_byte=0x00;
	#if defined(CONFIG_SPI_FLASH)
	unsigned int  cnt2=0;
	#endif
	dst= 0x80500000;
	if(argv[0][0]=='l')
		offset=HW_SETTING_11N_RESERVED7_OFFSET;//write reserved 5th byte
	if(argv[0][0]=='r')
		offset=HW_SETTING_11N_RESERVED8_OFFSET;//write reserved 6th byte
	if(argv[0][0]=='w')
		offset=HW_SETTING_11N_RESERVED9_OFFSET;//write reserved 7th byte
	if(argv[0][0]=='f')
		offset=HW_SETTING_11N_RESERVED10_OFFSET;//write reserved 8th byte
	memset((void*)dst, 0x00, 0x2000);
	if (flashread(dst, HW_SETTING_OFFSET, 0x2000)){
	
		memcpy(&hw_setting, (void*)dst, sizeof(SETTING_HEADER_T));

	if(argv[1][0] !='0' ){
		option_byte=argv[1][0];
	}
		if(argv[0][0]=='f')
			option_byte = argv[1][0]-'0';
		memcpy((void*)(dst+offset),&option_byte , 1);
		checksum = CHECKSUM((void*)(dst+sizeof(SETTING_HEADER_T)), hw_setting.len-1);
	
		memcpy((void*)(dst+hw_setting.len - 1+sizeof(SETTING_HEADER_T)), &checksum, 1);
		#if !defined(CONFIG_SPI_FLASH)
			if (flashwrite(HW_SETTING_OFFSET, dst, 0x2000)){
				printf("Flash Write Successed!\n");
			}else
				printf("Flash Write Failed!\n");
		#else	
			 #if defined(SUPPORT_SPI_MIO_8198_8196C) && defined(CONFIG_SPI_FLASH)
			   	spi_pio_init_8198();	//JSW: Must add
				spi_flw_image_mio_8198(cnt2, HW_SETTING_OFFSET, dst , 0x2000);	
			  #else			
				spi_pio_init();	//JSW: Must add
				spi_flw_image(cnt2, HW_SETTING_OFFSET, dst , 0x2000);	
			 #endif	
		#endif	
			
		return;
	}
		
}


int CmdFlr_sig(int argc, char* argv[])
{
	char image_sig[5]={0};
	unsigned char TARGET[1]={0};
	unsigned int offset=0;
	memcpy(image_sig, FW_SIGNATURE_WITH_ROOT_615, SIG_LEN);
	offset=HW_SETTING_11N_RESERVED7_OFFSET;//read reserved 7th byte
	flashread((unsigned long)TARGET,HW_SETTING_OFFSET+offset, 1);
	if(TARGET[0] !=0x00)
		image_sig[3] = TARGET[0];
	image_sig[4]='\0';	
	printf("Linux kernel sig: %s\n",  image_sig);
	
	memcpy(image_sig, ROOT_SIGNATURE_615, SIG_LEN);
	offset=HW_SETTING_11N_RESERVED8_OFFSET;//read reserved 8th byte
	flashread((unsigned long)TARGET,HW_SETTING_OFFSET+offset, 1);
	if(TARGET[0] !=0x00)
		image_sig[3] = TARGET[0];
	image_sig[4]='\0';		
	printf("Root filesystem sig: %s\n",  image_sig);
	
	
	memcpy(image_sig, WEB_ROOT_SIGNATURE_615, SIG_LEN);
	offset=HW_SETTING_11N_RESERVED9_OFFSET;//read reserved 9th byte
	flashread((unsigned long)TARGET,HW_SETTING_OFFSET+offset, 1);
	if(TARGET[0] !=0x00)
		image_sig[3] = TARGET[0];
	image_sig[4]='\0';		
	printf("Language Pack sig: %s\n",  image_sig);
	
	offset=HW_SETTING_11N_RESERVED10_OFFSET;//read reserved 10th byte
	flashread((unsigned long)TARGET,HW_SETTING_OFFSET+offset, 1);
	printf("Flag: %d\n",  TARGET[0]);
		return;
}

#endif


#ifndef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdFlw(int argc, char* argv[])
{
	unsigned long dst,src;
	unsigned long length;

#define FLASH_WRITE_BYTE 4096

	dst = strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	src = strtoul((const char*)(argv[1]), (char **)NULL, 16);		
	length= strtoul((const char*)(argv[2]), (char **)NULL, 16);		
//	length= (length + (FLASH_WRITE_BYTE - 1)) & FLASH_WRITE_BYTE;

/*Cyrus Tsai*/
/*file_length_to_server;*/
//length=file_length_to_server;
//length=length & (~0xffff)+0x10000;
/*Cyrus Tsai*/

	
	printf("Flash Program from %X to %X with %X bytes	?\n",src,dst,length);
	printf("(Y)es, (N)o->");
	if (YesOrNo())
		if (flashwrite(dst, src, length))
			printf("Flash Write Successed!\n");
		else
			printf("Flash Write Failed!\n");
	else
		printf("Abort!\n");
#undef FLASH_WRITE_BYTE //4096

        //---------------------------------------------------------------------------

}
#endif



#endif
//---------------------------------------------------------------------------
#endif //RTL8197B

#if 0
//---------------------------------------------------------------------------
int CmdTimer(int argc, char* argv[])
{
//	extern void timer_task(void);
//	timer_task();	
#if 1 //wei edit
	int i=0;
	dprintf("Endless loop, every 1 sec show a message.\r\n");
	while(1)
	{
		i=jiffies;
		if(i%100 ==0 )		
			dprintf("1 sec.\r\n");

		while(i==jiffies) ;

	}
#endif
#if 0		//memory read and write test
	
	unsigned int val=0;
	volatile unsigned long *p;
	//for(p=0x81000000; p< 0x82000000; p+=4)	//32M16b test 16M
	for(p=0x80f00000; p< 0x81000000; p+=4)	//16M test 8M	
	{	*p=p;
		if( *p != p)
			dprintf("Error Addr=%x, Read=%x\r\n", p, *p);

	}
#endif
	return TRUE;
	
}
#endif
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#if 0
int CmdTFTP(int argc, char* argv[])  //wei add
{
//	unsigned long addr;
	volatile unsigned int reg;	
//	addr = strtoul((const char*)(argv[1]), (char **)NULL, 10);
	
	eth_listening();	//do while(1) receive packet


}
#endif
#if !defined(CONFIG_BOOT_DEBUG_ENABLE)
extern char eth0_mac[6];
#endif
#ifndef DNI_BOOTCODE_DOWNSIZE
#if defined(CONFIG_BOOT_DEBUG_ENABLE)
#ifdef REMOVED_UNUSED
int CmdTFTP_TX(int argc, char* argv[])  //wei add
{
//	unsigned long addr;
	volatile unsigned int reg;	
//	addr = strtoul((const char*)(argv[1]), (char **)NULL, 10);
	int i;

//ARP protocol
	unsigned char tx_buffer[64]={0xff, 0xff, 0xff, 0xff, 0xff, 0xff,  //DMAC
							 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,  //SMAC
							 0x08, 0x06, //ARP
							 0x00, 0x01, //ETH
							//----------
							 0x08, 0x00, //IP Protocol
							 0x06,0x04,
							 0x00,0x01, //OPcode request
							 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,  //SMAC
							 0xc0, 0xa8,0x01,0x06,  //Sender IP:192.168.1.6
							 //-------------
							 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //Target MAC
							 0xc0, 0xa8, 0x01, 0x07,  //Target IP	:192.168.1.7	
							 //0x00,0x04,0x00,0x01,  //checksum
							 
							}; 
//UDP protocol
	unsigned char tx_buffer1[]={					 
	0x56,0xaa,0xa5,0x5a,0x7d,0xe8,
	0x00,0x1a,0x92,0x30,0xc1,0x49,
	0x08,0x00,
	0x45,0x00, 
	//----------          
	0x00,0x30,0x68,0x51,0x00,0x00,0x80,0x11,0x4f,0x0e,
	0xc0,0xa8,0x01,0x07,  //SIP
	0xc0,0xa8,0x01,0x06, //DIP
	0x13,0x3c,
	0x00,0x45,
	0x00,0x1c,0x8a,0x3a,0x00,0x02,0x70,0x64,0x66,0x69,
	//------------
	0x6e,0x66,0x6f,0x2e,0x69,0x6e,0x69,0x00,0x6f,0x63,0x74,0x65,0x74,0x00                                                                               
							 
							}; 


	
	dprintf("My MAC= %x : %x : %x : %x : %x : %x \n", eth0_mac[0]&0xff, eth0_mac[1]&0xff,eth0_mac[2]&0xff,
											eth0_mac[3]&0xff,eth0_mac[4]&0xff,eth0_mac[5]&0xff);

	if(argc < 1) 
	{	dprintf("Usage: TFTP pattern number \n");
		return 0;		
	}

	int patt=0;	
	patt = strtoul((const char*)(argv[0]), (char **)NULL, 16);

	



	if(patt==0)
	{

	for(i=0;i<6;i++)
		tx_buffer[6+i]=eth0_mac[i];
//	for(i=0;i<6;i++)
//		tx_buffer[22+i]=eth0_mac[i];
#if CONFIG_SW_DIFFIP
		tx_buffer[31]=5;
#endif
		unsigned int checksum=0; 
		for(i=0;i<60;i++)
			checksum+=tx_buffer[i];
		dprintf("checksum=%x \n", checksum);
		//tx_buffer[60]
	
		swNic_send(tx_buffer,60);//42+4);//strlen(tx_buffer));
	}
	else if(patt==1)
	{
		swNic_send(tx_buffer1,sizeof(tx_buffer1) );//strlen(tx_buffer));
	}
	else if(patt==8)
	{
		/*
		int *p=0x80300000;
		int len=file_length_to_server;
		dprintf("buff=%x, len=%d \n", p, len);
		swNic_send(p,len);//strlen(tx_buffer));
		*/

		//swNic_send(tx_buffer9,42);//strlen(tx_buffer));
	}
	else if(patt==9)
	{		

		//#define MSCR 0xbb804110
 		//REG32(MSCR) = 0;  // for NFBI all pkt coming
 		//REG32(0xbb804048)=1; //skip L2 checksum		
 
		//memset(tx_buffer,0,64);
		//for(i=0;i<60;i++)
		//	tx_buffer[i]=i;
		for(i=0;i<10;i++)
		{
			tx_buffer[16]=i;		
			swNic_send(tx_buffer,60);
		}
		
	}






}
#endif
#endif
#endif
/*
------------------------------------------  ---------------------------------
; Command Help
---------------------------------------------------------------------------
*/
  



int CmdHelp( int argc, char* argv[] )
{
	int	i, LineCount ;

    printf("----------------- COMMAND MODE HELP ------------------\n");
	for( i=0, LineCount = 0 ; i < (sizeof(MainCmdTable) / sizeof(COMMAND_TABLE)) ; i++ )
	{
		if( MainCmdTable[i].msg )
		{
			LineCount++ ;
			printf( "%s\n", MainCmdTable[i].msg );
			if( LineCount == PAGE_ECHO_HEIGHT )
			{
				printf("[Hit any key]\r");
				WaitKey();
				printf("	     \r");
				LineCount = 0 ;
			}
		}
	}
	/*Cyrus Tsai*/
#if 0
 	/*Cyrus Tsai*/
    flash_test();   
#endif    
    
	return TRUE ;
}



//---------------------------------------------------------------------------

#ifndef DNI_BOOTCODE_DOWNSIZE
int YesOrNo(void)
{
	unsigned char iChar[2];

	GetLine( iChar, 2,1);
	printf("\n");//vicadd
	if ((iChar[0] == 'Y') || (iChar[0] == 'y'))
		return 1;
	else
		return 0;
}
#endif
//---------------------------------------------------------------------------
#ifdef CONFIG_SPI_FLASH
#ifndef DNI_BOOTCODE_DOWNSIZE
int CmdSFlw(int argc, char* argv[])
{
	unsigned int  cnt2=0;//strtoul((const char*)(argv[3]), (char **)NULL, 16);	
	unsigned int  dst_flash_addr_offset=strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	unsigned int  src_RAM_addr=strtoul((const char*)(argv[1]), (char **)NULL, 16);
	unsigned int  length=strtoul((const char*)(argv[2]), (char **)NULL, 16);
	unsigned int  end_of_RAM_addr=src_RAM_addr+length;	
	printf("Write 0x%x Bytes to SPI flash#%d, offset 0x%x<0x%x>, from RAM 0x%x to 0x%x\n" ,length,cnt2+1,dst_flash_addr_offset,dst_flash_addr_offset+0xbd000000,src_RAM_addr,end_of_RAM_addr);
	printf("(Y)es, (N)o->");
	if (YesOrNo())
	{
		  #if defined(SUPPORT_SPI_MIO_8198_8196C) && defined(CONFIG_SPI_FLASH)
		   	spi_pio_init_8198();	//JSW: Must add
#ifdef DNI_PATCH
			spi_flw_image_mio_8198(cnt2, dst_flash_addr_offset, src_RAM_addr , length, 0);
#else
			spi_flw_image_mio_8198(cnt2, dst_flash_addr_offset, src_RAM_addr , length);	
#endif				
		  #else			
			spi_pio_init();	//JSW: Must add
			spi_flw_image(cnt2, dst_flash_addr_offset, src_RAM_addr , length);	
		 #endif
	}//end if YES
	else
		printf("Abort!\n");
}
#endif
#endif
//---------------------------------------------------------------------------
#ifdef SUPPORT_NAND 

int CmdNFlr(int argc, char* argv[])
{
	int i;
	unsigned long dst,src;
	unsigned int length;

	if(argc < 3) {
		printf("Parameters not enough!\n");
		return 1;
	}

	dst = strtoul((const char*)(argv[0]), (char **)NULL, 16);
	src = strtoul((const char*)(argv[1]), (char **)NULL, 16);
	length= strtoul((const char*)(argv[2]), (char **)NULL, 16);		

	file_length_to_client=length;

	printf("Read NAND Flash from %X to %X with %X bytes	?\n",src,dst,length);
	printf("(Y)es , (N)o ? --> ");

	if (YesOrNo())
		    if (read_data(src,length,(unsigned char *)dst))
			printf("Read NAND Flash Successed!\n");
		    else
			printf("Read NAND Flash Failed!\n");
	else
		printf("Abort!\n");

}
//---------------------------------------------------------------------------
int CmdNFlw(int argc, char* argv[])
{
	unsigned long dst,src;
	unsigned long length;


	if(argc < 3) {
		printf("Parameters not enough!\n");
		return 1;
	}
	dst = strtoul((const char*)(argv[0]), (char **)NULL, 16);		
	src = strtoul((const char*)(argv[1]), (char **)NULL, 16);		
	length= strtoul((const char*)(argv[2]), (char **)NULL, 16);		

	printf("Program NAND flash from %X to %X with %X bytes	?\n",src,dst,length);
	printf("(Y)es, (N)o->");
	if (YesOrNo())
		if (write_data(dst, length, src))
			printf("Write Nand Flash Successed!\n");
		else
			printf("Write Nand Flash Failed!\n");
	else
		printf("Abort!\n");
				
}
#endif

//---------------------------------------------------------------------------
#ifndef DNI_BOOTCODE_DOWNSIZE
void RunMonitor(char *PROMOPT, COMMAND_TABLE *TestCmdTable, int len)
{
	char		buffer[ MAX_MONITOR_BUFFER +1 ];
	int		argc ;
	char**		argv ;
	int		i, retval ;
	
	
	while(1)
	{	
		//printf( "%s", TEST_PROMPT );
		dprintf( "%s", PROMOPT );
		memset( buffer, 0, MAX_MONITOR_BUFFER );
		GetLine( buffer, MAX_MONITOR_BUFFER,1);
		dprintf( "\n" );
		argc = GetArgc( (const char *)buffer );
		argv = GetArgv( (const char *)buffer );
		if( argc < 1 ) continue ;
		StrUpr( argv[0] );

		if(!strcmp( argv[0], "..") || !strcmp( argv[0], "Q") )
			return;
		
		//for( i=0 ; i < (sizeof(TestCmdTable) / sizeof(COMMAND_TABLE)) ; i++ )
		for( i=0 ; i < (len) ; i++ )
		{
			if( ! strcmp( argv[0], TestCmdTable[i].cmd ) )
			{
				if(TestCmdTable[i].func)
					retval = TestCmdTable[i].func( argc - 1 , argv+1 );
				//dprintf("End run code\n");
				memset(argv[0],0,sizeof(argv[0]));
				break;
			}
		}
		//if(i==sizeof(TestCmdTable) / sizeof(COMMAND_TABLE)) printf("Unknown command !\r\n");
		if(i==len) printf("Unknown command !\r\n");
	}
}
#endif
//---------------------------------------------------------------------------

#if defined(CONFIG_PCIE_MODULE) 
int CmdTestPCIE(int argc, char* argv[])
{
	extern void TestMonitorEntry();
	TestMonitorEntry();		
};
#endif
//---------------------------------------------------------------------------
#if defined(CONFIG_R8198EP_HOST) || defined(CONFIG_R8198EP_DEVICE)
int CmdTestSlavePCIE(int argc, char* argv[])
{
	extern void Test_Slv_PCIe_Entry();
	Test_Slv_PCIe_Entry();		
};
#endif
//---------------------------------------------------------------------------

#if 1//defined(CONFIG_R8198EP_HOST) || defined(CONFIG_R8198EP_DEVICE)


#ifdef  CONFIG_SPI_TEST
int CmdSTEST(int argc, char* argv[])
{
	
	unsigned short  test_cnt=strtoul((const char*)(argv[0]), (char **)NULL, 16);
	unsigned long DRAM_starting_addr=strtoul((const char*)(argv[1]), (char **)NULL, 16);
	unsigned int spi_clock_div_num=strtoul((const char*)(argv[2]), (char **)NULL, 16);

	printf("sizeof(unsigned short)=%d\n",sizeof(unsigned short));
	printf("sizeof(unsigned long)=%d\n",sizeof(unsigned long));
	printf("sizeof(unsigned int)=%d\n",sizeof(unsigned int));
	printf("sizeof(unsigned long long)=%d\n",sizeof(unsigned long long));
	//HEX_DRAM_test_starting_addr
	unsigned short i;
	for (i=1;i<=test_cnt;i++)
	{
		
		//auto_memtest(start,end);
		printf("\n================================\n");	
		printf("===  SPI/DRAM Test Count(%d)  ===\n",i);	
		
		printf("================================\n");	

		  #if defined(SUPPORT_SPI_MIO_8198_8196C) && defined(CONFIG_SPI_FLASH)
			auto_spi_memtest_8198(DRAM_starting_addr,spi_clock_div_num);
		  #else
			//auto_spi_memtest(DRAM_starting_addr,spi_clock_div_num); //for 8196 Auto test
		  #endif
	}
}

#endif //end if CONFIG_SPI_TEST


#ifdef CONFIG_NOR_TEST
  //JSW : For NOR/DRAM test  @2007/09/03
int CmdNTEST(int argc, char* argv[])
{
    unsigned short  test_cnt=strtoul((const char*)(argv[0]), (char **)NULL, 16);
    unsigned long DRAM_starting_addr=strtoul((const char*)(argv[1]), (char **)NULL, 16);
    unsigned short i;
    for (i=1;i<=test_cnt;i++)
    {
        //auto_memtest(start,end);
        printf("\n================================\n");
        printf("===  NOR/DRAM Test Count(%d)  ===\n",i);
        printf("================================\n");
        auto_nor_memtest(DRAM_starting_addr);
    }

}
#endif //end if CONFIG_NOR_TEST




        //in spi_flash.c
extern unsigned int rand2(void);
#ifndef DNI_BOOTCODE_DOWNSIZE
extern unsigned int get_ctimestamp( void );
#endif
//extern void srand2( unsigned int seed );
  //unsigned int dram_seed = 0xDeadC0de;


#define MAX_SAMPLE  0x8000
//#define START_ADDR  0x100000               //1MB
#define START_ADDR  0x800000              //8MB
//#define END_ADDR      0x800000		//8MB
#define END_ADDR      0x1000000         //16MB
//#define END_ADDR      0x2000000        //32MB
//#define END_ADDR      0x4000000       //64MB
//#define END_ADDR      0x8000000         //128MB      
#define BURST_COUNTS  256

#ifdef CONFIG_DRAM_TEST
void Dram_test(int argc, char* argv[])
{
    unsigned int i, j,k,k2=0;
    unsigned int cache_type=0;
    unsigned int access_type=0;
    unsigned int addr;
    unsigned int burst=0;
    unsigned long int wdata;
    unsigned int samples,test_range;

    unsigned int enable_delay,delay_time,PM_MODE;//JSW:For DRAM Power Management
       
    unsigned int wdata_array[BURST_COUNTS];         //JSW:It must equal to Burst size

    
   

    /*JSW: Auto set DRAM test range*/
    /*
    //unsigned int END_ADDR;
    if (REG32(MCR_REG)==0x52080000)//8MB
	END_ADDR=0x800000;
    else if ((REG32(MCR_REG)==0x5a080000) ||(REG32(MCR_REG)==0x52480000)) //16MB
       END_ADDR=0x1000000;
    else if ((REG32(MCR_REG)==0x5a480000) ||(REG32(MCR_REG)==0x54480000)) //32MB
       END_ADDR=0x2000000;
    else if ((REG32(MCR_REG)==0x5c480000) ||(REG32(MCR_REG)==0x54880000)) //64MB
       END_ADDR=0x4000000;
    else if(REG32(MCR_REG)==0x5c880000)  //128MB
       END_ADDR=0x4000000;
    */


    unsigned int keep_W_R_mode;
   
    //prom_printf("argc=%d\n",argc);

	
	if(argc<=1)
	{	 		
		prom_printf("ex:dramtest <R/W> <enable_random_delay> <PowerManagementMode>\r\n");
	       prom_printf("ex:<R/W>:<0>=R+W, <1>=R,<2>=W\r\n");
		prom_printf("ex:<enable_random_delay>: <0>=Disable, <1>=Enable\r\n");
		prom_printf("ex:<PowerManagementMode> : <0>=Normal, <1>=PowerDown, <2>=SeldRefresh\r\n");
		prom_printf("    <3>:Reserved,<4>:CPUSleep + Self Refresh in IMEM   \r\n"); 				
		return;	
	}

	 keep_W_R_mode= strtoul((const char*)(argv[0]), (char **)NULL, 16);
        enable_delay= strtoul((const char*)(argv[1]), (char **)NULL, 16);
	 PM_MODE= strtoul((const char*)(argv[2]), (char **)NULL, 16);

    while(1)
    {

      

#if 1                                       //RTL8196_208PIN_SUPPORT_DDR
        prom_printf("\n================================\n");
        k2++;
        prom_printf("\nBegin DRAM Test : %d\n",k2);
        printf("Dram Test parameter\n" );
        printf("1.DDCR(0xb8001050)=%x\n",READ_MEM32(DDCR_REG) );
        printf("2.DTR(0xb8001008)=%x\n",READ_MEM32(DTR_REG) );
        printf("3.DCR(0xb8001004)=%x\n",READ_MEM32(DCR_REG) );
        printf("4.DRAM Freq=%d MHZ\n",check_dram_freq_reg());
        printf("5.Burst Size=%d \n",burst);
        printf("6.cache_type(0:cache)(1:Un-cache)=%d \n",cache_type);
        printf("7.Access_type(0:8bit)(1:16bit)(2:32bit)=%d \n",access_type);
	 printf("7.Tested size=0x%x \n",END_ADDR);
        printf("9.Tested addr=%x \n",addr);

        prom_printf("\n===============================\n");
#endif

     

        for (samples = 0; samples < MAX_SAMPLE; samples++)
        {
            cache_type = rand2() % ((unsigned int) 2);

            access_type = rand2()  % ((unsigned int) 3);
            burst = rand2() % (unsigned int) BURST_COUNTS;

     

            addr = 0x80000000 + START_ADDR + (rand2() % (unsigned int) (END_ADDR - START_ADDR));

   
            addr = cache_type ? (addr | 0x20000000) : addr;
            wdata = rand2();

            if (access_type == 0)
            {
                wdata = wdata & 0xFF;
            }
            else if (access_type == 1)
            {
                addr = (addr) & 0xFFFFFFFE;
                wdata = wdata & 0xFFFF;
            }
            else
            {
                addr = (addr) & 0xFFFFFFFC;
            }

        /* Check if Exceed Limit */
            if ( ((addr + (burst << access_type)) & 0x1FFFFFFF) > END_ADDR)
            {
                burst = (END_ADDR - ((addr) & 0x1FFFFFFF)) >> access_type;
            }

#if 1
            if (samples % 100 == 0)
            {
                printf("\nSamples: %d", samples);
		  
		
		 
		   #if 1 //JSW @20091106 :For DRAM Test + Power Management 
		 	if(enable_delay)
		 	{
			     delay_time=rand2() % ((unsigned int) 1000*1000);
			     prom_printf("  delay_time=%d\n",delay_time);
			     for(k=0;k<=delay_time;k++); //delay_loop				     
		 	}

			
		 	if(PM_MODE)
			{
				
				  //set bit[31:30]=0 for default "Normal Mode"
			    REG32(MPMR_REG)= 0x3FFFFFFF ;

			    switch(PM_MODE)
			    {
			        case 0:
			            dprintf("\nDRAM : Normal mode\n");
				     //return 0;
			            break;

			        case 1:
			            dprintf("\nDRAM :Auto Power Down mode\n");
			            REG32(MPMR_REG)= READ_MEM32(MPMR_REG)|(0x1 <<30) ;
				     //return 0;
			            break;

			        case 2:
			            dprintf("\nDRAM : Self Refresh mode\n");
				     REG32(MPMR_REG)= 0x3FFFFFFF ;
				     delay_ms(100);
			            REG32(MPMR_REG)|= (0x2 <<30) ;
				     delay_ms(100);
			            REG32(MPMR_REG)|= (0x2 <<30) ;	
				     delay_ms(100);
				     //return 0;	
			            break;

			        case 3:
			            dprintf("\nReserved!\n");
			            //REG32(MPMR_REG)= READ_MEM32(MPMR_REG)|(0x3 <<30) ;
			            REG32(MPMR_REG)= 0x3FFFFFFF ;
				     //return 0;
			            break;
				#ifdef CONFIG_CPUsleep_PowerManagement_TEST
				case 4:
			            dprintf("\nCPUSleep + Self Refresh in IMEM!\n");
				     CmdCPUSleepIMEM();
			            //return 0;
			            break;
				#endif
			        default :
			            dprintf("\nError Input,should be 0~4\n");
			            break;
			     }   //end of switch(PM_MODE)
		 	}//end of if(PM_MODE)			
		 #endif
		 
            }//end of switch(PM_MODE)
#endif

     

        /* Prepare Write Data */
            for (i = 0; i < burst ; i++)
            {            
                    wdata = (unsigned int)(rand2());// ???

                if (access_type == 0)               //8 bit
                    wdata = wdata & 0xFF;
                else if (access_type == 1)          //16bit
                    wdata = wdata & 0xFFFF;

                wdata_array[i] = wdata;
     
            }

            keep_writing:
        /* Write */
            for (i = 0, j = addr; i < burst ; i++)
            {
                if (access_type == 0)
                    *(volatile unsigned char *) (j) = wdata_array[i];
                else if (access_type == 1)
                    *(volatile unsigned short *) (j) = wdata_array[i];
                else
                    *(volatile unsigned int *) (j) = wdata_array[i];

                j = j + (1 << access_type);
        //keep writing
                if (keep_W_R_mode==1)
                {
                    goto keep_writing;
                }
            }

            if (keep_W_R_mode==2)
            {
                keep_reading:
                WRITE_MEM32(0xa0800000,0xa5a55a5a);
       
                goto keep_reading;
            }

        /* Read Verify */
            for (i = 0, j = addr; i < burst ; i++)
            {
                unsigned rdata;

                if (access_type == 0)
                {
                    rdata = *(volatile unsigned char *) (j);
                }
                else if (access_type == 1)
                {
                    rdata = *(volatile unsigned short *) (j);
                }
                else
                {
                    rdata = *(volatile unsigned int *) (j);
                }
        //printf("\n==========In Read Verify========= \n");
        // printf("\nrdata: %d\n", rdata);
        //printf("\nwdata_array[i]: %d\n",wdata_array[i]);
        // printf("\n==========End Read Verify========= \n");

                if (rdata != wdata_array[i])
                {
                    printf("\nWrite Data Array: 0x%X", wdata_array[i]);

                    if (cache_type)
                        printf("\n==> Uncached Access Address: 0x%X, Type: %d bit, Burst: %d",
                            addr, (access_type == 0) ? 8 : (access_type == 1) ? 16 : 32, burst);
                    else
                        printf("\n==>   Cached Access Address: 0x%X, Type: %d bit, Burst: %d",
                            addr, (access_type == 0) ? 8 : (access_type == 1) ? 16 : 32, burst);

                    printf("\n====> Verify Error! Addr: 0x%X = 0x%X, expected to be 0x%X\n", j, rdata, wdata_array[i]);

        //HaltLoop:
        //goto HaltLoop;
                    return 0;

                }

                j = j + (1 << access_type);

            }                                       //end of reading

        }

    }                                               //end while(1)
}
#endif


#endif




   // 1 sec won't sleep,5 secs will sleep
#define MPMR_REG 0xB8001040
#ifdef CONFIG_CPUsleep_PowerManagement_TEST
int CmdCPUSleep(int argc, char* argv[])
{

     dprintf("\nCPU Sleep..\n");

   /*
            PM_MODE=0 , normal
            PM_MODE=1 , auto power down
            PM_MODE=2 , seld refresh
        */
    if( !argv[0])                                   //read
    {
        dprintf("Usage: sleep <0~2>  \r\n");
	 dprintf("sleep <0>:CPU sleep + DRAM Normal mode \r\n"); 
	 dprintf("sleep <1>:CPU sleep + DRAM Power down  \r\n"); 
	 dprintf("sleep <2>:CPU sleep + DRAM Self refresh  \r\n"); 
	 dprintf("sleep <3>:Reserved  \r\n"); 
	 dprintf("sleep <4>:CPUSleep + Self Refresh in IMEM  \r\n"); 
        return;
    }   
#if 0//def timer_test
    unsigned short sleep_time = strtoul((const char*)(argv[0]), (char **)NULL, 16);
    dprintf("About to sleep 0x%x secs\n",sleep_time );


    tc0_init(sleep_time);
        //tc1_init(sleep_time);
        //tc2_init(sleep_time);
        //tc3_init(sleep_time);


        // REG32(GIMR_REG) =  0x0;//Disable all interrupt

        //dprintf("GIMR_REG = 0x%x\n",*(volatile unsigned int *)GIMR_REG);
    dprintf("GIMR_REG = 0x%x\n",*(volatile unsigned int *)GIMR_REG);
    dprintf("GISR_REG = 0x%x\n",*(volatile unsigned int *)GISR_REG);
    dprintf("TCCNR_REG = 0x%x\n",*(volatile unsigned int *)TCCNR_REG);
    dprintf("TCIR_REG = 0x%x\n",*(volatile unsigned int *)TCIR_REG);

    unsigned short  i;
    for (i=0;i<=100;i++)
        dprintf("TC1CNT_REG = 0x%x\n",*(volatile unsigned int *)TC1CNT_REG);

    dprintf("GIMR_REG = 0x%x\n",*(volatile unsigned int *)GIMR_REG);
    dprintf("GISR_REG = 0x%x\n",*(volatile unsigned int *)GISR_REG);
    dprintf("TCCNR_REG = 0x%x\n",*(volatile unsigned int *)TCCNR_REG);
    dprintf("TCIR_REG = 0x%x\n",*(volatile unsigned int *)TCIR_REG);

        // Enter Sleep Mode
    dprintf("\nCPU Enter Sleep Mode ,it will wake up after %x secs.....\n",sleep_time);
#endif

 unsigned short PM_MODE = strtoul((const char*)(argv[0]), (char **)NULL, 16);


if(PM_MODE)
{
	
	  //set bit[31:30]=0 for default "Normal Mode"
    REG32(MPMR_REG)= 0x3FFFFFFF ;

    switch(PM_MODE)
    {
        case 0:
            dprintf("\nDRAM : Normal mode\n");
            break;

        case 1:
            dprintf("\nDRAM :Auto Power Down mode\n");
            REG32(MPMR_REG)= READ_MEM32(MPMR_REG)|(0x1 <<30) ;
            break;

        case 2:
            dprintf("\nDRAM : Self Refresh mode\n");
	     REG32(MPMR_REG)= 0x3FFFFFFF ;
	      delay_ms(1000);
            REG32(MPMR_REG)|= (0x2 <<30) ;
	    delay_ms(1000);
            REG32(MPMR_REG)|= (0x2 <<30) ;
			
            break;

        case 3:
            dprintf("\nReserved!\n");
            REG32(MPMR_REG)= READ_MEM32(MPMR_REG)|(0x3 <<30) ;
            break;

	case 4:
            dprintf("\nCPUSleep + Self Refresh in IMEM!\n");
	     CmdCPUSleepIMEM();
            
            break;

        default :
            dprintf("\nError Input,should be 0~3\n");
            break;
    }

    //dprintf("After setting, MPMR(0xB8001040)=%x\n",READ_MEM32(MPMR_REG) );
}                                                   //End of DRAMPM



   
     REG32(GIMR_REG)=0x0;
	//cli();    
   
        //JSW: SLEEP
  
    __asm__ __volatile__ (    "sleep\n\t"   );    

      //JSW: Make sure CPU do sleep and below won't be printed
     delay_ms(1000);                         //delay 1.25 sec in 40MHZ(current OSC), 25/40=1.25 sec
        // After Counter Trigger interrupt
      dprintf("Counter Trigger interrupt,CPI Leave Sleep...\n");

}



#define __IRAM_IN_865X      __attribute__ ((section(".iram-rtkwlan")))
#define __IRAM_FASTEXTDEV        __IRAM_IN_865X
__IRAM_FASTEXTDEV
void CmdCPUSleepIMEM()
{

	 
      //while( (*((volatile unsigned int *)(0xb8001050))& 0x40000000) != 0x40000000);

      //dprintf("\nDRAM : Self Refresh mode IMEM01\n");

	     REG32(MPMR_REG)= 0x3FFFFFFF ;
	     delay_ms(1000);
            REG32(MPMR_REG)|= (0x2 <<30) ;
	     delay_ms(1000);
            REG32(MPMR_REG)|= (0x2 <<30) ;


   
     REG32(GIMR_REG)=0x0;
	//cli();    
   
        //JSW: SLEEP  
    __asm__ __volatile__ (    "sleep\n\t"   );    

      //JSW: Just make sure CPU do sleep and below won't be printed
     delay_ms(1000);                        
     dprintf("Counter Trigger interrupt,CPI Leave Sleep...\n");

}



#endif  //end if CONFIG_CPUsleep_PowerManagement_TEST




#ifndef DNI_BOOTCODE_DOWNSIZE
//anson add

int CmdPHYregR(int argc, char* argv[])
{
    unsigned long phyid, regnum;
    unsigned int uid,tmp;

    phyid = strtoul((const char*)(argv[0]), (char **)NULL, 16);
    regnum = strtoul((const char*)(argv[1]), (char **)NULL, 16);

    rtl8651_getAsicEthernetPHYReg( phyid, regnum, &tmp );
    uid=tmp;
    dprintf("PHYID=0x%x, regID=0x%x ,Find PHY Chip! UID=0x%x\r\n", phyid, regnum, uid);

}

int CmdPHYregW(int argc, char* argv[])
{
    unsigned long phyid, regnum;
    unsigned long data;
    unsigned int uid,tmp;

    phyid = strtoul((const char*)(argv[0]), (char **)NULL, 16);
    regnum = strtoul((const char*)(argv[1]), (char **)NULL, 16);
    data= strtoul((const char*)(argv[2]), (char **)NULL, 16);

    rtl8651_setAsicEthernetPHYReg( phyid, regnum, data );
    rtl8651_getAsicEthernetPHYReg( phyid, regnum, &tmp );
    uid=tmp;
    dprintf("PHYID=0x%x ,regID=0x%x, Find PHY Chip! UID=0x%x\r\n", phyid, regnum, uid);

}
#endif

#ifdef CONFIG_RTL8198_TAROKO
int CmdIMEM98TEST(int argc, char* argv[])
{
	imem_test();
	return 0;
}

int CmdWBMG(int argc, char* argv[])
{
	write_buf_merge();
	return 0;
}

#endif


//----------------------------------------------------------------------------------------------
#ifdef RTL8198
int CmdEEEPatch(int argc, char* argv[])
{

unsigned int p[]={
 0x1f, 0x0000,
 0x17, 0x2179,
 0x1f, 0x0007,
 0x1e, 0x0040,
 0x18, 0x0004,
 0x18, 0x0004,
 0x19, 0x4000,
 0x18, 0x0014,
 0x19, 0x7f00,
 0x18, 0x0024,
 0x19, 0x0000,
 0x18, 0x0034,
 0x19, 0x0100,
 0x18, 0x0044,
 0x19, 0xe000,
 0x18, 0x0054,
 0x19, 0x0000,
 0x18, 0x0064,
 0x19, 0x0000,
 0x18, 0x0074,
 0x19, 0x0000,
 0x18, 0x0084,
 0x19, 0x0400,
 0x18, 0x0094,
 0x19, 0x8000,
 0x18, 0x00a4,
 0x19, 0x7f00,
 0x18, 0x00b4,
 0x19, 0x4000,
 0x18, 0x00c4,
 0x19, 0x2000,
 0x18, 0x00d4,
 0x19, 0x0100,
 0x18, 0x00e4,
 0x19, 0x8400,
 0x18, 0x00f4,
 0x19, 0x7a00,
 0x18, 0x0104,
 0x19, 0x4000,
 0x18, 0x0114,
 0x19, 0x3f00,
 0x18, 0x0124,
 0x19, 0x0100,
 0x18, 0x0134,
 0x19, 0x7800,
 0x18, 0x0144,
 0x19, 0x0000,
 0x18, 0x0154,
 0x19, 0x0000,
 0x18, 0x0164,
 0x19, 0x0000,
 0x18, 0x0174,
 0x19, 0x0400,
 0x18, 0x0184,
 0x19, 0x8000,
 0x18, 0x0194,
 0x19, 0x7f00,
 0x18, 0x01a4,
 0x19, 0x8300,
 0x18, 0x01b4,
 0x19, 0x8300,
 0x18, 0x01c4,
 0x19, 0xe100,
 0x18, 0x01d4,
 0x19, 0x9c00,
 0x18, 0x01e4,
 0x19, 0x8800,
 0x18, 0x01f4,
 0x19, 0x0300,
 0x18, 0x0204,
 0x19, 0xe100,
 0x18, 0x0214,
 0x19, 0x0800,
 0x18, 0x0224,
 0x19, 0x4000,
 0x18, 0x0234,
 0x19, 0x7f00,
 0x18, 0x0244,
 0x19, 0x0400,
 0x18, 0x0254,
 0x19, 0x0100,
 0x18, 0x0264,
 0x19, 0x4000,
 0x18, 0x0274,
 0x19, 0x3e00,
 0x18, 0x0284,
 0x19, 0x0000,
 0x18, 0x0294,
 0x19, 0xe000,
 0x18, 0x02a4,
 0x19, 0x1200,
 0x18, 0x02b4,
 0x19, 0x8000,
 0x18, 0x02c4,
 0x19, 0x7f00,
 0x18, 0x02d4,
 0x19, 0x8900,
 0x18, 0x02e4,
 0x19, 0x8300,
 0x18, 0x02f4,
 0x19, 0xe000,
 0x18, 0x0304,
 0x19, 0x0000,
 0x18, 0x0314,
 0x19, 0x4000,
 0x18, 0x0324,
 0x19, 0x7f00,
 0x18, 0x0334,
 0x19, 0x0000,
 0x18, 0x0344,
 0x19, 0x2000,
 0x18, 0x0354,
 0x19, 0x4000,
 0x18, 0x0364,
 0x19, 0x3e00,
 0x18, 0x0374,
 0x19, 0xfd00,
 0x18, 0x0384,
 0x19, 0x0000,
 0x18, 0x0394,
 0x19, 0x1200,
 0x18, 0x03a4,
 0x19, 0xab00,
 0x18, 0x03b4,
 0x19, 0x0c00,
 0x18, 0x03c4,
 0x19, 0x0600,
 0x18, 0x03d4,
 0x19, 0xa000,
 0x18, 0x03e4,
 0x19, 0x3d00,
 0x18, 0x03f4,
 0x19, 0xfb00,
 0x18, 0x0404,
 0x19, 0xe000,
 0x18, 0x0414,
 0x19, 0x0000,
 0x18, 0x0424,
 0x19, 0x4000,
 0x18, 0x0434,
 0x19, 0x7f00,
 0x18, 0x0444,
 0x19, 0x0000,
 0x18, 0x0454,
 0x19, 0x0100,
 0x18, 0x0464,
 0x19, 0x4000,
 0x18, 0x0474,
 0x19, 0xc600,
 0x18, 0x0484,
 0x19, 0xff00,
 0x18, 0x0494,
 0x19, 0x0000,
 0x18, 0x04a4,
 0x19, 0x1000,
 0x18, 0x04b4,
 0x19, 0x0200,
 0x18, 0x04c4,
 0x19, 0x7f00,
 0x18, 0x04d4,
 0x19, 0x4000,
 0x18, 0x04e4,
 0x19, 0x7f00,
 0x18, 0x04f4,
 0x19, 0x0200,
 0x18, 0x0504,
 0x19, 0x0200,
 0x18, 0x0514,
 0x19, 0x5200,
 0x18, 0x0524,
 0x19, 0xc400,
 0x18, 0x0534,
 0x19, 0x7400,
 0x18, 0x0544,
 0x19, 0x0000,
 0x18, 0x0554,
 0x19, 0x1000,
 0x18, 0x0564,
 0x19, 0xbc00,
 0x18, 0x0574,
 0x19, 0x0600,
 0x18, 0x0584,
 0x19, 0xfe00,
 0x18, 0x0594,
 0x19, 0x4000,
 0x18, 0x05a4,
 0x19, 0x7f00,
 0x18, 0x05b4,
 0x19, 0x0000,
 0x18, 0x05c4,
 0x19, 0x0a00,
 0x18, 0x05d4,
 0x19, 0x5200,
 0x18, 0x05e4,
 0x19, 0xe400,
 0x18, 0x05f4,
 0x19, 0x3c00,
 0x18, 0x0604,
 0x19, 0x0000,
 0x18, 0x0614,
 0x19, 0x1000,
 0x18, 0x0624,
 0x19, 0x8a00,
 0x18, 0x0634,
 0x19, 0x7f00,
 0x18, 0x0644,
 0x19, 0x4000,
 0x18, 0x0654,
 0x19, 0x7f00,
 0x18, 0x0664,
 0x19, 0x0100,
 0x18, 0x0674,
 0x19, 0x2000,
 0x18, 0x0684,
 0x19, 0x0000,
 0x18, 0x0694,
 0x19, 0xe600,
 0x18, 0x06a4,
 0x19, 0xfe00,
 0x18, 0x06b4,
 0x19, 0x0000,
 0x18, 0x06c4,
 0x19, 0x5000,
 0x18, 0x06d4,
 0x19, 0x9d00,
 0x18, 0x06e4,
 0x19, 0xff00,
 0x18, 0x06f4,
 0x19, 0x8500,
 0x18, 0x0704,
 0x19, 0x7f00,
 0x18, 0x0714,
 0x19, 0xac00,
 0x18, 0x0724,
 0x19, 0x0800,
 0x18, 0x0734,
 0x19, 0xfc00,
 0x18, 0x0744,
 0x19, 0x9500,
 0x18, 0x0754,
 0x19, 0x0400,
 0x18, 0x0764,
 0x19, 0x4000,
 0x18, 0x0774,
 0x19, 0x4000,
 0x18, 0x0784,
 0x19, 0x1000,
 0x18, 0x0794,
 0x19, 0x4000,
 0x18, 0x07a4,
 0x19, 0x3f00,
 0x18, 0x07b4,
 0x19, 0x0200,
 0x18, 0x07c4,
 0x19, 0x0000,
 0x18, 0x07d4,
 0x19, 0xff00,
 0x18, 0x07e4,
 0x19, 0x7f00,
 0x18, 0x07f4,
 0x19, 0x0000,
 0x18, 0x0804,
 0x19, 0x4200,
 0x18, 0x0814,
 0x19, 0x0500,
 0x18, 0x0824,
 0x19, 0x9000,
 0x18, 0x0834,
 0x19, 0x8000,
 0x18, 0x0844,
 0x19, 0x7d00,
 0x18, 0x0854,
 0x19, 0x8c00,
 0x18, 0x0864,
 0x19, 0x8300,
 0x18, 0x0874,
 0x19, 0xe000,
 0x18, 0x0884,
 0x19, 0x0000,
 0x18, 0x0894,
 0x19, 0x4000,
 0x18, 0x08a4,
 0x19, 0x0400,
 0x18, 0x08b4,
 0x19, 0xff00,
 0x18, 0x08c4,
 0x19, 0x0500,
 0x18, 0x08d4,
 0x19, 0x8500,
 0x18, 0x08e4,
 0x19, 0x8c00,
 0x18, 0x08f4,
 0x19, 0xfa00,
 0x18, 0x0904,
 0x19, 0xe000,
 0x18, 0x0914,
 0x19, 0x0000,
 0x18, 0x0924,
 0x19, 0x4000,
 0x18, 0x0934,
 0x19, 0x1f00,
 0x18, 0x0944,
 0x19, 0x0000,
 0x18, 0x0954,
 0x19, 0xfe00,
 0x18, 0x0964,
 0x19, 0x7300,
 0x18, 0x0974,
 0x19, 0x0d00,
 0x18, 0x0984,
 0x19, 0x0300,
 0x18, 0x0994,
 0x19, 0x4000,
 0x18, 0x09a4,
 0x19, 0x2000,
 0x18, 0x09b4,
 0x19, 0x0000,
 0x18, 0x09c4,
 0x19, 0x0900,
 0x18, 0x09d4,
 0x19, 0x9d00,
 0x18, 0x09e4,
 0x19, 0x0800,
 0x18, 0x09f4,
 0x19, 0x9000,
 0x18, 0x0a04,
 0x19, 0x0700,
 0x18, 0x0a14,
 0x19, 0x7b00,
 0x18, 0x0a24,
 0x19, 0x4000,
 0x18, 0x0a34,
 0x19, 0x7f00,
 0x18, 0x0a44,
 0x19, 0x1000,
 0x18, 0x0a54,
 0x19, 0x0000,
 0x18, 0x0a64,
 0x19, 0x0000,
 0x18, 0x0a74,
 0x19, 0x0400,
 0x18, 0x0a84,
 0x19, 0x7300,
 0x18, 0x0a94,
 0x19, 0x0d00,
 0x18, 0x0aa4,
 0x19, 0x0100,
 0x18, 0x0ab4,
 0x19, 0x0900,
 0x18, 0x0ac4,
 0x19, 0x8e00,
 0x18, 0x0ad4,
 0x19, 0x0800,
 0x18, 0x0ae4,
 0x19, 0x7d00,
 0x18, 0x0af4,
 0x19, 0x4000,
 0x18, 0x0b04,
 0x19, 0x7f00,
 0x18, 0x0b14,
 0x19, 0x1000,
 0x18, 0x0b24,
 0x19, 0x0000,
 0x18, 0x0b34,
 0x19, 0x0200,
 0x18, 0x0b44,
 0x19, 0x0000,
 0x18, 0x0b54,
 0x19, 0x7000,
 0x18, 0x0b64,
 0x19, 0x0c00,
 0x18, 0x0b74,
 0x19, 0x0100,
 0x18, 0x0b84,
 0x19, 0x0900,
 0x18, 0x0b94,
 0x19, 0x7f00,
 0x18, 0x0ba4,
 0x19, 0x4000,
 0x18, 0x0bb4,
 0x19, 0x7f00,
 0x18, 0x0bc4,
 0x19, 0x3000,
 0x18, 0x0bd4,
 0x19, 0x8300,
 0x18, 0x0be4,
 0x19, 0x0200,
 0x18, 0x0bf4,
 0x19, 0x0000,
 0x18, 0x0c04,
 0x19, 0x7000,
 0x18, 0x0c14,
 0x19, 0x0d00,
 0x18, 0x0c24,
 0x19, 0x0100,
 0x18, 0x0c34,
 0x19, 0x9a00,
 0x18, 0x0c44,
 0x19, 0xff00,
 0x18, 0x0c54,
 0x19, 0x0a00,
 0x18, 0x0c64,
 0x19, 0x7d00,
 0x18, 0x0c74,
 0x19, 0x4000,
 0x18, 0x0c84,
 0x19, 0x7f00,
 0x18, 0x0c94,
 0x19, 0x1000,
 0x18, 0x0ca4,
 0x19, 0x0000,
 0x18, 0x0cb4,
 0x19, 0x8200,
 0x18, 0x0cc4,
 0x19, 0x0000,
 0x18, 0x0cd4,
 0x19, 0x7000,
 0x18, 0x0ce4,
 0x19, 0x0d00,
 0x18, 0x0cf4,
 0x19, 0x0100,
 0x18, 0x0d04,
 0x19, 0x8400,
 0x18, 0x0d14,
 0x19, 0x7f00,
 0x18, 0x0d24,
 0x19, 0x4000,
 0x18, 0x0d34,
 0x19, 0x7f00,
 0x18, 0x0d44,
 0x19, 0x1000,
 0x18, 0x0d54,
 0x19, 0x0000,
 0x18, 0x0d64,
 0x19, 0x0200,
 0x18, 0x0d74,
 0x19, 0x0000,
 0x18, 0x0d84,
 0x19, 0x7000,
 0x18, 0x0d94,
 0x19, 0x0f00,
 0x18, 0x0da4,
 0x19, 0x0100,
 0x18, 0x0db4,
 0x19, 0x9b00,
 0x18, 0x0dc4,
 0x19, 0x7f00,
 0x18, 0x0dd4,
 0x19, 0x4000,
 0x18, 0x0de4,
 0x19, 0x7f00,
 0x18, 0x0df4,
 0x19, 0x1000,
 0x18, 0x0e04,
 0x19, 0x9000,
 0x18, 0x0e14,
 0x19, 0x0200,
 0x18, 0x0e24,
 0x19, 0x0400,
 0x18, 0x0e34,
 0x19, 0x7300,
 0x18, 0x0e44,
 0x19, 0x1d00,
 0x18, 0x0e54,
 0x19, 0x0100,
 0x18, 0x0e64,
 0x19, 0x0b00,
 0x18, 0x0e74,
 0x19, 0x9000,
 0x18, 0x0e84,
 0x19, 0x8000,
 0x18, 0x0e94,
 0x19, 0x7d00,
 0x18, 0x0ea4,
 0x19, 0x4000,
 0x18, 0x0eb4,
 0x19, 0x7f00,
 0x18, 0x0ec4,
 0x19, 0x5000,
 0x18, 0x0ed4,
 0x19, 0x0000,
 0x18, 0x0ee4,
 0x19, 0x0200,
 0x18, 0x0ef4,
 0x19, 0x0000,
 0x18, 0x0f04,
 0x19, 0x7000,
 0x18, 0x0f14,
 0x19, 0x0f00,
 0x18, 0x0f24,
 0x19, 0x0100,
 0x18, 0x0f34,
 0x19, 0x9b00,
 0x18, 0x0f44,
 0x19, 0x7f00,
 0x18, 0x0f54,
 0x19, 0xe000,
 0x18, 0x0f64,
 0x19, 0xdd00,
 0x18, 0x0f74,
 0x19, 0x4000,
 0x18, 0x0f84,
 0x19, 0x7f00,
 0x18, 0x0f94,
 0x19, 0x1000,
 0x18, 0x0fa4,
 0x19, 0x0000,
 0x18, 0x0fb4,
 0x19, 0x0000,
 0x18, 0x0fc4,
 0x19, 0x0400,
 0x18, 0x0fd4,
 0x19, 0x7300,
 0x18, 0x0fe4,
 0x19, 0x0d00,
 0x18, 0x0ff4,
 0x19, 0x0100,
 0x18, 0x1004,
 0x19, 0x0400,
 0x18, 0x1014,
 0x19, 0x0300,
 0x18, 0x1024,
 0x19, 0xe000,
 0x18, 0x1034,
 0x19, 0x7400,
 0x18, 0x1044,
 0x19, 0x0500,
 0x18, 0x1054,
 0x19, 0x7b00,
 0x18, 0x1064,
 0x19, 0xe000,
 0x18, 0x1074,
 0x19, 0x9200,
 0x18, 0x1084,
 0x19, 0x4000,
 0x18, 0x1094,
 0x19, 0x7f00,
 0x18, 0x10a4,
 0x19, 0x0400,
 0x18, 0x10b4,
 0x19, 0x0100,
 0x18, 0x10c4,
 0x19, 0x4400,
 0x18, 0x10d4,
 0x19, 0x0000,
 0x18, 0x10e4,
 0x19, 0x0000,
 0x18, 0x10f4,
 0x19, 0x0000,
 0x18, 0x1104,
 0x19, 0x4000,
 0x18, 0x1114,
 0x19, 0x8000,
 0x18, 0x1124,
 0x19, 0x7f00,
 0x18, 0x1134,
 0x19, 0x4000,
 0x18, 0x1144,
 0x19, 0x3f00,
 0x18, 0x1154,
 0x19, 0x0400,
 0x18, 0x1164,
 0x19, 0x5000,
 0x18, 0x1174,
 0x19, 0xf800,
 0x18, 0x1184,
 0x19, 0x0000,
 0x18, 0x1194,
 0x19, 0xe000,
 0x18, 0x11a4,
 0x19, 0x4000,
 0x18, 0x11b4,
 0x19, 0x8000,
 0x18, 0x11c4,
 0x19, 0x7f00,
 0x18, 0x11d4,
 0x19, 0x8900,
 0x18, 0x11e4,
 0x19, 0x8300,
 0x18, 0x11f4,
 0x19, 0xe000,
 0x18, 0x1204,
 0x19, 0x0000,
 0x18, 0x1214,
 0x19, 0x4000,
 0x18, 0x1224,
 0x19, 0x7f00,
 0x18, 0x1234,
 0x19, 0x0200,
 0x18, 0x1244,
 0x19, 0x1000,
 0x18, 0x1254,
 0x19, 0x0000,
 0x18, 0x1264,
 0x19, 0xfc00,
 0x18, 0x1274,
 0x19, 0xff00,
 0x18, 0x1284,
 0x19, 0x0000,
 0x18, 0x1294,
 0x19, 0x4000,
 0x18, 0x12a4,
 0x19, 0xbc00,
 0x18, 0x12b4,
 0x19, 0x0e00,
 0x18, 0x12c4,
 0x19, 0xfe00,
 0x18, 0x12d4,
 0x19, 0x8a00,
 0x18, 0x12e4,
 0x19, 0x8300,
 0x18, 0x12f4,
 0x19, 0xe000,
 0x18, 0x1304,
 0x19, 0x0000,
 0x18, 0x1314,
 0x19, 0x4000,
 0x18, 0x1324,
 0x19, 0x7f00,
 0x18, 0x1334,
 0x19, 0x0100,
 0x18, 0x1344,
 0x19, 0xff00,
 0x18, 0x1354,
 0x19, 0x0000,
 0x18, 0x1364,
 0x19, 0xfc00,
 0x18, 0x1374,
 0x19, 0xff00,
 0x18, 0x1384,
 0x19, 0x0000,
 0x18, 0x1394,
 0x19, 0x4000,
 0x18, 0x13a4,
 0x19, 0x9d00,
 0x18, 0x13b4,
 0x19, 0xff00,
 0x18, 0x13c4,
 0x19, 0x8900,
 0x18, 0x13d4,
 0x19, 0x8300,
 0x18, 0x13e4,
 0x19, 0xe000,
 0x18, 0x13f4,
 0x19, 0x0000,
 0x18, 0x1404,
 0x19, 0xac00,
 0x18, 0x1414,
 0x19, 0x0800,
 0x18, 0x1424,
 0x19, 0xfa00,
 0x18, 0x1434,
 0x19, 0x4000,
 0x18, 0x1444,
 0x19, 0x3f00,
 0x18, 0x1454,
 0x19, 0x0200,
 0x18, 0x1464,
 0x19, 0x0000,
 0x18, 0x1474,
 0x19, 0xfd00,
 0x18, 0x1484,
 0x19, 0x7f00,
 0x18, 0x1494,
 0x19, 0x0000,
 0x18, 0x14a4,
 0x19, 0x4000,
 0x18, 0x14b4,
 0x19, 0x0500,
 0x18, 0x14c4,
 0x19, 0x9000,
 0x18, 0x14d4,
 0x19, 0x8000,
 0x18, 0x14e4,
 0x19, 0x7d00,
 0x18, 0x14f4,
 0x19, 0x8c00,
 0x18, 0x1504,
 0x19, 0x8300,
 0x18, 0x1514,
 0x19, 0xe000,
 0x18, 0x1524,
 0x19, 0x0000,
 0x18, 0x1534,
 0x19, 0x4000,
 0x18, 0x1544,
 0x19, 0x0400,
 0x18, 0x1554,
 0x19, 0xff00,
 0x18, 0x1564,
 0x19, 0x0500,
 0x18, 0x1574,
 0x19, 0x8500,
 0x18, 0x1584,
 0x19, 0x8c00,
 0x18, 0x1594,
 0x19, 0xfa00,
 0x18, 0x15a4,
 0x19, 0xe000,
 0x18, 0x15b4,
 0x19, 0x0000,
 0x18, 0x15c4,
 0x19, 0x4000,
 0x18, 0x15d4,
 0x19, 0x1f00,
 0x18, 0x15e4,
 0x19, 0x0000,
 0x18, 0x15f4,
 0x19, 0xfc00,
 0x18, 0x1604,
 0x19, 0x7300,
 0x18, 0x1614,
 0x19, 0x0d00,
 0x18, 0x1624,
 0x19, 0x0100,
 0x18, 0x1634,
 0x19, 0x4000,
 0x18, 0x1644,
 0x19, 0x2000,
 0x18, 0x1654,
 0x19, 0x0000,
 0x18, 0x1664,
 0x19, 0x0400,
 0x18, 0x1674,
 0x19, 0xdc00,
 0x18, 0x1684,
 0x19, 0x0800,
 0x18, 0x1694,
 0x19, 0x9100,
 0x18, 0x16a4,
 0x19, 0x0900,
 0x18, 0x16b4,
 0x19, 0x9900,
 0x18, 0x16c4,
 0x19, 0x0700,
 0x18, 0x16d4,
 0x19, 0x7900,
 0x18, 0x16e4,
 0x19, 0x4000,
 0x18, 0x16f4,
 0x19, 0x3f00,
 0x18, 0x1704,
 0x19, 0x0000,
 0x18, 0x1714,
 0x19, 0x0000,
 0x18, 0x1724,
 0x19, 0x0400,
 0x18, 0x1734,
 0x19, 0x7300,
 0x18, 0x1744,
 0x19, 0x0d00,
 0x18, 0x1754,
 0x19, 0x0100,
 0x18, 0x1764,
 0x19, 0x0900,
 0x18, 0x1774,
 0x19, 0x8d00,
 0x18, 0x1784,
 0x19, 0x0800,
 0x18, 0x1794,
 0x19, 0x7d00,
 0x18, 0x17a4,
 0x19, 0x4000,
 0x18, 0x17b4,
 0x19, 0x3f00,
 0x18, 0x17c4,
 0x19, 0x0000,
 0x18, 0x17d4,
 0x19, 0x0000,
 0x18, 0x17e4,
 0x19, 0x0000,
 0x18, 0x17f4,
 0x19, 0x7000,
 0x18, 0x1804,
 0x19, 0x0c00,
 0x18, 0x1814,
 0x19, 0x0100,
 0x18, 0x1824,
 0x19, 0x0900,
 0x18, 0x1834,
 0x19, 0x7f00,
 0x18, 0x1844,
 0x19, 0x4000,
 0x18, 0x1854,
 0x19, 0x3f00,
 0x18, 0x1864,
 0x19, 0x0000,
 0x18, 0x1874,
 0x19, 0x0000,
 0x18, 0x1884,
 0x19, 0x0000,
 0x18, 0x1894,
 0x19, 0x7000,
 0x18, 0x18a4,
 0x19, 0x0d00,
 0x18, 0x18b4,
 0x19, 0x0100,
 0x18, 0x18c4,
 0x19, 0x0b00,
 0x18, 0x18d4,
 0x19, 0x7f00,
 0x18, 0x18e4,
 0x19, 0x4000,
 0x18, 0x18f4,
 0x19, 0x3f00,
 0x18, 0x1904,
 0x19, 0x0000,
 0x18, 0x1914,
 0x19, 0x0000,
 0x18, 0x1924,
 0x19, 0x0000,
 0x18, 0x1934,
 0x19, 0x7200,
 0x18, 0x1944,
 0x19, 0x0d00,
 0x18, 0x1954,
 0x19, 0x0100,
 0x18, 0x1964,
 0x19, 0x0500,
 0x18, 0x1974,
 0x19, 0xc500,
 0x18, 0x1984,
 0x19, 0x0400,
 0x18, 0x1994,
 0x19, 0x7d00,
 0x18, 0x19a4,
 0x19, 0xe100,
 0x18, 0x19b4,
 0x19, 0x4300,
 0x18, 0x19c4,
 0x19, 0x4000,
 0x18, 0x19d4,
 0x19, 0x7f00,
 0x18, 0x19e4,
 0x19, 0x0400,
 0x18, 0x19f4,
 0x19, 0x0100,
 0x18, 0x1a04,
 0x19, 0x4000,
 0x18, 0x1a14,
 0x19, 0x3e00,
 0x18, 0x1a24,
 0x19, 0x0000,
 0x18, 0x1a34,
 0x19, 0xe000,
 0x18, 0x1a44,
 0x19, 0x1200,
 0x18, 0x1a54,
 0x19, 0x8000,
 0x18, 0x1a64,
 0x19, 0x7f00,
 0x18, 0x1a74,
 0x19, 0x8900,
 0x18, 0x1a84,
 0x19, 0x8300,
 0x18, 0x1a94,
 0x19, 0xe000,
 0x18, 0x1aa4,
 0x19, 0x0000,
 0x18, 0x1ab4,
 0x19, 0x4000,
 0x18, 0x1ac4,
 0x19, 0x7f00,
 0x18, 0x1ad4,
 0x19, 0x0000,
 0x18, 0x1ae4,
 0x19, 0x2000,
 0x18, 0x1af4,
 0x19, 0x4000,
 0x18, 0x1b04,
 0x19, 0x3e00,
 0x18, 0x1b14,
 0x19, 0xff00,
 0x18, 0x1b24,
 0x19, 0x0000,
 0x18, 0x1b34,
 0x19, 0x1200,
 0x18, 0x1b44,
 0x19, 0x8000,
 0x18, 0x1b54,
 0x19, 0x7f00,
 0x18, 0x1b64,
 0x19, 0x8600,
 0x18, 0x1b74,
 0x19, 0x8500,
 0x18, 0x1b84,
 0x19, 0x8900,
 0x18, 0x1b94,
 0x19, 0xfd00,
 0x18, 0x1ba4,
 0x19, 0xe000,
 0x18, 0x1bb4,
 0x19, 0x0000,
 0x18, 0x1bc4,
 0x19, 0x9500,
 0x18, 0x1bd4,
 0x19, 0x0400,
 0x18, 0x1be4,
 0x19, 0x4000,
 0x18, 0x1bf4,
 0x19, 0x4000,
 0x18, 0x1c04,
 0x19, 0x1000,
 0x18, 0x1c14,
 0x19, 0x4000,
 0x18, 0x1c24,
 0x19, 0x3f00,
 0x18, 0x1c34,
 0x19, 0x0200,
 0x18, 0x1c44,
 0x19, 0x4000,
 0x18, 0x1c54,
 0x19, 0x3700,
 0x18, 0x1c64,
 0x19, 0x7f00,
 0x18, 0x1c74,
 0x19, 0x0000,
 0x18, 0x1c84,
 0x19, 0x0200,
 0x18, 0x1c94,
 0x19, 0x0200,
 0x18, 0x1ca4,
 0x19, 0x9000,
 0x18, 0x1cb4,
 0x19, 0x8000,
 0x18, 0x1cc4,
 0x19, 0x7d00,
 0x18, 0x1cd4,
 0x19, 0x8900,
 0x18, 0x1ce4,
 0x19, 0x8300,
 0x18, 0x1cf4,
 0x19, 0xe000,
 0x18, 0x1d04,
 0x19, 0x0000,
 0x18, 0x1d14,
 0x19, 0x4000,
 0x18, 0x1d24,
 0x19, 0x0400,
 0x18, 0x1d34,
 0x19, 0xff00,
 0x18, 0x1d44,
 0x19, 0x0200,
 0x18, 0x1d54,
 0x19, 0x8500,
 0x18, 0x1d64,
 0x19, 0x8900,
 0x18, 0x1d74,
 0x19, 0xfa00,
 0x18, 0x1d84,
 0x19, 0xe000,
 0x18, 0x1d94,
 0x19, 0x0000,
 0x18, 0x1da4,
 0x19, 0x4000,
 0x18, 0x1db4,
 0x19, 0x7f00,
 0x18, 0x1dc4,
 0x19, 0x1000,
 0x18, 0x1dd4,
 0x19, 0x0000,
 0x18, 0x1de4,
 0x19, 0x4000,
 0x18, 0x1df4,
 0x19, 0x3700,
 0x18, 0x1e04,
 0x19, 0x7300,
 0x18, 0x1e14,
 0x19, 0x0500,
 0x18, 0x1e24,
 0x19, 0x0200,
 0x18, 0x1e34,
 0x19, 0x0100,
 0x18, 0x1e44,
 0x19, 0xd800,
 0x18, 0x1e54,
 0x19, 0x0400,
 0x18, 0x1e64,
 0x19, 0x7d00,
 0x18, 0x1e74,
 0x19, 0x4000,
 0x18, 0x1e84,
 0x19, 0x7f00,
 0x18, 0x1e94,
 0x19, 0x1000,
 0x18, 0x1ea4,
 0x19, 0x0000,
 0x18, 0x1eb4,
 0x19, 0x4000,
 0x18, 0x1ec4,
 0x19, 0x0000,
 0x18, 0x1ed4,
 0x19, 0x7200,
 0x18, 0x1ee4,
 0x19, 0x0400,
 0x18, 0x1ef4,
 0x19, 0x0000,
 0x18, 0x1f04,
 0x19, 0x0800,
 0x18, 0x1f14,
 0x19, 0x7f00,
 0x18, 0x1f24,
 0x19, 0x4000,
 0x18, 0x1f34,
 0x19, 0x7f00,
 0x18, 0x1f44,
 0x19, 0x3000,
 0x18, 0x1f54,
 0x19, 0x0000,
 0x18, 0x1f64,
 0x19, 0xc000,
 0x18, 0x1f74,
 0x19, 0x0000,
 0x18, 0x1f84,
 0x19, 0x7200,
 0x18, 0x1f94,
 0x19, 0x0500,
 0x18, 0x1fa4,
 0x19, 0x0000,
 0x18, 0x1fb4,
 0x19, 0x0400,
 0x18, 0x1fc4,
 0x19, 0xeb00,
 0x18, 0x1fd4,
 0x19, 0x8400,
 0x18, 0x1fe4,
 0x19, 0x7d00,
 0x18, 0x1ff4,
 0x19, 0x4000,
 0x18, 0x2004,
 0x19, 0x7f00,
 0x18, 0x2014,
 0x19, 0x1000,
 0x18, 0x2024,
 0x19, 0x0000,
 0x18, 0x2034,
 0x19, 0x4000,
 0x18, 0x2044,
 0x19, 0x0000,
 0x18, 0x2054,
 0x19, 0x7200,
 0x18, 0x2064,
 0x19, 0x0700,
 0x18, 0x2074,
 0x19, 0x0000,
 0x18, 0x2084,
 0x19, 0x0400,
 0x18, 0x2094,
 0x19, 0xde00,
 0x18, 0x20a4,
 0x19, 0x9b00,
 0x18, 0x20b4,
 0x19, 0x7d00,
 0x18, 0x20c4,
 0x19, 0x4000,
 0x18, 0x20d4,
 0x19, 0x7f00,
 0x18, 0x20e4,
 0x19, 0x1000,
 0x18, 0x20f4,
 0x19, 0x9000,
 0x18, 0x2104,
 0x19, 0x4000,
 0x18, 0x2114,
 0x19, 0x0400,
 0x18, 0x2124,
 0x19, 0x7300,
 0x18, 0x2134,
 0x19, 0x1500,
 0x18, 0x2144,
 0x19, 0x0000,
 0x18, 0x2154,
 0x19, 0x0400,
 0x18, 0x2164,
 0x19, 0xd100,
 0x18, 0x2174,
 0x19, 0x9400,
 0x18, 0x2184,
 0x19, 0x9200,
 0x18, 0x2194,
 0x19, 0x8000,
 0x18, 0x21a4,
 0x19, 0x7b00,
 0x18, 0x21b4,
 0x19, 0x4000,
 0x18, 0x21c4,
 0x19, 0x7f00,
 0x18, 0x21d4,
 0x19, 0x5000,
 0x18, 0x21e4,
 0x19, 0x0000,
 0x18, 0x21f4,
 0x19, 0x4000,
 0x18, 0x2204,
 0x19, 0x0000,
 0x18, 0x2214,
 0x19, 0x7200,
 0x18, 0x2224,
 0x19, 0x0700,
 0x18, 0x2234,
 0x19, 0x0000,
 0x18, 0x2244,
 0x19, 0x0400,
 0x18, 0x2254,
 0x19, 0xc200,
 0x18, 0x2264,
 0x19, 0x9b00,
 0x18, 0x2274,
 0x19, 0x7d00,
 0x18, 0x2284,
 0x19, 0xe200,
 0x18, 0x2294,
 0x19, 0x0c00,
 0x18, 0x22a4,
 0x19, 0x4000,
 0x18, 0x22b4,
 0x19, 0x7f00,
 0x18, 0x22c4,
 0x19, 0x1000,
 0x18, 0x22d4,
 0x19, 0x0000,
 0x18, 0x22e4,
 0x19, 0x4000,
 0x18, 0x22f4,
 0x19, 0x3700,
 0x18, 0x2304,
 0x19, 0x7300,
 0x18, 0x2314,
 0x19, 0x0500,
 0x18, 0x2324,
 0x19, 0x0000,
 0x18, 0x2334,
 0x19, 0x0100,
 0x18, 0x2344,
 0x19, 0x0300,
 0x18, 0x2354,
 0x19, 0xe100,
 0x18, 0x2364,
 0x19, 0xbc00,
 0x18, 0x2374,
 0x19, 0x0200,
 0x18, 0x2384,
 0x19, 0x7b00,
 0x18, 0x2394,
 0x19, 0xe100,
 0x18, 0x23a4,
 0x19, 0xda00,
 0x18, 0x0000,
 0x1f, 0x0000,
 0x17, 0x2100,

    
    //##### patch INRX sram code #####
    0x1f,0x0007,
    0x1e, 0x0020,   
    0x15, 0x0100,   
    0x17, 0x000A,   
    0x18, 0x0003,   
    0x1b, 0x2F3C,                                  
    //#INRX PRG FSM Enable       
    0x1e, 0x0040,   
    0x18, 0x0001,   
    //#PCS Dedicate FSM Enable   
    0x1e, 0x0023,   
    0x16, 0x000A,   
    //##EEE buffer setting       
    //#mdcmdio_cmd w $port 0x1e 0x0028;   
    //#mdcmdio_cmd w $port 0x15 0x0010;                                 
    //#change EEE giga circuit   
    0x1e, 0x0041, 
    0x1c, 0x0008,   
    
    
    //####### phy patch###############
    0x1e, 0x0021,
    0x19, 0x2828,
    0x1e, 0x0020,
    0x1b, 0x2f38,
    
     
                               
    //######## Restart nway ##########
    0x1f, 0x0000,   
    0x00, 0x9140, //#reset PHY
    0x09, 0x0000,    
    0x00, 0x1340,    

};

	int len=sizeof(p)/sizeof(unsigned int);
	int port;
	int i;

	dprintf("Writing EEE pattern %08d records \n", len/2);
	for(port=0; port<5; port++)
	{
		for(i=0;i<len/2;i++)
		{	rtl8651_setAsicEthernetPHYReg(port, p[i], p[i+1]);
		}
	}

};
#endif
//-----------------------------------------------------------------------------------------------


