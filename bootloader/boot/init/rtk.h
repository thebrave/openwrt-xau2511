#ifndef __RTK_H__
#define __RTK_H__

#ifdef DNI_PATCH
#define MODEL_NAME "XAU2511"
#define CONFIG_MAXIMUM_SIZE 1216 // You need to check the real length of HW config.
#define MIB_WSC_PIN 0x48a
#define WSC_PIN_LEN 8
#define TEMP_MAXIMUM_SIZE 512
#define MIB_EXTRA_SETTINGS 0x7000
#define MIB_EXTRA_SETTINGS_REGION MIB_EXTRA_SETTINGS
#define EXTRA_SETTINGS_REGION_LEN 2
#define MIB_EXTRA_SETTINGS_SN (MIB_EXTRA_SETTINGS+EXTRA_SETTINGS_REGION_LEN)
#define EXTRA_SETTINGS_SN_LEN 13
#define MIB_EXTRA_SETTINGS_HWVER (MIB_EXTRA_SETTINGS_SN+EXTRA_SETTINGS_SN_LEN)
#define EXTRA_SETTINGS_HWVER_LEN 2
#define EXTRA_SETTINGS_LEN (EXTRA_SETTINGS_REGION_LEN+EXTRA_SETTINGS_SN_LEN+EXTRA_SETTINGS_HWVER_LEN)

#define DEFAULT_HW_SETTING_MAC "00:30:AB:00:00:02"
#define DEFAULT_HW_SETTING_PINCODE "12345670"
#define DEFAULT_HW_SETTING_REGION "NA"
#define DEFAULT_HW_SETTING_SN "1234567890123"

#define DEFAULT_RESET_BUTTON_TIME 700

#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT
#define EXTRA_HW_SETTINGS_CHECK 0
#define EXTRA_HW_SETTINGS_SET_DEFAULT 1

extern void extra_hw_settings_cmd(int cmd);

extern void tftp_filename_cmds_set(char filename[]);
extern void tftp_filename_ini_cmds_set(char filename[], unsigned long image_address, unsigned long file_length_to_client);
#endif
#endif

// david ----------------------------------------------------------------
#if defined(KLD)
	#define FW_SIGNATURE			((char *)"klln")	// fw signature
	#define FW_SIGNATURE_WITH_ROOT_615	((char *)"k6ln")	// fw signature with root fs
	#define ROOT_SIGNATURE_615          ((char *)"k6rt")
	#define WEB_ROOT_SIGNATURE_615          ((char *)"w6rt")
#elif defined(EC)
	#define FW_SIGNATURE			((char *)"fe6c")	// fw signature
	#define FW_SIGNATURE_WITH_ROOT			((char *)"FE6C")	// fw signature
	#define ROOT_SIGNATURE          ((char *)"re6c")	

#elif defined(RTL8196B) && !defined(RTL8197B) && !defined(RTL8196C)
	#define FW_SIGNATURE			((char *)"cs6b")	// fw signature
	#define FW_SIGNATURE_WITH_ROOT	((char *)"cr6b")	// fw signature with root fs
	#define ROOT_SIGNATURE          ((char *)"r6br")
#elif defined(RTL8196B) && !defined(RTL8197B) && defined(RTL8196C)
	#define FW_SIGNATURE			((char *)"cs6c")	// fw signature
	#define FW_SIGNATURE_WITH_ROOT	((char *)"cr6c")	// fw signature with root fs
	#define ROOT_SIGNATURE          ((char *)"r6cr")
#elif defined(RTL8198)
	#define FW_SIGNATURE			((char *)"cs6b")	// fw signature
	#define FW_SIGNATURE_WITH_ROOT	((char *)"cr6b")	// fw signature with root fs
	#define ROOT_SIGNATURE          ((char *)"r6br")

#else
	#define FW_SIGNATURE			((char *)"csys")	// fw signature
	#define FW_SIGNATURE_WITH_ROOT	((char *)"csro")	// fw signature with root fs
	#define ROOT_SIGNATURE          ((char *)"root")
#endif

#define SQSH_SIGNATURE		((char *)"sqsh")
#define SQSH_SIGNATURE_LE       ((char *)"hsqs")

#if defined(RTL8196B) && !defined(RTL8197B) && !defined(RTL8196C)
#define WEB_SIGNATURE		((char *)"w6bp")
#elif defined(RTL8196B) && !defined(RTL8197B) && defined(RTL8196C)
#define WEB_SIGNATURE		((char *)"w6cp")
#elif defined(RTL8198)
#define WEB_SIGNATURE		((char *)"w6bp")
#else
#define WEB_SIGNATURE		((char *)"webp")
#endif

#define BOOT_SIGNATURE		((char *)"boot")
#define ALL1_SIGNATURE		((char *)"ALL1")
#define ALL2_SIGNATURE		((char *)"ALL2")

#define HW_SETTING_OFFSET		0x6000

// Cyrus ----------------------------------------------------------------
#define HW_SIGNATURE		((char *)"HS")	// hw signature
#define SW_SIGNATURE_D		((char *)"DS")	// sw_default signature
#define SW_SIGNATURE_C		((char *)"CS")	// sw_current signature
#define SIG_LEN			4

/* Firmware image header */
typedef struct _header_ {
	unsigned char signature[SIG_LEN];
	unsigned long startAddr;
	unsigned long burnAddr;
	unsigned long len;
} IMG_HEADER_T, *IMG_HEADER_Tp;

typedef struct _signature__ {
	unsigned char *signature;
	unsigned char *comment ;
	int sig_len;
	int skip ;
	int maxSize;
	int reboot;
} SIGN_T ;

#endif

