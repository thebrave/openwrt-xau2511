#include <asm/system.h>
#include "etherboot.h"
#include "nic.h"
#include "rtk.h"

#if defined(RTL8196B)	
#include <asm/rtl8196.h>
#endif
#if defined(RTL8198)	
#include <asm/rtl8198.h>
#endif
#ifdef DNI_PATCH
#include "nmrp.h" /* add by alfa */
#include "rtl_depend.h"
#endif
struct arptable_t  arptable_tftp[3];
   
/*Cyrus Tsai*/
#ifndef JUMP_ADDR 
	#define FILESTART 0x80300000
#else
	#define FILESTART JUMP_ADDR
#endif

#ifdef DNI_PATCH
#ifdef FILESTART
#undef FILESTART
#endif
#define FILESTART 0x80500000 /* modify by alfa */
#endif

/*Cyrus Tsai for vm_test.*/
#define TEST_FILENAME	((char *)"nfjrom")	
//#define TESTSTART FILESTART
#define BOOT_FILENAME	((char *)"boot.img")
#define BOOTSTART 0x80000000

#define prom_printf dprintf

#ifndef DNI_BOOTCODE_DOWNSIZE
extern unsigned long ETH0_ADD;
#endif

int jump_to_test=0;
void (*jumpF)(void);

#define CODESTART 0x10000

#ifdef RTL8197B
extern volatile int get_timer_jiffies(void);
#define REG32(reg)   (*(volatile unsigned int   *)((unsigned int)reg))
int tftpd_is_ready = 0;
int rx_kickofftime=0;
#endif
Int8 one_tftp_lock=0;	//Cyrus for version 2

struct nic nic;
/*Data structure shared by ethernet driver and tftpd */
Int8 eth_packet[ETH_FRAME_LEN + 4];
#ifndef DNI_BOOTCODE_DOWNSIZE
Int8 ETH_BROADCAST[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
#endif
#define IPTOUL(a,b,c,d)	((a << 24)| (b << 16) | (c << 8) | d )

/*Address to store image file*/
Int32 image_address=FILESTART;
Int32 address_to_store;

/*Cyrus Tsai*/
Int32 file_length_to_server;
Int32 file_length_to_client;
/*this is the file length, should extern to flash driver*/
/*Cyrus Tsai*/

#ifdef DNI_NMRP_V2 //for new NMRP
extern int NmrpFwUPOption;
extern int NmrpSTUPOption;
extern unsigned NmrpPeerReqStMask;
extern int UploadingStringTableIdx;
extern int TFTPFromNMRP;
extern int NmrpDoFlashWriteFw;
#endif

/*store the block number*/
volatile Int16 block_expected;
int it_is_EOF=0;
#if defined(HTTP_SERVER)
char message[128];
#endif
char filename[TFTP_DEFAULTSIZE_PACKET];

typedef void (* Func_t) (void);
/*there are 3 states in the boot downloader*/
/*Different frames received, these event can
  trigger the state-event machine move.    */
#ifndef DNI_PATCH
typedef enum BootStateTag
{
  INVALID_BOOT_STATE       = -1,
  BOOT_STATE0_INIT_ARP         = 0,
  BOOT_STATE1_TFTP_CLIENT_RRQ  = 1,
  BOOT_STATE2_TFTP_CLIENT_WRQ  = 2,
  NUM_OF_BOOT_STATES       = 3
}
BootState_t;

/*if we are server, we should never receive an OACK    */
/*if we are client, receive an OACK and goto oackTFTP()*/
typedef enum BootEventTag  
{                          
  INVALID_BOOT_EVENT = -1,
  BOOT_EVENT0_ARP_REQ     = 0,
  BOOT_EVENT1_ARP_REPLY   = 1,
  BOOT_EVENT2_TFTP_RRQ    = 2,
  BOOT_EVENT3_TFTP_WRQ    = 3,
  BOOT_EVENT4_TFTP_DATA   = 4,
  BOOT_EVENT5_TFTP_ACK    = 5,
  BOOT_EVENT6_TFTP_ERROR  = 6,
  BOOT_EVENT7_TFTP_OACK   = 7,
  NUM_OF_BOOT_EVENTS  = 8
}
BootEvent_t;

BootState_t  bootState;
BootEvent_t  bootEvent;

/*State-Event functions*/

static void errorDrop(void);
#endif
static void errorTFTP(void);
#ifdef DNI_PATCH
static void doARPReply(struct nic rx);
static void updateARPTable(struct nic rx);
#ifndef DNI_BOOTCODE_DOWNSIZE
static void setTFTP_RRQ(struct nic rx);
#endif
static void setTFTP_WRQ(struct nic rx);
static void prepareACK(struct nic rx);
#ifndef DNI_BOOTCODE_DOWNSIZE
static void prepareDATA(struct nic rx);
#endif
#else
static void doARPReply(void);
static void updateARPTable(void);
static void setTFTP_RRQ(void);
static void setTFTP_WRQ(void);
static void prepareACK(void);
static void prepareDATA(void);
#endif

#ifdef DNI_PATCH
extern bool nmrp_tftp_wait;    /* add by alfa */
extern bool nmrp_tftp_success; /* add by alfa */
extern bool nmrp_tftp_runing;  /* add by alfa */
bool tftp_data;    /* add by alfa */
int tftp_data_time; /* add by alfa */
extern unsigned long image_dst , image_src ,image_len;   /* add by alfa */
extern ip_route_t nmrp_server_info;
extern void nmrp_packet(struct nic rx);/* add by alfa */
//dvd.chen, for new NMRP
#ifdef DNI_NMRP_V2
extern bool NMRPSendKeepAliveREQ(void);
#endif
#endif
Int16 CLIENT_port;
Int16 SERVER_port;

/*tftpd server entry point*/
void tftpd_entry(void);

void tftpd_send_ack(Int16 number);
int check_image(Int32* data_to_transmit);
Int16 ipheader_chksum(Int16*ip,int len);
void tftpd_send_data(char* filename,Int16 block_number);
#ifdef DNI_PATCH
void kick_tftpd(struct nic rx);
#else
void kick_tftpd(void);
#endif
/*in misc.c*/
extern void twiddle(void);

extern void prepare_txpkt(int etherport,Int16 type,Int8* destaddr,Int8* data ,Int16 len); 

#ifndef DNI_PATCH
static const Func_t BootStateEvent[NUM_OF_BOOT_STATES][NUM_OF_BOOT_EVENTS]
= {
    /*BOOT_STATE0_INIT_ARP*/
    { 
  /*BOOT_EVENT0_ARP_REQ*/     doARPReply,
  /*BOOT_EVENT1_ARP_REPLY*/   updateARPTable,
  /*BOOT_EVENT2_TFTP_RRQ*/    setTFTP_RRQ,
  /*BOOT_EVENT3_TFTP_WRQ*/    setTFTP_WRQ,
  /*BOOT_EVENT4_TFTP_DATA*/   errorDrop,/*ERROR in state transition*/
  /*BOOT_EVENT5_TFTP_ACK*/    errorDrop,/*ERROR in state transition*/
  /*BOOT_EVENT6_TFTP_ERROR*/  errorDrop,/*ERROR in state transition*/
  /*BOOT_EVENT7_TFTP_OACK*/   errorDrop,/*ERROR in state transition*/
    	},                                               
    /*BOOT_STATE1_TFTP_CLIENT_RRQ*/
    { 
  /*BOOT_EVENT0_ARP_REQ*/     doARPReply,
  /*BOOT_EVENT1_ARP_REPLY*/   updateARPTable,
  /*BOOT_EVENT2_TFTP_RRQ*/    setTFTP_RRQ,
  /*BOOT_EVENT3_TFTP_WRQ*/    errorTFTP,/*ERROR in TFTP protocol*/
  /*BOOT_EVENT4_TFTP_DATA*/   prepareACK,
  /*BOOT_EVENT5_TFTP_ACK*/    prepareDATA,
  /*BOOT_EVENT6_TFTP_ERROR*/  errorTFTP,/*ERROR in TFTP protocol*/
  /*BOOT_EVENT7_TFTP_OACK*/   errorTFTP,/*ERROR in TFTP protocol*/
    	},                                               
    /*BOOT_STATE2_TFTP_CLIENT_WRQ*/
    { 
  /*BOOT_EVENT0_ARP_REQ*/     doARPReply,
  /*BOOT_EVENT1_ARP_REPLY*/   updateARPTable,
  /*BOOT_EVENT2_TFTP_RRQ*/    errorTFTP,/*ERROR in TFTP protocol*/
  /*BOOT_EVENT3_TFTP_WRQ*/    setTFTP_WRQ,
  /*BOOT_EVENT4_TFTP_DATA*/   prepareACK,
  /*BOOT_EVENT5_TFTP_ACK*/    prepareDATA,
  /*BOOT_EVENT6_TFTP_ERROR*/  errorTFTP,/*ERROR in TFTP protocol*/
  /*BOOT_EVENT7_TFTP_OACK*/   errorTFTP,/*ERROR in TFTP protocol*/
    	}                                               
};
#endif
//----------------------------------------------------------------------------------------
#ifndef DNI_BOOTCODE_DOWNSIZE
static void errorDrop(void)
{
#ifdef RTL8197B
    if (!tftpd_is_ready)
        return;
#endif
/*no need to change boot state*/
#ifdef DNI_PATCH
prom_printf("Boot state error \n");
#else
prom_printf("Boot state error,%d,%d\n",bootState,bootEvent);
#endif
//bootState=BOOT_STATE0_INIT_ARP;
/*error in boot state machine*/	
}
#endif
//----------------------------------------------------------------------------------------
static void errorTFTP(void)
{
#ifdef RTL8197B
    if (!tftpd_is_ready)
        return;
#endif
/*no need to change boot state*/
#ifdef DNI_PATCH
prom_printf("TFTP procotol error\n");
#else
//prom_printf("TFTP procotol error,%d,%d\n",bootState,bootEvent);
bootState=BOOT_STATE0_INIT_ARP;
#endif
}
//----------------------------------------------------------------------------------------
#ifdef DNI_PATCH
static void doARPReply(struct nic rx)
#else
static void doARPReply(void)
#endif
{
 /*we receive an ARP request.*/
 /*In our configuration, all we need is one TFTP_CLIENT, */
 struct	arprequest *arppacket;
 struct arprequest arpreply;
 int i;
#ifndef DNI_BOOTCODE_DOWNSIZE
 Int8 checkIP[4];
#endif
 Int32 targetIP; 

//dprintf("execute ARP reply function\n");
#ifdef DNI_PATCH
 arppacket=(struct arprequest *) &(rx.packet[ETH_HLEN]);
#else
 arppacket=(struct arprequest *) &(nic.packet[ETH_HLEN]);
#endif
//memcpy(arptable_tftp[TFTP_CLIENT].ipaddr.ip, arppacket->sipaddr, sizeof(in_addr)); 
 //prom_printf("DoARPRpy 2.:update CLIENT ip address %x\n",arptable_tftp[TFTP_CLIENT].ipaddr.s_addr);
 
 memcpy(&targetIP,arppacket->tipaddr,4);
#if defined(HTTP_SERVER)	
/*we have two pairs of mac and ip. let http ack arp first if http's and tftp's ip are same*/
 if(targetIP ==arptable_tftp[HTTPD_ARPENTRY].ipaddr.s_addr )
 {
 	    /*Fill in the arp reply payload.*/
    arpreply.hwtype = htons(1);
    arpreply.protocol = htons(FRAME_IP);/*that is 0x0800*/
    arpreply.hwlen = ETH_ALEN;
    arpreply.protolen = 4;
    arpreply.opcode = htons(ARP_REPLY);
    memcpy(&(arpreply.shwaddr), &(arptable_tftp[HTTPD_ARPENTRY].node), ETH_ALEN);
    memcpy(&(arpreply.sipaddr), &(arptable_tftp[HTTPD_ARPENTRY].ipaddr), sizeof(in_addr));
    memcpy(&(arpreply.thwaddr), arppacket->shwaddr, ETH_ALEN);
    memcpy(&(arpreply.tipaddr), arppacket->sipaddr, sizeof(in_addr));
    prepare_txpkt(0,FRAME_ARP,arppacket->shwaddr,(Int8*)&arpreply,(Int16)sizeof(arpreply));	
 }
 else
 //now lock tftp.. 
 //now we have to check mac address, for safety..
#endif
 if(targetIP==arptable_tftp[TFTP_SERVER].ipaddr.s_addr)/*that is us*/
 {
#ifdef RTL8197B
    /*Fill in the arp reply payload.*/
    arpreply.hwtype = htons(1);
    arpreply.protocol = htons(FRAME_IP);/*that is 0x0800*/
    arpreply.hwlen = ETH_ALEN;
    arpreply.protolen = 4;
    arpreply.opcode = htons(ARP_REPLY);

    memcpy(&(arpreply.shwaddr), &(arptable_tftp[TFTP_SERVER].node), ETH_ALEN);
    memcpy(&(arpreply.sipaddr), &(arptable_tftp[TFTP_SERVER].ipaddr), sizeof(in_addr));
    memcpy(&(arpreply.thwaddr), arppacket->shwaddr, ETH_ALEN);
    memcpy(&(arpreply.tipaddr), arppacket->sipaddr, sizeof(in_addr));

    prepare_txpkt(0,FRAME_ARP,arppacket->shwaddr,(Int8*)&arpreply,(Int16)sizeof(arpreply));
#else
    if(one_tftp_lock==1)
	 {
      if(memcmp(arppacket->shwaddr,arptable_tftp[TFTP_CLIENT].node,6))
	  return ;
	 }

	
    /*Update TFTP_CLIENT enty in ARP table*/
    memcpy(arptable_tftp[TFTP_CLIENT].node, arppacket->shwaddr, 6);
    arptable_tftp[TFTP_CLIENT].ipaddr.ip[0]=arppacket->sipaddr[0];
    arptable_tftp[TFTP_CLIENT].ipaddr.ip[1]=arppacket->sipaddr[1];
    arptable_tftp[TFTP_CLIENT].ipaddr.ip[2]=arppacket->sipaddr[2];
    arptable_tftp[TFTP_CLIENT].ipaddr.ip[3]=arppacket->sipaddr[3];
	
    /*Fill in the arp reply payload.*/
    arpreply.hwtype = htons(1);
    arpreply.protocol = htons(FRAME_IP);/*that is 0x0800*/
    arpreply.hwlen = ETH_ALEN;
    arpreply.protolen = 4;
    arpreply.opcode = htons(ARP_REPLY);

    memcpy(&(arpreply.shwaddr), &(arptable_tftp[TFTP_SERVER].node), ETH_ALEN);
    memcpy(&(arpreply.sipaddr), &(arptable_tftp[TFTP_SERVER].ipaddr), sizeof(in_addr));
    memcpy(&(arpreply.thwaddr), &(arptable_tftp[TFTP_CLIENT].node), ETH_ALEN);
    memcpy(&(arpreply.tipaddr), &(arptable_tftp[TFTP_CLIENT].ipaddr), sizeof(in_addr));

    prepare_txpkt(0,FRAME_ARP,arptable_tftp[TFTP_CLIENT].node,(Int8*)&arpreply,(Int16)sizeof(arpreply));
#endif
   } 
}
#ifdef DNI_PATCH
static void doARPReq(struct nic rx)
{
 /*we receive an ARP request.*/
 /*In our configuration, all we need is one TFTP_CLIENT, */
 struct	arprequest *arppacket;
 struct arprequest arpreply;
 int i;
#ifndef DNI_BOOTCODE_DOWNSIZE
 Int8 checkIP[4];
#endif
 Int32 targetIP; 
 char broadcast[6]={0xff,0xff,0xff,0xff,0xff,0xff};


 arppacket=(struct arprequest *) &(rx.packet[ETH_HLEN]);


    /*Fill in the arp request payload.*/
    arpreply.hwtype = htons(1);
    arpreply.protocol = htons(FRAME_IP);/*that is 0x0800*/
    arpreply.hwlen = ETH_ALEN;
    arpreply.protolen = 4;
    arpreply.opcode = htons(ARP_REQUEST);

    memcpy(&(arpreply.shwaddr), &(arptable_tftp[TFTP_SERVER].node), ETH_ALEN);
    memcpy(&(arpreply.sipaddr), &(arptable_tftp[TFTP_SERVER].ipaddr), sizeof(in_addr));
    memcpy(&(arpreply.thwaddr), broadcast, ETH_ALEN);
    memcpy(&(arpreply.tipaddr), &(arptable_tftp[TFTP_CLIENT].ipaddr), sizeof(in_addr));

    prepare_txpkt(0,FRAME_ARP,broadcast,(Int8*)&arpreply,(Int16)sizeof(arpreply));

}
#endif
//----------------------------------------------------------------------------------------
#ifdef DNI_PATCH
static void updateARPTable(struct nic rx)
#else
static void updateARPTable(void)
#endif
{
#ifndef RTL8197B
 /*??? do we really need this in TFTP server operation*/
 struct	arprequest*arppacket;
 arppacket=(struct arprequest *)&(rx.packet[ETH_HLEN]);
 /*update anyway.*/
 memcpy(arptable_tftp[TFTP_CLIENT].node, arppacket->shwaddr, ETH_ALEN);
 memcpy(arptable_tftp[TFTP_CLIENT].ipaddr.ip, arppacket->sipaddr, sizeof(in_addr)); 
#endif
}
//----------------------------------------------------------------------------------------
#ifndef DNI_BOOTCODE_DOWNSIZE
#ifdef DNI_PATCH
static void setTFTP_RRQ(struct nic rx)
#else
static void setTFTP_RRQ(void)
#endif
{
#if  1 //sc_yang
 struct udphdr *udpheader;
 struct tftp_t *tftppacket;
 
 Int16 tftpopcode;
 int find_zero;

#ifdef RTL8197B
    if (!tftpd_is_ready)
        return;
#endif
#ifdef DNI_PATCH
 udpheader = (struct udphdr *)&rx.packet[ETH_HLEN+ sizeof(struct iphdr)];
#else
 udpheader = (struct udphdr *)&nic.packet[ETH_HLEN+ sizeof(struct iphdr)];
#endif
 if( udpheader->dest==htons(TFTP_PORT) )
   {
   	
    prom_printf("\nFile Start: %x,length=%x\n",image_address,file_length_to_client);
    /*memorize CLIENT IP address*/
#ifdef DNI_PATCH
    memcpy(&(arptable_tftp[TFTP_CLIENT].ipaddr.s_addr),(Int8*)&rx.packet[ETH_HLEN+12],4);
#else	
    memcpy(&(arptable_tftp[TFTP_CLIENT].ipaddr.s_addr),(Int8*)&nic.packet[ETH_HLEN+12],4);
#endif
    /*memorize CLIENT mac address*/
#ifdef DNI_PATCH
    memcpy(arptable_tftp[TFTP_CLIENT].node,(Int8*)&(rx.packet[ETH_ALEN]),ETH_ALEN);
#else
    memcpy(arptable_tftp[TFTP_CLIENT].node,(Int8*)&(nic.packet[ETH_ALEN]),ETH_ALEN);
#endif

    /*memorize CLIENT port*/
    CLIENT_port=  ntohs(udpheader->src); 
#ifdef DNI_PATCH
    tftppacket = (struct tftp_t *)&rx.packet[ETH_HLEN];
#else	
    tftppacket = (struct tftp_t *)&nic.packet[ETH_HLEN];
#endif
    tftpopcode = tftppacket->opcode;      
    
    
    for(find_zero=0;find_zero<TFTP_DEFAULTSIZE_PACKET;find_zero++)
        if( *( (Int8*)(tftppacket->u.rrq)+find_zero ) ==0 )
           break;
    
    memcpy(filename,tftppacket->u.rrq,find_zero);
    filename[find_zero]='\0';
    memcpy(arptable_tftp[TFTP_CLIENT].node,(Int8*)&(rx.packet[ETH_ALEN]), ETH_ALEN); 
   
    prom_printf("\n**TFTP GET File %s,Size %X Byte\n",filename,file_length_to_client);                
    /*Initialziation of RRQ file*/   
    //image_address=FILESTART; //sc_yang
    /*now we can use fiile_length_to_client, if we have meet WRQ*/
    one_tftp_lock=1;
    /*we should send a data block numbered 1, waiting for number 1 ACK.*/
    tftpd_send_data(filename,0x0001);
    block_expected=1;
//prom_printf("line=%d:		block_expected=%d\n", __LINE__,  block_expected); // for debug
		
    /*now change state to RRQ*/
#ifndef DNI_PATCH   
    bootState=BOOT_STATE1_TFTP_CLIENT_RRQ; 
#endif    
   }
#else
    prom_printf("\ntftp read request is not supported\n");              
#endif
}
#endif  //#ifndef DNI_BOOTCODE_DOWNSIZE
/*DEBUG*/
//int upload_start=0;
/*DEBUG*/
//----------------------------------------------------------------------------------------
#ifdef DNI_PATCH
char tftp_filename[65];
static void setTFTP_WRQ(struct nic rx)
#else
static void setTFTP_WRQ(void)
#endif
{
 struct udphdr *udpheader;
 
 struct tftp_t *tftppacket;
 Int16 tftpopcode;

#ifdef RTL8197B
    if (!tftpd_is_ready)
        return;
#endif

//dprintf("Receive TFTP WRQ\n");
#ifdef DNI_PATCH
 udpheader = (struct udphdr *)&rx.packet[ETH_HLEN+ sizeof(struct iphdr)];
#else
 udpheader = (struct udphdr *)&nic.packet[ETH_HLEN+ sizeof(struct iphdr)];
#endif
 if( udpheader->dest==htons(TFTP_PORT) )
   {
     /*memorize CLIENT port*/
    CLIENT_port=  ntohs(udpheader->src); 
#ifdef DNI_PATCH
    tftppacket = (struct tftp_t *)&rx.packet[ETH_HLEN];
#else	
    tftppacket = (struct tftp_t *)&nic.packet[ETH_HLEN];
#endif
    /*memorize CLIENT mac address*/
#ifdef DNI_PATCH
    memcpy(arptable_tftp[TFTP_CLIENT].node,(Int8*)&(rx.packet[ETH_ALEN]),ETH_ALEN);
#else	
    memcpy(arptable_tftp[TFTP_CLIENT].node,(Int8*)&(nic.packet[ETH_ALEN]),ETH_ALEN);
#endif
    /*memorize CLIENT IP address*/
#ifdef DNI_PATCH
    sprintf(tftp_filename,"%s", tftppacket->u.wrq);
    memcpy(&(arptable_tftp[TFTP_CLIENT].ipaddr.s_addr),(Int8*)&rx.packet[ETH_HLEN+12],4);
#else
    memcpy(&(arptable_tftp[TFTP_CLIENT].ipaddr.s_addr),(Int8*)&nic.packet[ETH_HLEN+12],4);
#endif
    /*here we can parse the file name if required.*/
    prom_printf("\n**TFTP Client Upload, File Name: %s\n",tftppacket->u.wrq);   
    /*Cyrus Tsai*/
    /*initializaiton of writing file.*/
    //image_address=FILESTART; //sc_yang

//#if 0
//    if(!strcmp(tftppacket->u.wrq,TEST_FILENAME))
    if(strstr(tftppacket->u.wrq,TEST_FILENAME))
    {
       jump_to_test=1;
       //image_address=TESTSTART;
    }
    else if(!strcmp(tftppacket->u.wrq,BOOT_FILENAME))
    {
       jump_to_test=1;
       image_address=BOOTSTART;
    }
//#endif 
#ifdef DNI_PATCH
    doARPReq(rx); /* add by alfa */
#endif
    address_to_store=image_address;
    file_length_to_server=0;  
    /*now send one ACK out, to identify this.*/
    tftpd_send_ack(0x0000);/*Block number 0*/
    block_expected=1;/*later client will send an Data number 1*/
    //prom_printf("line=%d:		block_expected=%d\n", __LINE__,  block_expected); // for debug
		
	//now lock the tftp..till upload finished
	one_tftp_lock=1;
    /*Change state to WRQ state.*/
#ifndef DNI_PATCH   
    bootState=BOOT_STATE2_TFTP_CLIENT_WRQ;
#endif    
   }
}
//----------------------------------------------------------------------------------------

SIGN_T sign_tbl[]={ //	sigature, name, skip_header, max_len, reboot
	{FW_SIGNATURE, "Linux kernel", SIG_LEN, 0, 0x1E0000 ,1},
#if defined(KLD)
	{FW_SIGNATURE_WITH_ROOT_615, "Linux kernel (root-fs)", SIG_LEN, 0, 0x1E0000 ,1},	
#else
	{FW_SIGNATURE_WITH_ROOT, "Linux kernel (root-fs)", SIG_LEN, 0, 0x1E0000 ,1},		
#endif	
	{WEB_SIGNATURE, "Webpages", 3, 0, 0x20000, 0},
#if defined(CONFIG_SFLASH_4M)
	{ROOT_SIGNATURE, "Root filesystem", SIG_LEN, 1, 0x200000, 1},
#elif defined(CONFIG_SFLASH_8M)
	{ROOT_SIGNATURE, "Root filesystem", SIG_LEN, 1, 0x700000, 1},
#else
#if defined(KLD)	
	{ROOT_SIGNATURE_615, "Root filesystem", SIG_LEN, 1, 0x100000, 0},
	{WEB_ROOT_SIGNATURE_615, "WEB filesystem", SIG_LEN, 1, 0x100000, 0},
#else
	{ROOT_SIGNATURE, "Root filesystem", SIG_LEN, 1, 0x100000, 0},
#endif
#endif
	{BOOT_SIGNATURE, "Boot code", SIG_LEN, 1, 0x6000, 1},
#if defined(CONFIG_SFLASH_4M)
	{ALL1_SIGNATURE, "Total Image", SIG_LEN, 1, 0x400000, 1},
	{ALL2_SIGNATURE, "Total Image (no check)", SIG_LEN, 1, 0x400000, 1}
#elif defined(CONFIG_SFLASH_8M)
	{ALL1_SIGNATURE, "Total Image", SIG_LEN, 1, 0x800000, 1},
	{ALL2_SIGNATURE, "Total Image (no check)", SIG_LEN, 1, 0x800000, 1}	
#else	
	{ALL1_SIGNATURE, "Total Image", SIG_LEN, 1, 0x200000, 1},
	{ALL2_SIGNATURE, "Total Image (no check)", SIG_LEN, 1, 0x200000, 1}	
#endif
};

#define MAX_SIG_TBL (sizeof(sign_tbl)/sizeof(SIGN_T))
int autoBurn=1;
#if defined(KLD)	
#define HW_SETTING_11N_RESERVED7_OFFSET 0xB2
#define HW_SETTING_11N_RESERVED8_OFFSET 0xB3
#define HW_SETTING_11N_RESERVED9_OFFSET 0xB4
#define HW_SETTING_11N_RESERVED10_OFFSET 0xB5


#endif
//----------------------------------------------------------------------------------------
#if defined(HTTP_SERVER)
int imageFileValid(unsigned long startAddr, int len)
{
	int i=0;
	unsigned long head_offset=0;
	unsigned short sum=0;
	unsigned char sum1=0;
	IMG_HEADER_T Header ; //avoid unalign problem
       int	skip_check_signature=0;
	   
	#if defined(KLD)	
	char image_sig_check[1]={0};
	char image_sig[4]={0};
	char image_sig_root[4]={0};
	unsigned int offset=0;
	#endif	   
	
	int found=0;
	   
	while((head_offset + sizeof(IMG_HEADER_T)) <  len){
		/*as soon as we found a correct header. we thinks the file is valid*/
		memcpy(&Header, ((char *)startAddr + head_offset), sizeof(IMG_HEADER_T));
		#if defined(KLD)
		image_sig_check[0]=0x00;
		if(!memcmp(Header.signature, FW_SIGNATURE_WITH_ROOT_615, SIG_LEN-1))
			offset = HW_SETTING_OFFSET+HW_SETTING_11N_RESERVED7_OFFSET;
		else if(!memcmp(Header.signature, ROOT_SIGNATURE_615, SIG_LEN-1))
			offset = HW_SETTING_OFFSET+HW_SETTING_11N_RESERVED8_OFFSET;
		else if(!memcmp(Header.signature, WEB_ROOT_SIGNATURE_615, SIG_LEN-1))
			offset = HW_SETTING_OFFSET+HW_SETTING_11N_RESERVED9_OFFSET;
		
		if(offset !=0){
			check_image_signature_tag(offset, (&image_sig_check[0]));
		}
		
		#endif
		if(!skip_check_signature)
		{
			for(i=0 ;i < MAX_SIG_TBL ; i++) {
			#if defined(KLD)	
				memcpy(image_sig, sign_tbl[i].signature,sign_tbl[i].sig_len); 
				if(image_sig_check[0] !=0x00){
						image_sig[3]=image_sig_check[0];
				}
				if(!memcmp(Header.signature, (char *)image_sig, sign_tbl[i].sig_len)){
					found++;
					break;
				}
			
			#else
				if(!memcmp(Header.signature, (char *)sign_tbl[i].signature, sign_tbl[i].sig_len)){
					found++;
					break;			
				}
			#endif
				
			}
			if(i == MAX_SIG_TBL){
				head_offset += Header.len + sizeof(IMG_HEADER_T);
				continue ;
			}	
		}
		else
		{
			if(!memcmp(Header.signature, BOOT_SIGNATURE, SIG_LEN))
			{
				found++;
			}
			else 
			{
				unsigned char *pRoot =((unsigned char *)startAddr) + head_offset + sizeof(IMG_HEADER_T);
				if (!memcmp(pRoot, SQSH_SIGNATURE, SIG_LEN))
				{
					found++;
				}
			}				
		}

		if(skip_check_signature || 
			memcmp(Header.signature, WEB_SIGNATURE, 3)){
			//calculate checksum
			if(!memcmp(Header.signature, ALL1_SIGNATURE, SIG_LEN) ||
					!memcmp(Header.signature, ALL2_SIGNATURE, SIG_LEN)) {
				for (i=0; i< Header.len+sizeof(IMG_HEADER_T); i+=2) {
					sum += *((unsigned short *)(startAddr+ head_offset + i));
				}				
			}	
			else {			
			unsigned char x=0,y=0;
			unsigned short temp=0;	
			for (i=0; i< Header.len; i+=2) 
			{
#if defined(RTL8196B) || defined(RTL8198)				
#if 1				
				//sum +=*((unsigned short *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));
				memcpy(&temp, (startAddr+ head_offset + sizeof(IMG_HEADER_T) + i), 2); // for alignment issue
				sum+=temp;
#else
				x=*((unsigned char *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));						
				y=*((unsigned char *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i+1));
				sum+=(y|x<<8)&0xFFFF;
#endif
#else
				sum += *((unsigned short *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));
#endif				
				}
			}
			if ( sum ) {
				for(i=0 ;i < MAX_SIG_TBL ; i++) {
					#if defined(KLD)	
						memcpy(image_sig, sign_tbl[i].signature,sign_tbl[i].sig_len); 
						if(image_sig_check[0] !=0x00){
								image_sig[3]=image_sig_check[0];
						}
						if(!memcmp(Header.signature, (char *)image_sig, sign_tbl[i].sig_len)){
								break;
						}
					#else
					if(!memcmp(Header.signature, (char *)sign_tbl[i].signature, sign_tbl[i].sig_len)){
						break;
					}			
					#endif
				}
				SprintF(message,"%s imgage checksum error at %X!\n",sign_tbl[i].comment, startAddr+head_offset);
				return -1;
			}

		if(!memcmp(Header.signature, ALL1_SIGNATURE, SIG_LEN)){
					found++;
				head_offset += sizeof(IMG_HEADER_T);
				continue;		
		}	
		if(!memcmp(Header.signature, ALL2_SIGNATURE, SIG_LEN)){
				found ++;
			skip_check_signature = 1;
			head_offset += sizeof(IMG_HEADER_T);			
			continue;		
		}						
	     }else
	     {  
	     		//web page use different checksum algorimth
			for (i=0; i< Header.len; i++)
			       sum1 += *((unsigned char *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));
			if ( sum1 ) {
				for(i=0 ;i < MAX_SIG_TBL ; i++) {
					if(!memcmp(Header.signature, (char *)sign_tbl[i].signature, sign_tbl[i].sig_len))
					{
						break;
					}	
				}
				SprintF(message,"%s imgage checksum error at %X!\n",sign_tbl[i].comment, startAddr+head_offset);
				return -1;
			}
		}
		head_offset += Header.len + sizeof(IMG_HEADER_T);
	} //while	
	return found;
}
#endif

void autoreboot()
{
	jumpF = (void *)(0xbfc00000);
	outl(0,GIMR0); // mask all interrupt	    
	cli();

	flush_cache(); 
	prom_printf("\nreboot.......\n");
#if defined(RTL865X) || defined(RTL8196B) || defined(RTL8198)
	/* this is to enable 865xc watch dog reset */
	*(volatile unsigned long *)(0xB800311c)=0; 
	for(;;);
#endif
	/* reboot */
	jumpF();	
}

#ifndef RTL8197B
#ifdef DNI_PATCH
int model_image = 0;
void
auto_checksum_verify(unsigned long startAddr,int len)
{
	unsigned char *cksum_start = (unsigned char *)startAddr;
	unsigned char *cksum_end = (unsigned char *)(startAddr+len-2);
	unsigned char cksum_result = 0;

	while(cksum_start <= cksum_end)
		cksum_result = (cksum_result + *cksum_start++) % 0x100;
	
	cksum_result = (0xFF - cksum_result);

	if(cksum_result != *(cksum_end + 1))
		model_image =0;
	else
		model_image =1;
}

int auto_check_region(unsigned long startAddr,int len)
{
#ifdef DNI_REGION_CHECK_SUPPORT
	unsigned char product_region[64];
	unsigned char ram_header[64];
	unsigned int i;
	unsigned char region[EXTRA_SETTINGS_REGION_LEN+1];
	
	for(i=0;i<=EXTRA_SETTINGS_REGION_LEN;i++)
		region[i]=0;
		
	for(i=0;i<64;i++)
		ram_header[i]=0;
	
	flashread(region, MIB_EXTRA_SETTINGS_REGION,EXTRA_SETTINGS_REGION_LEN);
	
	if ((region[0]>='A')&&(region[1]>='A'))
	{
		memcpy(ram_header, (unsigned char *)startAddr, 63);
		SprintF(product_region,"region:%s", region);
		if(strstr(ram_header, product_region))
		{
			prom_printf("\nThe image's region(%s) is correct!!\n", region);
			return 1;
		}
		prom_printf("\nThe image's region isn't %s!!\n", region);
		return 0;
	}
	else
	{
		prom_printf("\nBOARD's region is NULL, please type-in its region!!\n");
		return 1;
	}
#else
	return 1;
#endif 
}

int auto_check_pid(unsigned long startAddr,int len)
{

	unsigned char *product_string = MODEL_NAME;
	unsigned char *ram_header = (unsigned char *)startAddr;

	//if(memcmp(product_string, ram_header + 7, 8) == 0)
	if(memcmp(product_string, ram_header + 7, strlen(product_string)) == 0)
	{
	 	if (auto_check_region(startAddr, len))
	  		auto_checksum_verify(startAddr,len);
	  	else
	  		model_image =0;
	}
	else
	  model_image =0; 
	  
	return(model_image);   
}

#endif
void checkAutoFlashing(unsigned long startAddr, int len)
{
	int i=0;
	unsigned long head_offset=0, srcAddr, burnLen;
	unsigned short sum=0;
	unsigned char sum1=0;
	int skip_header=0;
	int reboot=0;
	IMG_HEADER_T Header ; //avoid unalign problem
	int skip_check_signature=0;

	#if defined(KLD)	
	char image_sig_check[1]={0};
	char image_sig[4]={0};
	char image_sig_root[4]={0};
	unsigned int offset=0;
	#endif
#ifdef DNI_PATCH
#if defined(CONFIG_SFLASH_4M)||defined(CONFIG_SFLASH_8M)
  //  Set_POWER_LED_OFF(); /* add by alfa */
    if(auto_check_pid(startAddr,len) == 1)
      {
      	prom_printf("%s Image checksum Ok !\n", MODEL_NAME);
      	//Header.burnAddr = 0x10000; /* test by alfa */
      	Header.burnAddr = CONFIG_ONE__IMAGE_START_OFFSET_FOR_TFTP; // 
      	srcAddr = startAddr + 128;
      	burnLen = len - 128 - 1 ;
      	prom_printf("burn Addr =0x%x! srcAddr=0x%x len =0x%x \n", Header.burnAddr, srcAddr, burnLen);
      	
      	if(nmrp_tftp_wait)
	{
		image_dst = Header.burnAddr;
		image_src = srcAddr;
		image_len = burnLen;
		nmrp_tftp_runing = 0;
		return;
	}
	else
	{
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			if(spi_flw_image_mio_8198(0,Header.burnAddr, srcAddr, burnLen, 0))
		#else
			if(spi_flw_image(0,Header.burnAddr, srcAddr, burnLen))
		#endif
#else
		if (flashwrite(Header.burnAddr, srcAddr, burnLen))
#endif
#ifdef DNI_PATCH
	  		prom_printf( "\nFlash Write Successed!\n<%s>", MODEL_NAME );
#else 
                	prom_printf("\nFlash Write Successed!\n%s", "<RealTek>");
#endif                	
		else
	     	{
#ifdef DNI_PATCH
	  		prom_printf( "\nFlash Write Failed!\n<%s>", MODEL_NAME );
#else 	     		
		  	prom_printf("\nFlash Write Failed!\n%s", "<RealTek>");
#endif		  	
			return ;
		}
		autoreboot();    
	}  
      }	
#endif      
#endif
	while( (head_offset + sizeof(IMG_HEADER_T)) <  len){
		sum=0; sum1=0;
		memcpy(&Header, ((char *)startAddr + head_offset), sizeof(IMG_HEADER_T));
		
		#if defined(KLD)	
		image_sig_check[0]=0x00;
		if(!memcmp(Header.signature, FW_SIGNATURE_WITH_ROOT_615, SIG_LEN-1)){
			offset = HW_SETTING_OFFSET+HW_SETTING_11N_RESERVED7_OFFSET;
		}else if(!memcmp(Header.signature, ROOT_SIGNATURE_615, SIG_LEN-1)){
			offset = HW_SETTING_OFFSET+HW_SETTING_11N_RESERVED8_OFFSET;
		}else if(!memcmp(Header.signature, WEB_ROOT_SIGNATURE_615, SIG_LEN-1))
			offset = HW_SETTING_OFFSET+HW_SETTING_11N_RESERVED9_OFFSET;
		
		if(offset !=0){
			check_image_signature_tag(offset, (&image_sig_check[0]));
			
		}
		
		#endif
		if (!skip_check_signature) {
			for(i=0 ;i < MAX_SIG_TBL ; i++) {
			#if defined(KLD)	
				memcpy(image_sig, sign_tbl[i].signature,sign_tbl[i].sig_len); 
				if(image_sig_check[0] !=0x00){
						image_sig[3]=image_sig_check[0];
				}
				if(!memcmp(Header.signature, (char *)image_sig, sign_tbl[i].sig_len)){
					
					break;
				}			
			#else
				if(!memcmp(Header.signature, (char *)sign_tbl[i].signature, sign_tbl[i].sig_len))
					break;			
				
			#endif
			}
			if(i == MAX_SIG_TBL){
#ifdef DNI_PATCH
				if(nmrp_tftp_wait)
				   nmrp_tftp_success= 0; 
				nmrp_tftp_runing = 0;   
			    prom_printf("Image faile \n");   
				return ;
#else
				head_offset += Header.len + sizeof(IMG_HEADER_T);
				continue ;
#endif				
			}			
			skip_header = sign_tbl[i].skip ;
			if(skip_header){
				srcAddr = startAddr + head_offset + sizeof(IMG_HEADER_T);
					burnLen = Header.len; // +checksum
			}else{
				srcAddr = startAddr + head_offset ;
				burnLen = Header.len + sizeof(IMG_HEADER_T) ;
			}	
			reboot |= sign_tbl[i].reboot;
			prom_printf("\n%s upgrade.\n", sign_tbl[i].comment);
		}
		else {
			if(!memcmp(Header.signature, BOOT_SIGNATURE, SIG_LEN))
				skip_header = 1;
			else {
				unsigned char *pRoot =((unsigned char *)startAddr) + head_offset + sizeof(IMG_HEADER_T);
				if (!memcmp(pRoot, SQSH_SIGNATURE, SIG_LEN))
					skip_header = 1;
				else				
					skip_header = 0;
			}				
			if(skip_header){
				srcAddr = startAddr + head_offset + sizeof(IMG_HEADER_T);
				burnLen = Header.len ; // +checksum

			}else{
				srcAddr = startAddr + head_offset ;
				burnLen = Header.len + sizeof(IMG_HEADER_T) ;
			}			
		}		

		if(skip_check_signature || 
			memcmp(Header.signature, WEB_SIGNATURE, 3)){
			//calculate checksum
			if(!memcmp(Header.signature, ALL1_SIGNATURE, SIG_LEN) ||
					!memcmp(Header.signature, ALL2_SIGNATURE, SIG_LEN)) {										
				for (i=0; i< Header.len+sizeof(IMG_HEADER_T); i+=2) {
					sum += *((unsigned short *)(startAddr+ head_offset + i));
				}				
			}	
				else 
				{
					unsigned char x=0,y=0;
					unsigned short temp=0;
					
					for (i=0; i< Header.len; i+=2) 
					{
						
#if defined(RTL8196B) || defined(RTL8198)																		
#if 1				
						//sum +=*((unsigned short *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));
						memcpy(&temp, (startAddr+ head_offset + sizeof(IMG_HEADER_T) + i), 2); // for alignment issue
						sum+=temp;
#else						
						x=*((unsigned char *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));						
						y=*((unsigned char *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i+1));
						sum+=(y|x<<8)&0xFFFF;
#endif
#else
				sum += *((unsigned short *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));
#endif	//#if defined(RTL8196B) || defined(RTL8198)
				}
			}
			if ( sum ) {
				prom_printf("%s imgage checksum error at %X!\n"
				, Header.signature, startAddr+head_offset);
#ifdef DNI_PATCH
				if(nmrp_tftp_wait)
				   nmrp_tftp_success= 0; /* add by alfa */
			    nmrp_tftp_runing = 0;
#endif								
				return ;
			}
			if(!memcmp(Header.signature, ALL1_SIGNATURE, SIG_LEN)){
				head_offset += sizeof(IMG_HEADER_T);
				continue;		
			}		
			if(!memcmp(Header.signature, ALL2_SIGNATURE, SIG_LEN)){
				skip_check_signature = 1;
				head_offset += sizeof(IMG_HEADER_T);			
				continue;		
			}						
		}else
		{  //web page use different checksum algorimth

			for (i=0; i< Header.len; i++)
			       sum1 += *((unsigned char *)(startAddr+ head_offset + sizeof(IMG_HEADER_T) + i));
			if ( sum1 ) {
				prom_printf("%s imgage checksum error at %X!\n"
				, Header.signature, startAddr+head_offset);
#ifdef DNI_PATCH
				if(nmrp_tftp_wait) /* add by alfa */
				   nmrp_tftp_success= 0;
				nmrp_tftp_runing = 0;
#endif
				return ;
			}
		}
		prom_printf("checksum Ok !\n");
#ifdef DNI_PATCH
		if(nmrp_tftp_wait)
		  {
		   image_dst = Header.burnAddr;
		   image_src = srcAddr;
		   image_len = burnLen;
		   nmrp_tftp_runing = 0;
		   return;
		  } 
#endif
		prom_printf("burn Addr =0x%x! srcAddr=0x%x len =0x%x \n", Header.burnAddr, srcAddr, burnLen);
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
#ifdef DNI_PATCH
			if(spi_flw_image_mio_8198(0,Header.burnAddr, srcAddr, burnLen, 0))
#else		
			if(spi_flw_image_mio_8198(0,Header.burnAddr, srcAddr, burnLen))
#endif
		#else
			if(spi_flw_image(0,Header.burnAddr, srcAddr, burnLen))
		#endif
#else
		if (flashwrite(Header.burnAddr, srcAddr, burnLen))
#endif
		{
#ifdef DNI_PATCH
	  		prom_printf( "\nFlash Write Successed!\n<%s>", MODEL_NAME );
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT	  		
	  		tftp_filename_cmds_set(tftp_filename);
#endif
#else 
			prom_printf("\nFlash Write Successed!\n%s", "<RealTek>");
#endif		
		}	
		else{
#ifdef DNI_PATCH
	  		prom_printf( "\nFlash Write Failed!\n<%s>", MODEL_NAME );
#else 		
			prom_printf("\nFlash Write Failed!\n%s", "<RealTek>");
#endif			
			return ;
		}

		head_offset += Header.len + sizeof(IMG_HEADER_T);
	} //while
#ifdef DNI_PATCH
	if(reboot && nmrp_tftp_wait==0 ){
#else
	if(reboot){
#endif
	    	autoreboot();
	}
		
}
#endif

#ifdef DNI_NMRP_V2 //dvd.chen, for new NMRP
#define UPLOAD_ST_FILE_WITH_CHKSUM	1

int chk_ST_ID(unsigned long FileStartAddr)
{
	unsigned char *ST_ID = "language_table";

	//the tgz header with file name
	//format: ru.language_table.tar.gzXXXXXX or language_table.tar.gzXXXXXX
	if( (memcmp(FileStartAddr, ST_ID, 14) != 0) &&
		(memcmp(FileStartAddr + 3, ST_ID, 14) != 0) )
		return 0;
	else
		return 1;
}

#ifdef DNI_NMRP_STRING_TABLE_WRITE_SUPPORT
void FlashWriteStringTable(unsigned long StringTableStartAddr, int file_size)
{
	unsigned char *addr2;;
	unsigned long burnAddr, table_length;
	unsigned char high_bit, low_bit;
#if 1 //!ST_FILE_WITH_CHKSUM
	unsigned char *string_table;
	int i;
	unsigned char chksum = 0;
#endif

	if( UploadingStringTableIdx == 1 )
		burnAddr = STRING_TABLE1_ADDRESS; //language_table1 (flash address)
	else if( UploadingStringTableIdx == 2 )
		burnAddr = STRING_TABLE2_ADDRESS; //language_table2 (flash address)
	else
		prom_printf("#FlashWriteStringTable:: error!! UploadingStringTableIdx[%d]\n", UploadingStringTableIdx );
	
#if UPLOAD_ST_FILE_WITH_CHKSUM
	string_table = StringTableStartAddr;
	table_length = file_size - 1;
	for(i=0; i<table_length; i++)
	{
		chksum = (chksum + string_table[i]) & 0xff;
	}
	chksum = ~chksum;
	//prom_printf("#FlashWriteStringTable:: ST_chksum[%d], cal_chksum[%d], StringTable_len[%d]\n", string_table[table_length], chksum, table_length);

	if( chksum != string_table[table_length])
	{
		prom_printf("#%s::ST chksum error!\n", __FUNCTION__);
		return;
	}	
	
	/* Save string table checksum to (100K - 1) */
    	memcpy(StringTableStartAddr + STRING_TABLE_SIZE - 1, StringTableStartAddr + file_size- 1, 1);
    	/* Remove checksum from string table file's tail */
    	memset(StringTableStartAddr + file_size - 1, 0, 1);
#else
	string_table = StringTableStartAddr;
	table_length = file_size;
	for(i=0; i<table_length; i++)
	{
		chksum = (chksum + string_table[i]) & 0xff;
	}
	chksum = ~chksum;
	//prom_printf("#FlashWriteStringTable:: cal_chksum[%d], StringTable_len[%d]\n", chksum, table_length);
	
	/* Save string table checksum to (100K - 1) */
    	//memcpy(StringTableStartAddr + STRING_TABLE_SIZE - 1, &chksum, 1);
	string_table[STRING_TABLE_SIZE - 1] = chksum;
#endif

    	/* Save (string table length / 1024)to 100K-3 */
    	high_bit = table_length / 1024;
    	addr2 = StringTableStartAddr + STRING_TABLE_SIZE - 3;
    	memcpy(addr2, &high_bit, sizeof(high_bit));

	/* Save (string table length % 1024)to 100K-2 */
	low_bit = table_length % 1024;
	addr2 = StringTableStartAddr + STRING_TABLE_SIZE - 2;
	memcpy(addr2, &low_bit, sizeof(low_bit));

	prom_printf("#FlashWriteStringTable:: burn Addr =0x%x! srcAddr=0x%x len =0x%x \n", burnAddr, StringTableStartAddr, STRING_TABLE_SIZE);
	
#ifdef CONFIG_SPI_FLASH
	#ifdef SUPPORT_SPI_MIO_8198_8196C
		#ifdef DNI_PATCH
	if(spi_flw_image_mio_8198(0, burnAddr, StringTableStartAddr, STRING_TABLE_SIZE, 0))
		#else		
	if(spi_flw_image_mio_8198(0, burnAddr, StringTableStartAddr, STRING_TABLE_SIZE))
		#endif
	#else
	if(spi_flw_image(0, burnAddr, StringTableStartAddr, STRING_TABLE_SIZE))
	#endif
#else
	if (flashwrite(burnAddr, StringTableStartAddr, STRING_TABLE_SIZE))
#endif
		prom_printf("Flash Write Successed!\n");
	else
		prom_printf("Flash Write Failed!\n");

	return;
}
#endif
#endif

/*Cyrus Tsai*/
extern int flashwrite(unsigned long FLASH, unsigned long SDRAM, unsigned long length);
extern void ddump(unsigned char * pData, int len);
//----------------------------------------------------------------------------------------
/*Cyrus Tsai*/
/*Why we prepare ACK, because we have data uploaded by client*/

#ifdef DNI_PATCH
static void prepareACK(struct nic rx)
#else
static void prepareACK(void)
#endif
{
 struct udphdr *udpheader;
 struct tftp_t *tftppacket;
 Int16 tftpopcode;
 Int32 tftpdata_length;
 volatile Int16 block_received=0;
#ifdef RTL8197B
    IMG_HEADER_T header;
    int ret;
    extern int check_system_image(unsigned long addr,IMG_HEADER_Tp pHeader);
#endif
#ifdef DNI_PATCH
    tftp_data_time=get_timer_jiffies();
    tftp_data = 1;
#endif
#ifdef RTL8197B
    if (!tftpd_is_ready)
        return;
#endif

	//dprintf("Receive TFTP Data\n");
#ifdef DNI_PATCH
 udpheader = (struct udphdr *)&rx.packet[ETH_HLEN+ sizeof(struct iphdr)];
#else
 udpheader = (struct udphdr *)&nic.packet[ETH_HLEN+ sizeof(struct iphdr)];
#endif
 if(udpheader->dest==htons(SERVER_port))
   {
    /*memorize CLIENT port*/
    CLIENT_port=  ntohs(udpheader->src);
#ifdef DNI_PATCH
    tftppacket = (struct tftp_t *)&rx.packet[ETH_HLEN];
#else
    tftppacket = (struct tftp_t *)&nic.packet[ETH_HLEN];
#endif
    /*no need to check opcode, this is a Data packet*/     
    /*parse the TFTP block number*/	
    block_received=tftppacket->u.data.block;
//prom_printf("line=%d:		block_received=%d\n", __LINE__,  block_received); // for debug
    
    if(block_received != (block_expected))
    {
     prom_printf("line=%d: block_received=%d, block_expected=%d\n", __LINE__,  block_received,  block_expected); // for debug
     prom_printf("TFTP #\n");
     /*restore the block number*/
     tftpd_send_ack(block_expected-1);    
    }
    else 
     {      
          
      tftpdata_length=ntohs(udpheader->len)-4-sizeof(struct udphdr);
      /*put the image into memory address*/
      memcpy((void *)address_to_store, tftppacket->u.data.download, tftpdata_length);


	// ddump(address_to_store, tftpdata_length);
      //prom_printf("a %x. l %x\n",address_to_store,tftpdata_length);
      
      address_to_store=address_to_store+tftpdata_length;
      /*use this to count the image bytes*/
      file_length_to_server=file_length_to_server+tftpdata_length;
      /*this is for receiving one packet*/
      //prom_printf("%x.\n",address_to_store);
#ifndef DNI_PATCH	  /* mask by alfa */
      twiddle();
#endif
      //prom_printf(" <- ");
      //prom_printf("%x. %x. %x\n",block_expected,address_to_store,tftpdata_length);
      
      tftpd_send_ack(block_expected);               
#ifdef DNI_NMRP_V2 //dvd.chen, for new NMRP
	//send NMRP_CODE_KEEP_ALIVE_REQ every 1000 block during FW uploading
	if( TFTPFromNMRP && NmrpFwUPOption && !NmrpSTUPOption )
	{
		//we gonna recv about 42XX blocks. So, it will be 30, 1030, 2030,3030, 4030
		if( block_expected % 1000 == 30 ) 
		{
			//prom_printf("##prepareACK:: send NMRP_KEEP_ALIVE_REQ, block_expected[%d]\n", block_expected);
			NMRPSendKeepAliveREQ();
		}
	}
#endif
      block_expected=block_expected+1;
//prom_printf("line=%d:		block_expected=%d\n", __LINE__,  block_expected); // for debug
      
      /*remember to check if it is the last packet*/      
      if(tftpdata_length < TFTP_DEFAULTSIZE_PACKET)
        {
         prom_printf("\n**TFTP Client Upload File Size = %X Bytes at %X\n",file_length_to_server,image_address);          
         /*change the boot state back to orignal, and some variables also*/
#ifdef DNI_PATCH
          tftp_data = 0;    /* add by alfa */
#endif
         nic.packet = eth_packet;
         nic.packetlen = 0;        
         block_expected =0;   
//prom_printf("line=%d:		block_expected=%d\n", __LINE__,  block_expected); // for debug
				 
         /*reset the file position*/
         //image_address=FILESTART;
         address_to_store=image_address;
         file_length_to_client=file_length_to_server;
         /*file_length_to_server can not be reset,only when another WRQ */
         /*and export to file_length_to_client for our SDRAM direct RRQ*/
         it_is_EOF=0;
#ifndef DNI_PATCH        
         bootState=BOOT_STATE0_INIT_ARP;
#endif         
         /*Cyrus Tsai*/
         one_tftp_lock=0; 
         SERVER_port++;
#ifdef DNI_PATCH
 	 prom_printf( "\nSuccess!\n<%s>", MODEL_NAME );
#ifndef DNI_RESET_TO_DEFAULT_HW_SETTINGS_SUPPORT 	 
 	 tftp_filename_ini_cmds_set(tftp_filename, image_address, file_length_to_server);
#endif
#else
 	 prom_printf( "\nSuccess!\n%s", "<RealTek>" );
#endif
 	      
#ifdef RTL8197B
        ret = check_system_image((unsigned long)image_address,&header);
    	if(ret == 1)
	    {
			//prom_printf("\nheader.startAddr=%X header.len=%d image_address=%x\n", header.startAddr, header.len, image_address);
			// move image to SDRAM
			//memcpy((void*)header.startAddr, (void*)(0x80700000+sizeof(header)), header.len-2);
			
			jump_to_test = 1;
			image_address = (void *)(header.startAddr); //0x80700000
		}
		else {
		    prom_printf( "ret=%d\n", ret);
		}
#endif  //RTL8197B

         if(jump_to_test==1)
           {
            jump_to_test=0;
	    /*we should clear all irq mask.*/
	    //jumpF = (void *)(TESTSTART); //sc_yang
	    jumpF = (void *)(image_address);
	    /*we should clear all irq mask.*/
	    outl(0,GIMR0); // mask all interrupt	    
	   cli();

		dprintf("Jump to 0x%x\n", image_address);
		flush_cache(); 
	    jumpF();	
           }
#ifndef RTL8197B
	   else if(autoBurn )
	   {
#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP
			checkAutoFlashing(image_address, file_length_to_server);
#else
		if (TFTPFromNMRP == 0 )  //TFTP  from cmd_download, check is string table or firmware image.
		{
			if(chk_ST_ID(image_address))
			{
				prom_printf("#CmdDownload a String Table.\n");
#ifdef DNI_NMRP_STRING_TABLE_WRITE_SUPPORT
				FlashWriteStringTable(image_address, file_length_to_server);
#endif
			}
			else
			{
				prom_printf("#CmdDownload an image.\n");
				checkAutoFlashing(image_address, file_length_to_server);
			}
		}
		else
		{
			//now it could be string table(s) or firmware image.
			///prom_printf("**NmrpFwUPOption[%d], NmrpSTUPOption[%d], file_length_to_server[%d]\n", 
			//			NmrpFwUPOption, NmrpSTUPOption, file_length_to_server);
			//for string table(s), we also need to check the mask bits to know if we've got all needed tables,
			//for now, we accept only ONE string table
			if( NmrpSTUPOption && NmrpFwUPOption )
			{
				//after succefully write string table or firmimage to flash, clear to correspond flag to 0, then finish
				//because NmrpFwUPOption is still 1, NMRP will request for FW later.
#ifdef DNI_NMRP_STRING_TABLE_WRITE_SUPPORT				
				prom_printf("#Tftp got ST, Writting to flash. Get FW later\n");
				FlashWriteStringTable(image_address, file_length_to_server);
#else
				prom_printf("#Tftp got ST, Skip write to Flash. Get FW later\n");
#endif
				if( NmrpPeerReqStMask == 0 )  
					NmrpSTUPOption = 0; //we write string table first, after all string table(s) are written, then burm image
				else
					prom_printf("have more ST...\n");
			}
			else if( NmrpSTUPOption )
			{
				//write string table(s) to flash
#ifdef DNI_NMRP_STRING_TABLE_WRITE_SUPPORT
				prom_printf("#Tftp got ST, Writting to flash\n");
				FlashWriteStringTable(image_address, file_length_to_server);
#else
				prom_printf("#Tftp got ST, Skip write to Flash\n");
#endif				
				if( NmrpPeerReqStMask == 0 )  
					NmrpSTUPOption = 0;
				else
					prom_printf("have more ST...2\n");
			}
			else if( NmrpFwUPOption )
			{
				prom_printf("#Tftp got FW, Chking img...\n");
				NmrpFwUPOption = 0;
				checkAutoFlashing(image_address, file_length_to_server);
				//set flag for later writting FW to flash in nmrp_entry()
				NmrpDoFlashWriteFw = 1;
			}
				
			nmrp_tftp_runing = 0;
		}
#endif
	   }
#endif

        }       
      
      
     }
   }
//else 
//   prom_printf("\n**TFTP port number error");   

}
//----------------------------------------------------------------------------------------
/*Why we prepare DATA, because we receive the ACK*/
#ifndef DNI_BOOTCODE_DOWNSIZE
#ifdef DNI_PATCH
static void prepareDATA(struct nic rx)
#else
static void prepareDATA(void)
#endif
{
 /*support CLIENT RRQ now.*/
 struct udphdr *udpheader;
 struct tftp_t *tftppacket;
 Int16 tftpopcode;
 Int32 tftpdata_length;
 Int16 block_received=0;

#ifdef RTL8197B
    if (!tftpd_is_ready)
        return;
#endif
#ifdef DNI_PATCH
 udpheader = (struct udphdr *)&rx.packet[ETH_HLEN+ sizeof(struct iphdr)];
#else
 udpheader = (struct udphdr *)&nic.packet[ETH_HLEN+ sizeof(struct iphdr)];
#endif
 if(udpheader->dest==htons(SERVER_port))
   {
    /*memorize CLIENT port*/
    CLIENT_port=  ntohs(udpheader->src);
#ifdef DNI_PATCH
    tftppacket = (struct tftp_t *)&rx.packet[ETH_HLEN];
#else	
    tftppacket = (struct tftp_t *)&nic.packet[ETH_HLEN];
#endif
    /*no need to check opcode, this is a ACK packet*/     
    /*parse the TFTP ACK number*/	
    block_received=tftppacket->u.ack.block;
    if(block_received != (block_expected))
    {
     //prom_printf("line=%d: block_received=%d, block_expected=%d\n", __LINE__,  block_received,  block_expected); // for debug
     prom_printf("\n**TFTP #\n");
     tftpd_send_data(filename,block_expected);
    }
    else 
     {      
      block_expected=block_expected+1;      
      if(!(it_is_EOF))
          tftpd_send_data(filename,block_expected);     
      else 
         {
         /*After we receive the last ACK then we can go on.*/	
#ifndef DNI_PATCH         
          bootState=BOOT_STATE0_INIT_ARP;  
#endif           
          one_tftp_lock=0; 
          //prom_printf("\n**TFTP Client Upload Success! File Size = %X Bytes\n",file_length_to_server);                        
          prom_printf("\n*TFTP Client Download Success! File Size = %X Bytes\n",file_length_to_client);          
#ifdef DNI_PATCH
	  prom_printf( ".Success!\n<%s>", MODEL_NAME );
#else          
          prom_printf( ".Success!\n%s", "<RealTek>" );         
#endif                  
          nic.packet = eth_packet;
          nic.packetlen = 0;        
          block_expected =0;       
//prom_printf("line=%d:		block_expected=%d\n", __LINE__,  block_expected); // for debug
          
          it_is_EOF=0;
         }
     }
   }
//else 
//   prom_printf("\n**TFTP port number error\n");   
  

}                               
#endif //#ifndef DNI_BOOTCODE_DOWNSIZE
//----------------------------------------------------------------------------------------
//char eth0_mac[6]={0x56, 0xaa, 0xa5, 0x5a, 0x7d, 0xe8};
extern char eth0_mac[6];
#ifdef DNI_PATCH
extern int packet_in;
#endif
void tftpd_entry(void)
{
 int i,j;

#ifdef DNI_PATCH
 struct nic rx;
 int status;
 NMRP_MSG *msg;
 int nmrp_tftp_ul_req_time;
 
//arptable_tftp[TFTP_SERVER].ipaddr.s_addr = IPTOUL(192,168,1,1);
#else
#if defined(KLD)
arptable_tftp[TFTP_SERVER].ipaddr.s_addr = IPTOUL(192,168,0,1);
#elif defined(EC)
arptable_tftp[TFTP_SERVER].ipaddr.s_addr = IPTOUL(192,168,1,1);
#else
arptable_tftp[TFTP_SERVER].ipaddr.s_addr = IPTOUL(192,168,1,6);
#endif
#endif


arptable_tftp[TFTP_CLIENT].ipaddr.s_addr = IPTOUL(192,162,1,116);
 /*This is ETH0. we treat ETH0 as the TFTP server*/
 /*char eth0_mac[6]={0x56, 0xaa, 0xa5, 0x5a, 0x7d, 0xe8};*/
arptable_tftp[TFTP_SERVER].node[5]=eth0_mac[5];
arptable_tftp[TFTP_SERVER].node[4]=eth0_mac[4];
arptable_tftp[TFTP_SERVER].node[3]=eth0_mac[3];
arptable_tftp[TFTP_SERVER].node[2]=eth0_mac[2];
arptable_tftp[TFTP_SERVER].node[1]=eth0_mac[1];
arptable_tftp[TFTP_SERVER].node[0]=eth0_mac[0];

//arptable_tftp[TFTP_SERVER].node[5]=0xe8;
//arptable_tftp[TFTP_SERVER].node[4]=0x7d;
//arptable_tftp[TFTP_SERVER].node[3]=0x5a;
//arptable_tftp[TFTP_SERVER].node[2]=0xa5;
//arptable_tftp[TFTP_SERVER].node[1]=0xaa;
//arptable_tftp[TFTP_SERVER].node[0]=0x56;


 /*intialize boot state*/	
#ifndef DNI_PATCH 	
 bootState=BOOT_STATE0_INIT_ARP;
#endif 
 /*this nic is the expected data structure to be processed.*/
 nic.packet=eth_packet;
 nic.packetlen=0;

 block_expected=0;
 one_tftp_lock=0;
 it_is_EOF=0;

//prom_printf("line=%d:		block_expected=%d\n", __LINE__,  block_expected); // for debug

 
 //image_address=FILESTART; //sc_yang
 address_to_store=image_address;
 
 file_length_to_server=0;
 file_length_to_client=0;
 
#ifdef DNI_PATCH
 SERVER_port=69; /* modify by alfa */

 nmrp_tftp_ul_req_time = get_timer_jiffies();
 while(1)
 {
 	if(nmrp_tftp_wait && (one_tftp_lock == 0)&& ((get_timer_jiffies()- nmrp_tftp_ul_req_time) > 50))
 	  {
 	  	if(NULL==(msg=(NMRP_MSG *)malloc(NMRP_HDR_LEN)))
		   { 
		   	nmrp_tftp_success = 0 ;
		   	break ;
		 }
		 msg->reserved = 0;
		 msg->code =NMRP_CODE_TFTP_UL_REQ;
		 msg->id = 0;
		 msg->length = htons(NMRP_HDR_LEN);
		 prom_printf("##sending NMRP_CODE_TFTP_UL_REQ again...\n");
		 prepare_txpkt(0,ETHER_NMRP,&(nmrp_server_info.enet_addr),(Int8*)msg,NMRP_HDR_LEN);
		 nmrp_tftp_ul_req_time = get_timer_jiffies();
		 free(msg); 
	 }	 
 	
 	if((status=polling_rx()) == 1)
 	  {
 	  	rx.packet = mac_rx_buff[buff_rd_index].packet;
 	  	rx.packetlen = mac_rx_buff[buff_rd_index].packet_size;
        ++buff_rd_index;
        if(buff_rd_index > ( MAX_RX_BUFF-1 ))
           buff_rd_index = 0 ;
        kick_tftpd(rx); 
        buff_bz_num--;
      
        if((nmrp_tftp_wait == 1) && ( nmrp_tftp_runing == 0))
           break;
       }
     if(nmrp_tftp_wait && tftp_data && (one_tftp_lock == 1))
       {    
       	if((get_timer_jiffies() - tftp_data_time) > 300)
          {
          	prom_printf("TFTP Server Time out\n");
          	nmrp_tftp_success= 0;
          	nmrp_tftp_runing = 0;
          	tftp_data = 0 ;
          	one_tftp_lock = 0;
          	nic.packet = eth_packet;
            nic.packetlen = 0;        
            block_expected =0;   
            address_to_store=image_address;
          	break; 
          }
       }
           
 }
#else
 SERVER_port=2098;
#endif
#ifdef RTL8197B
#ifndef CONFIG_FPGA_PLATFORM
    tftpd_is_ready = 1;
#endif
    //toggle bit 5 of SYSSR to indicate bootloader is ready for TFTP transfer
    if ( REG32(0xb801900c) & 0x0020 )
        REG32(0xb801900c)= REG32(0xb801900c) & (~0x0020);
	else
        REG32(0xb801900c)= REG32(0xb801900c) | 0x0020;
#endif
}
//----------------------------------------------------------------------------------------
void tftpd_send_ack(Int16 number)
{
 /*UDP source port: SERVER_port*/
 /*UDP target port: CLIENT_port*/
 struct iphdr *ip;
 struct udphdr *udp;
 struct tftp_t tftp_tx;
 /*generate the TFTP body*/
 tftp_tx.opcode=htons(TFTP_ACK);
 tftp_tx.u.ack.block=htons(number);
 
 ip = (struct iphdr *)&tftp_tx;
 udp = (struct udphdr *)((Int8*)&tftp_tx + sizeof(struct iphdr));
 
 /*IP header*/
 ip->verhdrlen = 0x45;
 ip->service = 0;
 ip->len = htons(32);
 ip->ident = 0;
 ip->frags = 0;
 ip->ttl = 60;
 ip->protocol = IP_UDP;
 ip->chksum = 0;
 ip->src.s_addr = arptable_tftp[TFTP_SERVER].ipaddr.s_addr;
 ip->dest.s_addr = arptable_tftp[TFTP_CLIENT].ipaddr.s_addr;
 ip->chksum = ipheader_chksum((Int16 *)&tftp_tx, sizeof(struct iphdr));
 /*generate the UDP header*/
 udp->src  = htons(SERVER_port);
 udp->dest = htons(CLIENT_port);
 udp->len  = htons(32 - sizeof(struct iphdr));/*TFTP IP packet is 32 bytes.*/
 udp->chksum = 0;

 prepare_txpkt(0,FRAME_IP,arptable_tftp[TFTP_CLIENT].node,(Int8*)&tftp_tx,(Int16)sizeof(struct iphdr)+sizeof(struct udphdr)+4);
}
//----------------------------------------------------------------------------------------
void tftpd_send_data(char* filename, Int16 block_number)
{
 /*because we only have 1 image supported */
 /*do nothing to char* file is of no use.*/
 /*UDP source port: SERVER_port*/
 /*UDP target port: CLIENT_port*/

 struct iphdr *ip;
 struct udphdr *udp;
 struct tftp_t tftp_tx;
 Int32* data; 
 int length;


 /********************************************/  
   data=(Int32 *)(image_address+ 512*(block_number-1));
   //prom_printf("send data start at %x\n",data);
 if (512* block_number==(file_length_to_client+512))
    {
    /*it is over that means a length=0 data is required*/
    length=0;
    //prom_printf("TFTP RRQ last NULL data to send\n");
    it_is_EOF=1;
    }
 else if( 512* block_number > file_length_to_client)
    { 
     length=file_length_to_client-512*(block_number-1);
     //prom_printf("TFTP RRQ last data to send\n");
     it_is_EOF=1;
    }
 else
    length=512;
 
 /********************************************/
 /*generate the TFTP body*/
 tftp_tx.opcode=htons(TFTP_DATA);
 memcpy(tftp_tx.u.data.download,(Int8*)data,length);
 tftp_tx.u.data.block=htons(block_number);
 
 ip = (struct iphdr *)&tftp_tx;
 udp = (struct udphdr *)((Int8*)&tftp_tx + sizeof(struct iphdr));
 
 /*generate the IP header*/
 ip->verhdrlen = 0x45;
 ip->service = 0;
 ip->len = htons(32+length);
 ip->ident = 0;
 ip->frags = 0;
 ip->ttl = 60;
 ip->protocol = IP_UDP;
 ip->chksum = 0;
 ip->src.s_addr = arptable_tftp[TFTP_SERVER].ipaddr.s_addr;
 ip->dest.s_addr = arptable_tftp[TFTP_CLIENT].ipaddr.s_addr;
 ip->chksum = ipheader_chksum((Int16 *)&tftp_tx, sizeof(struct iphdr));
 /*generate the UDP header*/
 udp->src  = htons(SERVER_port);
 udp->dest = htons(CLIENT_port);
 udp->len  = htons(length+4+8);
 udp->chksum = 0;
 
 /*use twiddle here*/
 twiddle();
 //prom_printf(" -> ");
 
 prepare_txpkt(0,FRAME_IP,arptable_tftp[TFTP_CLIENT].node,(Int8*)&tftp_tx,(Int16)sizeof(struct iphdr)+sizeof(struct udphdr)+length+4);
}
               
//----------------------------------------------------------------------------------------
#ifdef DNI_PATCH
void kick_tftpd(struct nic rx)
#else
void kick_tftpd(void)
#endif
{
    /*We always have the global nic structure, never change it directly*/
    int i;
    /*First of all parse the packet type*/	
    /*that is the first 13 and 14 byte that is IP:(UDP) 0800 ARP 0806*/
    Int16 pkttype=0;
    struct	arprequest *arppacket;
    Int16 arpopcode;
    struct tftp_t *tftppacket;
    Int16 tftpopcode;
      
    struct iphdr  *ipheader;
    
    struct udphdr *udpheader;
    //Cyrus Dick
    in_addr ip_addr;
    // in_addr source_ip_addr;
    
    void	(*jump)(void);
#ifndef DNI_PATCH    
    BootEvent_t  kick_event=NUM_OF_BOOT_EVENTS;
#endif
    Int32 UDPIPETHheader = ETH_HLEN + sizeof(struct iphdr)  + sizeof(struct udphdr);		 
    
#ifdef DNI_PATCH
 if (rx.packetlen >= ETH_HLEN+sizeof(struct arprequest)) 
    {
     pkttype =( (Int16)(rx.packet[12]<< 8)  |(Int16)(rx.packet[13])   );   /*This BIG byte shifts right 8*/             
    } 
#else
    if (nic.packetlen >= ETH_HLEN+sizeof(struct arprequest)) {
    	 pkttype =( (Int16)(nic.packet[12]<< 8)  |(Int16)(nic.packet[13])   );   /*This BIG byte shifts right 8*/             
    } 
#endif
    switch (pkttype) {
        //--------------------------------------------------------------------------
#ifdef DNI_PATCH
    	case htons(ETHER_NMRP):
         	nmrp_packet(rx); 
         	break;
#endif
	   	case htons(FRAME_ARP):
			// for debug			
			//              dprintf("rx arp packet\n");	//wei add

			/*keep parsing, check the opcode is request or reply*/
#ifdef DNI_PATCH
			arppacket = (struct arprequest *)&rx.packet[ETH_HLEN];
#else			
			arppacket = (struct arprequest *)&nic.packet[ETH_HLEN];
#endif
			/*Parse the opcode, 01->req, 02 ->reply*/ 	
			arpopcode = arppacket->opcode;
						  
            switch(arpopcode) {
                case htons(ARP_REQUEST):     														
				    // check dst ip, david+2007-12-26											
                    if (!memcmp(arppacket->tipaddr, &arptable_tftp[TFTP_SERVER].ipaddr, 4)
#if defined(HTTP_SERVER)/*for httpd*/
                        || !memcmp(arppacket->tipaddr, &arptable_tftp[HTTPD_ARPENTRY].ipaddr, 4)
#endif
                        )
#ifdef DNI_PATCH
						doARPReply(rx); 
#else						 
                        kick_event= BOOT_EVENT0_ARP_REQ;    
#endif
				
                    //doARPReply();	//wei add
                    //jump = (void *)(*BootStateEvent[bootState][BOOT_EVENT0_ARP_REQ]);
		            //jump();
                    break;
                case htons(ARP_REPLY):
#ifdef DNI_PATCH
						updateARPTable(rx);  
#else				                      
                    kick_event= BOOT_EVENT1_ARP_REPLY;                 
#endif
                         //jump =(*BootStateEvent[bootState][BOOT_EVENT1_ARP_REPLY]);
                         //jump();
                    break;
            }
            //wei del
#ifndef DNI_PATCH			
            if (kick_event!=NUM_OF_BOOT_EVENTS) {
                jump = (void *)(*BootStateEvent[bootState][kick_event]);
                jump();
            }
#endif			
            break;/*ptype=ARP*/

        //--------------------------------------------------------------------------	
        case htons(FRAME_IP):
            //dprintf("rx ip packet\n");	//wei add
#ifdef DNI_PATCH
             ipheader = (struct iphdr *)&rx.packet[ETH_HLEN];
#else			
            ipheader = (struct iphdr *)&nic.packet[ETH_HLEN];
#endif
            // word alignment
            //Cyrus Dick
            ip_addr.ip[0] = ipheader->dest.ip[0];
            ip_addr.ip[1] = ipheader->dest.ip[1];
            ip_addr.ip[2] = ipheader->dest.ip[2];
            ip_addr.ip[3] = ipheader->dest.ip[3];
            //source_ip_addr.ip[0] = ipheader->src.ip[0];
            //source_ip_addr.ip[1] = ipheader->src.ip[1];
            //source_ip_addr.ip[2] = ipheader->src.ip[2];
            //source_ip_addr.ip[3] = ipheader->src.ip[3];
            //Cyrus Dick
            /*Even type is IP, but the total payload must at least UDPH+IPH*/
#ifdef DNI_PATCH
            if (rx.packetlen > UDPIPETHheader) {
#else
            if (nic.packetlen > UDPIPETHheader) {
#endif
                /*keep parsing, check the TCP/UDP, here is meaningful*/
                if (ipheader->verhdrlen==0x45) {
                    //Cyrus Dick
                    /*check the destination ip addr*/
                    if (ip_addr.s_addr==arptable_tftp[TFTP_SERVER].ipaddr.s_addr 
#if defined(DHCP_SERVER)/*for DHCP dst ip  broadcast*/
                        || ip_addr.s_addr == 0xFFFFFFFF 
#endif
#if defined(HTTP_SERVER)/*for httpd*/
                        || ip_addr.s_addr  == arptable_tftp[HTTPD_ARPENTRY].ipaddr.s_addr 
#endif
                        ) {
                        //if(source_ip_addr.s_addr==arptable_tftp[TFTP_CLIENT].ipaddr.s_addr)
                        //Cyrus Dick
                        if (!ipheader_chksum((Int16*)ipheader,sizeof(struct iphdr))) {
                            if (ipheader->protocol==IP_UDP) {                                                 
                                /*udpheader = (struct udphdr *)&nic.packet[ETH_HLEN+ sizeof(struct iphdr)];*/
#ifdef DHCP_SERVER
#ifdef DNI_PATCH
                                udpheader = (struct udphdr *)&rx.packet[ETH_HLEN+ sizeof(struct iphdr)];
#else
                                udpheader = (struct udphdr *)&nic.packet[ETH_HLEN+ sizeof(struct iphdr)];
#endif
                                if (/*DHCP server port*/67 == ntohs(udpheader->dest)) {
                                    dhcps_input();
                                    return;
                                }
#endif
                                /*All we care is TFTP protocol, no other  protocol*/
#ifdef DNI_PATCH
                                tftppacket = (struct tftp_t *)&rx.packet[ETH_HLEN];
#else
                                tftppacket = (struct tftp_t *)&nic.packet[ETH_HLEN];
#endif
                                tftpopcode  = tftppacket->opcode;      
                                switch (tftpopcode) {
                                    case htons(TFTP_RRQ):
#ifndef DNI_PATCH									
                                        if (one_tftp_lock==0)
                                            kick_event= BOOT_EVENT2_TFTP_RRQ;                 
#endif
                                        break;                     
                                    case htons(TFTP_WRQ):
#ifdef RTL8197B
                                        if (one_tftp_lock==0) {
                                            kick_event = BOOT_EVENT3_TFTP_WRQ; 
                                            rx_kickofftime = get_timer_jiffies(); //wei add
                                        }
                                        else {
                                           // prom_printf("TFTP_WRQ: one_tftp_lock=%d block_expected=%d\n",one_tftp_lock, block_expected);
                                            //fix TFTP WRQ retransmit issue and add timout mechanism for second TFTP WRQ coming issue
                                            if ((block_expected == 1) || ((get_timer_jiffies() - rx_kickofftime) > 2000)) { //wait 20sec, unit is 10ms
                                                kick_event = BOOT_EVENT3_TFTP_WRQ;
                                                rx_kickofftime = get_timer_jiffies();
                                            }
                                        }
#else
                                        if (one_tftp_lock==0)
#ifdef DNI_PATCH
											setTFTP_WRQ(rx); 
#else										
                                            kick_event = BOOT_EVENT3_TFTP_WRQ; 
#endif
#endif
                                        //setTFTP_WRQ();
                                        break;
                                    case htons(TFTP_DATA):
                                        // for debug
#ifdef DNI_PATCH
										prepareACK(rx);
#else										
                                        kick_event= BOOT_EVENT4_TFTP_DATA;
#endif
#ifdef RTL8197B
                                        rx_kickofftime = get_timer_jiffies();
#endif
                                        //	prepareACK();
                                        break;
                                    case htons(TFTP_ACK):
#ifndef DNI_BOOTCODE_DOWNSIZE
#ifdef DNI_PATCH
										prepareDATA(rx);
#else									
                                        kick_event= BOOT_EVENT5_TFTP_ACK;                 
#endif
#endif  //#ifndef DNI_BOOTCODE_DOWNSIZE
                                        break; 
                                    case htons(TFTP_ERROR):
#ifdef DNI_PATCH
										errorTFTP();
#else									
                                        kick_event= BOOT_EVENT6_TFTP_ERROR;                 
#endif
                                        break;
                                    case htons(TFTP_OACK):
#ifdef DNI_PATCH
										errorTFTP();
#else									
                                        kick_event= BOOT_EVENT7_TFTP_OACK;                 
#endif
                                        break;
                                }
#ifndef DNI_PATCH
                                if (kick_event!=NUM_OF_BOOT_EVENTS) {
                                    jump = (void *)(*BootStateEvent[bootState][kick_event]);
                                    jump();
                                }
#endif								
                            }/*UDP packet,all TFTP case.*/
#ifdef HTTP_SERVER
                            else if (IP_TCP == ipheader->protocol) {
    		  	                tcpinput();
                            }
#endif
                        }
                    }
                }
            }
            break;/*ptype=IP*/ 
    }
}
//----------------------------------------------------------------------------------------
Int16 ipheader_chksum(Int16*ip,int len)
{
 Int32 sum = 0;
 len >>= 1;
 while (len--)
 {
  sum += *(ip++);
  if (sum > 0xFFFF)
  sum -= 0xFFFF;
 }                           /*Correct return 0*/
 return((~sum) & 0x0000FFFF);/*only 2 bytes*/
}

//----------------------------------------------------------------------------------------
#if 0
static void oackTFTP(void)
{
 /*According to RFC1782*/
 /*A server does not support options, it ignores them*/
 /*Server will return a DATA for a RRQ and an ACK for a WRQ*/
}
#endif

