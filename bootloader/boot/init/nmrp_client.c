#include "nmrp.h"
#include "etherboot.h"
#include <nic.h>
//#include <asm/rtl865x.h>
//#include <rtl_types.h>
#if defined(RTL8196B)	
#include <asm/rtl8196.h>
#endif
#if defined(RTL8198)	
#include <asm/rtl8198.h>
#endif
#include "rtl_depend.h"

int nmrp_packet_recv = 0;
bool nmrp_tftp_wait=false;
bool nmrp_tftp_success=true;
bool nmrp_tftp_runing = false;
extern int power_led_blink_slow; /* add by alfa */
extern int power_led_blink_quick; /* add by alfa */

unsigned long image_dst , image_src ,image_len;

#ifdef DNI_NMRP_V2  //dvd.chen, for new NMRP
//#define SUPPORT_SPI_MIO_8198_8196C  //MUST
int NmrpFwUPOption = 0;
int NmrpSTUPOption = 0;
unsigned int NmrpPeerReqStMask = 0;
int UploadingStringTableIdx = 0;
int TFTPFromNMRP = 0;
int NmrpDoFlashWriteFw = 0;
static unsigned short NmrpDevRegionOption = 0;
#endif

ip_route_t nmrp_server_info;
unsigned char *in_nmrp_pkt;
extern char eth0_mac[6];
extern struct arptable_t  arptable_tftp[3];
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			extern int spi_flw_image_mio_8198(unsigned int cnt, unsigned int flash_addr_offset , unsigned char *image_addr, unsigned int image_size, unsigned int flag);
		#else
			extern int spi_flw_image(unsigned int chip, unsigned int flash_addr_offset ,unsigned char *image_addr, unsigned int image_size);
		#endif
#else
extern int flashwrite(unsigned long dst, unsigned long src, unsigned long length);
#endif	
eth_header_t eth_hdr;
bool nmrp_packet_flag = 0 ;
 
typedef enum {
	NMRPC_NO_ERROR = 0,
	NMRPC_INVALID_PARM
} NMRPC_RET_CODE;


typedef struct {
	int reqCode;
	char reqName[32], respName[32];

} NMRP_REQ_CUSTOM;


#define MIN_ETHER_NMRP_LEN	(ETH_HLEN + NMRP_HDR_LEN)


#define IS_NMRP_PKT(etherNmrpPkt, pktLen, parsedNmrpMsg, sourceMAC) \
	(pktLen > MIN_ETHER_NMRP_LEN &&  \
	(sourceMAC == NULL ||  0 == 0) && \
	NMRP_MsgParsing(etherNmrpPkt, pktLen, parsedNmrpMsg) == NMRP_ERR_NONE)

static bool NMRPConfiguring( char *destMac,bool *fwUpgrade);
static bool NMRPClosing( char *serverMac);

bool NMRPTFTPWaiting(char *destMac);
extern struct nic nic;

int nmrp_status = 0 ;

NMRP_PARSED_OPT *NMRP_MsgGetOpt(NMRP_PARSED_MSG *msg, int optType)
{
	NMRP_PARSED_OPT *opt, *optEnd;
	int i=0;



	for (; i< msg->numOptions; i++)
	{
		if (msg->options[i].type == optType)
			break;
		
	}

	if (i == msg->numOptions || msg->numOptions == 0)
		return 0;
	else 
		return &(msg->options[i]);
}
static bool IsIPAddrValid(unsigned int ipAddr, unsigned int netmask)
{
	unsigned int tmp, mask;

	
	if (ipAddr == 0 || netmask == 0 || (ipAddr & ~netmask) == 0 || (ipAddr & ~netmask) == (0xffffffff & ~netmask))
		return false;

	return true;
}
static int NMRP_OptValParsing(NMRP_PARSED_OPT *optParsed, unsigned char* value)
{
	int retVal = NMRP_ERR_NONE;

	switch (optParsed->type) {
		case NMRP_OPT_MAGIC_NO: /* We require the lenght MUST be correct for the MAGIC-NO option */
			if (optParsed->len != (NMRP_MIN_OPT_LEN + NMRP_MAGIC_NO_LEN) || memcmp(value, NMRP_MAGIC_NO, NMRP_MAGIC_NO_LEN))
				retVal = NMRP_ERR_MSG_INVALID_OPT;
			else
				optParsed->value.streamVal = value;
			break;

		case NMRP_OPT_DEV_IP: /* save the value of IP in the byte stream format, the actual IP is streamVal[0].streamVal[1].streamVal[2].streamVal[3]. */
			//if (  !IsIPAddrValid(ntohl(*(unsigned int *) value), ntohl(*(unsigned int *) (value + 4))) )
			//	retVal = NMRP_ERR_MSG_INVALID_OPT;
			//else
				//prom_printf("##NMRP_OptValParsing:: DEV_IP parsed\n");
				optParsed->value.streamVal = value;
			break;

#ifdef DNI_NMRP_V2  //dvd.chen, for new NMRP
		case NMRP_OPT_DEV_REGION: /* save the value of device region in the byte stream format. */
			//prom_printf("##NMRP_OptValParsing:: DEV_REGION parsed, value[0x%04x]\n", *((unsigned short *)value));
			optParsed->value.wordVal = *((unsigned short *)value);
			break;

		case NMRP_OPT_ST_UP: /* don't check the length of FW-UP option for future compatibility */
			//prom_printf("##NMRP_OptValParsing:: ST_UP parsed, value[0x%02x%02x%02x%02x]\n", *value, *(value+1), *(value+2), *(value+3));
			//optParsed->value.dwordVal = *((unsigned int *)value);
			memcpy(&(optParsed->value.dwordVal), value, 4);
			break;
#endif

		case NMRP_OPT_FW_UP: /* don't check the length of FW-UP option for future compatibility */
			//prom_printf("##NMRP_OptValParsing:: FW_UP parsed\n");
			break;

		default:
			retVal = NMRP_ERR_MSG_UNKNOWN_OPT;
			break;
	}

	return retVal;
}

int NMRP_MsgParsing( unsigned char *pkt, int pktLen, NMRP_PARSED_MSG *msg)
{
	if (pktLen < NMRP_HDR_LEN)
		return NMRP_ERR_MSG_INVALID_LEN;

	NMRP_MSG *pkt_nmpr=pkt;
	msg->code = pkt_nmpr->code;
	msg->id = pkt_nmpr->id;
	
	//prom_printf("code = %x id = %x len = %x \n",pkt_nmpr->code, pkt_nmpr->id,pkt_nmpr->length);
	
	if ((msg->length = ntohs(pkt_nmpr->length)) <= pktLen && msg->length >= (NMRP_HDR_LEN)) {
		int retVal;
		int optLen=0;
		NMRP_OPT *opt;
		NMRP_PARSED_OPT *optParsed;

		optParsed = msg->options;
		msg->numOptions = 0;

		pkt +=NMRP_HDR_LEN;
		/* use the length indicated in the NMRP header */
		//modified by dvd.chen
		//for (pktLen = msg->length - NMRP_HDR_LEN; pktLen >= NMRP_MIN_OPT_LEN; ) {
		pktLen = msg->length - NMRP_HDR_LEN;
		while( pktLen >= NMRP_MIN_OPT_LEN ) {
			optParsed->type = ntohs(*(unsigned short *)(pkt+optLen));
			optParsed->len = ntohs(*(unsigned short *)(pkt+optLen+2));

			//prom_printf("NMRP_MsgParsing-1::pktLen[%d], optParsed->type[%d], optParsed->len[%d], optLen[%d]\n",  
			//				pktLen, optParsed->type, optParsed->len, optLen);	
			
			if (optParsed->len > pktLen)
				return NMRP_ERR_MSG_INVALID_OPT;

			//modified by dvd.chen, option could be 4 (only header, no option-value)
			//if(optParsed->len > 4)
			if(optParsed->len >= 4)
			{
				if (((retVal = NMRP_OptValParsing(optParsed, (pkt+optLen+4))) == NMRP_ERR_NONE)) {
					//marked off by dvd.chen, increment here is WRONG WRONG WRONG !!!
					//optParsed++;
					if (++msg->numOptions >= NMRP_MAX_OPT_PER_MSG)
						return NMRP_ERR_MSG_TOO_MANY_OPT;
				}
				else if (retVal == NMRP_ERR_MSG_UNKNOWN_OPT)
					return NMRP_ERR_MSG_INVALID_OPT;

			}
			pktLen -= optParsed->len;
			optLen +=optParsed->len;
			optParsed++;
			//prom_printf("NMRP_MsgParsing-2::pktLen[%d], optLen[%d]\n\n", pktLen, optLen);
		}
	} /* end if the msg->length is larger than the received packet length */

	//return pktLen == 0 ? NMRP_ERR_NONE : NMRP_ERR_MSG_INVALID_LEN;
	return NMRP_ERR_NONE;
}

bool NMRPRequest( char *destMac, NMRP_REQ_CUSTOM *custom, bool *userData)
{
	int pktLen, retries,retry_start,max_retries;
	//pktbuf_t *pkt;
	NMRP_MSG *msg;
	unsigned char rec_pkt[80];
	//eth_header_t eth_hdr;
	unsigned int configip,configmask;
	int status;
    	struct nic rx;
#ifdef DNI_NMRP_V2 //dvd.chen, for new NMRP
	NMRP_PARSED_MSG nmrpMsg;
#endif
  
	if(custom->reqCode == NMRP_CODE_CONF_REQ )
		max_retries = NMRP_MAX_RETRY_CONF;
	else if (custom->reqCode == NMRP_CODE_CLOSE_REQ )
		max_retries = NMRP_MAX_RETRY_CLOSE;
	
	if(custom->reqCode == NMRP_CODE_TFTP_UL_REQ)
	{
#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP, we nee more space for option
		if(NULL==(msg=(NMRP_MSG *)malloc(NMRP_HDR_LEN)))
#else
		//to send TFTP_UL_REQ, we need at most (NMRP_HDR_LEN + STRING_TABLE_FILENAME_OPT_LEN) == 25 bytes
		if(NULL==(msg=(NMRP_MSG *)malloc(32)))
#endif
		 { 
		   	nmrp_tftp_success = 0 ;
		    return nmrp_tftp_success ;
		 }
		 msg->reserved = 0;
		 msg->code =custom->reqCode;
		 msg->id = 0;
#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP
		 msg->length = htons(NMRP_HDR_LEN);
		 prepare_txpkt(0,ETHER_NMRP,destMac,(Int8*)msg,NMRP_HDR_LEN);
#else
		if (NmrpSTUPOption == 1) 
		{
			//prom_printf("send TFTP_UL_REQ with [string table 01]\n");
			msg->length = htons(NMRP_HDR_LEN + STRING_TABLE_FILENAME_OPT_LEN);
			msg->opt.type = NMRP_OPT_FILE_NAME;
			msg->opt.len = STRING_TABLE_FILENAME_OPT_LEN;
			//note by dvd.chen, we now support only 1 string table
			//To handle more string tables, we should use bitmask as NmrpSTUPOption for determining which table to request
			if( (NmrpPeerReqStMask & 0x00000001) )
			{
				prom_printf("send TFTP_UL_REQ with [string table 01]\n");
				memcpy(msg->opt.value, "string table 01", STRING_TABLE_FILENAME_LEN);
				NmrpPeerReqStMask &= ~(0x00000001);
				UploadingStringTableIdx = 1;
			}
			else if( (NmrpPeerReqStMask & 0x00000002)  )
			{
				prom_printf("send TFTP_UL_REQ with [string table 02]\n");
				memcpy(msg->opt.value, "string table 02", STRING_TABLE_FILENAME_LEN);
				NmrpPeerReqStMask &= ~(0x00000002);
				UploadingStringTableIdx = 2;
			}
			
			/* No string table updates, or all string table updates finished.
			 * And received FW-UP option, upgrading firmware,
			 * add FILE-NAME option to TFTP-UL-REQ */
		} else
		{
			prom_printf("send TFTP_UL_REQ with [%s]\n", FIRMWARE_FILENAME);
			msg->length = htons(NMRP_HDR_LEN + FIRMWARE_FILENAME_OPT_LEN);
			msg->opt.type = NMRP_OPT_FILE_NAME;
			msg->opt.len = FIRMWARE_FILENAME_OPT_LEN;
			memcpy(msg->opt.value, FIRMWARE_FILENAME, FIRMWARE_FILENAME_LEN);
		}
		prepare_txpkt(0,ETHER_NMRP,destMac,(Int8*)msg, ntohs(msg->length));
#endif
		 free(msg);
		 
		 nmrp_tftp_wait=true;
		 nmrp_tftp_runing = true;
		
		 tftpd_entry();
		 
         if(nmrp_tftp_success)
            prom_printf("NMRP TFTP Server success \n");
         return nmrp_tftp_success;
	
	}
 	
    if(NULL==(msg=(NMRP_MSG *)malloc(NMRP_HDR_LEN)))
			return -1 ;
	msg->reserved = 0;
	msg->code = custom->reqCode;
	msg->id = 0;
	msg->length = htons(NMRP_HDR_LEN); /* header only, no option */
	retries = 0;

	/* send the packet */
		
	do{
		prepare_txpkt(0,ETHER_NMRP,destMac,(Int8*)msg,NMRP_HDR_LEN);	
		retry_start = get_timer_jiffies();
		do{
			pktLen = 0;
			 
            	if((status=polling_rx()) == 1)
 	     	{
			rx.packet = mac_rx_buff[buff_rd_index].packet;
 	           	rx.packetlen = mac_rx_buff[buff_rd_index].packet_size;
               	++buff_rd_index;
               	if(buff_rd_index > ( MAX_RX_BUFF-1 ))
                  		buff_rd_index = 0 ;
               	kick_tftpd(rx); 
               	buff_bz_num--;
                
               	if(nmrp_packet_flag == 1)
		 	{
			       pktLen = rx.packetlen;
			       nmrp_packet_flag = 0;
		  	}  	
		}
		
		if ( pktLen > 0 && memcmp(&(eth_hdr.source),destMac,6) ==0 && memcmp(&(eth_hdr.destination),eth0_mac,6)==0)
		{
				
			NMRP_MSG 	*nmrp_pkt=(NMRP_MSG *)in_nmrp_pkt;
			int opt_len=nmrp_pkt->length - NMRP_HDR_LEN;
			NMRP_PARSED_OPT *pase_opt;
			if(custom->reqCode==NMRP_CODE_CONF_REQ )
			{	
			  	if(nmrp_pkt->code == NMRP_CODE_CONF_ACK)
				{
					prom_printf("NMRP Config Ack received \n"); 
			#ifndef DNI_NMRP_V2 //dvd.chen, for new NMRP
					pase_opt = (NMRP_PARSED_OPT *)(in_nmrp_pkt+NMRP_HDR_LEN);
					if(pase_opt->type == NMRP_OPT_DEV_IP)
			#else
					if (IS_NMRP_PKT(in_nmrp_pkt, pktLen, &nmrpMsg, NULL) )//&& nmrpMsg.code == NMRP_CODE_ADVERTISE &&  NMRP_MsgGetOpt(&nmrpMsg, NMRP_OPT_MAGIC_NO) != NULL)
					{
						if( (pase_opt = NMRP_MsgGetOpt(&nmrpMsg, NMRP_OPT_DEV_IP)) != NULL )
			#endif
						{
						//	prom_printf("NMRP_OPT_DEV_IP\n"); 
							memcpy(&configip,(in_nmrp_pkt+NMRP_HDR_LEN +4),4);
							memcpy(&configmask,(in_nmrp_pkt+NMRP_HDR_LEN +8),4);
							if(IsIPAddrValid(configip,configmask))
							{
								memcpy(&(arptable_tftp[TFTP_SERVER].ipaddr.s_addr), (in_nmrp_pkt+NMRP_HDR_LEN +4), 4);
								prom_printf("NMRP set tftp-server IP success\n");
							}
							else 
								continue;
					}
			#ifndef DNI_NMRP_V2	//dvd.chen for new NMRP supporting DEV-REGION and ST-UP
						else
							continue;
						opt_len -=pase_opt->len;
						if(opt_len >= 4 && (*(unsigned short *)(in_nmrp_pkt+NMRP_HDR_LEN+pase_opt->len))==NMRP_OPT_FW_UP )
							*userData = true;
			#else
						if( (pase_opt = NMRP_MsgGetOpt(&nmrpMsg, NMRP_OPT_DEV_REGION)) != NULL )
						{
							/* Save DEV-REGION value to board */
							prom_printf("Get DEV-REGION option, value[0x%04x]\n", pase_opt->value.wordVal);
							NmrpDevRegionOption = ntohs(pase_opt->value.wordVal);
							//prom_printf("Write Region Number 0x%04x to board\n", NmrpDevRegionOption);
							//set_region(NmrpDevRegionOption);
							NmrpSetRegion(NmrpDevRegionOption);  //pass 99 to indicate it is from NRMP
						}
						else
							prom_printf("No DEV-REGION option\n");
							
						if( (pase_opt = NMRP_MsgGetOpt(&nmrpMsg, NMRP_OPT_FW_UP)) != NULL )
						{
							prom_printf("Recv FW-UP option\n");
							*userData = true;  //indicating that need to do TFTP later
							NmrpFwUPOption = 1;	
						}
						else
						{
							prom_printf("No FW-UP option\n");
							NmrpFwUPOption = 0;
						}

						if( (pase_opt = NMRP_MsgGetOpt(&nmrpMsg, NMRP_OPT_ST_UP)) != NULL )
						{
					#if SUPPORT_MORE_STRING_TABLES
							unsigned int us_max_st_mask=0;
							int shift_bit, i;

							NmrpPeerReqStMask = ntohl(pase_opt->value.dwordVal);
							shift_bit = MAX_STRING_TABLE_NUM-1;
							for(i=0; i<MAX_STRING_TABLE_NUM; i++)
							{
								us_max_st_mask += 1 << shift_bit;
								shift_bit--;
							}
							prom_printf("Recv ST-UP option, req_mask[%08x], us_max_mask[%08x]\n", NmrpPeerReqStMask, us_max_st_mask);
							if( (NmrpPeerReqStMask & (~us_max_st_mask)) == 0)
							{
								*userData = true; //indicating that need to do TFTP later
								NmrpSTUPOption = 1;
							}
							else
							{
								prom_printf("REQ ST number out of supported range, won't update ST !\n");
								NmrpSTUPOption = 0;
							}
					#else
							//we now support 2 strting tables only, so the req_mask must be 1 
							NmrpPeerReqStMask = ntohl(pase_opt->value.dwordVal);
							prom_printf("Recv ST-UP option, req_mask[%08x]\n", NmrpPeerReqStMask);
							//if( NmrpPeerReqStMask == 0x00000001 )
							if( (NmrpPeerReqStMask & 0x00000001) || (NmrpPeerReqStMask & 0x00000002) )
							{
								*userData = true; //indicating that need to do TFTP later
								NmrpSTUPOption = 1;
							}
							else
							{
								prom_printf("REQ ST number out of supported range, won't update ST !\n");
								NmrpSTUPOption = 0;
							}
					#endif
						}
						else
						{
							prom_printf("No ST-UP option\n");
							NmrpSTUPOption = 0;
						}
					}
			#endif
					free(msg);
					return true;				 
				}
				else
					continue;
			}	
			else if (custom->reqCode==NMRP_CODE_CLOSE_REQ)
			 {
				if(nmrp_pkt->code == NMRP_CODE_CLOSE_ACK)
				{
					free(msg);
					return true;
				}
				else
					continue;
			 }					
		}
	   }while ( (get_timer_jiffies() - retry_start) < 50  );  // 0.5 sec
	} while ((++retries) <= max_retries);
	free(msg);
	return false;
}

#ifdef DNI_NMRP_V2  //dvd.chen, for new NMRP
bool NMRPSendKeepAliveREQ()
{
	NMRP_MSG *msg;
	
	if(NULL==(msg=(NMRP_MSG *)malloc(NMRP_HDR_LEN)))
	{ 
	   	//nmrp_tftp_success = 0 ;
		return 0;
	}
	msg->reserved = 0;
	msg->code =NMRP_CODE_KEEP_ALIVE_REQ;
	msg->id = 0;
	msg->length = htons(NMRP_HDR_LEN);
	//prom_printf("##sending NMRP_CODE_KEEP_ALIVE_REQ\n");
	prepare_txpkt(0,ETHER_NMRP,&(nmrp_server_info.enet_addr),(Int8*)msg,NMRP_HDR_LEN);
	//nmrp_tftp_ul_req_time = get_timer_jiffies();
	free(msg); 

	return 1;
}
#endif

#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP
static bool NMRPConfiguring( char *destMac, bool *fwUpgrade)
#else
static bool NMRPConfiguring( char *destMac, bool *doTftpUpgrade)
#endif
{
	NMRP_REQ_CUSTOM reqConf = { NMRP_CODE_CONF_REQ, "CONF-REQ", "CONF-ACK"};

#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP
	if (NMRPRequest(destMac, &reqConf, fwUpgrade) == false)
#else
	if (NMRPRequest(destMac, &reqConf, doTftpUpgrade) == false)
#endif
		return false;

	return true;
}

bool NMRPTFTPWaiting(char *destMac)
{
        NMRP_REQ_CUSTOM reqTftpUl = { NMRP_CODE_TFTP_UL_REQ, "TFTP-UL-REQ", "TFTP-WRQ"};

        return NMRPRequest(destMac, &reqTftpUl, NULL);
}

static bool NMRPClosing(char *serverMac)
{
	NMRP_REQ_CUSTOM reqClose = { NMRP_CODE_CLOSE_REQ, "CLOSE-REQ", "CLOSE-ACK"};

	return NMRPRequest( serverMac, &reqClose, NULL);
}

void nmrp_packet(struct nic rx)
{
  in_nmrp_pkt = (unsigned char *)&rx.packet[ETH_HLEN];
  memcpy(&(eth_hdr.destination),&rx.packet[0],sizeof(eth_header_t));
  nmrp_packet_flag = 1;
}

extern volatile int get_timer_jiffies(void);

bool nmrp_entry(void)
{
 bool succeeds,fwUpgrade;
 int listening_start,packet_wait;
 NMRP_PARSED_MSG nmrpMsg;
 int pktLen,status;
 struct nic rx;
 bool listening = true ;
 bool ipset = false;
 succeeds = false;
 bool ret=true, doTftpUpgrade=0;
  
 while(!succeeds) 
	{
		prom_printf("\n");
     if (listening) 
		{ //##### Listening state #####
		 prom_printf("NMRP Client starts Listening for ADVERTISE...\n"); 
		 listening_start=get_timer_jiffies();
		 do{	
		    pktLen = 0;
		    if((status=polling_rx()) == 1)
 	          {
 	       	   rx.packet = mac_rx_buff[buff_rd_index].packet;
 	           rx.packetlen = mac_rx_buff[buff_rd_index].packet_size;
               ++buff_rd_index;
               if(buff_rd_index > ( MAX_RX_BUFF-1 ))
                  buff_rd_index = 0 ;
               kick_tftpd(rx); 
               buff_bz_num--;
                
               if(nmrp_packet_flag == 1)
			     {
			      pktLen = rx.packetlen;
			      nmrp_packet_flag = 0;
		         }  	
               }
     
			if ( pktLen > 0 )
				{
					if (IS_NMRP_PKT(in_nmrp_pkt, pktLen, &nmrpMsg, NULL) && nmrpMsg.code == NMRP_CODE_ADVERTISE &&  NMRP_MsgGetOpt(&nmrpMsg, NMRP_OPT_MAGIC_NO) != NULL)
					{
						listening = false;
						break;
					}
				}
		    }while ( (get_timer_jiffies() - listening_start) < 400  );  // 4 sec
			if (listening)
			{
				prom_printf("NMRP timeoust exit;");
				return false;
			}
			else
			{
				ipset = false;
				memcpy(&(nmrp_server_info.enet_addr),&(eth_hdr.source),sizeof(enet_addr_t));
				prom_printf("NMRP advertise received.\n");
			}
		  }	
		   if ( !ipset ) { //##### Configuring state #####
			prom_printf("NMRP client Configuring Request send\n");
#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP
			if (NMRPConfiguring(&nmrp_server_info.enet_addr, &fwUpgrade) == false) {
#else
			if (NMRPConfiguring(&nmrp_server_info.enet_addr, &doTftpUpgrade) == false) {
#endif
				listening = true;
				prom_printf("timeouts...Back to Listening state.\n");
				continue;
			}
			
#ifdef DNI_NMRP_V2  //for new NMRP
			//fwUpgrade = false;  //temply for not downloading firmware image.
			if( doTftpUpgrade )
			{
				// 1. get string table(s); 2. get firmware image
				while (ret)
				{
					TFTPFromNMRP = 1;
					//prom_printf("\n\n##calling NMRPTFTPWaiting\n");
					ret = NMRPTFTPWaiting(&(nmrp_server_info.enet_addr));
					if( NmrpFwUPOption == 0 && NmrpSTUPOption ==0 )
					{
						//NmrpSTUPOption and NmrpFwUPOption are processed
						//prom_printf("##nmrp_entry:: all tftp got files all burn into flash\n");
						break;
					}
				} 
				TFTPFromNMRP = 0;
				
				if( ret == false )
				{
					prom_printf("timeouts...Back to tftp waiting state.\n");
					continue;
				}
			
				//then .....
				nmrp_tftp_wait = false;
			}
			
			
			prom_printf("NMRP Client Close Request send\n");
			if (NMRPClosing(&(nmrp_server_info.enet_addr)) == false)
				prom_printf("NMRP timeouts...exits without CLOSE-ACK.\n");
			else
				prom_printf("NMRP received CLOSE-ACK.\nNMRP Client done, exits.\n\n");

			if( NmrpDoFlashWriteFw )
			{
				prom_printf("#NMRP writting FW to flash, burn Addr =0x%x! srcAddr=0x%x len =0x%x \n", image_dst, image_src, image_len);
				
#ifdef CONFIG_SPI_FLASH
	#ifdef SUPPORT_SPI_MIO_8198_8196C
				if(spi_flw_image_mio_8198(0, image_dst, image_src, image_len, 0))
	#else
				if(spi_flw_image(0, image_dst, image_src, image_len))
	#endif
#else
				if (flashwrite(image_dst, image_src, image_len))
#endif
		       	{
			       	 prom_printf("\nNMRP write firmware to flash finished\n");	
			       	 power_led_blink_slow = 0;
			       	 power_led_blink_quick = 1;
			       	 while(1);
		       	}	 
		    		else
		        	{
		         		prom_printf("NMRP write firmware to flash fail\n");	
		         		nmrp_tftp_wait=false;
		                 	nmrp_tftp_success=true;
		                 	nmrp_tftp_runing = false;	  
					NmrpDoFlashWriteFw = 0;
		                 	continue;
                		} 
			}
			succeeds = true;
#endif
			//prom_printf("NMRP IP setting success\n");
			ipset = true;
		}

#ifndef DNI_NMRP_V2  //dvd.chen, for new NMRP
		if (fwUpgrade) 
		{

			prom_printf("NMRP Client TFTP Upload Request send\n");
			nmrp_tftp_success = true;
			
			if (NMRPTFTPWaiting(&(nmrp_server_info.enet_addr)) == false) { //##### TFTP-Waiting state #####
				prom_printf("timeouts...Back to tftp waiting state.\n");
				continue;
			}
			
            prom_printf("NMRP Client Close Request send\n");
			if (NMRPClosing(&(nmrp_server_info.enet_addr)) == false)
				prom_printf("NMRP timeouts...exits without CLOSE-ACK.\n");
			else
				prom_printf("NMRP received CLOSE-ACK.\nNMRP Client done, exits.\n");
#ifdef CONFIG_SPI_FLASH
		#ifdef SUPPORT_SPI_MIO_8198_8196C
			if(spi_flw_image_mio_8198(0,image_dst, image_src, image_len, 0))
		#else
			if(spi_flw_image(0,image_dst, image_src, image_len))
		#endif
#else
		if (flashwrite(image_dst, image_src, image_len))
#endif
		       {
		       	 prom_printf("\nNMRP write firmware to flash finished\n");	
		       	 power_led_blink_slow = 0;
		       	 power_led_blink_quick = 1;
		       	 while(1);
		       }	 
		    else
		        {
		         prom_printf("NMRP write firmware to flash fail\n");	
		         nmrp_tftp_wait=false;
                 nmrp_tftp_success=true;
                 nmrp_tftp_runing = false;	  
                 continue;
                } 
			nmrp_tftp_wait = false;		
			succeeds = true;
			
		} 
#endif
	}
}			


