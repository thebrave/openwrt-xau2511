/*
 * FILE NAME rtl_gpio.c
 *
 * BRIEF MODULE DESCRIPTION
 *  GPIO For Flash Reload Default
 *
 *  Author: jimmylin@realtek.com.tw
 *
 * Copyright 2005 Realtek Semiconductor Corp.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  THIS  SOFTWARE  IS PROVIDED   ``AS  IS'' AND   ANY  EXPRESS OR IMPLIED
 *  WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 *  NO  EVENT  SHALL   THE AUTHOR  BE	LIABLE FOR ANY   DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 *  USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  You should have received a copy of the  GNU General Public License along
 *  with this program; if not, write  to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */

//#define CONFIG_USING_JTAG 1

#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <asm/errno.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/reboot.h>
#include <linux/kmod.h>
#include <linux/proc_fs.h>
//#include  "bspchip.h"
#define AUTO_CONFIG

// 2009-0414
//#define	DET_WPS_SPEC
#ifdef CONFIG_DNI_RTL8196C_FOR_DEMO_BOARD
#ifndef CONFIG_RTK_VOIP_DRIVERS_ATA_DECT //DECT SPI use GPIO E interrupt, need refine code to share irq.
#ifndef CONFIG_SERIAL_SC16IS7X0 //SC16IS7x0 use GPIO E interrupt, too.  
#define USE_INTERRUPT_GPIO
#endif
#endif
#endif

#if defined(CONFIG_RTL_8196C) || defined(CONFIG_RTL_8198)
#ifndef CONFIG_RTK_VOIP
	#define READ_RF_SWITCH_GPIO
#endif
#endif

#if defined(CONFIG_RTL_8196C) || defined(CONFIG_RTL_8198)
	#include "drivers/net/rtl819x/AsicDriver/rtl865xc_asicregs.h"
/*define the GPIO physical address to customer_gpio.h*/
#ifdef CONFIG_RTK_VOIP
	#if defined (CONFIG_RTK_VOIP_GPIO_8954C_V100)
		#define RESET_PIN_IOBASE PEFGH_CNR 		//RTL_GPIO_PEFGH_CNR
		#define RESET_PIN_DIRBASE PEFGH_DIR		//RTL_GPIO_PEFGH_DIR 
		#define RESET_PIN_DATABASE PEFGH_DAT 	//RTL_GPIO_PEFGH_DATA
		#define RESET_PIN_NO 7 					//pin number of the EFGH
		#define RESET_LED_IOBASE PEFCH_CNR 		//RTL_GPIO_PEFGH_CNR
		#define RESET_LED_DIRBASE PEFGH_DIR		//RTL_GPIO_PEFGH_DIR 
		#define RESET_LED_DATABASE PEFGH_DAT 	//RTL_GPIO_PEFGH_DATA
		#define RESET_LED_NO 16 				//pin number of the EFGH
		#define AUTOCFG_LED_IOBASE PEFGH_CNR 	//RTL_GPIO_PEFGH_CNR
		#define AUTOCFG_LED_DIRBASE PEFGH_DIR	//RTL_GPIO_PEFGH_DIR 
		#define AUTOCFG_LED_DATABASE PEFGH_DAT 	//RTL_GPIO_PEFGH_DATA
		#define AUTOCFG_LED_NO 17 				//pin number of the EFGH
		#define AUTOCFG_PIN_IOBASE PEFGH_CNR 	//RTL_GPIO_PEFGH_CNR
		#define AUTOCFG_PIN_DIRBASE PEFGH_DIR	//RTL_GPIO_PEFGH_DIR 
		#define AUTOCFG_PIN_DATABASE PEFGH_DAT  //RTL_GPIO_PEFGH_DATA
		#define AUTOCFG_PIN_NO 2 				//pin number of the EFGH)
		#define AUTOCFG_PIN_IMR PEF_IMR
		#define RTL_GPIO_MUX_DATA 0x00004300 	//MUX for GPIO
	#elif defined(CONFIG_RTK_VOIP_GPIO_8954C_V200)
		#define RESET_PIN_IOBASE PEFGH_CNR 		//RTL_GPIO_PEFGH_CNR
		#define RESET_PIN_DIRBASE PEFGH_DIR		//RTL_GPIO_PEFGH_DIR 
		#define RESET_PIN_DATABASE PEFGH_DAT 	//RTL_GPIO_PEFGH_DATA
		#define RESET_PIN_NO 12 				//pin number of the EFGH
		#define RESET_LED_IOBASE PEFGH_CNR 		//RTL_GPIO_PEFGH_CNR
		#define RESET_LED_DIRBASE PEFGH_DIR		//RTL_GPIO_PEFGH_DIR 
		#define RESET_LED_DATABASE PEFGH_DAT 	//RTL_GPIO_PEFGH_DATA
		#define RESET_LED_NO 7 					//number of the EFGH
		#define AUTOCFG_LED_IOBASE PEFGH_CNR 	//RTL_GPIO_PEFGH_CNR
		#define AUTOCFG_LED_DIRBASE PEFGH_DIR	//RTL_GPIO_PEFGH_DIR 
		#define AUTOCFG_LED_DATABASE PEFGH_DAT  //RTL_GPIO_PEFGH_DATA
		#define AUTOCFG_LED_NO 8 				//pin number of the EFGH
		#define AUTOCFG_PIN_IOBASE PEFGH_CNR 	//RTL_GPIO_PEFGH_CNR
		#define AUTOCFG_PIN_DIRBASE PEFGH_DIR	//RTL_GPIO_PEFGH_DIR 
		#define AUTOCFG_PIN_DATABASE PEFGH_DAT  //RTL_GPIO_PEFGH_DATA
		#define AUTOCFG_PIN_NO 11 				//pin number of the EFGH
		#define AUTOCFG_PIN_IMR PEF_IMR
		#define RTL_GPIO_MUX_DATA 0x00000300 	//MUX for GPIO
    #endif
	#define RTL_GPIO_MUX 0xB8000040
	#define AUTOCFG_BTN_PIN         AUTOCFG_PIN_NO
	#define AUTOCFG_LED_PIN         AUTOCFG_LED_NO
	#define RESET_LED_PIN           RESET_LED_NO
	#define RESET_BTN_PIN           RESET_PIN_NO
#else
#if defined(CONFIG_RTL_8196C)	
	#define RESET_PIN_IOBASE PABCD_CNR 	//RTL_GPIO_PABCD_CNR
	#define RESET_PIN_DIRBASE PABCD_DIR	//RTL_GPIO_PABCD_DIR 
	#define RESET_PIN_DATABASE PABCD_DAT //RTL_GPIO_PABCD_DATA
	#define RESET_PIN_NO 0 /*number of the ABCD*/

	#define RESET_LED_IOBASE PABCD_CNR 	//RTL_GPIO_PABCD_CNR
	#define RESET_LED_DIRBASE PABCD_DIR	//RTL_GPIO_PABCD_DIR 
	#define RESET_LED_DATABASE PABCD_DAT //RTL_GPIO_PABCD_DATA
	#define RESET_LED_NO 18 /*number of the ABCD*/

	#define AUTOCFG_LED_IOBASE PABCD_CNR 	//RTL_GPIO_PABCD_CNR
	#define AUTOCFG_LED_DIRBASE PABCD_DIR	//RTL_GPIO_PABCD_DIR 
	#define AUTOCFG_LED_DATABASE PABCD_DAT //RTL_GPIO_PABCD_DATA
	#define AUTOCFG_LED_NO 20 /*number of the ABCD*/

	#define AUTOCFG_PIN_IOBASE PABCD_CNR 	//RTL_GPIO_PABCD_CNR
	#define AUTOCFG_PIN_DIRBASE PABCD_DIR	//RTL_GPIO_PABCD_DIR 
	#define AUTOCFG_PIN_DATABASE PABCD_DAT //RTL_GPIO_PABCD_DATA
	#define AUTOCFG_PIN_NO 1 /*number of the ABCD)*/
	#define AUTOCFG_PIN_IMR PAB_IMR

#ifdef CONFIG_POCKET_ROUTER_SUPPORT
	#define RTL_GPIO_MUX_GPIOA2	(3<<20)
	#define RTL_GPIO_MUX_GPIOB3	(3<<2)
	#define RTL_GPIO_MUX_GPIOB2	(3<<0)	
	#define RTL_GPIO_MUX_GPIOC0	(3<<12)
	#define RTL_GPIO_MUX_POCKETAP_DATA	(RTL_GPIO_MUX_GPIOA2 | RTL_GPIO_MUX_GPIOB3 | RTL_GPIO_MUX_GPIOB2 | RTL_GPIO_MUX_GPIOC0)

	#define RTL_GPIO_CNR_GPIOA2	(1<<2)
	#define RTL_GPIO_CNR_GPIOB3	(1<<11)
	#define RTL_GPIO_CNR_GPIOB2	(1<<10)	
	#define RTL_GPIO_CNR_GPIOC0	(1<<16)
	#define RTL_GPIO_CNR_POCKETAP_DATA	(RTL_GPIO_CNR_GPIOA2 | RTL_GPIO_CNR_GPIOB3 | RTL_GPIO_CNR_GPIOB2 | RTL_GPIO_CNR_GPIOC0)

	#define RTL_GPIO_DIR_GPIOA2	(1<<2) /* &- */
	#define RTL_GPIO_DIR_GPIOB3	(1<<11) /* &- */
	#define RTL_GPIO_DIR_GPIOB2	(1<<10) /* |*/	
	#define RTL_GPIO_DIR_GPIOC0	(1<<16) /* &- */

	#define RTL_GPIO_DAT_GPIOA2	(1<<2) 
	#define RTL_GPIO_DAT_GPIOB3	(1<<11) 
	#define RTL_GPIO_DAT_GPIOB2	(1<<10) 	
	#define RTL_GPIO_DAT_GPIOC0	(1<<16) 

	static int ap_cli_rou_time_state[2] = {0};
	static char ap_cli_rou_state = 0;
	static char ap_cli_rou_idx=0;
	static char pocketAP_hw_set_flag='0';

	static char dc_pwr_plugged_time_state = 0;
	static char dc_pwr_plugged_state = 0;
	static char dc_pwr_plugged_flag = '0';

	static int pwr_saving_state=0;
	static char pwr_saving_led_toggle = 0;
#endif

	
#elif defined(CONFIG_RTL_8198)
	#define RESET_PIN_IOBASE PEFGH_CNR 	//RTL_GPIO_PABCD_CNR
	#define RESET_PIN_DIRBASE PEFGH_DIR	//RTL_GPIO_PABCD_DIR 
	#define RESET_PIN_DATABASE PEFGH_DAT //RTL_GPIO_PABCD_DATA
	#define RESET_PIN_NO 25 /*number of the ABCD*/

	#define RESET_LED_IOBASE PEFGH_CNR 	//RTL_GPIO_PABCD_CNR
	#define RESET_LED_DIRBASE PEFGH_DIR	//RTL_GPIO_PABCD_DIR 
	#define RESET_LED_DATABASE PEFGH_DAT //RTL_GPIO_PABCD_DATA
	#define RESET_LED_NO 27 /*number of the ABCD*/

	#define AUTOCFG_LED_IOBASE PEFGH_CNR 	//RTL_GPIO_PABCD_CNR
	#define AUTOCFG_LED_DIRBASE PEFGH_DIR	//RTL_GPIO_PABCD_DIR 
	#define AUTOCFG_LED_DATABASE PEFGH_DAT //RTL_GPIO_PABCD_DATA
	#define AUTOCFG_LED_NO 20 /*number of the ABCD*/

	#define AUTOCFG_PIN_IOBASE PEFGH_CNR 	//RTL_GPIO_PABCD_CNR
	#define AUTOCFG_PIN_DIRBASE PEFGH_DIR	//RTL_GPIO_PABCD_DIR 
	#define AUTOCFG_PIN_DATABASE PEFGH_DAT //RTL_GPIO_PABCD_DATA
	#define AUTOCFG_PIN_NO 1 /*number of the ABCD)*/
	#define AUTOCFG_PIN_IMR PGH_IMR
	
#endif
	#define WIFI_ONOFF_PIN_IOBASE PABCD_CNR 	//RTL_GPIO_PABCD_CNR
	#define WIFI_ONOFF_PIN_DIRBASE PABCD_DIR	//RTL_GPIO_PABCD_DIR 
	#define WIFI_ONOFF_PIN_DATABASE PABCD_DAT //RTL_GPIO_PABCD_DATA
	#define WIFI_ONOFF_PIN_NO 19/*umber of the ABCD)*/
	#define WIFI_ONOFF_PIN_IMR PAB_IMR

/*
	#define RTL_GPIO_MUX 0xB8000030
	#define RTL_GPIO_MUX_DATA 0x0FC00380//for WIFI ON/OFF and GPIO

	

	#define AUTOCFG_BTN_PIN	AUTOCFG_PIN_NO	// 1
	#define AUTOCFG_LED_PIN		AUTOCFG_LED_NO//20
	#define RESET_LED_PIN		RESET_LED_NO //18
	#define RESET_BTN_PIN		RESET_PIN_NO //0
	#define RTL_GPIO_WIFI_ONOFF	WIFI_ONOFF_PIN_NO
*/
	 #define RTL_GPIO_MUX 0xB8000040
	#ifdef CONFIG_8198_PORT5_GMII
		#define RTL_GPIO_MUX_DATA 0x00340000
	#else 

#if defined(CONFIG_RTL_8196C) && defined(CONFIG_DNI_GPIO_PATCH)	
#ifdef CONFIG_DNI_RTL8196C_NEW_DESIGN_SUPPORT_DEVICE_ON
		#define RTL_GPIO_MUX_DATA 0x003400FC//for WIFI ON/OFF and GPIO
		#define RTL_GPIO_MUX_LAN_DATA 0x003400FF//for WIFI ON/OFF and GPIO
#else
		#define RTL_GPIO_MUX_DATA 0x0034000C//for WIFI ON/OFF and GPIO
		#define RTL_GPIO_MUX_LAN_DATA 0x0034000F//for WIFI ON/OFF and GPIO
#endif
#else
		#define RTL_GPIO_MUX_DATA 0x00340C00//for WIFI ON/OFF and GPIO
#endif		
	#endif 
	#define RTL_GPIO_WIFI_ONOFF     19

#if defined(CONFIG_RTL_8196C)	 
#if defined(CONFIG_DNI_GPIO_PATCH)
	#define RTL_GPIO_PABDIR		PABCD_DIR
	#define RTL_GPIO_PABDATA	PABCD_DAT
	#define RTL_GPIO_PABCNR		PABCD_CNR
	#define AUTOCFG_BTN_PIN		3
#ifdef CONFIG_DNI_RTL8196C_NEW_DESIGN_SUPPORT_DEVICE_ON	
	#define AUTOCFG_LED_PIN		6
#else
	#define AUTOCFG_LED_PIN		2
#endif
	#define RESET_LED_PIN		18
	#define RESET_BTN_PIN		5
#ifdef CONFIG_RTL8192CD
	#define WLAN_BTN_PIN		11
#else
	#define USB_ENABLE_PIN		11
	#define USB_FLAG_PIN		12	
#endif
	#define LAN0_LED_PIN 10
	#define DNI_POWER_LED	AUTOCFG_LED_PIN
	#define PLC_POWER_SAVING_PIN	4
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF	
#ifdef CONFIG_DNI_RTL8196C_NEW_DESIGN_SUPPORT_DEVICE_ON	
#ifdef CONFIG_RTL8192CD
	#define PLC_LED_CTL_PIN	12
#else
	#define PLC_LED_CTL_PIN	13
#endif
	#define DNI_DEVICE_ON_PIN	2
#else
	#define DNI_DEVICE_ON_PIN	6
#endif
#endif
	#define DNI_LED_SW_CTL
#else 
	 #define AUTOCFG_BTN_PIN         3
	 #define AUTOCFG_LED_PIN         4
	 #define RESET_LED_PIN           6
	 #define RESET_BTN_PIN           5
#endif
#elif defined(CONFIG_RTL_8198)
	 #define AUTOCFG_BTN_PIN         24
	 #define AUTOCFG_LED_PIN         26
	 #define RESET_LED_PIN           27
	 #define RESET_BTN_PIN           25
#endif	 

#endif



#endif


// 2009-0414
#ifdef USE_INTERRUPT_GPIO
#ifdef CONFIG_RTK_VOIP
	#define GPIO_IRQ_NUM		(16+17)	// GPIO_EFGH
#else
	#define GPIO_IRQ_NUM		1	
#endif
#endif

#if defined(CONFIG_DNI_GPIO_PATCH)
	#define TICKS_PER_SEC	100
	#define RTL_GPIO_TIMER_RESOLUTION		10  //10 Ticks, 0.1 second
#ifdef CONFIG_RTL_8196C
#ifdef DNI_LED_SW_CTL
	#define LED_OFF 	0
	#define LED_ON		1
	#define WLAN_LED_STATE_RF 0
	#define WLAN_LED_STATE_GPIO 1
	#define WLAN_LED_STATE_DYN 2
	#define WLAN_LED_RECOVER_COUNT 120
	int LedCtl = LED_ON, WlanLedCtl, pWlanLedCtl, wlan_led_state = WLAN_LED_STATE_RF;
	unsigned long wlan_led_last_time=0, wlan_led_last_off_time=0;
#endif
#ifdef PLC_POWER_SAVING_PIN
	int isPlcPwrSaving = 0, hw_supported = 0;
#endif
#if 1//def CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
	int ButtonCheck_init=0;
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_INTERVAL_0_2_5_7
	//0~2:do nothing
	#define WLAN_DEVICE_OFF_START_TIME (2 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)  
#endif
	//0~5:do nothing, 5~10: device OFF, 10~: SOAP client
	#define DEVICE_OFF_TIME	(5 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)	
	#define WLAN_SOAPCLI_TIME	(7 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)	
#else
	//0~2:do nothing, 2~5: Wireless ON/OFF, 5~ :SOAP client
	#define WLAN_ONOFF_TIME	(2 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)  
	#define WLAN_SOAPCLI_TIME	(5 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)
#endif

#endif
	//0~2:do nothing, 2~5: reboot, 5~ :reset_default
	#define REBOOT_TIME	(2 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)  
//	#define PROBE_TIME	(5 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)  
	#define PROBE_TIME	5
#else
	#define PROBE_TIME	5
#endif

#if defined(CONFIG_DNI_RTL8196C_WPS_TIMER_TRIGGER)
	static unsigned int wps_button_hold_timeout = 0;
	static unsigned int wps_keep_time = 0;
	#define WPS_BUTTON_KEEP_TIME (3 * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION)
#endif

#define PROBE_NULL		0
#define PROBE_ACTIVE	1
#define PROBE_RESET		2
#define PROBE_RELOAD	3
#define RTL_R32(addr)		(*(volatile unsigned long *)(addr))
#define RTL_W32(addr, l)	((*(volatile unsigned long*)(addr)) = (l))
#define RTL_R8(addr)		(*(volatile unsigned char*)(addr))
#define RTL_W8(addr, l)		((*(volatile unsigned char*)(addr)) = (l))

//#define  GPIO_DEBUG
#ifdef GPIO_DEBUG
/* note: prints function name for you */
#  define DPRINTK(fmt, args...) printk("%s: " fmt, __FUNCTION__ , ## args)
#else
#  define DPRINTK(fmt, args...)
#endif

static struct timer_list probe_timer;
static unsigned int    reset_probe_counter;
static unsigned int    reset_probe_state;
#ifdef WLAN_BTN_PIN
static unsigned int    wlan_probe_counter;
static unsigned int    wlan_probe_state;
#endif

static char default_flag='0';
//Brad add for update flash check 20080711
int start_count_time=0;
int Reboot_Wait=0;

static int get_dc_pwr_plugged_state();



//#ifdef CONFIG_RTL865X_AC

#ifdef CONFIG_POCKET_ROUTER_SUPPORT
static struct timer_list pocket_ap_timer;
#endif

#ifdef	USE_INTERRUPT_GPIO
static int wps_button_push = 0;

#endif

#ifdef AUTO_CONFIG
static unsigned int		AutoCfg_LED_Blink;
static unsigned int		AutoCfg_LED_Toggle;

#if defined(CONFIG_DNI_GPIO_PATCH)
#if 0
static unsigned int		AutoCfg_LED_Blink_Time;
static unsigned int		AutoCfg_LED_Blink_Timeout;
#endif
static unsigned int		AutoCfg_LED_ON_MAx_Count;
static unsigned int		AutoCfg_LED_OFF_MAx_Count;
static unsigned int		AutoCfg_LED_ON_Count;
static unsigned int		AutoCfg_LED_OFF_Count;
#endif

#if defined(CONFIG_DNI_GPIO_PATCH)
int CtlCheck(void)
{
	int state=1;
	
#ifdef DNI_LED_SW_CTL
#ifdef CONFIG_RTL8192CD
	if (WlanLedCtl == LED_OFF)
		state=0;
	else 
#endif
	if (LedCtl == LED_OFF)
		state=0;
	else
#endif
#ifdef PLC_POWER_SAVING_PIN	
	if (isPlcPwrSaving == 1)
		state=0;
#endif
	return state;
}
#endif

#ifdef WLAN_BTN_PIN
void wlan_btn_init(void)
{
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(1 << WLAN_BTN_PIN))));
	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) & (~(1 << WLAN_BTN_PIN))));
}	
#endif

#ifdef USB_FLAG_PIN

void usb_en_on(void)
{
#ifdef USB_ENABLE_PIN
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << USB_ENABLE_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << USB_ENABLE_PIN))));
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | (1 << USB_ENABLE_PIN)));
	//printk(">>>>>>>USB ON<<<<<<");
#endif	
}

void usb_en_off(void)
{
#ifdef USB_ENABLE_PIN
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << USB_ENABLE_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << USB_ENABLE_PIN))));
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << USB_ENABLE_PIN))));
	//printk(">>>>>>>USB OFF<<<<<<");
#endif
}

int usb_flag=-1;
void usb_flag_check(void)
{
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(1 << USB_FLAG_PIN))));
	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) & (~(1 << USB_FLAG_PIN))));
	
	if (!ButtonCheck_init)
	{
		usb_en_off();
		return;
	}
	
	if (RTL_R32(RTL_GPIO_PABDATA) & (1 << USB_FLAG_PIN))
	{
		if (usb_flag != 1)
		{
			printk(">>>>>>>USB FLAG: 1<<<<<<\n");
		}
		usb_en_on();
		usb_flag=1;
	}
	else
	{
		
		if (usb_flag != 0)
		{
			printk(">>>>>>>USB FLAG: 0<<<<<<\n");
		}
		usb_en_off();
		usb_flag=0;
	}
}

#endif

#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
void plc_led_ctl_on(void)
{
#ifdef PLC_LED_CTL_PIN
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << PLC_LED_CTL_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << PLC_LED_CTL_PIN))));	
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | ((1 << PLC_LED_CTL_PIN))));
#endif
}

void plc_led_ctl_off(void)
{
#ifdef PLC_LED_CTL_PIN
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << PLC_LED_CTL_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << PLC_LED_CTL_PIN))));
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << PLC_LED_CTL_PIN))));
#endif
}
#endif

#ifdef DNI_LED_SW_CTL

void lan_off(void)
{
	// Turn off LAN0 port LED
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << LAN0_LED_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << LAN0_LED_PIN))));
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | (1 << LAN0_LED_PIN)));
}

void lan_on(void)
{
//	if (!CtlCheck())
//		lan_off();
#ifdef DNI_LED_SW_CTL
if (LedCtl == LED_OFF)
		lan_off();
	else
#endif
#ifdef PLC_POWER_SAVING_PIN	
	if (isPlcPwrSaving == 1)
		lan_off();
	else
#endif
	{
		// Turn on LAN0 port LED
		RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << LAN0_LED_PIN))));
		RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << LAN0_LED_PIN))));
		RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << LAN0_LED_PIN))));
	}
}

void plc_led_off_setting(void)
{
	// Change the LAN0 of shared pin to GPIO mode
	RTL_W32(RTL_GPIO_MUX, RTL_GPIO_MUX_LAN_DATA);
	
	// Turn off LAN0 port LED
	lan_off();
}

void plc_led_on_setting(void)
{
#ifdef CONFIG_DNI_RTL8196C_LED_TRAFFIC_DISABLED_PATCH
	RTL_W32(RTL_GPIO_MUX, RTL_GPIO_MUX_LAN_DATA);
#else
	RTL_W32(RTL_GPIO_MUX, RTL_GPIO_MUX_DATA);
#endif
#ifdef DNI_LED_SW_CTL
	if (CtlCheck())
	{
		wlan_led_state=WLAN_LED_STATE_DYN;
		wlan_led_last_off_time=jiffies;
	}
#endif	
}

void plc_led_off(void)
{
	LedCtl = LED_OFF;
	plc_led_off_setting();
}

void plc_led_on(void)
{
	LedCtl = LED_ON;
	plc_led_on_setting();
}

void autoconfig_gpio_off(void);
void wlan_radio_off(void)
{
	WlanLedCtl = LED_OFF;
	autoconfig_gpio_off();
}

void autoconfig_gpio_on(void);
void wlan_radio_on(void)
{
	WlanLedCtl = LED_ON;
	autoconfig_gpio_on();
}

void ctl_btn_gpio_init(void)
{
	if (!ButtonCheck_init)
	{
		ButtonCheck_init = 1;
#ifdef USB_FLAG_PIN		
		usb_flag=-1;
#endif
		wlan_radio_on();
	}
}

#ifdef USB_ENABLE_PIN
static int usb_en_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];
	
	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
		if (flag[0] == '0')
			usb_en_off();
		else if (flag[0] == '1')
			usb_en_on();
		else
			{}

		return count;
	}
	else
		return -EFAULT;
}

static int usb_led_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];
	
	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
		return count;
	}
	else
		return -EFAULT;
}
#endif

static int led_ctl_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];
	
	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
#ifdef DNI_LED_SW_CTL		
		wlan_led_state=WLAN_LED_STATE_GPIO;
		wlan_led_last_time=jiffies;
#endif		
		if (flag[0] == '0')
			plc_led_off();
		else if (flag[0] == '1')
			plc_led_on();
		else if (flag[0] == '4')
			ctl_btn_gpio_init();
		else
			{}

		return count;
	}
	else
		return -EFAULT;
}

static int wlan_radio_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];
	
	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
#ifdef DNI_LED_SW_CTL		
		wlan_led_state=WLAN_LED_STATE_GPIO;
		wlan_led_last_time=jiffies;
#endif		
		if (flag[0] == '0')
			wlan_radio_off();		
		else if (flag[0] == '1')
			wlan_radio_on();						
		else
			{}

		return count;
	}
	else
		return -EFAULT;
}
#endif

#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
void device_state_init(void)
{
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << DNI_DEVICE_ON_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) & (~(1 << DNI_DEVICE_ON_PIN))));
}
void device_off(void)
{
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << DNI_DEVICE_ON_PIN))));
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | ((1 << DNI_DEVICE_ON_PIN))));
}

void device_on(void)
{
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << DNI_DEVICE_ON_PIN))));
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << DNI_DEVICE_ON_PIN))));
}
#endif

#ifdef PLC_POWER_SAVING_PIN

static int CheckHwSetted( void)
{
	struct file	*fp;
	char statusfile[32],statusbuf[3];
	mm_segment_t	orgfs;

    	hw_supported=0;
#if 0    	
  	orgfs = get_fs();
  	set_fs(get_ds());
    	
	sprintf(statusfile, "/tmp/hw-setted");
	fp = filp_open(statusfile, O_RDONLY, 0);
	if (IS_ERR(fp)) 
	{
		printk("Could not open hw-setted file '%s' for reading.\n", statusfile);
		set_fs(orgfs);
		return 0;
	}
	fp->f_op->read(fp, statusbuf, 3, &fp->f_pos);
	filp_close(fp, NULL);
  	set_fs(orgfs);
  		
	if ((statusbuf[0] == '0') && (statusbuf[1] == '4'))
	{
			hw_supported=1;
	}
 #endif
	return 1;
}

void plc_power_saving_init(void)
{
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << PLC_POWER_SAVING_PIN))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << PLC_POWER_SAVING_PIN))));
}

void plc_power_saving_off(void)
{
	isPlcPwrSaving=0;
	plc_led_on_setting();
	plc_led_ctl_off();
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | ((1 << PLC_POWER_SAVING_PIN))));
}

void plc_power_saving_on(void)
{
	CheckHwSetted();
	isPlcPwrSaving=1;
	plc_led_off_setting();
	plc_led_ctl_on();
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << PLC_POWER_SAVING_PIN))));
}

static int plc_power_saving_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];

	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
#ifdef DNI_LED_SW_CTL		
		wlan_led_state=WLAN_LED_STATE_GPIO;
		wlan_led_last_time=jiffies;
#endif		
		if (flag[0] == '0')
			plc_power_saving_off();
		else if (flag[0] == '1')
			plc_power_saving_on();
		else
			{}

		return count;
	}
	else
		return -EFAULT;
}
#endif

static int btn_lock_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];

	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
		if (flag[0] == '0')
			ButtonCheck_init=1;
		else if (flag[0] == '1')
			ButtonCheck_init=0;
		else
			{}

		return count;
	}
	else
		return -EFAULT;
}

void autoconfig_gpio_init(void)
{

	RTL_W32(AUTOCFG_PIN_IOBASE,(RTL_R32(AUTOCFG_PIN_IOBASE)&(~(1 << AUTOCFG_BTN_PIN))));
	RTL_W32(AUTOCFG_LED_IOBASE,(RTL_R32(AUTOCFG_LED_IOBASE)&(~(1 << AUTOCFG_LED_PIN))));


	// Set GPIOA pin 1 as input pin for auto config button
	RTL_W32(AUTOCFG_PIN_DIRBASE, (RTL_R32(AUTOCFG_PIN_DIRBASE) & (~(1 << AUTOCFG_BTN_PIN))));

	// Set GPIOA ping 3 as output pin for auto config led
	RTL_W32(AUTOCFG_LED_DIRBASE, (RTL_R32(AUTOCFG_LED_DIRBASE) | (1 << AUTOCFG_LED_PIN)));

	// turn off auto config led in the beginning
	RTL_W32(AUTOCFG_LED_DATABASE, (RTL_R32(AUTOCFG_LED_DATABASE) | (1 << AUTOCFG_LED_PIN)));
}
#ifdef CONFIG_RTL_8196C_GW_MP

void all_led_on(void)
{
	//printk("Into MP GPIO");
	 #ifdef CONFIG_RTL_8196C_GW_MP
	RTL_W32(0xB8000030, (RTL_R32(0xB8000030) | 0x00F00F80 ));
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(1 << PS_LED_GREEN_PIN))));
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(1 << PS_LED_ORANGE_PIN))));

	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) | (1 << PS_LED_GREEN_PIN)));
	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) | (1 << PS_LED_ORANGE_PIN)));
	
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) | (1 << PS_LED_GREEN_PIN)));
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << PS_LED_ORANGE_PIN))));

	/* inet_led init setting */
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(1 << INET_LED_GREEN_PIN))));
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(1 << INET_LED_ORANGE_PIN))));

	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) | (1 << INET_LED_GREEN_PIN)));
	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) | (1 << INET_LED_ORANGE_PIN)));
	
	RTL_W32(AUTOCFG_LED_DIRBASE, (RTL_R32(AUTOCFG_LED_DIRBASE) & (~(1 << AUTOCFG_LED_PIN_MP))));

	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) | (1 << AUTOCFG_LED_PIN_MP)));

	//RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << INET_LED_GREEN_PIN))));
	//RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) | (1 << INET_LED_ORANGE_PIN)));
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << PS_LED_GREEN_PIN))));	
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << PS_LED_ORANGE_PIN))));
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << INET_LED_GREEN_PIN))));
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << INET_LED_ORANGE_PIN))));
	RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(1 << AUTOCFG_LED_PIN_MP))));
	RTL_W32(RESET_LED_DATABASE, (RTL_R32(RESET_LED_DATABASE) & (~(1 << RESET_LED_PIN_MP))));
	#endif

}
#endif
void autoconfig_gpio_off(void)
{
	RTL_W32(AUTOCFG_LED_DATABASE, (RTL_R32(AUTOCFG_LED_DATABASE) | (1 << AUTOCFG_LED_PIN)));
	AutoCfg_LED_Blink = 0;
}


void autoconfig_gpio_on(void)
{
#if defined(CONFIG_DNI_GPIO_PATCH)	
	if (!CtlCheck())
		autoconfig_gpio_off();
	else
#endif
	{
	RTL_W32(AUTOCFG_LED_DATABASE, (RTL_R32(AUTOCFG_LED_DATABASE) & (~(1 << AUTOCFG_LED_PIN))));
	AutoCfg_LED_Blink = 0;
	}
}

#if defined(CONFIG_DNI_GPIO_PATCH)
//max_on: led ON time during blink (unit: Tick)
//max_off: led OFF time during blink (unit: Tick)
void autoconfig_gpio_blink(unsigned int max_on, unsigned int max_off)
#else
void autoconfig_gpio_blink(void)
#endif
{
#if defined(CONFIG_DNI_GPIO_PATCH)	
	if (!CtlCheck())
		autoconfig_gpio_off();
	else
#endif
	{	
	RTL_W32(AUTOCFG_LED_DATABASE, (RTL_R32(AUTOCFG_LED_DATABASE) & (~(1 << AUTOCFG_LED_PIN))));

	AutoCfg_LED_Blink = 1;
	AutoCfg_LED_Toggle = 1;
#if defined(CONFIG_DNI_GPIO_PATCH)
	//while LED_ON or LED_off hit their max_count, toggle to the other state(ON->OFF/OFF->ON)
	AutoCfg_LED_ON_MAx_Count = max_on / RTL_GPIO_TIMER_RESOLUTION;
	AutoCfg_LED_OFF_MAx_Count = max_off / RTL_GPIO_TIMER_RESOLUTION;
	AutoCfg_LED_ON_Count = AutoCfg_LED_OFF_Count = 0;
#if 0
	AutoCfg_LED_Blink_Timeout = (timeout * TICKS_PER_SEC / RTL_GPIO_TIMER_RESOLUTION);
	AutoCfg_LED_Blink_Time = 0;
#endif
#endif
	}

}

#if defined(CONFIG_DNI_GPIO_PATCH)
static unsigned int		PW_LED_Blink;
static unsigned int		PW_LED_Toggle;
static unsigned int		PW_LED_ON_MAx_Count;
static unsigned int		PW_LED_OFF_MAx_Count;
static unsigned int		PW_LED_ON_Count;
static unsigned int		PW_LED_OFF_Count;



void pw_led_init(void)
{
	RTL_W32(RTL_GPIO_PABCNR, (RTL_R32(RTL_GPIO_PABCNR) & (~(1 << DNI_POWER_LED))));
	RTL_W32(RTL_GPIO_PABDIR, (RTL_R32(RTL_GPIO_PABDIR) | ((1 << DNI_POWER_LED))));
}


void pw_led_off(void)
{
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | ((1 << DNI_POWER_LED))));
	PW_LED_Blink = 0;
}


void pw_led_on(void)
{
#if defined(CONFIG_DNI_GPIO_PATCH)	
	if (!CtlCheck())
		pw_led_off();
	else
#endif
	{
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << DNI_POWER_LED))));
	PW_LED_Blink = 0;
	}
}


//note that params on_max and off_max must be (N * RTL_GPIO_TIMER_RESOLUTION), N >= 1
void pw_led_blink(unsigned int on_max, unsigned int off_max)  //in Ticks
//void pw_led_blink()
{
#if defined(CONFIG_DNI_GPIO_PATCH)	
	if (!CtlCheck())
		pw_led_off();
	else
#endif
	{	
	RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << DNI_POWER_LED))));
	PW_LED_Blink = 1;
	PW_LED_Toggle = 1;
	//while LED_ON or LED_off hit their max_count, toggle to the other state(ON->OFF/OFF->ON)
	PW_LED_ON_MAx_Count = on_max / RTL_GPIO_TIMER_RESOLUTION;
	PW_LED_OFF_MAx_Count = off_max / RTL_GPIO_TIMER_RESOLUTION;
	PW_LED_ON_Count = PW_LED_OFF_Count = 0;
	}
}

#if 0
static int pw_led_read_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag;


	if (RTL_R32(RTL_GPIO_PABDATA) & (1 << DNI_POWER_LED))
		flag = '0';
	else
		flag = '1';
		
	len = sprintf(page, "%c\n", flag);

	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;
	return len;
}
#endif

static int pw_led_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];

	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
		if (flag[0] == '0')
			pw_led_off();
		else if (flag[0] == '1')
			pw_led_on();
		else if (flag[0] == '2')
			pw_led_blink(30, 70);
		else
			{}

		return count;
	}
	else
		return -EFAULT;
}
#endif  //#if defined(CONFIG_DNI_GPIO_PATCH)


#endif // AUTO_CONFIG

//dvd.chen, add for sending reset_default signal to user space init task.
#if defined(CONFIG_DNI_GPIO_PATCH)
#define DNI_RESET_DEFAULT_NOTIFY
#endif
#ifdef DNI_RESET_DEFAULT_NOTIFY
#define SIG_RST_DFT	71  //this must be the same as the one in init program
#ifdef WLAN_BTN_PIN
#define SIG_WLAN_ONOFF	72
#define SIG_WLAN_SOAPCLI 73
#endif
#define SIG_REBOOT 	74  
void send_reboot_sig()
{
	reset_probe_counter = 0;
	reset_probe_state = PROBE_NULL;
	printk("##sending SIG_REBOOT to reboot DUT\n");
	kill_pid(find_get_pid(1), SIG_REBOOT, 1);
}

void send_rst_dft_sig()
{
	reset_probe_counter = 0;
	reset_probe_state = PROBE_NULL;
	printk("##sending SIG_RST_DFT to init task\n");
	kill_pid(find_get_pid(1), SIG_RST_DFT, 1);
}
#ifdef WLAN_BTN_PIN
#ifndef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
void send_wlan_onoff_sig()
{
	wlan_probe_counter = 0;
	wlan_probe_state = PROBE_NULL;	
	printk("##sending SIG_WLAN_ONOFF to init task\n");
	kill_pid(find_get_pid(1), SIG_WLAN_ONOFF, 1);
}
#endif
void send_wlan_soapcli_sig()
{
	wlan_probe_counter = 0;
	wlan_probe_state = PROBE_NULL;	
	printk("##sending SIG_WLAN_SOAPCLI to init task\n");
	kill_pid(find_get_pid(1), SIG_WLAN_SOAPCLI, 1);
}
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
void set_device_off()
{
	printk("##PLC module entry to Power Saving Mode !!\n");
	plc_power_saving_on();
	mdelay(500);
	printk("##Turn off DEVICE !!\n");
	wlan_probe_counter = 0;
	wlan_probe_state = PROBE_NULL;	
	device_off();
	device_on();
	device_off();	
#ifdef DNI_LED_SW_CTL				
	wlan_led_state=WLAN_LED_STATE_DYN;
	wlan_led_last_off_time=jiffies;
#endif	
}
#endif
#endif
#endif  //#ifdef DNI_LINK_CHANGE_NOTIFY

void ResetButtonCheck(void)
{
	unsigned int pressed=1;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
	struct pid *pid;
#endif
#if defined(CONFIG_DNI_GPIO_PATCH)
	static int pw_led_blink_triggered = 0;
#endif	

#if 1//def CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
		if (!ButtonCheck_init)
		{
			//printk(">>>>ResetButtonCheck] Waiting for the Button initial!!!<<<<<\n");
			return;
		}
#endif

#if  defined(CONFIG_RTL_8196C) || defined(CONFIG_RTL_8198)
	if (RTL_R32(RESET_PIN_DATABASE) & (1 << RESET_BTN_PIN))
#endif
	{
		pressed = 0;

		//turn off LED0
		#ifndef CONFIG_RTL_8196C_GW_MP
		RTL_W32(RESET_LED_DATABASE, (RTL_R32(RESET_LED_DATABASE) | ((1 << RESET_LED_PIN))));
		#endif
	}
	else
	{
#if defined(CONFIG_DNI_GPIO_PATCH)
		if ( (reset_probe_counter+1) % (TICKS_PER_SEC/RTL_GPIO_TIMER_RESOLUTION) == 0 )
		{
			DPRINTK("Key pressed %d!\n", (reset_probe_counter+1)/(TICKS_PER_SEC/RTL_GPIO_TIMER_RESOLUTION));
		}
#else		
		DPRINTK("Key pressed %d!\n", reset_probe_counter+1);
#endif
	}

	if (reset_probe_state == PROBE_NULL)
	{
		if (pressed)
		{
			reset_probe_state = PROBE_ACTIVE;
			reset_probe_counter++;
		}
		else
			reset_probe_counter = 0;
	}
	else if (reset_probe_state == PROBE_ACTIVE)
	{
#if  0//defined(CONFIG_RTL_8196C)
	  	if (reset_probe_counter >=  PROBE_TIME)
	  	{
			pw_led_blink(30, 70);
			pw_led_blink_triggered = 1;
			pressed = 0;
			reset_probe_state = PROBE_RELOAD;
	  	}
	 	else if (reset_probe_state == PROBE_RELOAD)
	 	{
	  		reset_probe_counter =0;
	  	}
#endif		
		if (pressed)
		{
			reset_probe_counter++;
			
#if defined(CONFIG_DNI_GPIO_PATCH)
			printk("!! reset button be held\n");
			
			//until pressed for PROBE_TIME, 
			if( (reset_probe_counter >=  PROBE_TIME) && (!pw_led_blink_triggered) )
			{
				pw_led_blink(30, 70);
				pw_led_blink_triggered = 1;
			}
#endif
#if defined(CONFIG_DNI_GPIO_PATCH)
			if ((reset_probe_counter >=REBOOT_TIME ) && (reset_probe_counter <=PROBE_TIME) && (REBOOT_TIME < PROBE_TIME))	
#else
			if ((reset_probe_counter >=2 ) && (reset_probe_counter <=PROBE_TIME))
#endif
			{


				DPRINTK("2-5 turn on led\n");
#ifndef CONFIG_DNI_GPIO_PATCH
				//turn on LED0
				RTL_W32(RESET_LED_DATABASE, (RTL_R32(RESET_LED_DATABASE) & (~(1 << RESET_LED_PIN))));
#endif

			}
			else if (reset_probe_counter >= PROBE_TIME)
			{

				// sparkling LED0
				DPRINTK(">5 \n");
#ifndef CONFIG_DNI_GPIO_PATCH
			if (reset_probe_counter & 1)
					RTL_W32(RESET_LED_DATABASE, (RTL_R32(RESET_LED_DATABASE) | ((1 << RESET_LED_PIN))));
				else
					RTL_W32(RESET_LED_DATABASE, (RTL_R32(RESET_LED_DATABASE) & (~(1 << RESET_LED_PIN))));
#endif

			}
		}
		else
		{
#if defined (CONFIG_DNI_GPIO_PATCH)
			pw_led_blink_triggered = 0;
			pw_led_on();
#endif			
			#if defined(CONFIG_RTL865X_SC)
			if (reset_probe_counter < 5)
			#else
#if defined (CONFIG_DNI_GPIO_PATCH)
			pw_led_on();
			if ((reset_probe_counter < REBOOT_TIME) && (REBOOT_TIME < PROBE_TIME))
#else			
			if (reset_probe_counter < 2)
#endif			
			#endif
			{
				reset_probe_state = PROBE_NULL;
				reset_probe_counter = 0;
				DPRINTK("<2 \n");
				#if defined(CONFIG_RTL865X_SC)
				ResetToAutoCfgBtn = 1;
				#endif
			}
			else if (reset_probe_counter >= PROBE_TIME)
			{


				//reload default
				default_flag = '1';
//added by dvd.chen
#ifdef DNI_RESET_DEFAULT_NOTIFY
				//send signal to init task
				send_rst_dft_sig();		
#endif				

				//kernel_thread(reset_flash_default, (void *)1, SIGCHLD);
				return;

			}
			else
			{
				DPRINTK("2-5 reset\n");
				
				send_reboot_sig();
				
				//kernel_thread(reset_flash_default, 0, SIGCHLD);
				return;
			}
		}
	}
}

#if defined(CONFIG_DNI_RTL8196C_WPS_TIMER_TRIGGER)
void WpsButtonCheck(void)
{
	if (RTL_R32(RTL_GPIO_PABDATA) & (1 << AUTOCFG_BTN_PIN)){
		if (wps_keep_time >= WPS_BUTTON_KEEP_TIME)
		{
			wps_button_hold_timeout = 1;
			printk("==> wps pbc trigger !! <==");
		}
		wps_keep_time = 0;
	}
	else{
		wps_keep_time++;
		printk("==> wps button be held <== %d\n", wps_keep_time);
	}
}
#endif

#ifdef WLAN_BTN_PIN
void WlanButtonCheck(void)
{
	unsigned int pressed=1;
	static int wlan_led_blink_triggered = 0;
#if 1//def CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
		if (!ButtonCheck_init)
		{
			//printk(">>>>WlanButtonCheck] Waiting for the Button initial!!!<<<<<\n");
			return;
		}
#endif
	if (RTL_R32(PABCD_DAT) & (1 << WLAN_BTN_PIN))
	{
		pressed = 0;

		RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | ((1 << WLAN_BTN_PIN))));
	}
	else
	{
		if ( (wlan_probe_counter+1) % (TICKS_PER_SEC/RTL_GPIO_TIMER_RESOLUTION) == 0 )
		{
			DPRINTK("Key pressed %d!\n", (wlan_probe_counter+1)/(TICKS_PER_SEC/RTL_GPIO_TIMER_RESOLUTION));
		}
	}

	if (wlan_probe_state == PROBE_NULL)
	{
		if (pressed)
		{
			wlan_probe_state = PROBE_ACTIVE;
			wlan_probe_counter++;
			pWlanLedCtl = WlanLedCtl; // save the previous state of WlanLedCtl
		}
		else
			wlan_probe_counter = 0;
	}
	else if (wlan_probe_state == PROBE_ACTIVE)
	{
		if (pressed)
		{
			wlan_probe_counter++;

			//until pressed for WLAN_SOAPCLI_TIME, 
			if( (wlan_probe_counter >= WLAN_SOAPCLI_TIME) && (!wlan_led_blink_triggered) )
			{
				wlan_led_blink_triggered = 1;
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_INTERVAL_0_2_5_7
				if (!PW_LED_Blink)
				{
					// The priority of WLAN button > wireless traffic
					wlan_led_state=WLAN_LED_STATE_DYN;
					wlan_led_last_off_time=0;
					WlanLedCtl = LED_ON; // For the LED blinking action excluded the WlanLedCtl state from CtlCheck in pw_led_blink.
					pw_led_blink(50, 50);
				}
#else				
				pw_led_blink(10, 10);
#endif
			}
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_INTERVAL_0_2_5_7
			else if((wlan_probe_counter >= WLAN_DEVICE_OFF_START_TIME)&&(wlan_probe_counter < DEVICE_OFF_TIME))
			{
				plc_led_ctl_on();
			}
#endif
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF	
			else if(wlan_probe_counter >= DEVICE_OFF_TIME)
#else
			else if(wlan_probe_counter >= WLAN_ONOFF_TIME)
#endif
			{
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_INTERVAL_0_2_5_7			
				plc_led_ctl_off();
#else
				if (!PW_LED_Blink)
				{
					// The priority of WLAN button > wireless traffic
					wlan_led_state=WLAN_LED_STATE_DYN;
					wlan_led_last_off_time=0;
					WlanLedCtl = LED_ON; // For the LED blinking action excluded the WlanLedCtl state from CtlCheck in pw_led_blink.
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
					pw_led_blink(20, 20);
#else
					pw_led_blink(50, 50);
#endif
				}
#endif
			}
		}
		else
		{
			wlan_led_blink_triggered = 0;
			pw_led_off();
			WlanLedCtl = pWlanLedCtl; // restore the previous state of the WlanLedCtl

			if (wlan_probe_counter >= WLAN_SOAPCLI_TIME)
			{
				send_wlan_soapcli_sig();
				return;
			}
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_INTERVAL_0_2_5_7
			else if((wlan_probe_counter >= WLAN_DEVICE_OFF_START_TIME)&&(wlan_probe_counter <= DEVICE_OFF_TIME))
			{
				set_device_off();
				return;
			}
#else
			else if((wlan_probe_counter >= DEVICE_OFF_TIME)&&(wlan_probe_counter <= WLAN_SOAPCLI_TIME))
			{
				set_device_off();
				return;
			}
#endif
#else			
			else if(wlan_probe_counter >= WLAN_ONOFF_TIME)
			{
				send_wlan_onoff_sig();				
				return;
			}
#endif
			else
			{
#ifdef DNI_LED_SW_CTL				
				if (CtlCheck())
					pw_led_on();
#endif
			}	
			wlan_probe_counter = 0;
			wlan_probe_state = PROBE_NULL;
		}
	}
}
#endif

#ifdef WLAN_LED_RECOVER_COUNT
void Recover_WLAN_LED(void)
{
	if (!CtlCheck())
	{
		pw_led_off();
		autoconfig_gpio_off();
		return;
	}

	if ((wlan_led_state==WLAN_LED_STATE_RF)||(wlan_led_state==WLAN_LED_STATE_DYN))
	{
		if (wlan_led_last_off_time)
		{
				if ((jiffies - wlan_led_last_off_time)>=WLAN_LED_RECOVER_COUNT)
				{
					  wlan_led_last_time=jiffies;
						wlan_led_last_off_time=0;
						pw_led_on();
						autoconfig_gpio_on();
						//printk(">>>Recover WLAN_LED!!<<<\n");
						wlan_led_state=WLAN_LED_STATE_RF;
				}
		}
	}
}

#ifdef CONFIG_DNI_RTL8196C_LED_TRAFFIC_DISABLED_PATCH
int RTL8196_LAN_LED=0;
void CheckLanStatus( void)
{
#ifndef CONFIG_RTL8192CD
	CheckLanPortStatus();
#endif
	if (RTL8196_LAN_LED)
		lan_on();
	else
		lan_off();
}
#endif
#endif

static void rtl_gpio_timer(unsigned long data)
{


#if defined(CONFIG_DNI_GPIO_PATCH)
#ifdef WLAN_BTN_PIN
	WlanButtonCheck();
#endif
#ifdef USB_FLAG_PIN
	usb_flag_check();
#endif
#ifdef WLAN_LED_RECOVER_COUNT
	Recover_WLAN_LED();
#endif
#ifdef CONFIG_DNI_RTL8196C_LED_TRAFFIC_DISABLED_PATCH
	CheckLanStatus();
#endif
#endif

#ifdef PLC_POWER_SAVING_PIN
	if ((isPlcPwrSaving == 1) && (hw_supported == 0))
	{
		;//printk("!! Based on HW design, we need to disable th reset button function because the function will cause reboot issue\n");
	}
	else
#endif
	ResetButtonCheck();
#if defined(CONFIG_DNI_RTL8196C_WPS_TIMER_TRIGGER)
	WpsButtonCheck();
#endif

#ifdef AUTO_CONFIG
	if (AutoCfg_LED_Blink==1)
	{
#if defined(CONFIG_DNI_GPIO_PATCH)
		if (AutoCfg_LED_Toggle) {
			if( AutoCfg_LED_OFF_Count == 0) {
				DPRINTK("wps off\n");
#ifdef DNI_LED_SW_CTL				
				wlan_led_state=WLAN_LED_STATE_DYN;
				wlan_led_last_time=jiffies;
				wlan_led_last_off_time=jiffies;
#endif				
				RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | (1 << AUTOCFG_LED_PIN)));
			}
			if( (++AutoCfg_LED_OFF_Count) >= AutoCfg_LED_OFF_MAx_Count ) {
				AutoCfg_LED_OFF_Count = 0;
				AutoCfg_LED_Toggle = 0;
			}
		}
		else {
			if( AutoCfg_LED_ON_Count == 0) {
				DPRINTK("wps on\n");
#ifdef DNI_LED_SW_CTL				
				wlan_led_state=WLAN_LED_STATE_DYN;
				wlan_led_last_time=jiffies;
				wlan_led_last_off_time=0;

				if (!CtlCheck())
					RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | (1 << AUTOCFG_LED_PIN)));
				else
#endif				
					RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << AUTOCFG_LED_PIN))));
			}
			if( (++AutoCfg_LED_ON_Count) >= AutoCfg_LED_ON_MAx_Count ) {
				AutoCfg_LED_ON_Count = 0;
				AutoCfg_LED_Toggle = 1;
			}
		}
	#if 0
		//check if having blink timeout
		if( AutoCfg_LED_Blink_Timeout ) {
			if(++AutoCfg_LED_Blink_Time >= AutoCfg_LED_Blink_Timeout)
				AutoCfg_LED_Blink = 0;
		}
	#endif
#else		
		if (AutoCfg_LED_Toggle) {
			RTL_W32(AUTOCFG_LED_DATABASE, (RTL_R32(AUTOCFG_LED_DATABASE) | (1 << AUTOCFG_LED_PIN)));
		}
		else {
			RTL_W32(AUTOCFG_LED_DATABASE, (RTL_R32(AUTOCFG_LED_DATABASE) & (~(1 << AUTOCFG_LED_PIN))));
		}
		AutoCfg_LED_Toggle = AutoCfg_LED_Toggle? 0 : 1;
#endif		
	}
#endif

#if defined(CONFIG_DNI_GPIO_PATCH)
	if (PW_LED_Blink==1)
	{
		//DPRINTK("[%d, %d, %d]\n", PW_LED_Toggle, PW_LED_OFF_Count, PW_LED_ON_Count);
		if (PW_LED_Toggle) {
			if( PW_LED_OFF_Count == 0 ) {
				DPRINTK("pw off\n");
#ifdef DNI_LED_SW_CTL				
				wlan_led_state=WLAN_LED_STATE_DYN;
				wlan_led_last_time=jiffies;
				wlan_led_last_off_time=jiffies;
#endif					
				RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | (1 << DNI_POWER_LED)));
			}
			if( (++PW_LED_OFF_Count) >= PW_LED_OFF_MAx_Count ) {
				PW_LED_OFF_Count = 0;
				PW_LED_Toggle = 0;
			}
		}
		else {
			if( PW_LED_ON_Count == 0 ) {
				DPRINTK("pw on\n");
#ifdef DNI_LED_SW_CTL				
				wlan_led_state=WLAN_LED_STATE_DYN;
				wlan_led_last_time=jiffies;
				wlan_led_last_off_time=0;
				
				if (!CtlCheck())
					RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) | (1 << DNI_POWER_LED)));
				else
#endif				
					RTL_W32(RTL_GPIO_PABDATA, (RTL_R32(RTL_GPIO_PABDATA) & (~(1 << DNI_POWER_LED))));
			}
			if( (++PW_LED_ON_Count) >= PW_LED_ON_MAx_Count) {
				PW_LED_ON_Count = 0;
				PW_LED_Toggle = 1;
			}
		}
		
		//PW_LED_Toggle = PW_LED_Toggle? 0 : 1;
	}
#endif

#if defined(CONFIG_DNI_GPIO_PATCH)
	mod_timer(&probe_timer, jiffies + RTL_GPIO_TIMER_RESOLUTION);
#else
	mod_timer(&probe_timer, jiffies + 100);
#endif
}

#ifdef CONFIG_RTL_FLASH_DUAL_IMAGE_ENABLE

#define SYSTEM_CONTRL_DUMMY_REG 0xb8000068

int is_bank2_root()
{
	//boot code will steal System's dummy register bit0 (set to 1 ---> bank2 booting
	//for 8198 formal chip 
	if ((RTL_R32(SYSTEM_CONTRL_DUMMY_REG)) & (0x00000001))  // steal for boot bank idenfy
		return 1;

	return 0;
}
static int read_bootbank_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag='1';

	if (is_bank2_root())  // steal for boot bank idenfy
		flag='2';
		
	len = sprintf(page, "%c\n", flag);

	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;
	return len;
}
#endif

#ifdef AUTO_CONFIG
#if defined(USE_INTERRUPT_GPIO)
static irqreturn_t gpio_interrupt_isr(int irq, void *dev_instance, struct pt_regs *regs)
{
	wps_button_push = 1;   	  	
#ifdef CONFIG_RTK_VOIP
	RTL_W32(PEFGH_ISR, RTL_R32(PEFGH_ISR)); 	
#else
  	RTL_W32(PABCD_ISR, RTL_R32(PABCD_ISR)); 	
#endif
	return IRQ_HANDLED;
}
#endif

static int read_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag;

#if  defined(USE_INTERRUPT_GPIO)
// 2009-0414		
	if (wps_button_push) {
		flag = '1';
		wps_button_push = 0;		
	}
	else{
		if (RTL_R32(AUTOCFG_PIN_DATABASE) & (1 << AUTOCFG_BTN_PIN)){
			flag = '0';
		}else{
			//panic_printk("wps button be held \n");
			flag = '1';
		}

	}
// 2009-0414		
#else

	if (RTL_R32(AUTOCFG_PIN_DATABASE) & (1 << AUTOCFG_BTN_PIN))
		flag = '0';
	else {
#ifdef PLC_POWER_SAVING_PIN
		if ((isPlcPwrSaving == 1) && (hw_supported == 0))
		{
			flag = '0';
			//printk("!! Per HW design, we need to disable th wps button function because the function will cause wireless security issue by PBC\n");
		}
		else
#endif
		{		
			printk("!! wps button be held\n");		
			flag = '1';
		}
	}
#endif // CONFIG_RTL865X_KLD		
#if defined(CONFIG_DNI_RTL8196C_WPS_TIMER_TRIGGER)
	if (wps_button_hold_timeout)
	{
		wps_button_hold_timeout = 0;	//ps. need set (wscd.conf : button_hold_time = 1)
		flag = '1';
	}
	else
		flag = '0';
#endif
	len = sprintf(page, "%c\n", flag);


	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;
	return len;
}



#ifdef CONFIG_RTL_KERNEL_MIPS16_CHAR
__NOMIPS16
#endif 
static int write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	char flag[20];
//Brad add for update flash check 20080711

	char start_count[10], wait[10];

	if (count < 2)
		return -EFAULT;

	DPRINTK("file: %08x, buffer: %s, count: %lu, data: %08x\n",
		(unsigned int)file, buffer, count, (unsigned int)data);

	if (buffer && !copy_from_user(&flag, buffer, 1)) {
#ifdef DNI_LED_SW_CTL		
		wlan_led_state=WLAN_LED_STATE_RF;
		wlan_led_last_time=jiffies;
#endif	
		if (flag[0] == 'E') {
			autoconfig_gpio_init();
			#ifdef CONFIG_RTL865X_CMO
			extra_led_gpio_init();
			#endif
		}
		else if (flag[0] == '0')
		{
			autoconfig_gpio_off();
#ifdef DNI_LED_SW_CTL			
			if (CtlCheck())
			{
					wlan_led_state=WLAN_LED_STATE_DYN;
					wlan_led_last_off_time=jiffies;
			}
#endif
		}
		else if (flag[0] == '1')
			autoconfig_gpio_on();
#if defined(CONFIG_DNI_GPIO_PATCH)
		else if (flag[0] == '2')  //normal blink rate(0.5/0.5) to indicate WPS start
			autoconfig_gpio_blink(50, 50);
		else if (flag[0] == '3')  //fast blink rate (0.1/0.1) to indicate WPS error
			autoconfig_gpio_blink(10, 10);
#else
		else if (flag[0] == '2')
			autoconfig_gpio_blink();
#ifdef CONFIG_RTL_8196C_GW_MP
		else if (flag[0] == '9') // disable system led
                {
			all_led_on();
		}
#endif	

#ifdef CONFIG_RTL865X_CMO
		else if (flag[0] == '3')
			wep_wpa_led_on();
		else if (flag[0] == '5')
			wep_wpa_led_off();
		else if (flag[0] == '6')
			mac_ctl_led_on();
		else if (flag[0] == '7')
			mac_ctl_led_off();
		else if (flag[0] == '8')
			bridge_repeater_led_on();
		else if (flag[0] == '9')
			bridge_repeater_led_off();
		else if (flag[0] == 'A')
			system_led_on();
//		else if (flag[0] == 'B')
//			system_led_off();
		else if (flag[0] == 'C')
			lan_led_on();
		else if (flag[0] == 'D')
			lan_led_off();
		else if (flag[0] == 'Z')
			printk("gpio test test\n");
//		else if (flag[0] == '9')
//			system_led_blink = 2;
#endif
#endif
//Brad add for update flash check 20080711

		else if (flag[0] == '4'){
			start_count_time= 1;
			sscanf(buffer, "%s %s", start_count, wait);
			Reboot_Wait = (simple_strtol(wait,NULL,0))*100;
		}


		else
			{}

		return count;
	}
	else
		return -EFAULT;
}
#endif // AUTO_CONFIG

static int default_read_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;

	len = sprintf(page, "%c\n", default_flag);
	if (len <= off+count) *eof = 1;
	  *start = page + off;
	len -= off;
	if (len>count) len = count;
	if (len<0) len = 0;
	  return len;
}

#ifdef CONFIG_RTL_KERNEL_MIPS16_CHAR
__NOMIPS16
#endif 
static int default_write_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	if (count < 2)
		return -EFAULT;
	if (buffer && !copy_from_user(&default_flag, buffer, 1)) {
		return count;
	}
	return -EFAULT;
}

#ifdef READ_RF_SWITCH_GPIO
static int rf_switch_read_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag;

	if (RTL_R32(WIFI_ONOFF_PIN_DATABASE) & (1<<WIFI_ONOFF_PIN_NO)){
		flag = '1';
	}else{
		flag = '0';
	}
	len = sprintf(page, "%c\n", flag);

	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;
	return len;
}
#endif

#ifdef CONFIG_POCKET_ROUTER_SUPPORT
static int write_pocketAP_hw_set_flag_proc(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	unsigned int reg_cnr, reg_dir;

	if (count != 2)
		return -EFAULT;
	if (buffer && !copy_from_user(&pocketAP_hw_set_flag, buffer, 1)) {

	}
	return -EFAULT;
}

static int read_pocketAP_hw_set_flag_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	
	len = sprintf(page, "%c\n", pocketAP_hw_set_flag);
	
	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;

//panic_printk("\r\n __[%s-%u]",__FILE__,__LINE__);	
	return len;

}



static int read_ap_client_rou_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag;
	int gpioA2_flag,gpioB3_flag;
	
	
	if(ap_cli_rou_state == 2)
	{
		len = sprintf(page, "%c\n", '2'); // AP
	}
	else if(ap_cli_rou_state == 1)
	{
		len = sprintf(page, "%c\n", '1'); // Client
	}
	else if(ap_cli_rou_state == 3)
	{
		len = sprintf(page, "%c\n", '3'); // Router
	}
	else
	{
		len = sprintf(page, "%c\n", '0'); 
	}
	
	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;

//panic_printk("\r\n __[%s-%u]",__FILE__,__LINE__);	
	return len;
}

static int read_dc_pwr_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag;
	int pluged_state=0;
//panic_printk("\r\n 0x%x__[%s-%u]",RTL_R32(RESET_PIN_DATABASE),__FILE__,__LINE__);		

	pluged_state = get_dc_pwr_plugged_state();
	if(pluged_state == 1)
	{
		len = sprintf(page, "%c\n", '1');
	}
	else if(pluged_state == 2)
	{
		len = sprintf(page, "%c\n", '2');
	}
	else
	{
		len = sprintf(page, "%c\n", '0');
	}		

	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;

//panic_printk("\r\n len=[%u]__[%s-%u]",len,__FILE__,__LINE__);	
	return len;
}

static int read_dc_pwr_plugged_flag_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	
	len = sprintf(page, "%c\n", dc_pwr_plugged_flag);
	
	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;

//panic_printk("\r\n __[%s-%u]",__FILE__,__LINE__);
	dc_pwr_plugged_flag = '0';
	return len;

}
static int read_EnablePHYIf_proc(char *page, char **start, off_t off,
				int count, int *eof, void *data)
{
	int len;
	char flag;

//panic_printk("\r\n 0x%x__[%s-%u]",RTL_R32(RESET_PIN_DATABASE),__FILE__,__LINE__);		

	if(RTL_R32(0xBB804114) & (0x01))
	{
		flag = '1';
	}
	else
	{
		flag = '0';
	}
		
	len = sprintf(page, "%c\n", flag);

	if (len <= off+count) *eof = 1;
	*start = page + off;
	len -= off;
	if (len > count) len = count;
	if (len < 0) len = 0;

//panic_printk("\r\n len=[%u]__[%s-%u]",len,__FILE__,__LINE__);	
	return len;
}

static int get_pocketAP_ap_cli_rou_state()
{
	int gpioA2_flag,gpioB3_flag;
	
	if(RTL_R32(RESET_PIN_DATABASE) & (RTL_GPIO_DAT_GPIOA2))
	{
		gpioA2_flag = 1;
	}
	else
	{
		gpioA2_flag = 0;
	}

	if(RTL_R32(RESET_PIN_DATABASE) & (RTL_GPIO_DAT_GPIOB3))
	{
		gpioB3_flag = 1;
	}
	else
	{
		gpioB3_flag = 0;
	}

	return ((1<<gpioA2_flag)|gpioB3_flag);
}

static int get_dc_pwr_plugged_state()
{
	
	if(RTL_R32(RESET_PIN_DATABASE) & (RTL_GPIO_DAT_GPIOC0))
	{
		return 1; //plugged
	}
	else
	{
		return 2; //unplugged
	}

}

static int check_EnablePHYIf()
{
	if(RTL_R32(0xBB804114) & (0x01))
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

static void pocket_ap_timer_func(unsigned long data)
{
//panic_printk("\r\n __[%s-%u]",__FILE__,__LINE__);

	if(ap_cli_rou_idx >= 1)
		ap_cli_rou_idx = 0;
	else
		ap_cli_rou_idx++;

	ap_cli_rou_time_state[ap_cli_rou_idx]=get_pocketAP_ap_cli_rou_state();
	dc_pwr_plugged_time_state = get_dc_pwr_plugged_state();

	if(ap_cli_rou_time_state[0] == ap_cli_rou_time_state[1] )
	{
		if(ap_cli_rou_state != ap_cli_rou_time_state[0])
		{
			ap_cli_rou_state = ap_cli_rou_time_state[0];
			pocketAP_hw_set_flag = '0';
		}
	}

	if(dc_pwr_plugged_state == 0)
	{
		dc_pwr_plugged_state = dc_pwr_plugged_time_state;
	}
	else if(dc_pwr_plugged_state != dc_pwr_plugged_time_state)
	{
		dc_pwr_plugged_state = dc_pwr_plugged_time_state;
		dc_pwr_plugged_flag = '1';
	}
	
//B8b00728 & 0x1F 0x11:L0 0x14:L1  
//panic_printk("\r\n [%d-%d-%d-%d],__[%s-%u]",ap_cli_rou_time_state[0],ap_cli_rou_time_state[1],ap_cli_rou_state,__FILE__,__LINE__);		

//panic_printk("\r\n [0x%x]",RTL_R32(0xB8b00728) & (0x1F));
	pwr_saving_state=(RTL_R32(0xB8b00728) & (0x1F));
//panic_printk("\r\n pwr_saving_state = [0x%x]",pwr_saving_state);

	if(pwr_saving_state == 0x14) // L1 state, in low speed
	{
		if (pwr_saving_led_toggle < 2) {
			RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) | (RTL_GPIO_DAT_GPIOB2)));
			pwr_saving_led_toggle++;
		}
		else if (pwr_saving_led_toggle < 4){
			RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(RTL_GPIO_DAT_GPIOB2))));
			pwr_saving_led_toggle++;
			if(pwr_saving_led_toggle == 4)
				pwr_saving_led_toggle = 0;
		}
		else
		{
			pwr_saving_led_toggle = 0;
		}
	}
	else // L0 state, always on
	{
		RTL_W32(PABCD_DAT, (RTL_R32(PABCD_DAT) & (~(RTL_GPIO_DAT_GPIOB2))));
	}


mod_timer(&pocket_ap_timer, jiffies + 50);
}
#endif
int __init rtl_gpio_init(void)
{
	struct proc_dir_entry *res=NULL;

	printk("Realtek GPIO Driver for Flash Reload Default\n");

	// Set GPIOA pin 10(8181)/0(8186) as input pin for reset button

#if  defined(CONFIG_RTL_8196C) || defined(CONFIG_RTL_8198)
#if defined(CONFIG_DNI_GPIO_PATCH)
#ifdef CONFIG_DNI_RTL8196C_LED_TRAFFIC_DISABLED_PATCH
	RTL_W32(RTL_GPIO_MUX, RTL_GPIO_MUX_LAN_DATA);
#else
	RTL_W32(RTL_GPIO_MUX, RTL_GPIO_MUX_DATA);
#endif
#else
	RTL_W32(RTL_GPIO_MUX, (RTL_R32(RTL_GPIO_MUX) | (RTL_GPIO_MUX_DATA)));
#endif

#if defined(CONFIG_RTL_8198)
	RTL_W32(RTL_GPIO_MUX, (RTL_R32(RTL_GPIO_MUX) | 0xf));
#endif

#ifdef CONFIG_POCKET_ROUTER_SUPPORT

//panic_printk("\r\n 0x%x__[%s-%u]",RTL_R32(RTL_GPIO_MUX),__FILE__,__LINE__);	
	RTL_W32(RTL_GPIO_MUX, (RTL_R32(RTL_GPIO_MUX) | (RTL_GPIO_MUX_POCKETAP_DATA)));
//panic_printk("\r\n 0x%x__[%s-%u]",RTL_R32(RTL_GPIO_MUX),__FILE__,__LINE__);	
	RTL_W32(PABCD_CNR, (RTL_R32(PABCD_CNR) & (~(RTL_GPIO_CNR_POCKETAP_DATA))));





	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) & (~(RTL_GPIO_DIR_GPIOA2))));

	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) & (~(RTL_GPIO_DIR_GPIOB3))));

	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) | ((RTL_GPIO_DIR_GPIOB2))));

	RTL_W32(PABCD_DIR, (RTL_R32(PABCD_DIR) & (~(RTL_GPIO_DIR_GPIOC0))));	

#endif //end of CONFIG_POCKET_ROUTER_SUPPORT 

	RTL_W32(RESET_PIN_IOBASE, (RTL_R32(RESET_PIN_IOBASE) & (~(1 << RESET_BTN_PIN))));
	RTL_W32(RESET_PIN_DIRBASE, (RTL_R32(RESET_PIN_DIRBASE) & (~(1 << RESET_BTN_PIN))));
	
#if defined(CONFIG_DNI_GPIO_PATCH)

#ifdef WLAN_BTN_PIN
	wlan_btn_init();	
#endif
#ifdef USB_FLAG_PIN
	usb_flag_check();
#endif
#ifdef CONFIG_DNI_RTL8196C_WLAN_BUTTON_SUPPORT_DEVICE_OFF
	device_state_init();
#endif
#ifdef PLC_POWER_SAVING_PIN
	plc_power_saving_init();
#endif
#endif
	
#if defined(READ_RF_SWITCH_GPIO)
	RTL_W32(WIFI_ONOFF_PIN_IOBASE, (RTL_R32(WIFI_ONOFF_PIN_DIRBASE) & ( ~(1<<RTL_GPIO_WIFI_ONOFF))));
	RTL_W32(WIFI_ONOFF_PIN_DIRBASE, (RTL_R32(WIFI_ONOFF_PIN_DIRBASE) & (~(1<<RTL_GPIO_WIFI_ONOFF))));
	RTL_W32(WIFI_ONOFF_PIN_DATABASE, (RTL_R32(WIFI_ONOFF_PIN_DATABASE) & (~(1<<RTL_GPIO_WIFI_ONOFF))));

#endif // #if defined(READ_RF_SWITCH_GPIO)
#endif // #if defined(CONFIG_RTL865X)

	// Set GPIOA ping 2 as output pin for reset led
	RTL_W32(RESET_LED_DIRBASE, (RTL_R32(RESET_LED_DIRBASE) | ((1 << RESET_LED_PIN))));

#ifdef CONFIG_RTL_FLASH_DUAL_IMAGE_ENABLE	
	res = create_proc_entry("bootbank", 0, NULL);
	if (res) {
		res->read_proc = read_bootbank_proc;
		//res->write_proc = write_bootbank_proc;
	}
	else {
		printk("read bootbank, create proc failed!\n");
	}
#endif

#if defined(CONFIG_DNI_GPIO_PATCH)

#ifdef USB_ENABLE_PIN
	//create proc entry for usb_en
	res = create_proc_entry("usb_en", 0, NULL);
	if (res) {
		res->read_proc = NULL;
		res->write_proc = usb_en_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create usb_en proc failed!\n");
	}
	//create proc entry for usbled
	res = create_proc_entry("usbled", 0, NULL);
	if (res) {
		res->read_proc = NULL;
		res->write_proc = usb_led_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create usbled proc failed!\n");
	}	
#endif

#ifdef DNI_LED_SW_CTL
	//create proc entry for led_ctl
	res = create_proc_entry("led_ctl", 0, NULL);
	if (res) {
		res->read_proc = NULL;
		res->write_proc = led_ctl_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create led_ctl proc failed!\n");
	}
	
	//create proc entry for wlanled
	res = create_proc_entry("wlanled", 0, NULL);
	if (res) {
		res->read_proc = NULL;
		res->write_proc = wlan_radio_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create wlanled proc failed!\n");
	}		
#endif
#ifdef PLC_POWER_SAVING_PIN
	//create proc entry for plc_pwr_sav
	res = create_proc_entry("plc_pwr_sav", 0, NULL);
	if (res) {
		res->read_proc = NULL;
		res->write_proc = plc_power_saving_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create plc_pwr_sav proc failed!\n");
	}	
#endif

	//create proc entry for btn_lock
	res = create_proc_entry("btn_lock", 0, NULL);
	if (res) {
		res->read_proc = NULL;
		res->write_proc = btn_lock_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create btn_block proc failed!\n");
	}
	
	//set DNI power_led for output and blink it indicating system is under booting up
	pw_led_init();
	//pw_led_blink(50, 50);
	
	//create proc entry for power_led
	res = create_proc_entry("pw_led", 0, NULL);
	if (res) {
		res->read_proc = NULL; //pw_led_read_proc;
		res->write_proc = pw_led_write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create pw_led proc failed!\n");
	}
	
#endif

#ifdef AUTO_CONFIG
	res = create_proc_entry("gpio", 0, NULL);
	if (res) {
		res->read_proc = read_proc;
		res->write_proc = write_proc;
	}
	else {
		printk("Realtek GPIO Driver, create proc failed!\n");
	}

// 2009-0414		
#if defined(USE_INTERRUPT_GPIO)
#if defined(CONFIG_RTL_8198) && !defined(CONFIG_RTK_VOIP)
	RTL_R32(AUTOCFG_PIN_IMR) |= (0x01 << (AUTOCFG_BTN_PIN-16)*2); // enable interrupt in falling-edge		
#else
	RTL_R32(AUTOCFG_PIN_IMR) |= (0x01 << AUTOCFG_BTN_PIN*2); // enable interrupt in falling-edge	
#endif
	if (request_irq(GPIO_IRQ_NUM, gpio_interrupt_isr, IRQF_DISABLED, "rtl_gpio", NULL)) {
		//panic_printk("gpio request_irq(%d) error!\n", GPIO_IRQ_NUM);		
   	}
#endif
// 2009-0414		
#endif

	res = create_proc_entry("load_default", 0, NULL);
	if (res) {
		res->read_proc = default_read_proc;
		res->write_proc = default_write_proc;
	}

#ifdef READ_RF_SWITCH_GPIO
	res = create_proc_entry("rf_switch", 0, NULL);
	if (res) {
		res->read_proc = rf_switch_read_proc;
		res->write_proc = NULL;
	}
#endif



#ifdef CONFIG_POCKET_ROUTER_SUPPORT

	res = create_proc_entry("dc_pwr", 0, NULL);
	if (res)
		res->read_proc = read_dc_pwr_proc;
	else
		printk("create read_dc_pwr_proc failed!\n");

	res = create_proc_entry("dc_pwr_plugged_flag", 0, NULL);
	if (res)
	{
		res->read_proc = read_dc_pwr_plugged_flag_proc;
	}
	else
		printk("create read_pocketAP_hw_set_flag_proc failed!\n");
	
	res = create_proc_entry("ap_client_rou", 0, NULL);
	if (res)
		res->read_proc = read_ap_client_rou_proc;
	else
		printk("create read_ap_client_rou_proc failed!\n");

	res = create_proc_entry("pocketAP_hw_set_flag", 0, NULL);
	if (res)
	{
		res->read_proc = read_pocketAP_hw_set_flag_proc;
		res->write_proc = write_pocketAP_hw_set_flag_proc;
	}
	else
		printk("create read_pocketAP_hw_set_flag_proc failed!\n");

	res = create_proc_entry("phyif", 0, NULL);
	if (res)
		res->read_proc = read_EnablePHYIf_proc;
	else
		printk("create read_EnablePHYIf_proc failed!\n");
				
	init_timer(&pocket_ap_timer);
	pocket_ap_timer.data = (unsigned long)NULL;
	pocket_ap_timer.function = &pocket_ap_timer_func;
	mod_timer(&pocket_ap_timer, jiffies + 100);
#endif

	init_timer(&probe_timer);
	reset_probe_counter = 0;
	reset_probe_state = PROBE_NULL;
	
#ifdef WLAN_BTN_PIN	
	wlan_probe_counter = 0;
	wlan_probe_state = PROBE_NULL;	
#endif
	
#if defined(CONFIG_DNI_GPIO_PATCH)
	probe_timer.expires = jiffies + RTL_GPIO_TIMER_RESOLUTION;
#else
	probe_timer.expires = jiffies + 100;
#endif
	probe_timer.data = (unsigned long)NULL;
	probe_timer.function = &rtl_gpio_timer;
#if defined(CONFIG_DNI_GPIO_PATCH)
	mod_timer(&probe_timer, jiffies + RTL_GPIO_TIMER_RESOLUTION);
#else	
	mod_timer(&probe_timer, jiffies + 100);
#endif

#ifdef CONFIG_RTL865X_CMO
	extra_led_gpio_init();
#endif
	return 0;
}


static void __exit rtl_gpio_exit(void)
{
	printk("Unload Realtek GPIO Driver \n");
	del_timer_sync(&probe_timer);
}


#ifdef CONFIG_DNI_RTL8192_WLAN_LED_PATCH
void RTL8196_WLAN_LED(int state)
{	
#ifdef CONFIG_DNI_RTL8196C_LED_TRAFFIC_DISABLED_PATCH
	return;
#endif
	if (!CtlCheck())
	{
		pw_led_off();
		return;
	}
#ifdef DNI_LED_SW_CTL	
	if (wlan_led_state!=WLAN_LED_STATE_RF)
	{
		if (wlan_led_state==WLAN_LED_STATE_DYN)
			return;
#ifdef WLAN_LED_RECOVER_COUNT
		if((jiffies - wlan_led_last_time)>=WLAN_LED_RECOVER_COUNT)
#else			
		if((jiffies - wlan_led_last_time)>=100)
#endif
		{
			wlan_led_state=WLAN_LED_STATE_RF;
		}
		else
			return;
	}
#if 0
	if (wlan_led_last_time)
			printk(">>>The WLAN LED(%d) controlled by RTL8192!!<<<\n", state);
#endif
	wlan_led_last_time=0;
#endif
	if(state)
	{
			pw_led_on();
#ifdef DNI_LED_SW_CTL			
			wlan_led_last_off_time=0;
#endif			
	}
	else
	{
			pw_led_off();
#ifdef DNI_LED_SW_CTL			
			wlan_led_last_off_time=jiffies;
#endif			
	}

	return;
}
#endif

module_exit(rtl_gpio_exit);
module_init(rtl_gpio_init);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("GPIO driver for Reload default");
