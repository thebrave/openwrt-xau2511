#ifndef __PRODUCT_INFO_H__
#define __PRODUCT_INFO_H__

#define PRODUCT_MODEL_NAME "XAU2511"
#define PRODUCT_MODEL_NUMBER "standard"
#define PRODUCT_MANUFACTUER_NAME "Delta Network Inc."
#define PRODUCT_SUPPORT_INFO "http://www.deltanetworks.com.tw/"
#define PRODUCT_MANUFACTURER_URL "http://www.deltanetworks.com.tw/"
#define PRODUCT_FRIENDLY_NAME "DNI AV USB extender"
#define PRODUCT_DESCRIPTION "AV USB extender"
#define PRODUCT_HW_ID "XAU2511"

#endif

