#
# Copyright (C) 2006-2009 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=libiconv
PKG_VERSION:=1.11
PKG_RELEASE:=1

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)
PKG_INSTALL_DIR:=$(PKG_BUILD_DIR)/ipkg-install

#PKG_FIXUP = libtool

include $(INCLUDE_DIR)/package.mk

define Build/Prepare
	-rm -rf $(PKG_BUILD_DIR)
	ln -s ${PWD}/$(PKG_NAME)/src $(PKG_BUILD_DIR)
endef

define Package/libiconv/Default
  URL:=http://www.gnu.org/software/libiconv/
  TITLE:=Character set conversion
endef

define Package/libiconv
  $(call Package/libiconv/Default)
  SECTION:=libs
  CATEGORY:=Libraries
  TITLE+= library
endef

define Package/libcharset
  $(call Package/libiconv/Default)
  SECTION:=libs
  CATEGORY:=Libraries
  TITLE+= library
endef

define Package/iconv
  $(call Package/libiconv/Default)
  DEPENDS:=+libiconv +libcharset
  SECTION:=utils
  CATEGORY:=Utilities
  TITLE+= utility
endef

TARGET_CFLAGS += $(FPIC) -DUSE_DOS

CONFIGURE_ARGS += \
	--enable-shared \
	--enable-static \
	--disable-rpath \
	--enable-relocatable

define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR) \
		CC="$(TARGET_CC)" \
		DESTDIR="$(PKG_INSTALL_DIR)" \
		install
endef

define Build/Clean
	rm -rf $(PKG_INSTALL_DIR)
	rm -rf $(PKG_BUILD_DIR)
endef

define Build/InstallDev
	$(INSTALL_DIR) $(STAGING_DIR)/include
	$(CP) $(PKG_INSTALL_DIR)/usr/include/*.* $(STAGING_DIR)/include/
	$(INSTALL_DIR) $(STAGING_DIR)/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/*.* $(STAGING_DIR)/lib/
	
	$(INSTALL_DIR) $(STAGING_DIR)/usr/include
	$(CP) $(PKG_INSTALL_DIR)/usr/include/*.* $(STAGING_DIR)/usr/include/
	$(INSTALL_DIR) $(STAGING_DIR)/usr/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/*.* $(STAGING_DIR)/usr/lib/
	
	$(INSTALL_DIR) $(STAGING_DIR)/$(ARCH)-linux-uclibc/include
	$(CP) $(PKG_INSTALL_DIR)/usr/include/*.* $(STAGING_DIR)/$(ARCH)-linux-uclibc/include/	
	$(INSTALL_DIR) $(STAGING_DIR)/$(ARCH)-linux-uclibc/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/*.* $(STAGING_DIR)/$(ARCH)-linux-uclibc/lib/
endef

define Package/libcharset/install
	$(INSTALL_DIR) $(1)/usr/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/libcharset.so* $(1)/usr/lib/
endef

define Package/libiconv/install
	$(INSTALL_DIR) $(1)/usr/lib
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/libiconv.so* $(1)/usr/lib/
endef

$(eval $(call BuildPackage,libcharset))
$(eval $(call BuildPackage,libiconv))
$(eval $(call BuildPackage,iconv))
