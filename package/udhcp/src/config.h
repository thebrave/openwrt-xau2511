/* DNI config.h
 * DNI add modified feature macro to this file,
 * but this is just an example file, if you want to compile these features
 * you should replace this file with your own config.h before compile
 */
#ifndef _DNI_CONFIG_H
#define _DNI_CONFIG_H

/*
 * Netgear router spec V1.6 4.1 : Netgear router SHOULD support fixed IP
 * settings via editing the Address Reservation Table to specific clients
 * #define DHCPD_STATIC_LEASE 1
*/

/*
  * The name of the connected PC can't be got by NETBIOS query sometimes,
  * and the PC host name is sent in DHCP Request packet, list all DHCP clients
  * host names for showing attached devices name if unknown by 'net-scan'.
  *
  * #define DHCPD_SHOW_HOSTNAME 1
  */

/*
 * Netgear router spec V1.6 4.4 WAN/LAN IP conflict detection.
 * when wan port in dhcp client mode get ip, do IP conflict detection
 * #define WAN_LAN_IPCONFLICT 1
 */

/* you should define your own ipconflict command
 * it may defined at package/net-util/
 * #ifdef WAN_LAN_IPCONFLICT
 * #define IP_CONFLICT_CMD "/sbin/ipconflict"
 * #endif
 */

/*
 * NETGEAR router spec V1.6, chapter 5.1 Classless Route Option support in dhcpc
 * option 121 (RFC 3442)
 * #define RFC3442_121_SUPPORT 1
 */

/*
 * NETGEAR router spec V1.6, chapter 5.1 Classless Route Option support in dhcpc
 * option 33 (RFC 2132) support
 * #define RFC2132_33_SUPPORT 1
 */

/*
 * check whether the request IP is the same with sever's IP
 * #define DHCPD_CHECK_SERVER_IP 1    
 */

#endif

