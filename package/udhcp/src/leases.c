/* 
 * leases.c -- tools to manage DHCP leases 
 * Russ Dill <Russ.Dill@asu.edu> July 2001
 */

#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <unistd.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include "debug.h"
#include "dhcpd.h"
#include "files.h"
#include "options.h"
#include "leases.h"
#include "arpping.h"

#include <linux/wireless.h>

#define GET_WLAN_MAC_RALINK            1       //1:Ralink method to get WLAN client macaddress, 0:Atheros method.

unsigned char blank_chaddr[] = {[0 ... 15] = 0};
uint8_t blank_hostname[] = "UNKNOWN";

static char *ether_ntoa_local(const struct ether_addr *addr);
static char *ether_ntoa_r_local(const struct ether_addr *addr, char *buf);

/* clear every lease out that chaddr OR yiaddr matches and is nonzero */
void clear_lease(u_int8_t *chaddr, u_int32_t yiaddr)
{
	unsigned int i, j;
	
	for (j = 0; j < 16 && !chaddr[j]; j++);
	
	for (i = 0; i < server_config.max_leases; i++)
		if ((j != 16 && !memcmp(leases[i].chaddr, chaddr, 16)) ||
		    (yiaddr && leases[i].yiaddr == yiaddr)) {
			memset(&(leases[i]), 0, sizeof(struct dhcpOfferedAddr));
		}
}

#if GET_WLAN_MAC_RALINK
/* write wireless dhcp client mac into /tmp/findport*/
static void write_wlan_client_lease(char *interface, char *file_name)
{
	FILE *filename;
	unsigned long i;
	int s;
	struct iwreq iwr;

#define RTPRIV_IOCTL_GET_MAC_TABLE		0x8BE0+0x0F
	typedef union _MACHTTRANSMIT_SETTING {
		struct  {
			unsigned short  MCS:7;  // MCS
			unsigned short  BW:1;   //channel bandwidth 20MHz or 40 MHz
			unsigned short  ShortGI:1;
			unsigned short  STBC:2; //SPACE
			unsigned short  rsv:3;
			unsigned short  MODE:2; // Use definition MODE_xxx.
		} field;
		unsigned short      word;
	} MACHTTRANSMIT_SETTING;

	typedef struct _RT_802_11_MAC_ENTRY {
		unsigned char            Addr[6];
		unsigned char            Aid;
		unsigned char            Psm;     // 0:PWR_ACTIVE, 1:PWR_SAVE
		unsigned char            MimoPs;  // 0:MMPS_STATIC, 1:MMPS_DYNAMIC, 3:MMPS_Enabled
		char                     AvgRssi0;
		char                     AvgRssi1;
		char                     AvgRssi2;
		unsigned int             ConnectedTime;
		MACHTTRANSMIT_SETTING    TxRate;
	} RT_802_11_MAC_ENTRY;

	typedef struct _RT_802_11_MAC_TABLE {
		unsigned long            Num;
		RT_802_11_MAC_ENTRY      Entry[32]; //MAX_LEN_OF_MAC_TABLE = 32
	} RT_802_11_MAC_TABLE;
	
	RT_802_11_MAC_TABLE table = {0};

	s = socket(AF_INET, SOCK_DGRAM, 0);
	strncpy(iwr.ifr_name, interface, IFNAMSIZ);
	iwr.u.data.pointer = (caddr_t) &table;

	if (s < 0) {
		printf("ioctl sock failed!");
		return;
	}

	if (ioctl(s, RTPRIV_IOCTL_GET_MAC_TABLE, &iwr) < 0) {
		printf("ioctl -> RTPRIV_IOCTL_GET_MAC_TABLE failed!");
		close(s);
		return;
	}

	filename = fopen(file_name,"a+");
	if(!filename)
	{
		printf("open portid file: %s error exit!!",file_name);
		return;
	}
	for (i = 0; i < table.Num; i++) {
		fprintf(filename,"%02X:%02X:%02X:%02X:%02X:%02X\n",
						table.Entry[i].Addr[0], table.Entry[i].Addr[1],
						table.Entry[i].Addr[2], table.Entry[i].Addr[3],
						table.Entry[i].Addr[4], table.Entry[i].Addr[5]);
	}
	fclose(filename);
}

/* converts mac string to upper case,because in /tmp/findport,the WLAN mac is upper case.*/
static void lower_to_upper(char *string, char *mac_upper)
{
	int i;

	for(i=0 ; string[i]!= '\0'; i ++)
	{
		mac_upper[i] = toupper(string[i]);
	}
	mac_upper[i]='\0';
}
#endif //GET_WLAN_MAC_RALINK

/* add a lease into the table, clearing out any old ones */
struct dhcpOfferedAddr *add_lease(u_int8_t *chaddr, u_int8_t *hostname, u_int32_t yiaddr, unsigned long lease, uint32_t portfrom)
{
	struct dhcpOfferedAddr *oldest;
	
	uint8_t host_len = 7;
	uint8_t findbrport[256];
	int is_lan = portfrom;
	/* clean out any old ones */
	clear_lease(chaddr, yiaddr);
		
	oldest = oldest_expired_lease();

	if (!hostname)
		hostname = blank_hostname;
	else
		host_len = strlen(hostname);

	if(portfrom == NEW_LEASE)
	{
#if 1
#define FIND_PORT      "/tmp/find-br-port"
		FILE *findport;
		uint8_t tmpname[33];
		char mac_upper[32];
	
#if !GET_WLAN_MAC_RALINK
		sprintf(findbrport,"wlanconfig ath0 list 2>/dev/null >/tmp/findport;wlanconfig ath1 list 2>/dev/null >>/tmp/findport;grep %s /tmp/findport -c >%s",ether_ntoa_local((const struct ether_addr *)chaddr),FIND_PORT);

#else
#define MAC_FILE	"/tmp/findport"
		unlink("/tmp/findport");
		write_wlan_client_lease("ra0",MAC_FILE);
		write_wlan_client_lease("ra1",MAC_FILE);
		/*converts mac string to upper case*/
		lower_to_upper(ether_ntoa_local((const struct ether_addr *)chaddr),mac_upper);

		sprintf(findbrport,"grep %s /tmp/findport -c >%s", mac_upper,FIND_PORT);
#endif //GET_WLAN_MAC_RALINK

		system(findbrport);
		findport = fopen(FIND_PORT,"r");
		if(!findport)
		{
			printf("open portid file: %s error exit!!",FIND_PORT);
			goto open_error;
		} 
		fscanf(findport,"%s",tmpname);
		is_lan = atoi(tmpname);
		fclose(findport);
	}

open_error:	//just go on.
#endif
	if (oldest) {
		memcpy(oldest->chaddr, chaddr, 16);
		memcpy(oldest->hostname, hostname, host_len>20?20:host_len);
		oldest->yiaddr = yiaddr;
		oldest->islan = is_lan;
		oldest->expires = time(0) + lease;
	}
	
	return oldest;
}


/* true if a lease has expired */
int lease_expired(struct dhcpOfferedAddr *lease)
{
	return (lease->expires < (unsigned long) time(0));
}	


/* Find the oldest expired lease, NULL if there are no expired leases */
struct dhcpOfferedAddr *oldest_expired_lease(void)
{
	struct dhcpOfferedAddr *oldest = NULL;
	unsigned long oldest_lease = time(0);
	unsigned int i;

	
	for (i = 0; i < server_config.max_leases; i++)
		if (oldest_lease > leases[i].expires) {
			oldest_lease = leases[i].expires;
			oldest = &(leases[i]);
		}
	return oldest;
		
}


/* Find the first lease that matches chaddr, NULL if no match */
struct dhcpOfferedAddr *find_lease_by_chaddr(u_int8_t *chaddr)
{
	unsigned int i;

	for (i = 0; i < server_config.max_leases; i++)
		if (!memcmp(leases[i].chaddr, chaddr, 16)) return &(leases[i]);
	
	return NULL;
}


/* Find the first lease that matches yiaddr, NULL is no match */
struct dhcpOfferedAddr *find_lease_by_yiaddr(u_int32_t yiaddr)
{
	unsigned int i;

	for (i = 0; i < server_config.max_leases; i++)
		if (leases[i].yiaddr == yiaddr) return &(leases[i]);
	
	return NULL;
}


/* find an assignable address, it check_expired is true, we check all the expired leases as well.
 * Maybe this should try expired leases by age... */
u_int32_t find_address(int check_expired) 
{
	u_int32_t addr, ret;
	struct dhcpOfferedAddr *lease = NULL;		

	addr = ntohl(server_config.start); /* addr is in host order here */
	for (;addr <= ntohl(server_config.end); addr++) {

		/* ie, 192.168.55.0 */
		if (!(addr & 0xFF)) continue;

		/* ie, 192.168.55.255 */
		if ((addr & 0xFF) == 0xFF) continue;

		/* lease is not taken */
		ret = htonl(addr);
		if ((!(lease = find_lease_by_yiaddr(ret)) ||

		     /* or it expired and we are checking for expired leases */
		     (check_expired  && lease_expired(lease))) &&

#ifdef DHCPD_STATIC_LEASE
		     /* check the ip is not a reserved ip */
		     !ip_reserved(ret) &&
#endif
		     /* and it isn't on the network */
	    	     !check_ip(ret)) {
			return ret;
			break;
		}
	}
	return 0;
}


/* check is an IP is taken, if it is, add it to the lease table */
int check_ip(u_int32_t addr)
{
	struct in_addr temp;

#ifdef DHCPD_CHECK_SERVER_IP
	if (addr == server_config.server) {
		server_config.conflict_time = ~0;
		add_lease(blank_chaddr, NULL, addr, server_config.conflict_time, NEW_LEASE);
		return 1;
	}
#endif

	if (arpping(addr, server_config.server, server_config.arp, server_config.interface, blank_chaddr) == 0) {
		temp.s_addr = addr;
	 	LOG(LOG_INFO, "%s belongs to someone, reserving it for %ld seconds", 
	 		inet_ntoa(temp), server_config.conflict_time);
		add_lease(blank_chaddr, NULL, addr, server_config.conflict_time, NEW_LEASE);
		return 1;
	} else return 0;
}
#ifdef DHCPD_STATIC_LEASE
/***************************************************************
 *             Static Lease
 ***************************************************************/

/* Check to see if a mac has an associated static lease */
uint32_t get_ip_by_mac(void *arg)
{
       uint8_t *mac = arg;
       struct static_lease *cur = server_config.static_leases;

       while (cur != NULL) {
               if (memcmp(cur->mac, mac, 6) == 0)
                       return cur->ip;

               cur = cur->next;
       }

       return 0;
}

/* Check to see if an ip is reserved as a static ip */
int ip_reserved(uint32_t ip)
{
       struct static_lease *cur = server_config.static_leases;

       while (cur != NULL) {
               if (cur->ip == ip)
                       return 1;

               cur = cur->next;
       }

       return 0;
}
#endif

static char *ether_ntoa_local(const struct ether_addr *addr)
{
	static char asc[18]; 
	return ether_ntoa_r_local(addr, asc);
}

static char *ether_ntoa_r_local(const struct ether_addr *addr, char *buf)
{
	sprintf(buf, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
				addr->ether_addr_octet[0], addr->ether_addr_octet[1],
				addr->ether_addr_octet[2], addr->ether_addr_octet[3],
				addr->ether_addr_octet[4], addr->ether_addr_octet[5]);
	return buf;
}
