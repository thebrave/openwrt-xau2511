/*
 * encrypt
 * Copyright (c) 2005-2006, Loren Hong <loren.hong@deltadg.com.cn>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Alternatively, this software may be distributed under the terms of BSD
 * license.
 *
 * See README and COPYING for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#define KEY 122406
static const char *encrypt__version =
"encrypt v0.0.1 - Copyright (c) 2005-2006, Loren Hong "
"<loren.hong@deltadg.com.cn>";

static void usage(void)
{
	printf("usage:\n"
	       "  encrypt -i<input file name>\n"
	       "  -h = show this help text\n"
	       "  -v = show version\n");
}
FILE* open_file(const char *filename, const char *mode)
{
    FILE *f;

    f = fopen(filename, mode);
    if (f == NULL)
        fprintf(stderr,
                "open_input(\"%s\") failed: %s\n",
                filename, strerror(errno));
    return f;
}

int close_file(FILE *f)
{
    int s = 0;
    
    if (f == NULL) return 0;
    errno = 0;
    s = fclose(f);
    if (s == EOF) perror("Close failed");
    return s;
}

int encrypt(const char *ifname)
{
        FILE   *fp1, *fp2;
	char   c, ch;
        char tmpfile[24]="/tmp/template-XXXXXX";

	mkstemp(tmpfile);
			
    	if ((fp1 = open_file(ifname, "rb")) == NULL){
		exit(errno);
    	}    
    	
	if ((fp2 = open_file(tmpfile, "wb")) == NULL){
		exit(errno);
	}

	srand((unsigned char)KEY);
	ch=fgetc(fp1);
	while(!feof(fp1)){
		c=rand();
		ch=ch^c;
		fputc(ch,fp2);
		ch=fgetc(fp1);
	}
	close_file(fp1);
	close_file(fp2);
	
	if ((fp1 = open_file(tmpfile, "rb")) == NULL){
		exit(errno);
	}
	if ((fp2 = open_file(ifname, "wb")) == NULL){
		exit(errno);
	}
	ch=fgetc(fp1);
	while(1){
		if(!feof(fp1))
		{
			fputc(ch,fp2);
			ch=fgetc(fp1);
		}
		else
			break;
	}

	close_file(fp1);
	close_file(fp2);
	remove(tmpfile);
	
	return 0;	
}

int main(int argc, char *argv[])
{
	int c;
	const char *ifname;
        int exitcode = 0;

	ifname = NULL;

	for (;;) {
		c = getopt(argc, argv, "hi:v");
		if (c < 0)
			break;
		switch (c) {
                    case 'h':
			usage();
			return -1;
                    case 'i':
			ifname = optarg;
			break;
                    case 'v':
			printf("%s\n", encrypt__version);
			return -1;
                    default:
                        usage();
			return -1;
		}
	}
        if (ifname == NULL){
            usage();
            return -1;
        }
        exitcode = encrypt(ifname);

	return exitcode;
}
