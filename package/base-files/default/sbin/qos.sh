#!/bin/sh
TC="/usr/sbin/tc"
IPTABLES="/usr/sbin/iptables"
NVRAM="/usr/sbin/nvram"
ECHO="/bin/echo"
WAN_IF="$($NVRAM get wan_ifnames)"
WAN_PROTO="$($NVRAM get wan_proto)"
FILTER_ADD="$TC filter add dev $WAN_IF"
UPRATE="$($NVRAM get qos_uprate)"
QoS_ENABLE="$($NVRAM get qos_endis_on)"
BANDCTL="$($NVRAM get qos_threshold)"

if [ "x$($NVRAM get wan_proto)" = "xpppoe" ]; then
	PROT=ppp_ses
else
	PROT=ip
fi

start(){
	if [ "x$QoS_ENABLE" != "x1" ]; then
		return
	fi

	if [ "x$WAN_PROTO" = "xpppoe" ]; then
		PROT=ppp_ses
	else
		PROT=ip
	fi

	if [ "x$WAN_PROTO" = "xpptp" ]; then
		if [ "x$BANDCTL" = "x0" ] || [ $UPRATE -le 0 ] || [ $UPRATE -gt 57344 ]; then
			UPRATE=57344
		fi
	elif [ "x$WAN_PROTO" = "xpppoe" ]; then
		if [ "x$BANDCTL" = "x0" ] || [ $UPRATE -le 0 ] || [ $UPRATE -gt 66560 ]; then
			UPRATE=66560
		fi
	else
		if [ "x$BANDCTL" = "x0" ] || [ $UPRATE -le 0 ] || [ $UPRATE -gt 81920 ]; then
			UPRATE=81920
		fi
	fi

	$ECHO -n $UPRATE > /proc/MFS

	$TC qdisc add dev $WAN_IF root handle 1: cbq bandwidth ${UPRATE}kbit avpkt 1000
	$TC class add dev $WAN_IF parent 1: classid 1:1 cbq rate $((45*${UPRATE}/100))kbit allot 1500 prio 1 borrow sharing
	$TC class add dev $WAN_IF parent 1: classid 1:2 cbq rate $((30*${UPRATE}/100))kbit allot 1500 prio 2 borrow sharing
	$TC class add dev $WAN_IF parent 1: classid 1:3 cbq rate $((15*${UPRATE}/100))kbit allot 1500 prio 3 borrow sharing
	$TC class add dev $WAN_IF parent 1: classid 1:4 cbq rate $((10*${UPRATE}/100))kbit allot 1500 prio 4 borrow sharing

	$TC qdisc add dev $WAN_IF parent 1:1 handle 10: sfq
	$TC qdisc add dev $WAN_IF parent 1:2 handle 20: sfq
	$TC qdisc add dev $WAN_IF parent 1:3 handle 30: sfq
	$TC qdisc add dev $WAN_IF parent 1:4 handle 40: sfq

	$FILTER_ADD parent 1:0 protocol $PROT prio 1 handle 1 fw classid 1:1
	$FILTER_ADD parent 1:0 protocol $PROT prio 2 handle 2 fw classid 1:2
	$FILTER_ADD parent 1:0 protocol $PROT prio 3 handle 3 fw classid 1:3
	$FILTER_ADD parent 1:0 protocol $PROT prio 4 handle 4 fw classid 1:4	

        $FILTER_ADD parent 1:0 protocol $PROT prio 5 u32 match ip tos 0x00 0xff flowid 1:3
        $FILTER_ADD parent 1:0 protocol $PROT prio 6 u32 match ip tos 0xc0 0xfc flowid 1:1
        $FILTER_ADD parent 1:0 protocol $PROT prio 7 u32 match ip tos 0x80 0xfc flowid 1:2
        $FILTER_ADD parent 1:0 protocol $PROT prio 5 u32 match ip tos 0x40 0xfc flowid 1:3
        $FILTER_ADD parent 1:0 protocol $PROT prio 8 u32 match ip tos 0x00 0xfc flowid 1:4

}

stop(){
	$TC qdisc del dev eth1 root >&- 2>&-
	$TC qdisc del dev ppp0 root >&- 2>&-
	$ECHO -n 0 > /proc/MFS
#	for i in 1 2 3 4
#	do
#		echo $i\0 > /proc/lan_prio
#	done
}

status(){
	echo "show qdisc ............ "
	$TC -d -s qdisc
#	echo "show filter ............ "
#		$TC -d -s filter ls dev $WAN_IF
	echo "show class ............ "
	if [ "x$WAN_IF" != "x" ];then
		$TC -d -s class ls dev $WAN_IF
	fi
}
								 
case "$1" in
	stop)
	stop
	;;
	start | restart )
	stop
	start
	;;
	status)
	status
	;;
	*)
	echo $"Usage:$0 {start|stop|restart|status}"
	exit 1
esac

