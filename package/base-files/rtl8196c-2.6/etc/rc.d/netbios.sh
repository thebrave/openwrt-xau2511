#!/bin/sh

RETVAL=0
prog="wins"
PID_FILE="/var/run/wins.pid"
NETBIOS_CONF_FILE="/tmp/netbios.conf"
workgroup=Workgroup
hostname=`nvram get ap_netbiosname`
sharename=`nvram get usb_deviceName`
if [ x"$sharename" =  x ]; then
sharename=$hostname
fi
lan_ifname=`nvram get lan_ifname`

start() {
	# Start daemons.
	echo $"Starting $prog: "

 	#update /tmp/netbios.conf file
 	rm -f ${NETBIOS_CONF_FILE}
 	# mac=`ifconfig $lan_ifname | sed -n 1p | awk '{ print $5 }'`
 	# mac4=`echo $mac | awk -F: '{ print $4 }'`
 	# mac5=`echo $mac | awk -F: '{ print $5 }'`
 	# mac6=`echo $mac | awk -F: '{ print $6 }'`
 	echo "interface = $lan_ifname"			>${NETBIOS_CONF_FILE}
 	# echo "hostname = $hostname-$mac4$mac5$mac6"	>>${NETBIOS_CONF_FILE}
 	echo "hostname = $hostname" >>${NETBIOS_CONF_FILE} 	
 	echo "sharename = $sharename"	>>${NETBIOS_CONF_FILE} 	
 	echo "workgroup = \"$workgroup\""     		>>${NETBIOS_CONF_FILE}
 	echo "comment   = \"NETGEAR $hostname\""	>>${NETBIOS_CONF_FILE}
 
	${prog} &
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	killall -9 ${prog} #for release
	rm -f ${PID_FILE}
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

