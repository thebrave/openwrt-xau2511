#!/bin/sh

prog="miniigd"
BRIDGE_INTERFACE=br0
IGD_PID_FILE=/var/run/miniupnpd.pid
UPNP_ENABLED=`nvram get upnp_enable`
wan_proto=`nvram get wan_proto`
LAN_MAC=$(nvram get lan_hwaddr | sed -n 's/://gp')
UUID="12342409-1234-1234-5678-${LAN_MAC}"

start() {
	# for miniigd patch	
	#Set a flag to allow mini-igd to append firewall rule
	echo 1 > /tmp/firewall_igd
	
	if [ $UPNP_ENABLED != 0 ]; then
		route del -net 239.255.255.250 netmask 255.255.255.255 br0
	  	route add -net 239.255.255.250 netmask 255.255.255.255 br0
		#eval `flash get WAN_DHCP`
		WAN_TYPE=1  #wan_type=DHCP, need to get nvram here 
		case "$wan_proto" in	
			static)
				WAN_TYPE=0
				;;
			dhcp)		
				WAN_TYPE=1
				;;
			pppoe)
				WAN_TYPE=3
				;;
			pptp)
				WAN_TYPE=4
				;;
			*)
				echo $"wan_proto invalid: $wan_proto"
		esac
		echo $"Starting $prog: "
		miniigd -e $WAN_TYPE -i $BRIDGE_INTERFACE -u $UUID
	fi
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	killall -15 miniigd 2> /dev/null
	if [ -f "$IGD_PID_FILE" ]; then
		rm -f $IGD_PID_FILE
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL