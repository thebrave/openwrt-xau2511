#!/bin/sh

#[ -f /usr/local/sbin/pppd ] || exit 0

RETVAL=0
prog="pppd"
PID_FILE="/var/run/ppp0.pid"
CALL_FILE="provider_pptp"
IP_UP="/etc/ppp/ip-up"
IP_DOWN="/etc/ppp/ip-down"
PLUG_IN="plugin dni-pptp.so"
#INTERFACE=`nvram get wan_hwifname`
wan_ifname=`nvram get wan_hwifname`
user=`nvram get wan_pptp_username`
password=`nvram get wan_pptp_password`
pptp_conn_id=`nvram get wan_pptp_connection_id`
mtu=$(nvram get wan_pptp_mtu)
mru=$(nvram get wan_pptp_mru)
if [ "$mtu" = "" ]; then
	mru=$mtu
fi
idle_time=$(nvram get wan_pptp_idle_time)
demand=`nvram get wan_pptp_demand`
dns_assign=`nvram get wan_pptp_dns_assign`
if [ -n "$(nvram get dns_pptp_server_addr)" ]; then
PPTP_SERVER_IP=`nvram get dns_pptp_server_addr`
nvram unset dns_pptp_server_addr
else
PPTP_SERVER_IP=`nvram get wan_pptp_server_ip`
fi
PPTP_MY_IP=`nvram get wan_pptp_local_ip`
PPTP_NETMASK=`nvram get wan_pptp_netmask`
wan_hostname=`nvram get wan_hostname`
static_dns=`nvram get wan_pptp_static_dns`
manual=$2

if [ "${demand}" = "0" ]; then
	persist_demand="persist"
	idle_time="0"
elif [ "${demand}" = "1" ]; then
	if [ "$idle_time" = "0" ]; then
		persist_demand="persist"
	else
		persist_demand="demand"
		echo 1 > /proc/sys/net/ipv4/ip_forward
	fi
else
	persist_demand="persist"	# for maual trigger 
	idle_time="0"
fi

if [ "$dns_assign" = "0" -o "$static_dns" = "1" ]; then
	usepeerdns="usepeerdns"
else
	usepeerdns=""
fi

if [ "$pptp_conn_id" = "" ]; then
	conn_id=""
else
	conn_id="pptp_connection_id $pptp_conn_id"
fi

if [ "$wan_hostname" = "" ]; then
	hostname=""
else
	hostname="pptp_hostname"
fi


PPTP="/usr/local/sbin/pptp $PPTP_SERVER_IP --nolaunchpppd $phone"

start() {
	# Start daemons.
	echo $"Starting $prog: "
	#${prog} call ${CALL_FILE}
	rmmod pptp
	insmod pptp	
	nvram set wan_ifname=ppp0
	nvram set pptp_have_gateway=0
	echo "1" > /proc/fast_pptp
	if [ "`nvram get dy_pptp`" = "0" ]; then
		ifconfig ${wan_ifname} ${PPTP_MY_IP} netmask ${PPTP_NETMASK} up
		gw_ip=`nvram get wan_pptp_gateway_ip`
		stat_dns=`nvram get wan_dns`
		fqdn="0" 
                area-ck $PPTP_SERVER_IP
		if [ "$?" = "1" -a "x$gw_ip" != "x" ]; then
			fqdn="1" 
                        for i in $stat_dns
                        do
                            area-ck $PPTP_MY_IP $i $PPTP_NETMASK
                            if [ "$?" != "1" ]; then
                                route add -net ${i} netmask 255.255.255.255 gw $gw_ip
                            fi
                        done
			/etc/rc.d/dnsmasq.sh restart
		fi
		if [ "x$gw_ip" != "x" ]; then
			forward_flag="`cat /proc/sys/net/ipv4/ip_forward`"
			echo 0 > /proc/sys/net/ipv4/ip_forward
			if [ "$fqdn" = "0" ]; then 
				area-ck $PPTP_MY_IP $PPTP_SERVER_IP $PPTP_NETMASK
			else
				PPTP_SERVER_IP_TEMP="`area-ck $PPTP_MY_IP $PPTP_SERVER_IP $PPTP_NETMASK`"
                		area-ck $PPTP_SERVER_IP_TEMP	# 0 -> IP, 1 -> name 
				if [ "$?" = "0" ]; then
					PPTP_SERVER_IP=$PPTP_SERVER_IP_TEMP
				fi
				area-ck $PPTP_MY_IP $PPTP_SERVER_IP $PPTP_NETMASK
			fi
			if [ "$?" != "1" ]; then
				route del default
				#route add default gw $gw_ip
				route add -net ${PPTP_SERVER_IP} netmask 255.255.255.255 gw $gw_ip
			fi
			#iptables -t nat -D POSTROUTING -o $wan_ifname -j SNATP2P --to-source ${PPTP_MY_IP}
			#iptables -t nat -A POSTROUTING -o $wan_ifname -j SNATP2P --to-source ${PPTP_MY_IP}
			nvram set pptp_have_gateway=1
			sleep 2
			echo $forward_flag > /proc/sys/net/ipv4/ip_forward
		fi
                #area-ck $PPTP_SERVER_IP
		#if [ "$?" = 1 ]; then 
                #        for i in $stat_dns
                #        do
                #            area-ck $PPTP_MY_IP $i $PPTP_NETMASK
                #            if [ "$?" != "1" ]; then
                #                route add -net ${i} netmask 255.255.255.255 gw $gw_ip
                #            fi
                #        done
		#fi
		iptables -t nat -D POSTROUTING -o $wan_ifname -j SNATP2P --to-source ${PPTP_MY_IP}
		iptables -t nat -A POSTROUTING -o $wan_ifname -j SNATP2P --to-source ${PPTP_MY_IP}
		echo 1 > /proc/sys/net/ipv4/ip_forward
		/etc/rc.d/dnsmasq.sh restart
	fi

	if [ "$manual" != "manual" ]; then
		if [ "${demand}" = "2" ]; then
			echo "Run Manual Connect..."
			RETVAL=$?
			return $RETVAL
		fi
	fi

	#do dial-on-demand and bring link up when starting
	if [ "`nvram get demandex`" = "1" ]; then
		nvram unset demandex	
		demandex="demandex"
	fi
	${prog} maxfail -1 ${PLUG_IN} ${PPTP_SERVER_IP} ${hostname} "${wan_hostname}" ${conn_id} user ${user} password "${password}" mru ${mru} mtu ${mtu} ${persist_demand} ${demandex} idle ${idle_time} ${usepeerdns} defaultroute lcp-echo-failure 3 lcp-echo-interval 10 unit 0 

	#${prog} user ${user} password ${password} mru ${mru} mtu ${mtu} ${persist_demand} idle ${idle} $usepeerdns defaultroute lcp-echo-failure 3 lcp-echo-interval 30 pty "${PPTP}"
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	/sbin/ifconfig $INTERFACE 0.0.0.0
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	#if [ "`nvram get dy_pptp`" = "0" ]; then
	#	/sbin/ifconfig $wan_ifname 0.0.0.0
	#fi
	killall -1 pppd
	sleep 2	
	killall -9 pppd
	rm -f ${PID_FILE}
	rmmod pptp

	#echo "0" > /proc/fast_pptp	
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

