#!/bin/sh

. /etc/rc.d/tools.sh

INITFILE=/tmp/bridge_init
lan_ifname=`nvram get lan_ifname`
lan_hwifname=`nvram get lan_hwifname`
wan_hwifname=`nvram get wan_hwifname`
wl_ifname=`nvram get wl_ifname`
router_disable=`nvram get router_disable`


lan_hwaddr=`nvram get lan_hwaddr`

#ifconfig  lo 127.0.0.1 netmask 255.0.0.0

ifconfig $lan_hwifname down
brctl delif $lan_ifname $lan_hwifname 2> /dev/null
brctl delif $lan_ifname $wan_hwifname 2> /dev/null

if [ -f $INITFILE ]; then
ifconfig $lan_ifname down
fi

if [ ! -f $INITFILE ]; then
brctl addbr $lan_ifname 2> /dev/null
fi

brctl setfd $lan_ifname 0 2> /dev/null
brctl stp $lan_ifname 0 2> /dev/null

#Use as Access Point
# To avoid to change mac address on br0, there are 2 ways as the followings,
# 1. changed eth1 hwaddr equal to eth0 
# 2. brctl addif br0 eth1, brctl addif br0 eth0

LAN_IPADDR=`nvram get lan_ipaddr`
LAN_SUBNET=`nvram get lan_netmask`

UDHCPC_INIT_PROG="/etc/rc.d/udhcpc.sh"
FIXED_INIT_PROG="/etc/rc.d/static.sh"

WAN_TYPE=`nvram get wan_ether_wan_assign`
WAN_NAME=`nvram get wan_ifname`
WAN_IPADDR=`nvram get wan_ipaddr`
WAN_SUBNET=`nvram get wan_netmask`

WAN_DHCP_IPADDR=`nvram get wan_dhcp_ipaddr`
WAN_DHCP_SUBNET=`nvram get wan_dhcp_netmask`

echo "Configuring LAN , lan_ifname = $lan_ifname ........"

ifconfig $lan_hwifname hw ether $lan_hwaddr
brctl addif $lan_ifname $lan_hwifname 2> /dev/null

ifconfig $lan_hwifname 0.0.0.0
ifconfig $WAN_NAME 0.0.0.0
if [ "$WAN_TYPE" = "1" ]; then
	ifconfig $lan_ifname $WAN_IPADDR netmask $WAN_SUBNET
	${FIXED_INIT_PROG} start
else
#	ifconfig $lan_ifname $LAN_IPADDR netmask $LAN_SUBNET
#	${UDHCPC_INIT_PROG} start $manual
	/etc/rc.d/service_start.sh udhcpc_start
fi

lan_hwaddr=`nvram get lan_hwaddr`
if [ "$router_disable" = "1" ]; then
        ifconfig $wan_hwifname down hw ether $lan_hwaddr
        ifconfig $wan_hwifname up
	#/sbin/set_trailer 1
      brctl addif $lan_ifname $wan_hwifname 2> /dev/null
      hw_ether="`nvram get true_lanif`"
      ifconfig $hw_ether up
      brctl addif $lan_ifname $hw_ether 2> /dev/null
      ifconfig $lan_ifname hw ether $lan_hwaddr
      nvram set wan_factory_mac=$lan_hwaddr
      /etc/rc.d/action.sh reload_lan $LAN_IPADDR
fi

if [ ! -f $INITFILE ]; then
	echo 1 > $INITFILE 	
fi