#!/bin/sh 

. /etc/rc.d/tools.sh

SP='   '

qt () { "$@" >/dev/null 2>&1 ; }
vb () { "$@" ; }
source () { . $1 ; }

[ -f /etc/udhcpc/udhcpc.conf ] && source /etc/udhcpc/udhcpc.conf
[ -f /etc/udhcpc/udhcpc-hooks ] && source /etc/udhcpc/udhcpc-hooks

NVRAM="/usr/sbin/nvram"
RESOLV_CONF="/etc/resolv.conf"
PPTP_PROG="/etc/rc.d/pptp.sh"
ROUTE="/sbin/route"
###############################################################################
#General utilities to process lists of environment variables by CS
###############################################################################
walk_list () 
{
  # x = Variable index, y = count of processed variables
  local BASENAME=$1 x=$2 PROCEDURE=$3 ITEM="" y="0"
  shift 3

  while
    eval ITEM="\$$BASENAME$x"
    [ "$ITEM" != "" ]
  do
    y=$(($y + 1))

    # 'Call' the procedure, passing the variable to process and any args
    eval $PROCEDURE $BASENAME$x $*

    x=$(($x + 1))
  done

  WALK_COUNT=$y
}

dns_writeline () 
{
	local LINE
	eval LINE="\$$1"
	echo "nameserver	$LINE" >>$2 && vb echo -n "$LINE "

	area-ck $LINE $ip $subnet
	if [ $? = 0 ];then
	$ROUTE add -net $LINE netmask 255.255.255.255 gw $router
	fi
}

check_subnet () 
{
	area-ck $1 $2 $3
	return $?
}

############################################################################
### Supercede function (requires SUPERCEDE=YES in /etc/udhcpc.conf)
############################################################################
# $1 : the path of resolv.conf file
#
supercede_it () 
{
	echo "" > $1
	echo "search	$DOMAINS" >>$1

	walk_list SUPERCEDE_DNS $INIT_INDEX dns_writeline $1

	echo "nameserver 127.0.0.1" >>$1 \
	&& vb echo -n "127.0.0.1"
	vb echo "Finished!"
	vb echo
}

###########################################################################
### Pre-append function
###########################################################################
# $1 : the path of resolv.conf file
#
preappend_it () 
{
	walk_list PRE_DNS $INIT_INDEX dns_writeline $1
	vb echo
}


###########################################################################
### Append function 
###########################################################################
# $1 : the path of resolv.conf file
#
append_it () 
{
	walk_list APPEND_DNS $INIT_INDEX dns_writeline $1
	vb echo
}

###########################################################################
### Function to convert dot-format to lenght format
###########################################################################

mask2bits () {
	case $1 in
	255.255.255.255)	echo 32	;;
	255.255.255.254)	echo 31	;;
	255.255.255.252)	echo 30	;;
	255.255.255.248)	echo 29	;;
	255.255.255.240)	echo 28	;;
	255.255.255.224)	echo 27	;;
	255.255.255.192)	echo 26	;;
	255.255.255.128)	echo 25	;;
	255.255.255.0)	echo 24	;;
	255.255.254.0)	echo 23	;;
	255.255.252.0)	echo 22	;;
	255.255.248.0)	echo 21	;;
	255.255.240.0)	echo 20	;;
	255.255.224.0)	echo 19	;;
	255.255.192.0)	echo 18	;;
	255.255.128.0)	echo 17	;;
	255.255.0.0)	echo 16	;;
	255.254.0.0)	echo 15	;;
	255.252.0.0)	echo 14	;;
	255.248.0.0)	echo 13	;;
	255.240.0.0)	echo 12	;;
	255.224.0.0)	echo 11	;;
	255.192.0.0)	echo 10	;;
	255.128.0.0)	echo 9	;;
	255.0.0.0)		echo 8	;;
	254.0.0.0)		echo 7	;;
	252.0.0.0)		echo 6	;;
	248.0.0.0)		echo 5	;;
	240.0.0.0)		echo 4	;;
	224.0.0.0)		echo 3	;;
	192.0.0.0)		echo 2	;;
	128.0.0.0)		echo 1	;;
	0.0.0.0)		echo 0	;;
	*)		echo 32	;;
	esac
} #End of mask2bits

############### run pptp ####################
up_pptp () 
{
	if [ -n $ip ]; then
		touch /tmp/ppp/pppd_up
		ifconfig $interface $ip netmask $subnet
		#$ROUTE add default gw $router
		$NVRAM set wan0_ipaddr=$ip
		$NVRAM set wan0_netmask=$subnet
		$NVRAM set wan_dhcp_ipaddr=$ip
		$NVRAM set wan_dhcp_netmask=$subnet
		$NVRAM set wan_dhcp_ifname=`$NVRAM get wan_hwifname`
		pptp_ser=`$NVRAM get wan_pptp_server_ip`
		for i in $dns
		do
			area-ck $i $ip $subnet
			if [ "$?" = "0" ];then
				$ROUTE add -net $i netmask 255.255.255.255 gw $router
			fi
		done
		if [ -n $router ]; then
			for i in $router
			do
				nvram set wan0_gateway=$i
			done
		fi
		/etc/rc.d/dnsmasq.sh restart 1		# 1 -> indicate dynpptp dhcp state
		sleep 1
		nvram unset dns_pptp_server_addr
		domain_ip=`area-ck $pptp_ser $ip $subnet`
		if [ $? = 0 ];then
			if [ -n "$domain_ip" ]; then
			nvram set dns_pptp_server_addr=$domain_ip
			$ROUTE add -net $domain_ip netmask 255.255.255.255 gw $router
			else
			$ROUTE add -net $pptp_ser netmask 255.255.255.255 gw $router
			fi
		fi
		
		$NVRAM set wan_dhcp_router=$router
		manual="`$NVRAM get manual_start`"
		$NVRAM unset manual_start
		#$PPTP_PROG start $manual
		/etc/rc.d/start_firewall_1.sh
		/etc/rc.d/start_firewall_2.sh
		$PPTP_PROG start $manual
	fi
	if [ -n "$sroute" ]; then
	  #echo "sroute : $sroute"
	  #sIp=`echo $sroute | cut -d" " -f1`
	  #sGw=`echo $sroute | cut -d" " -f2`
	  sIp=
	  sGw=
	  for ii in $sroute; do
		[ "$sIp" = "" ] && sIp=$ii && continue
		sGw=$ii

		staticRoute="`$NVRAM get lan_route`"
		find_route=0
		for i in $staticRoute
		do
			sr_ip=`echo $i | cut -d: -f1`
			sr_msk=`echo $i | cut -d: -f2`
			sr_gw=`echo $i | cut -d: -f3`
			if [ "$sr_ip" = "$sIp" ] && [ "$sr_msk" = "255.255.255.255" ]; then
				#echo "find same static route ..."
				find_route=1
			fi
		done
		if [ $find_route = 0 ]; then
			$ROUTE add -net $sIp netmask 255.255.255.255 gw $sGw
		else
			if [ "$sr_gw" != "$sGw" ]; then
				$ROUTE del -net $sIp netmask 255.255.255.255 gw $sGw
			fi
		fi
		sIp=
	  done
	fi
	if [ -n "$csroute" ]; then
	  #echo "csroute : $csroute"
	  #csGw=`echo $csroute | cut -d" " -f2`
	  #csTemp=`echo $csroute | cut -d" " -f1`
	  csTemp=
	  csGw=
	  for ii in $csroute; do
		[ "$csTemp" = "" ] && csTemp=$ii && continue
		csGw=$ii
		csMask=`echo $csTemp | cut -d. -f1`
		csIP1=`echo $csTemp | cut -d. -f2`
		csIP2=`echo $csTemp | cut -d. -f3`
		csIP3=`echo $csTemp | cut -d. -f4`
		csIP4=`echo $csTemp | cut -d. -f5`
		if [ -z "$csIP1" ]; then
			csIP1=0;
		fi
		if [ -z "$csIP2" ]; then
			csIP2=0;
		fi
		if [ -z "$csIP3" ]; then
			csIP3=0;
		fi
		if [ -z "$csIP4" ]; then
			csIP4=0;
		fi
		csIpaddr="$csIP1.$csIP2.$csIP3.$csIP4"
		staticRoute="`$NVRAM get lan_route`"
		find_route=0
		for i in $staticRoute
		do
			sr_ip=`echo $i | cut -d: -f1`
			sr_msk=`echo $i | cut -d: -f2`
			sr_gw=`echo $i | cut -d: -f3`
			if [ "$sr_ip" = "$csIpaddr" ] && [ "$sr_msk" = "`print_netmask $csMask`" ]; then
				find_route=1
			fi
		done
		if [ $find_route = 0 ]; then
			$ROUTE add -net $csIpaddr netmask `print_netmask $csMask` gw $csGw
		else
			if [ "$sr_gw" != "$csGw" ]; then 
				$ROUTE del -net $csIpaddr netmask `print_netmask $csMask` gw $csGw
			fi
		fi
		csTemp=
	  done
		
	fi
}

###################### Write the dns information to file ###############

write_dns_file () 
{
	echo -n > $RESOLV_CONF

	case $SET_DOMAIN in
	YES|Yes|yes)
		echo search	$DOMAINS>> $RESOLV_CONF
		$NVRAM set wan_dhcp_domain=$DOMAINS
		;;
	*)
		[ -n "$domain" ] && echo search	$domain>> $RESOLV_CONF
		$NVRAM set wan_dhcp_domain=$domain
		;;
	esac
	
	preappend_it $RESOLV_CONF
	j=0;
	
	$NVRAM unset wan_dhcp_dns0  
	$NVRAM unset wan_dhcp_dns1  
	$NVRAM unset wan_dhcp_dns2  
	for i in $dns
	do
		echo adding dns $i
	echo nameserver	$i >> $RESOLV_CONF
	$NVRAM set wan_dhcp_dns$j=$i
	j=$(($j+1));
	done

	case $D_SUPERCEDE in
		YES|Yes|yes)
		supercede_it $RESOLV_CONF
		;;
	*)
		echo "Finished!"
		;;
	esac
}

###########################################################################
### Let's do something !!! Main !!!
###########################################################################
case $1 in
	deconfig)
		nvram unset wan0_ipaddr
		nvram unset wan0_netmask
		nvram unset wan0_gateway
        /usr/sbin/nvram unset wan_dhcp_ipaddr
        /usr/sbin/nvram unset wan_dhcp_netmask
        /usr/sbin/nvram unset wan_dhcp_gateway
		#We need to give the kernel some time to get the interface up.
		sleep 3
		no_connection
		$PPTP_PROG stop
		echo 2 > /proc/fast_nat
		echo 1 > /proc/fast_pptp
        ifconfig $interface 0.0.0.0
		;;
	bound)
		if [ "$WRITE_DNS_FILE" = "YES" ]; then
			write_dns_file
			nvram set wan0_dns="$dns"
		fi
		up_pptp;
		reload_all;
		;;
	renew)
		if [ "`$NVRAM get wan_dhcp_ipaddr`" != "$ip" ]; then
			$PPTP_PROG stop
			if [ "$WRITE_DNS_FILE" = "YES" ]; then
				write_dns_file
				nvram set wan0_dns="$dns"
			fi
			up_pptp;

		elif [ "$WRITE_DNS_FILE" = "YES" ]; then
			local wan_dhcp_dns0=`$NVRAM get wan_dhcp_dns0`
			local wan_dhcp_dns1=`$NVRAM get wan_dhcp_dns1`	
			for i in $dns; do
				if [ "$i" != "$wan_dhcp_dns0" -a "$i" != "$wan_dhcp_dns0" ]; then
					if [ "$wan_dhcp_dns0" != "" ]; then
						$ROUTE del -net $wan_dhcp_dns0 netmask 255.255.255.255 
					fi
					if [ "$wan_dhcp_dns1" != "" ]; then
						$ROUTE del -net $wan_dhcp_dns1 netmask 255.255.255.255 
					fi
					j=0;
					for ii in $dns; do
						$NVRAM set wan_dhcp_dns$j=$ii
						j=$(($j+1));

						area-ck $ii $ip $subnet
						if [ "$?" = "0" ];then
							$ROUTE add -net $ii netmask 255.255.255.255 gw $router
						fi
					done
					break 1
				fi
			done	
		fi
		no_connection
		reload_all
		;;
	leasefail)
		#logger -- Lease failed. Now use the default ip.
		;;
	nak)
		;;
	*)
		### Connection test script
		echo This is an error ... unknown signal!
		if ping -q -c 1 $1; then
			timeout_using_old_lease=TRUE
			for i in $new_router; do
				ip route add default via $i
			done
			echo search $new_domain_name >/etc/resolv.conf.std
			for i in $dns; do
				echo adding dns $i
				echo nameserver	$i >>/etc/resolv.conf.std
			done
			if [ -f /etc/resolv.conf ]; then
				rm -f /etc/resolv.conf
				ln /etc/resolv.conf.std /etc/resolv.conf
			fi
			exit 0
		fi
		qt ip -f inet addr flush dev $interface
		ip -f inet link set dev $interface down
		exit 1
		;;
esac

exit 0
