#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

accept="ACCEPT"

RETVAL=0

iptables="iptables"

wan_ifname=`nvram get wan_ifname`
lan_ifname=`nvram get lan_ifname`


# forward_port format : d-d>ip:d-d,proto,on/off,description
# argument1: type [add|del]
# argument2: number of nvram
port_forward_entry()
{
	if [ "$1" = "add" ]; then
		ACT="-A"
	elif [ "$1" = "del" ]; then
		ACT="-D"
	else
		return;
	fi

	local forward="`nvram get forward_port$2`"
	local dport=`echo $forward | awk -F\> '{print $1}'`
	local token2=`echo $forward | awk -F\> '{print $2}'`
	local to_dest=`echo $token2 | awk -F, '{print $1}'`
	local proto=`echo $token2 | awk -F, '{print $2}'`
	local on_off=`echo $token2 | awk -F, '{print $3}'`
	local dport_s=`echo $dport | awk -F- '{print $1}'`
	local dport_e=`echo $dport | awk -F- '{print $2}'`
	dport="$dport_s:$dport_e"
	local wan_default_iface="0"
	local wan_ipaddr=`nvram get wan${wan_default_iface}_ipaddr`

	if [ "$on_off" = "on" ]; then
		if [ "$proto" = "*" ]; then
			proto="tcp"
			$iptables $ACT fwd_port_forward -p $proto --dport $dport -m log --log-prefix "[LAN access from remote]" -j $accept
			$iptables -t nat $ACT nat_port_forward -p $proto --dport $dport -j DNAT --to-destination $to_dest --dnat-type forwarding
			proto="udp"
		fi
		$iptables $ACT fwd_port_forward -p $proto --dport $dport -m log --log-prefix "[LAN access from remote]" -j $accept
		$iptables -t nat $ACT nat_port_forward -p $proto --dport $dport -j DNAT --to-destination $to_dest --dnat-type forwarding
	fi

}

add_one()
{
	[ "$1" = "" ] && return
	num=$1
	port_forward_entry add "${num}"
	num=$(($num + 1))
	local forward=`nvram get forwarding${num}`	#for NetMeeting
	[ "$forward" = "" ] && return
        local netmeeting=`echo $forward | awk -F' ' '{print $6}'`
	[ $netmeeting -eq 1 ] && port_forward_entry add "${num}"
}


delete_one()
{
	[ "$1" = "" ] && return
	num=$1
	port_forward_entry del "${num}"
	num=$((num+1))
	local forward=`nvram get forwarding${num}`	#for NetMeeting
	[ "$forward" = "" ] && return
        local netmeeting=`echo $forward | awk -F' ' '{print $6}'`
	[ $netmeeting -eq 1 ] && port_forward_entry del "${num}"
}


start() {
	num=1
	entry="$(nvram get forward_port${num})"
	while [ "$entry" != "" ]; do
		port_forward_entry add $num
		num=$(($num+1))
		entry="`nvram get forward_port${num}`"
	done
}


stop() {
	$iptables -F fwd_port_forward
	$iptables -F nat_port_forward -t nat
}

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  add_one)
	add_one $2
	;;
  delete_one)
	delete_one $2
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|add_one|delete_one|restart}"
	exit 1
esac

exit $RETVAL
