#!/bin/sh

[ -f /usr/sbin/ripd ] || exit 0

RETVAL=0
prog="/usr/sbin/ripd"
PID_FILE="/var/run/ripd.pid"
rip_ver=`nvram get rip_ver`

if [ "$rip_ver" = "1" ] || [ "$rip_ver" = "2" ]; then
	rip_type="broadcast"
elif [ "$rip_ver" = "3" ]; then
	rip_type="multicast"
fi

start() {
	# Start daemons.
	[ "$rip_ver" = "0" -o "$rip_ver" = "" ] && exit 0
	
		echo $"Starting $prog: "
		${prog} &
	iptables -I input_init 2 -p udp --dport 520 -m pkttype ! --pkt-type $rip_type -j DROP
	iptables -A local_server -p udp --dport 520 -j ACCEPT
	RETVAL=$?
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	killall -9 ripd
	iptables -D local_server -p udp --dport 520 -j ACCEPT > /dev/null 2>&1
	iptables -D input_init -p udp --dport 520 -m pkttype ! --pkt-type broadcast -j DROP > /dev/null 2>&1
	iptables -D input_init -p udp --dport 520 -m pkttype ! --pkt-type multicast -j DROP > /dev/null 2>&1
	rm -f ${PID_FILE}
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

