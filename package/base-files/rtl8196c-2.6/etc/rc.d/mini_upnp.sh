#!/bin/sh

prog="mini_upnpd"

start() {
	local UPNP_ENABLED=`nvram get upnp_enable`
	local WSC_DISABLE=`nvram get wsc_disable`

	_CMD=
	if [ $WSC_DISABLE = 0 ]; then
		_CMD="$_CMD -wsc /tmp/wscd_config"
	fi
	
	if [ $UPNP_ENABLED != 0 ]; then
		_CMD="$_CMD -igd /tmp/igd_config"
	fi
	
	#if [ "$GATEWAY" = 'true' ] && [ "$OP_MODE" != '1' ]; then
	#	eval `$GETMIB UPNP_ENABLED`
	#	if [ $UPNP_ENABLED != 0 ]; then
	#		_CMD="$_CMD -igd /tmp/igd_config"
	#	fi
	#fi	
		
	if [ "$_CMD" != "" ]; then
		echo $"Starting $prog: "
		mini_upnpd $_CMD &
	fi
	
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#f [ -e ${PID_FILE} ]; then
	#kill `cat ${PID_FILE}`
	#rm -f ${PID_FILE}
	#i
	killall -9 mini_upnpd
	
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL