#!/bin/sh

ACTION_PROG="/etc/rc.d/action.sh"
NVRAM=/usr/sbin/nvram


while true; do

	restore_enable=`${NVRAM} get restore_defaults`

	if [ "$restore_enable" = "1" ]; then
		
		${NVRAM} unset restore_defaults
		${ACTION_PROG} restore

	fi

	action=`${NVRAM} get action`
	${NVRAM} unset action
	
	if [ -n "$action" ]; then

		for i in $action 
		do 
			if [ "$i" = "1" ]; then
				
				${ACTION_PROG} reboot
				
			elif [ "$i" = "2" ]; then
				
				${ACTION_PROG} restart

			elif [ "$i" = "3" ]; then

				${ACTION_PROG} ip_up
				
         	elif [ "$i" = "4" ]; then
         	
				${ACTION_PROG} script

			elif [ "$i" = "5" ]; then
				
				${ACTION_PROG} commit
				
			else

				${ACTION_PROG} others "$i"
				
        	fi
        	
		done	
	fi	
		
	
      	${ACTION_PROG} wps_restart	
	
      	${ACTION_PROG} upnp_wan_restart
	       

	/sbin/alarm 0
done

