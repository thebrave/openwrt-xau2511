#!/bin/sh

NVRAM=/usr/sbin/nvram
CAT=/bin/cat
ECHO=/bin/echo

region=$(${CAT} /tmp/region-setted)
if [ "$region" = "NA" ]; then
#	${NVRAM} set GUI_Region="English"
#	${NVRAM} set wl_country_code=16
#	${NVRAM} set wl_country=16
	${NVRAM} set ntp_hidden_select=4
fi

#if [ "$region" = "CE" ]; then
#	${NVRAM} set GUI_Region="English"
#	${NVRAM} set wl_country_code=7
#	${NVRAM} set wl_country=7
#	${NVRAM} set ntp_hidden_select=17
#fi

country=$(${NVRAM} get wl_country)
if [ "$country" = "0" ]; then
	${NVRAM} set wl_country=7
fi

fw_version=$(${CAT} /firmware_version)
${NVRAM} set fw_version="$fw_version$region"
${ECHO} "firmware_version = $fw_version$region"

exit $RETVAL

