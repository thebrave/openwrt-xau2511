#!/bin/sh

# This script is called when DUT synchronizes with NTP server

nvram set ntp_bootup="`date +%s`"
nvram set dut_uptime="`cat /proc/uptime  | awk '{print $1}' | awk -F'.' '{print $1}'`"
/etc/rc.d/update_log_timestamp.sh
/etc/rc.d/cmdsched.sh restart both

#ticket 64 connection status-obtain time and lease time
if [ "`nvram get dhcpc_lease_obtain_update`" = 1 ]; then
	nvram set dhcpc_lease_obtain="`date +%c`"
	nvram unset dhcpc_lease_obtain_update
fi



