#!/bin/sh

. /etc/rc.d/tools.sh
. /etc/rc.d/firewall_setup_function.sh

iptables="iptables"
TC_PROG="/etc/rc.d/TC.sh"

#[ -f $iptables ] || exit 0

accept="ACCEPT"
drop="DROP"
reject="REJECT"
log="LOG"
wan_ifname=`nvram get wan_ifname`
lan_ifname=`nvram get lan_ifname`


LAN_IPADDR=`nvram get lan_ipaddr`
LAN_SUBNET=`nvram get lan_netmask`
LAN_masklen=`print_masklen $LAN_SUBNET`
LAN_subnet=`print_subnet $LAN_IPADDR $LAN_SUBNET`




RETVAL=0
#echo 2 > /proc/fast_nat
/etc/rc.d/wan_ping.sh
/etc/rc.d/remote.sh start

if [ `nvram get nat_disable` = 0 ]; then

	# Block WAN ping	
	#wan_ping $wan_ifname

	# virtual server
	/etc/rc.d/forward_port.sh start
	# port trigger
	/etc/rc.d/autofw.sh start
	#fw_dmz
	/etc/rc.d/dmz.sh start
	
	#Aron add natgear spec TCP mss item---> pptp=mtu-80, other=mtu-40
	WAN_PROTO=`nvram get wan_proto`
	case "$WAN_PROTO" in	
		static|dhcp)
			proto_mtu=`nvram get wan_dhcp_mtu`
			MSS_MAX=$(($proto_mtu-40))
			;;
		pppoe)
			proto_mtu="$(nvram get wan_ppp_mtu)"
			if [ "x$proto_mtu" = "x" ]; then
			proto_mtu=`nvram get wan_pppoe_mtu`
			fi
			if [ "$proto_mtu" -gt "1492" ]; then
				proto_mtu=1492
			fi
			MSS_MAX=$(($proto_mtu-40))
			;;
		pptp)
			proto_mtu="$(nvram get wan_ppp_mtu)"
			if [ "x$proto_mtu" = "x" ]; then
			proto_mtu=`nvram get wan_pptp_mtu`
			fi
			if [ "$proto_mtu" -gt "1492" ]; then
                                proto_mtu=1492
            fi
			MSS_MAX=$(($proto_mtu-80))
			;;
		*)
			proto_mtu=`nvram get wan_dhcp_mtu`
			MSS_MAX=$(($proto_mtu-40))
	esac
	WAN_IF=`nvram get wan_ifname`
	$iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -m tcpmss --mss $(($MSS_MAX+1)):1600 -j TCPMSS --set-mss $MSS_MAX
	
	
fi

/etc/rc.d/dns_hijack.sh start

# DOS firewall
dos_firewall_advance
dos_ip_spoofing
/etc/rc.d/spi_dos.sh start 

# Inbound defaults
#$iptables -A INPUT -j $log --log-level info --log-prefix "INPUT DROP : "
#$iptables -A FORWARD -j $log --log-level info --log-prefix "FORWARD DROP : "

if [ `nvram get DHCPRelayEnabled` = 1 ]; then
#	iptables -A INPUT -i `nvram get lan_ifname` -p udp --dport 67 -m state --state NEW -j ACCEPT
	iptables -A local_server -p udp --dport 67 -m state --state NEW -j ACCEPT
fi

# accept GRE for PPTP pass-through
iptables -I FORWARD -p 0x2f -j ACCEPT
# for PPTP server send LCP-request 
if [ "`nvram get wan_proto`" = "pptp" ]; then
    HW_WAN_IF="`nvram get wan_hwifname`"
    iptables -A INPUT -i $HW_WAN_IF -p 0x2f -j ACCEPT
    iptables -A INPUT -i $HW_WAN_IF -p tcp --sport 1723 -j ACCEPT
fi

# Inbound accepts
$iptables -A input_init -i $lan_ifname -m state --state NEW -j $accept
$iptables -A forward_init -i $lan_ifname -m state --state NEW -j $accept

if [ `nvram get nat_disable` = "1" ]; then
$iptables -A forward_init -i $wan_ifname -m state --state NEW -j $accept
fi

# Allow established outbound connections back in
$iptables -A input_init -m state --state ESTABLISHED,RELATED -j $accept
$iptables -A forward_init -m state --state ESTABLISHED,RELATED -j $accept

#Traffic Control
${TC_PROG} restart

exit $RETVAL
