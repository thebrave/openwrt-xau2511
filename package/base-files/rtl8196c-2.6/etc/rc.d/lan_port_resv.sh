#!/bin/sh

modulue_load=`lsmod | grep lan_port_resv`

if [ "$modulue_load" != "" ]; then

	# First, flush the reserved port table
	proc_fs_name="lan_port_reserved_table"
	echo "CLEAN" > /proc/$proc_fs_name
	
	# Remote Management
	rmEnable=`nvram get remote_endis`

	if [ "$rmEnable" = "1" ]; then
		rmPort=`nvram get remote_port`
		wan_ipaddr=`nvram get wan_default_ipaddr`
		echo "$wan_ipaddr TCP $rmPort $rmPort" > /proc/$proc_fs_name
	fi

	# Port Forwarding
	# example: forwarding1=NetMeeting TCP 1720 1720 192.168.1.100 0
	for item in `nvram show | grep ^forwarding | awk '{print $5 "_" $3 "_" $4 "_" $2}'`
	do
		forward_IP=`echo $item | awk -F"_" '{print $1}'`
		forward_PORT_s=`echo $item | awk -F"_" '{print $2}'`
		forward_PORT_e=`echo $item | awk -F"_" '{print $3}'`
		forward_Proto=`echo $item | awk -F"_" '{print $4}'`
		if [ "$forward_Proto" = "TCP/UDP" ]; then
			forward_Proto="ANY"
		fi
		
		echo "$forward_IP $forward_Proto $forward_PORT_s $forward_PORT_e" > /proc/$proc_fs_name
	done

	# Port Triggering
	# example: trigger2 0 any TCP 1234 TCP/UDP 12345 12345 0
	trigger_en=`nvram get disable_port_trigger`
	for item in `nvram show | grep ^triggering | awk '{print $3 "_" $7 "_" $8 "_" $6}'`
	do
		trigger_IP=`echo $item | awk -F"_" '{print $1}'`
		# Currently, we ignore trigger out IP address == any condition
		if [ "$trigger_IP" = "any" ]; then
			trigger_IP="255.255.255.255"
		fi
		trigger_PORT_s=`echo $item | awk -F"_" '{print $2}'`
		trigger_PORT_e=`echo $item | awk -F"_" '{print $3}'`
		trigger_in_Proto=`echo $item | awk -F"_" '{print $4}'`
		if [ "$trigger_in_Proto" = "TCP/UDP" ]; then
			trigger_in_Proto="ANY"
		fi
		echo "$trigger_IP $trigger_in_Proto $trigger_PORT_s $trigger_PORT_e" > /proc/$proc_fs_name
	done
	
	# UPNP
	# example: YES;TCP;9999;8888;192.168.1.2;UPnP; @#$&*!
	upnpEnable=`nvram get upnp_enable`
	if [ "$upnpEnable" = "1" ] && [ -f /tmp/upnp_portmap ]; then
		for item in `cat /tmp/upnp_portmap | awk '{print $1}'`
		do
			upnp_IP=`echo $item | awk -F";" '{print $5}'`
			upnp_Proto=`echo $item | awk -F";" '{print $2}'`
			upnp_PORT=`echo $item | awk -F";" '{print $4}'`
			echo "$upnp_IP $upnp_Proto $upnp_PORT $upnp_PORT" > /proc/$proc_fs_name
		done
	fi

fi # modulue_load ??

