#!/bin/sh

#Using SIGUSR1 to stop sleeping and update nvram in monitor.sh immediately 
trap 'exit 0' 10

if [ $# -ge 1 ]; then
	sleep_seconds=$1
else
	sleep_seconds=5	#default
fi

count=1

while [ "$count" -le "$sleep_seconds" ]
do
	sleep 1
	count=$(($count+1))
done

