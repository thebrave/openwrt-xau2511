#!/bin/sh

update_v1() {
	model_firmware=`cat /module_name`
  	nvram set model_firmware="$model_firmware"
}
update_v2() {
  nvram set qos_mac_count="0"
  nvram set qos_enable_su="0"
  nvram set qos_su_ip="0.0.0.0"
  nvram set qos_mac_count="0"
  nvram set local_pc_set="0"
}
#update_v3() {
#  nvram set lasergun="pewpewpew"
#}

nvram_last_ver=$1
nvram_current_ver=`nvram get nvram_ver`

if [ "$nvram_current_ver" -lt "$nvram_last_ver" ];then
  tmp_ver=$nvram_current_ver
  while [ "$tmp_ver" != "$nvram_last_ver" ]
  do
    tmp_ver=$(($tmp_ver+1)) 
    update_v$tmp_ver
  done
  
  nvram set nvram_ver=$nvram_last_ver
  nvram commit
#  echo "nvram update to version $tmp_ver from version $nvram_current_ver"
#else
 # echo "nvram version is up to date: version $1"
fi
