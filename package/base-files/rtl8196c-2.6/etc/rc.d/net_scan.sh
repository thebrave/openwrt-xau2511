#!/bin/sh

RETVAL=0
prog="net-scan"
PID_FILE="/var/run/net-scan.pid"

start() {
	[ -e ${PID_FILE} ] && exit 0
	# Start daemons.
	${prog}
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	killall -9 ${prog}
	if [ -e ${PID_FILE} ]; then
		rm -f ${PID_FILE}
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

