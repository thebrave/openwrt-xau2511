#!/bin/sh

RETVAL=0

wan_proto=`nvram get wan_proto`
wan_hwifname=`nvram get wan_hwifname`
case "$wan_proto" in	
	static)
		nvram set wan_ifname=$wan_hwifname
		;;
	dhcp|bigpond)		
		nvram set wan_ifname=$wan_hwifname
		;;
	pppoe)
		nvram set wan_ifname=ppp0
		;;
	pptp)
		nvram set wan_ifname=ppp0
		;;
	*)
esac
RETVAL=$?
echo
exit $RETVAL
