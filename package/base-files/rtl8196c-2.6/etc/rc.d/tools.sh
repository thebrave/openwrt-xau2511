
print_masklen() # $1: netmask (eg, 255.255.255.0)
{
	local value=`echo $1 | awk -F. '{print $1$2$3$4}'`
	
	case "$value" in
	128000)
		echo "1"
		;;
	192000)
		echo "2"
		;;
	224000)
		echo "3"
		;;
	240000)
		echo "4"
		;;
	248000)
		echo "5"
		;;
	252000)
		echo "6"
		;;
	254000)
		echo "7"
		;;
	255000)
		echo "8"
		;;
	25512800)
		echo "9"
		;;
	25519200)
		echo "10"
		;;
	25522400)
		echo "11"
		;;
	25524000)
		echo "12"
		;;
	25524800)
		echo "13"
		;;
	25525200)
		echo "14"
		;;
	25525400)
		echo "15"
		;;
	25525500)
		echo "16"
		;;
	2552551280)
		echo "17"
		;;
	2552551920)
		echo "18"
		;;
	2552552240)
		echo "19"
		;;
	2552552400)
		echo "20"
		;;
	2552552480)
		echo "21"
		;;
	2552552520)
		echo "22"
		;;
	2552552540)
		echo "23"
		;;
	2552552550)
		echo "24"
		;;
	255255255128)
		echo "25"
		;;
	255255255192)
		echo "26"
		;;
	255255255224)
		echo "27"
		;;
	255255255240)
		echo "28"
		;;
	255255255248)
		echo "29"
		;;
	255255255252)
		echo "30"
		;;
	255255255254)
		echo "31"
		;;
	255255255255)
		echo "32"
		;;
	esac
}

print_netmask() # $1: masklen (eg, 24)
{
	local quotient=$(( $1 / 8 ))
	local remainder=$(( $1 % 8 ))
	local mask

	case "$remainder" in
	0)
		mask=0
		;;
	1)
		mask=128
		;;
	2)
		mask=192
		;;
	3)
		mask=224
		;;
	4)
		mask=240
		;;
	5)
		mask=248
		;;
	6)
		mask=252
		;;
	7)
		mask=254
		;;
	esac

	case "$quotient" in
	0)
		echo "$mask.0.0.0"
		;;
	1)
		echo "255.$mask.0.0"
		;;
	2)
		echo "255.255.$mask.0"
		;;
	3)
		echo "255.255.255.$mask"
		;;
	4)
		echo "255.255.255.255"
		;;
	*)
		oc echo "error in print_netmask !!"
		;;
	esac
}

print_subnet() # $1: ip, $2: mask
{
	local ip1=`echo $1 | awk -F. '{print $1}'`
	local ip2=`echo $1 | awk -F. '{print $2}'`
	local ip3=`echo $1 | awk -F. '{print $3}'`
	local ip4=`echo $1 | awk -F. '{print $4}'`
	local mask1=`echo $2 | awk -F. '{print $1}'`
	local mask2=`echo $2 | awk -F. '{print $2}'`
	local mask3=`echo $2 | awk -F. '{print $3}'`
	local mask4=`echo $2 | awk -F. '{print $4}'`
	local subnet1=$(( $mask1 & $ip1 ))
	local subnet2=$(( $mask2 & $ip2 ))
	local subnet3=$(( $mask3 & $ip3 ))
	local subnet4=$(( $mask4 & $ip4 ))

	echo "$subnet1.$subnet2.$subnet3.$subnet4"
}


print_subnet_zero() # $1: ip, $2: mask
{
	local ip1=`echo $1 | awk -F. '{print $1}'`
	local ip2=`echo $1 | awk -F. '{print $2}'`
	local ip3=`echo $1 | awk -F. '{print $3}'`
	local ip4=`echo $1 | awk -F. '{print $4}'`
	local mask1=`echo $2 | awk -F. '{print $1}'`
	local mask2=`echo $2 | awk -F. '{print $2}'`
	local mask3=`echo $2 | awk -F. '{print $3}'`
	local mask4=`echo $2 | awk -F. '{print $4}'`
	local subnet1=$(( $mask1 & $ip1 ))
	local subnet2=$(( $mask2 & $ip2 ))
	local subnet3=$(( $mask3 & $ip3 ))
	local subnet4=$(( $mask4 & $ip4 ))

	if [ "$mask3" = "0" ]; then
		echo "$subnet1.$subnet2"
	else
		echo "$subnet1.$subnet2.$subnet3"
	fi
}


print_broadcast() # $1: ip, $2: mask
{
	local ip1=`echo $1 | awk -F. '{print $1}'`
	local ip2=`echo $1 | awk -F. '{print $2}'`
	local ip3=`echo $1 | awk -F. '{print $3}'`
	local ip4=`echo $1 | awk -F. '{print $4}'`
	local mask1=`echo $2 | awk -F. '{print $1}'`
	local mask2=`echo $2 | awk -F. '{print $2}'`
	local mask3=`echo $2 | awk -F. '{print $3}'`
	local mask4=`echo $2 | awk -F. '{print $4}'`
	local inv_mask1=$((255 - $mask1))
	local inv_mask2=$((255 - $mask2))
	local inv_mask3=$((255 - $mask3))
	local inv_mask4=$((255 - $mask4))
	local broadcast1=$(( $inv_mask1 | $ip1 ))
	local broadcast2=$(( $inv_mask2 | $ip2 ))
	local broadcast3=$(( $inv_mask3 | $ip3 ))
	local broadcast4=$(( $inv_mask4 | $ip4 ))
}

