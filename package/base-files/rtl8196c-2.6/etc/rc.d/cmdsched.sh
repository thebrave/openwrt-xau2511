#!/bin/sh
# this script is used to manage schedule job for blk_site/blk_svc.
# We still not implement 23:59 cron job bug... need to implemented
#

RETVAL=0

CROND="crond"

CRON_CONF_DIR=/tmp/etc/crontabs
CRON_CONF_FILE="$CRON_CONF_DIR"/root
CRON_CONF_DIR_TMP=/tmp/crontabs
CRON_CONF_FILE_EMAIL_TMP="$CRON_CONF_DIR_TMP"/email
CRON_CONF_FILE_SITE_SVC_TMP="$CRON_CONF_DIR_TMP"/site_svc

blk_site_prog="/etc/rc.d/blk_site.sh"
blk_svc_prog="/etc/rc.d/blk_svc.sh"
ALWAYS=0	#all weekdays and 24 hours, service will not stop
case=0

email_full_prog="/etc/email/email_full_log"
email_log_prog="/etc/email/email_log"

crontabs_type="$2"
if [ "x$crontabs_type" = "x" ]; then
	crontabs_type="both"
fi

email_ctl=`nvram get email_notify`

blk_site_ctl=`nvram get block_skeyword`
if [ "$blk_site_ctl" = "1" ]; then
	blk_site_sched=1
fi

blk_svc_ctl=`nvram get blockserv_ctrl`
if [ "$blk_svc_ctl" = "1" ]; then
	blk_svc_sched=1
fi

blk_days=`nvram get schedule_days_to_block`
if [ "$blk_days" = "everyday" ]; then
	blk_days="0,1,2,3,4,5,6,"
fi
end_blk_days=$blk_days
original_blk_days=$blk_days

if [ "$blk_days" = "" ]; then
	blk_site_sched=0
	blk_svc_sched=0
fi

blk_start_time=`nvram get schedule_start_block_time`
blk_start_time_h=`echo "$blk_start_time" | awk -F":" '{print $1}'`
blk_start_time_m=`echo "$blk_start_time" | awk -F":" '{print $2}'`

blk_end_time=`nvram get schedule_end_block_time`
blk_end_time_h=`echo "$blk_end_time" | awk -F":" '{print $1}'`
blk_end_time_m=`echo "$blk_end_time" | awk -F":" '{print $2}'`

[ "$blk_end_time_h" = "24" ] && blk_end_time_h=0	# special case: end hour is 24:00

crond_start()
{
	time_ctl=`nvram get time_zone`
	echo $time_ctl > /tmp/configs/time_zone
	echo start crond !! conf_dir: "$CRON_CONF_DIR"
	"$CROND" -c "$CRON_CONF_DIR"
}

crond_stop()
{
	echo stop crond !!
	killall -9 "$CROND"
}

#
# INPUT 	:H1 M1 H2 M2
# OUTPUT	: if H1:M1 < H2:M2 return 0
#		  if H1:M1 > H2:M2 return 1
#		  if H1:M1 = H2:M2 return 2
# DESCRIPTION	: compare which time is earlier.
#
time_compare()
{
        if [ $3 -gt $1 ]; then
                echo  0
                return
        elif [ $3 -lt $1 ]; then
                echo 1
                return
        elif [ $4 -gt $2 ]; then
                echo 0
                return
        elif [ $4 -lt $2 ]; then
                echo 1
                return
        else	#the same
                echo 2
        fi
}


#
# OUTPUT:	case (global variable)
# DESCRIPT:	distinct the schedule into 3 cases by time of day to block
#
set_case()
{
	if [ "`nvram get schedule_all_day`" = "1" ]; then
        	#case 1: all day, shift the weekday for stopping service
		case=1
        elif [ "$blk_start_time_m" = "$blk_end_time_m" ] && [ "$blk_start_time_h" = "$blk_end_time_h" ]; then
		#another case 1: Happening at user configure 00:00 to 24:00 (24:00 would be transfered to 00:00 above)
		case=1
        elif [ "$(time_compare $blk_start_time_h $blk_start_time_m $blk_end_time_h $blk_end_time_m)" = "0" ]; then
		#case 2: start_time is earlier than end_time.
		case=2
        else
        	#case 3" end_time is earlier than start_time, needs to shift the weekday for stopping service
		case=3
	fi
}


former_day() {
        case $1 in
                0) echo 6;;
                1) echo 0;;
                2) echo 1;;
                3) echo 2;;
                4) echo 3;;
                5) echo 4;;
                6) echo 5;;
        esac
}

later_day() {
        case $1 in
                0) echo 1;;
                1) echo 2;;
                2) echo 3;;
                3) echo 4;;
                4) echo 5;;
                5) echo 6;;
                6) echo 0;;
        esac
}

#
# INPUT 	: if $1 is continuous, get the head and end day from serial days
# OUTPUT	: change global variable end_blk_days and blk_days
# DESCRIPTION	: The function shift blk_days. i.e., 0,1,2,4,5, to 1,2,3,5,6,
#		  (if continuous is set, 0,1,2,4,5 will tranfer to blk_day = 0,4, and end_blk_day=3,6 )
#
shift_blk_days()
{
	days=`echo "$blk_days" | sed -e "s/,/ /g"`
	end_blk_days=
	[ "$1" = "continuous" ] && blk_days=

        for i in $days; do
		if [ "$1" != "continuous" ]; then
			[ "$i" = "6" ] && i=-1
			end_blk_days=${end_blk_days}$(($i+1)),
		else
			local former_exist=0
			local later_exist=0
			for k in $days; do
				[ "$former_exist" = "1" ] || [ "$(former_day $i)" = "$k" ] && former_exist=1
				[ "$later_exist" = "1" ]  || [ "$(later_day $i)" = "$k" ]  && later_exist=1
				[ "$former_exist" = "1" ] && [ "$later_exist" = "1" ] && break
			done
			[ "$former_exist" = "0" ] && blk_days=${blk_days}${i},
			[ "$i" = "6" ] && i=-1
			[ "$later_exist" = "0" ]  && end_blk_days=${end_blk_days}$(($i+1)),
		fi
	done
}

cat_crontabs_email_site_svc()
{
	cat "$CRON_CONF_FILE_EMAIL_TMP" >> "$CRON_CONF_FILE"
	cat "$CRON_CONF_FILE_SITE_SVC_TMP" >> "$CRON_CONF_FILE"
}

make_crontab_conf_email()
{
	local type=`nvram get email_cfAlert_Select`
	local email_hour=`nvram get email_schedule_hour`
	local email_day=`nvram get email_cfAlert_Day`
	
	case "$type" in 
	0) # check_log_len.sh will email full log
		;;
	1)
		echo "0 * * * *" "$email_log_prog" >> "$CRON_CONF_FILE_EMAIL_TMP"
		;;
	2)
		echo "0 $email_hour * * *" "$email_log_prog" >> "$CRON_CONF_FILE_EMAIL_TMP"
		;;
	3)
		echo "0 $email_hour * * $email_day" "$email_log_prog" >> "$CRON_CONF_FILE_EMAIL_TMP"
		;;
  	*)
        	echo "ERROR: unknown email $type type !!!"
		;;
	esac

}

make_crontab_conf_site_svc()
{
	case "$case" in
  	1)	#case 1: all day
		if [ "$blk_days" = "0,1,2,3,4,5,6," ]; then	#special case: services are always on
			ALWAYS=1
		else
			shift_blk_days continuous
		fi
        	;;
  	2)	#case 2: start time < end time, do nothing
		;;
	3)	#case 3: end time < start time
		if [ "$blk_days" != "0,1,2,3,4,5,6," ]; then
			shift_blk_days
		fi
        	;;
  	*)
        	echo "ERROR: schedule error!!!"
        	exit 1
	esac

	if [ "$ALWAYS" = "1" ]; then		#all weekdays and 24 hours, service will not stop
		if [ "$blk_site_sched" -eq "1" ]; then
			`$blk_site_prog restart`
		fi
		if [ "$blk_svc_sched" -eq "1" ]; then
			`$blk_svc_prog stop`
			`$blk_svc_prog start`
		fi
		return
	fi

	if [ "$blk_site_sched" -eq "1" ]; then
		#echo "write blk_site_sched !!!"
		echo "$blk_start_time_m" "$blk_start_time_h" "* *" "$blk_days" "$blk_site_prog" "start" >> "$CRON_CONF_FILE_SITE_SVC_TMP"
		echo "$blk_end_time_m" "$blk_end_time_h" "* *" "$end_blk_days" "$blk_site_prog" "stop" >> "$CRON_CONF_FILE_SITE_SVC_TMP"
	fi

	if [ "$blk_svc_sched" -eq "1" ]; then
		#echo "write blk_svc_sched !!!"
		echo "$blk_start_time_m" "$blk_start_time_h" "* *" "$blk_days" "$blk_svc_prog" "start" >> "$CRON_CONF_FILE_SITE_SVC_TMP"
		echo "$blk_end_time_m" "$blk_end_time_h" "* *" "$end_blk_days" "$blk_svc_prog" "stop" >> "$CRON_CONF_FILE_SITE_SVC_TMP"
	fi
}

#
# DESCRIPTION	: if service should be active now, call them up directly.
#
chk_force_start()
{
	curHour=`date +%H`
	curMin=`date +%M`
	day_in_week=`date +%w`
	days=`echo "$original_blk_days" | sed -e "s/,/ /g"`

	for day in $days
	do
		#special case for case 3, when end time < start time, it means service end time should be in next day.
		#So we check next day for case 3 only.
		[ "$day_in_week" = "$(($day+1))" ] && [ "$case" = "3" ] && 
		[ "$(time_compare $curHour $curMin $blk_end_time_h $blk_end_time_m)" = "0" ] && force_start=1 && break

		[ "$day_in_week" != "$day" ] && continue
			
		case "$case" in
	        1)      #case 1: all day
			force_start=1
			break
                	;;
		2)      #case 2: start time <= curr <  end time
			[ "$(time_compare $blk_start_time_h $blk_start_time_m $curHour $curMin)" != "1" ] &&
			[ "$(time_compare $curHour $curMin $blk_end_time_h $blk_end_time_m)" = "0" ] &&
			force_start=1 && break
              		;;
		3)      #case 3: start time <= curr
			[ "$(time_compare $blk_start_time_h $blk_start_time_m $curHour $curMin)" != "1" ] && 
			force_start=1 && break
               		;;
	       	*)
                	echo "ERROR: schedule error!!!"
		        exit 1
		esac
	done


	if [ "$force_start" -eq "1" ]; then
                if [ "$blk_site_ctl" -eq "1" ]; then
                        "$blk_site_prog" restart
                fi

                if [ "$blk_svc_ctl" -eq "1" ]; then
                        "$blk_svc_prog" stop
                        "$blk_svc_prog" start
                fi
        else
                if [ "$blk_site_ctl" -eq "1" ]; then
                        "$blk_site_prog" stop
                fi

                if [ "$blk_svc_ctl" -eq "1" ]; then
                        "$blk_svc_prog" stop
                fi
        fi
		
}


start()
{
	# we need add e-mail notification scheduler job here
	
	if [ ! -d "$CRON_CONF_DIR" ]; then
		mkdir -p "$CRON_CONF_DIR"
	fi

	if [ -f "$CRON_CONF_FILE" ]; then
		rm -f "$CRON_CONF_FILE"
	fi

	if [ ! -d "$CRON_CONF_DIR_TMP" ]; then
		mkdir -p "$CRON_CONF_DIR_TMP"
	fi

	if [ ! -e "$CRON_CONF_FILE_EMAIL_TMP" ]; then
		touch "$CRON_CONF_FILE_EMAIL_TMP"				
	fi

	if [ ! -e "$CRON_CONF_FILE_SITE_SVC_TMP" ]; then
		touch "$CRON_CONF_FILE_SITE_SVC_TMP"				
	fi
	
	if [ "$crontabs_type" = "both" ] || [ "$crontabs_type" = "site_svc" ]; then
		rm -f "$CRON_CONF_FILE_SITE_SVC_TMP"			
		touch "$CRON_CONF_FILE_SITE_SVC_TMP"			
		if [ "$blk_site_ctl" = "1" ] || [ "$blk_svc_ctl" = "1" ]; then
			set_case
			make_crontab_conf_site_svc
			#crond_stop
			#crond_start
			[ $ALWAYS -eq 0 ] && chk_force_start
		fi
	fi

	if [ "$crontabs_type" = "both" ] || [ "$crontabs_type" = "email" ]; then
		rm -f "$CRON_CONF_FILE_EMAIL_TMP"				
		touch "$CRON_CONF_FILE_EMAIL_TMP"				
		if [ "$email_ctl" = "1" ]; then
			make_crontab_conf_email
			#email start
		fi
	fi

	if [ "$blk_site_ctl" = "1" ] || [ "$blk_svc_ctl" = "1" ] || [ "$email_ctl" = "1" ]; then
		cat_crontabs_email_site_svc
		crond_stop
		crond_start
	fi
		
}

stop()
{
	crond_stop
	rm -f $CRON_CONF_FILE

}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

