#!/bin/sh

WSC_DISABLE=`nvram get wsc_disable`
LAN_MAC=$(nvram get lan_hwaddr | sed -n 's/://gp')
WAN_PROTO=`nvram get wan_proto`

#for IGD device
mkdir -p /var/linuxigd
cp /etc/tmp/picsdesc.skl /var/linuxigd/picsdesc.skl.1
cp /etc/tmp/picsdesc.xml /var/linuxigd/picsdesc.xml.1
/bin/sed -e 's/ee1234cc5678/'${LAN_MAC}'/g' /var/linuxigd/picsdesc.skl.1 > /var/linuxigd/picsdesc.skl
/bin/sed -e 's/ee1234cc5678/'${LAN_MAC}'/g' /var/linuxigd/picsdesc.xml.1 > /var/linuxigd/picsdesc.xml
if [ "$WAN_PROTO" = "pptp" -o "$WAN_PROTO" = "pppoe" ]; then
     /bin/sed -e 's/WANIPConnection/WANPPPConnection/g' /var/linuxigd/picsdesc.skl > /var/linuxigd/picsdesc.skl.2
	 /bin/sed -e 's/WANIPConn1/WANPPPConn1/g' /var/linuxigd/picsdesc.skl.2 > /var/linuxigd/picsdesc.skl.1
	 /bin/sed -e 's/wanipcn.xml/wanpppcn.xml/g' /var/linuxigd/picsdesc.skl.1 > /var/linuxigd/picsdesc.skl
	 /bin/sed -e 's/WANIPConnection/WANPPPConnection/g' /var/linuxigd/picsdesc.xml > /var/linuxigd/picsdesc.xml.2
	 /bin/sed -e 's/WANIPConn1/WANPPPConn1/g' /var/linuxigd/picsdesc.xml.2 > /var/linuxigd/picsdesc.xml.1
	 /bin/sed -e 's/wanipcn.xml/wanpppcn.xml/g' /var/linuxigd/picsdesc.xml.1 > /var/linuxigd/picsdesc.xml
else
    /bin/sed -e 's/WANPPPConnection/WANIPConnection/g' /var/linuxigd/picsdesc.skl > /var/linuxigd/picsdesc.skl.2
	/bin/sed -e 's/WANPPPConn1/WANIPConn1/g' /var/linuxigd/picsdesc.skl.2 > /var/linuxigd/picsdesc.skl.1
	/bin/sed -e 's/wanpppcn.xml/wanipcn.xml/g' /var/linuxigd/picsdesc.skl.1 > /var/linuxigd/picsdesc.skl
	/bin/sed -e 's/WANPPPConnection/WANIPConnection/g' /var/linuxigd/picsdesc.xml > /var/linuxigd/picsdesc.xml.2
	/bin/sed -e 's/WANPPPConn1/WANIPConn1/g' /var/linuxigd/picsdesc.xml.2 > /var/linuxigd/picsdesc.xml.1
	/bin/sed -e 's/wanpppcn.xml/wanipcn.xml/g' /var/linuxigd/picsdesc.xml.1 > /var/linuxigd/picsdesc.xml 
fi
rm /var/linuxigd/picsdesc.skl.1
rm /var/linuxigd/picsdesc.xml.1

#for WFA device
mkdir -p /var/wps
cp /etc/rc.d/simplecfg* /var/wps
cp /etc/rc.d/wscd.conf /var/wps/wscd.conf.1
/bin/sed -e 's/aabbccddeeff/'${LAN_MAC}'/g' /var/wps/wscd.conf.1 > /var/wps/wscd.conf
rm /var/wps/wscd.conf.1

