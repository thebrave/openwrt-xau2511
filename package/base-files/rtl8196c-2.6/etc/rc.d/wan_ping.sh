#!/bin/sh

wan_ping=`nvram get wan_endis_rspToPing`
wan_ifname=`nvram get wan_ifname`

#iptables -D local_server -p icmp --icmp-type 8 -j LOG --log-level info --log-prefix "[WAN PING ACCEPT]"

iptables -D local_server -p icmp --icmp-type 8 -j ACCEPT
iptables -t nat -D nat_dmz -p icmp --icmp-type 8 -j ACCEPT

if [ "$wan_ping" = "1" ]; then
		#iptables -A local_server -p icmp --icmp-type 8 -j LOG --log-level info --log-prefix "[WAN PING ACCEPT]"
		iptables -A local_server -p icmp --icmp-type 8 -j ACCEPT
		iptables -t nat -I nat_dmz -p icmp --icmp-type 8 -j ACCEPT

fi
