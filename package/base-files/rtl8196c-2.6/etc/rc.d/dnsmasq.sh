#!/bin/sh
# -f /usr/local/sbin/dnsmasq ] || exit 0
#[ -f /etc/resolv.conf ] || exit 0

RETVAL=0
prog="dnsmasq"
PID_FILE="/var/run/dnsmasq.pid"
CONFIG_FILE="/var/resolv.conf"
INTERFACE=`nvram get lan_ifname`

wan_proto=`nvram get wan_proto`
auto_dns=`nvram get wan_ether_dns_assign`
wan_domain=`nvram get wan_hostname`
static_dns=`nvram get wan_pptp_static_dns`
dynpptp_dhcp_state="$2"

fixed_dns1=`nvram get wan_ether_dns1`
fixed_dns2=`nvram get wan_ether_dns2`
fixed_dns3=`nvram get wan_ether_dns3`

start() {
	# Start daemons.
	echo $"Starting $prog: "
	if [ "$auto_dns" = "0" -a "$dynpptp_dhcp_state" != "1" ]; then
		dns=`nvram get wan_dns`
		#dns=`nvram get wan$(nvram get wan_default_iface)_dns`
		#echo search $wan_domain > $CONFIG_FILE
		#for i in $dns; do
		#	echo nameserver $i >> $CONFIG_FILE
		#done
		#/usr/sbin/nvram set wan0_dns="$dns"
		if [ "`nvram get router_disable`" = "0" ]; then
			if [ "$wan_proto" = "pptp" -a "$static_dns" = "1" -a "$(nvram get wan0_dns)" != "" ]; then
		    	dns=`nvram get wan0_dns`
			fi
		fi
	else
		if [ "$auto_dns" = "0" ]; then
			dns=`nvram get wan0_dns`
		else
			dns="$fixed_dns1 $fixed_dns2 $fixed_dns3"
		fi
	fi
	
	if [ "`nvram get router_disable`" = "1" ]; then
			mac=`ifconfig $INTERFACE | sed -n 1p | awk '{ print $5 }'`
 			mac4=`echo $mac | awk -F: '{ print $4 }'`
 			mac5=`echo $mac | awk -F: '{ print $5 }'`
 			mac6=`echo $mac | awk -F: '{ print $6 }'`
 			domain=`nvram get wan_hostname`
 			wan_domain="$domain-$mac4$mac5$mac6"
 	fi
 	
	nvram set wan_default_dns="$dns"
	echo search $wan_domain > $CONFIG_FILE
	#echo nameserver 127.0.0.1 >> $CONFIG_FILE
	if [ "$dns" = "" ]; then
		echo "nameserver 206.82.202.46		#netgear" >> $CONFIG_FILE
	else
		cnt=1
		for i in $dns; do
			echo nameserver $i >> $CONFIG_FILE
			if [ "`nvram get router_disable`" = "1" -a "$auto_dns" = "0" ]; then
				nvram set wan_ether_dns${cnt}=$i
			fi
			cnt=$(($cnt+1))
		done
	fi
	#${prog} -h -n -i ${INTERFACE} -r ${CONFIG_FILE}
	# NETGEAR SPEC: Cache should not be implemented
	# add dnsmasq parameter "-c 0"
	${prog} -h -n -c 0 -i ${INTERFACE} -r ${CONFIG_FILE}
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	killall -9 dnsmasq
	rm -f ${PID_FILE}
	rm -f ${CONFIG_FILE}
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

