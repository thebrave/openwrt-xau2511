#!/bin/sh

#[ -f /usr/local/sbin/pppd ] || exit 0

RETVAL=0
prog="pppd"
PID_FILE="/var/run/ppp0.pid"
CALL_FILE="provider_pppoe_1492"
IP_UP="/etc/ppp/ip-up"
IP_DOWN="/etc/ppp/ip-down"
PLUG_IN="plugin rp-pppoe.so"
#INTERFACE=`nvram get wan_hwifname`
wan_ifname=`nvram get wan_hwifname`
user=`nvram get wan_pppoe_username`
password=`nvram get wan_pppoe_passwd`
#mru=`nvram get wan_pppoe_mru`
mru=`nvram get wan_pppoe_mtu`
mtu=`nvram get wan_pppoe_mtu`
idle=`nvram get wan_pppoe_idletime`
demand=`nvram get wan_pppoe_demand`
auto_dns=`nvram get wan_autodns`
manual=$2
pppoe_netmask=`nvram get wan_pppoe_netmask`

if [ "$demand" = "0" ]; then
	persist_demand="persist"
	idle="0"
elif [ "$demand" = "1" ]; then
	if [ "$idle" = "0" ]; then
		persist_demand="persist"
	else
		persist_demand="demand"
		echo 1 > /proc/sys/net/ipv4/ip_forward
	fi
else
	persist_demand="persist"	# for maual trigger 
	idle="0"
fi

if [ "$auto_dns" = "0" ]; then
	usepeerdns=""
else
	usepeerdns="usepeerdns"
fi

if [ "$(nvram get wan_pppoe_wan_assign)" = "1" ]; then
	if [ "$pppoe_netmask" = "0.0.0.0" ];then
	pppoe_ip="$(nvram get wan_pppoe_ip):0.0.0.0"
else
	pppoe_ip=""
		dyn_wan_ip=`nvram get wan_pppoe_ip`
fi
else
	pppoe_ip=""
fi

if [ "$(nvram get wan_pppoe_service)" = "" ]; then
	service_name=""
else
	service_name="rp_pppoe_service $(nvram get wan_pppoe_service)"
fi

start() {
	# Start daemons.
	echo $"Starting $prog: "
	#${prog} call ${CALL_FILE}

	nvram set wan_ifname=ppp0
	ifconfig ${wan_ifname} 0.0.0.0 up
	if [ "$manual" != "manual" ]; then
		if [ "$demand" = "2" ]; then
			echo "Run Manual Connect...."
			RETVAL=$?
			return $RETVAL
		fi
	fi 
	
	#do dial-on-demand and bring link up when starting
	if [ "`nvram get demandex`" = "1" ]; then
		nvram unset demandex	
		demandex="demandex"
	fi

	if [ "$(nvram get wan_pppoe_wan_assign)" = "1" ] && [ "$pppoe_netmask" != "0.0.0.0" ]; then
		wan_hwifname=`nvram get wan_hwifname`
		ifconfig $wan_hwifname $dyn_wan_ip netmask $pppoe_netmask up
		iptables -t nat -D POSTROUTING -o $wan_hwifname -j SNATP2P --to-source $dyn_wan_ip > /dev/null 2>&1
		iptables -t nat -A POSTROUTING -o $wan_hwifname -j SNATP2P --to-source $dyn_wan_ip
		echo 1 > /proc/sys/net/ipv4/ip_forward 
	fi

	${prog} maxfail -1 ${PLUG_IN} $service_name ${wan_ifname} ${pppoe_ip} user ${user} password "${password}" mru ${mru} mtu ${mtu} ${persist_demand} ${demandex} idle ${idle} $usepeerdns defaultroute lcp-echo-failure 3 lcp-echo-interval 10 

	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	if [ "$(nvram get wan_pppoe_wan_assign)" = "1" ] && [ "$pppoe_netmask" != "0.0.0.0" ]; then
		wan_hwifname=`nvram get wan_hwifname`
		ifconfig $wan_hwifname 0.0.0.0
		iptables -t nat -D POSTROUTING -o $wan_hwifname -j SNATP2P --to-source $dyn_wan_ip > /dev/null 2>&1
	fi

	killall -1 pppd
	sleep 2	
	killall -9 pppd
	rm -f ${PID_FILE}

	ETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

