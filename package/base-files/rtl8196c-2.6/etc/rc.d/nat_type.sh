#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

accept="ACCEPT"
log="LOG"
drop="DROP"
reject="REJECT"

RETVAL=0

iptables="iptables"


start() {
	local nat_type=`nvram get nat_type_fullcone`
	local wan_ifname=`nvram get wan_ifname`

	#
	# Add Cone-NAT rules
	#
	if [ "$nat_type" = "1" ]; then #Full Cone NAT
        	$iptables -t nat -A dnat  -j CONENAT --conenat-step dnat --conenat-type full
	        $iptables -A wan_forward -j CONENAT --conenat-step in --conenat-type full

	else    # Restricted Cone NAT
        	$iptables -t nat -A dnat -j CONENAT --conenat-step dnat --conenat-type restrict
        	$iptables -A wan_forward -j CONENAT --conenat-step in --conenat-type restrict
	fi
}

stop() {
	local nat_type=`nvram get nat_type_fullcone`
	local wan_ifname=`nvram get wan_ifname`

	#
	# Add Cone-NAT rules
	#
	if [ "$nat_type" = "1" ]; then #Full Cone NAT
        	$iptables -t nat -D dnat -j CONENAT --conenat-step dnat --conenat-type full
	        $iptables -D wan_forward -j CONENAT --conenat-step in --conenat-type full

	else    # Restricted Cone NAT
        	$iptables -t nat -D dnat -j CONENAT --conenat-step dnat --conenat-type restrict
        	$iptables -D wan_forward -j CONENAT --conenat-step in --conenat-type restrict
	fi
}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL
