#!/bin/sh

RETVAL=0
BOA_INIT_PROG="/etc/rc.d/boa.sh"
UDHCPD_INIT_PROG="/etc/rc.d/udhcpd.sh"
UDHCPC_INIT_PROG="/etc/rc.d/udhcpc.sh"
DNSMASQ_INIT_PROG="/etc/rc.d/dnsmasq.sh"
NTPCLIENT_INIT_PROG="/etc/rc.d/ntpclient.sh"
UPNP_INIT_PROG="/etc/rc.d/upnp.sh"
SYSLOGD_INIT_PROG="/etc/rc.d/syslogd.sh"
DROPBEAR_INIT_PROG="/etc/rc.d/dropbear.sh"
TELNETD_INIT_PROG="/etc/rc.d/telnetd.sh"
SNMPD_INIT_PROG="/etc/rc.d/snmpd.sh"
RESET_BUTTON_PROG="/etc/rc.d/resetbutton.sh"
RIPD_INIT_PROG="/etc/rc.d/ripd.sh"
QOS_INIT_PROG="/etc/rc.d/qos.sh"
ROUTING_PROG="/etc/rc.d/routing.sh"
LLTD_PROG="/etc/rc.d/lltd.sh"
SIP_ALG_PROG="/etc/rc.d/sip_alg.sh"
TC_PROG="/etc/rc.d/TC.sh"
NET_SCAN="/etc/rc.d/net_scan.sh"
SCHE_PROG="/etc/rc.d/cmdsched.sh"
DNS_HIJACK="/etc/rc.d/dns_hijack.sh"
NETBIOS_INIT_PROG="/etc/rc.d/netbios.sh"

route_dns_renew()
{
		router=`/usr/sbin/nvram get wan_default_gateway`
		interface=`nvram get lan_ifname`
		if [ "x$router" != "x" ]; then
			route del default gw $router dev $interface
		fi
		router=`/usr/sbin/nvram get wan_gateway`
		if [ "x$router" != "x" ]; then
			route add default gw $router dev $interface
		fi
		${DNSMASQ_INIT_PROG} restart
}

route_dns_release()
{
		router=`/usr/sbin/nvram get wan_default_gateway`
		interface=`nvram get lan_ifname`
		if [ "x$router" != "x" ]; then
			route del default gw $router dev $interface
		fi
		${DNSMASQ_INIT_PROG} stop
}

start() {
	# Start daemons.
	echo $"Starting Service: "
	#${QOS_INIT_PROG} start
	${BOA_INIT_PROG} start
	${SYSLOGD_INIT_PROG} start
	if [ "`nvram get router_disable`" = "0" ]; then # NOT APmode
		if [ "`nvram get nat_disable`" = "0" ]; then # NAT Enable
			${DNSMASQ_INIT_PROG} start
		fi
#		${UDHCPD_INIT_PROG} start
#		${ROUTING_PROG} start
		${RIPD_INIT_PROG} start
		${SCHE_PROG} start both		# site_svc/email/both  
#		/usr/sbin/telnetenable
#		/usr/sbin/getdeviceinfo&

		lan_ifname=`nvram get lan_ifname`
		lan_ipaddr=`nvram get lan_ipaddr`	
		${DNS_HIJACK} start
		if [ "`nvram get wan_enable_ipv6_passthrough`" = "1" ];then
			echo 1 > /proc/custom_Passthru
		else
			echo 0 > /proc/custom_Passthru
		fi
	fi
	${UPNP_INIT_PROG} start
	${NET_SCAN} start
	${LLTD_PROG} start
	if [ `nvram get router_disable` = 0 ]; then
		#/etc/rc.d/lan_port_resv.sh
		/sbin/lan_port_resv&
	else
#		if [ `nvram get lan_dhcpclient` = 1 ]; then
#			${UDHCPC_INIT_PROG} start
#		else
			route_dns_renew
#		fi
		${NETBIOS_INIT_PROG} start
		${TELNETD_INIT_PROG} reload
		/usr/sbin/getdeviceinfo &
	fi
	
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting Service: "
	${BOA_INIT_PROG} stop
	if [ `nvram get router_disable` = 0 ]; then
#		${UDHCPD_INIT_PROG} stop
		${DNSMASQ_INIT_PROG} stop
		${UPNP_INIT_PROG} stop
		${RIPD_INIT_PROG} stop
		${SCHE_PROG} stop		# site_svc/email/both  
	else
#		if [ `nvram get lan_dhcpclient` = 1 ]; then
#			${UDHCPC_INIT_PROG} stop
#		else
			route_dns_release
#		fi
		${NETBIOS_INIT_PROG} stop
		${TELNETD_INIT_PROG} stop
		killall getdeviceinfo
	fi
	${SYSLOGD_INIT_PROG} stop
	${LLTD_PROG} stop
	${NET_SCAN} stop

	if [ "`nvram get wan_enable_ipv6_passthrough`" = "1" ];then
		echo 0 > /proc/custom_Passthru
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

udhcpc_start()
{
	UDHCPC_PROG="/usr/sbin/udhcpc"
	NETBIOS_NAME=`nvram get ap_netbiosname`
	lan_ifname=`nvram get lan_ifname`
	LAN_IPADDR=`nvram get lan_ipaddr`
	WAN_TYPE=`nvram get wan_ether_wan_assign`
	WAN_IPADDR=`nvram get wan_ipaddr`
	WAN_SUBNET=`nvram get wan_netmask`

	if [ "$WAN_TYPE" = "0" ]; then
		${UDHCPC_PROG} -b -i $lan_ifname -h $NETBIOS_NAME -r 0.0.0.0
		sleep 4
		if [ "`nvram get wan_dhcp_ipaddr`" = "0.0.0.0" ]; then
			ifconfig $lan_ifname $LAN_IPADDR
		fi
	else
		if [ "$WAN_TYPE" = "1" ]; then
			ifconfig $lan_ifname $WAN_IPADDR
		fi
	fi
	sleep 4
	route_dns_renew
}

udhcpc_stop()
{
	killall udhcpc
	sleep 4
}

ui_start() {
	# Start daemons.
	echo $"Starting Service: "
#	#${QOS_INIT_PROG} start
	if [ "`nvram get router_disable`" = "0" ]; then # NOT APmode
		if [ "`nvram get nat_disable`" = "0" ]; then # NAT Enable
			${UPNP_INIT_PROG} start
			${DNSMASQ_INIT_PROG} start
		fi
#		${UDHCPD_INIT_PROG} start
		${ROUTING_PROG} start
		${RIPD_INIT_PROG} start
		${TELNETD_INIT_PROG} reload
		if [ "`nvram get wan_enable_ipv6_passthrough`" = "1" ];then
			echo 1 > /proc/custom_Passthru
		else
			echo 0 > /proc/custom_Passthru
		fi
	else
#		if [ `nvram get lan_dhcpclient` = 1 ]; then
#			${UDHCPC_INIT_PROG} start
#		else
			route_dns_renew
#		fi
		udhcpc_start
		${NETBIOS_INIT_PROG} start
		
		${TELNETD_INIT_PROG} reload
		/usr/sbin/getdeviceinfo &
	fi
	${NET_SCAN} start
	RETVAL=$?
	echo
	return $RETVAL
}

ui_stop() {
	# Stop daemons.
	echo $"Shutting Service: "
#	${UDHCPD_INIT_PROG} stop
	${DNSMASQ_INIT_PROG} stop
	${UPNP_INIT_PROG} stop
	${RIPD_INIT_PROG} stop
	${ROUTING_PROG} stop
	${NET_SCAN} stop
	if [ "`nvram get router_disable`" = "1" ]; then
#		if [ `nvram get lan_dhcpclient` = 1 ]; then
#			${UDHCPC_INIT_PROG} stop
#		else
			route_dns_release
#		fi
		udhcpc_stop
		${NETBIOS_INIT_PROG} stop
		${TELNETD_INIT_PROG} stop
		killall getdeviceinfo
	fi
	if [ "`nvram get wan_enable_ipv6_passthrough`" = "1" ];then
		echo 0 > /proc/custom_Passthru
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
	udhcpc_start)
	udhcpc_start
	;;
	udhcpc_stop)
	udhcpc_stop
	;;
  start_ui)
	ui_start
	;;
  stop_ui)
	ui_stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart|udhcpc_start|udhcpc_stop}"
	exit 1
esac

exit $RETVAL

