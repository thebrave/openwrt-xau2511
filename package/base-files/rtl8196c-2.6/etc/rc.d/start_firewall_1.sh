#!/bin/sh

. /etc/rc.d/tools.sh
. /etc/rc.d/firewall_setup_function.sh

iptables="iptables"

accept="ACCEPT"
log="LOG"
drop="DROP"
reject="REJECT"
lan_ifname=`nvram get lan_ifname`
wan_ifname=`nvram get wan_ifname`
NNTP_PORT=119

#[ -f $iptables ] || exit 0



#for f in /proc/sys/net/ipv4/conf/*/rp_filter; do
#	echo 1 > $f
#done

$iptables -F 
#$iptables -X
$iptables -Z		
$iptables -F -t nat
#$iptables -X -t nat
$iptables -Z -t nat
$iptables -F -t mangle
#$iptables -X -t mangle
#$iptables -Z -t mangle


# Inbound drops
# Drop invalid packets

$iptables -A input_init -m state --state INVALID -j $drop
$iptables -A forward_init -m state --state INVALID -j $drop

# Inbound accepts
#$iptables -A INPUT -i $lan_ifname -m state --state NEW -j $accept
#$iptables -A FORWARD -i $lan_ifname -m state --state NEW -j $accept

# Allow established outbound connections back in
#$iptables -A INPUT  -m state --state ESTABLISHED,RELATED -j $accept
#$iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j $accept

iptables -A INPUT -i $wan_ifname -j input_dos
iptables -A INPUT -j input_init
iptables -A INPUT -i $wan_ifname -j local_server


iptables -A FORWARD -o $wan_ifname -j fwd_block_svc
iptables -A FORWARD -i $lan_ifname -o $wan_ifname -p tcp -j fwd_block_site
iptables -A FORWARD -i $wan_ifname -o $lan_ifname -p tcp --sport $NNTP_PORT -j fwd_block_site_nntp
iptables -A FORWARD -j fwd_port_trigger
iptables -A FORWARD -j forward_init
#iptables -A FORWARD -j fwd_dos_input
iptables -A FORWARD -i $lan_ifname -j lan_forward
iptables -A FORWARD -i $wan_ifname -j wan_forward
iptables -A FORWARD -j fwd_port_forward
iptables -A FORWARD -j fwd_upnp
iptables -A FORWARD -j fwd_dmz
iptables -A FORWARD -j fwd_local_server

iptables -A OUTPUT -j output_init
iptables -A OUTPUT -j output_dos

# IP masquerade(NAT)
if [ `nvram get nat_disable` = "0" ]; then
	/etc/rc.d/nat_start.sh
fi

RETVAL=0

# Inbound defaults

$iptables -P INPUT $drop
#$iptables -P INPUT $accept
$iptables -P FORWARD $drop
#iptables -P FORWARD $accept

$iptables -P OUTPUT $accept

exit $RETVAL
