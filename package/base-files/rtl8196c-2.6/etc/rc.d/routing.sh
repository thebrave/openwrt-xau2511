#!/bin/sh
#
#remove_routes	: Remove old routes
#intput		: interface(lan/wan0)
#
. /etc/rc.d/tools.sh

IPTABLES=/usr/sbin/iptables

LAN_IPADDR=`nvram get lan_ipaddr`
LAN_SUBNET=`nvram get lan_netmask`
LAN_MASKLEN=`print_masklen $LAN_SUBNET`
LAN_SUBNET=`print_subnet $LAN_IPADDR $LAN_SUBNET`

remove_routes()
{
	if [ ! -f /tmp/configs/$1_route ]; then
		return
	fi

	for route in `cat /tmp/configs/$1_route`; do
		#ipaddress:netmask:gateway:metric:rip(on:1, off:0)
		ipaddress=`echo $route | awk -F: '{print $1}'`
		netmask=`echo $route | awk -F: '{print $2}'`
		gateway=`echo $route | awk -F: '{print $3}'`
		metric=`echo $route | awk -F: '{print $4}'`
		rip=`echo $route | awk -F: '{print $5}'`

		area-ck $gateway $LAN_IPADDR $LAN_SUBNET
		if [ "$?" = "1" ]; then
		    masklen=`print_masklen $netmask`
		    subnet=`print_subnet $ipaddress $netmask`
		    $IPTABLES -t nat -D static_privateroute -d $subnet/$masklen -j ACCEPT
		fi
		route del -net $ipaddress netmask $netmask metric $metric gw $gateway
		echo "route del -net $ipaddress netmask $netmask metric $metric gw $gateway (lan_rip=$rip)"
	done
}
#
#add_routes	: Add all routes in lan_route and wan0_route
#input		: interface
#
add_routes()
{
	for route in `nvram get $1_route`; do
		#ipaddress:netmask:gateway:metric:rip(on:1, off:0)
		ipaddress=`echo $route | awk -F: '{print $1}'`
		netmask=`echo $route | awk -F: '{print $2}'`
		gateway=`echo $route | awk -F: '{print $3}'`
		metric=`echo $route | awk -F: '{print $4}'`
		rip=`echo $route | awk -F: '{print $5}'`

		area-ck $gateway $LAN_IPADDR $LAN_SUBNET
		if [ "$?" = "1" ]; then
		    masklen=`print_masklen $netmask`
		    subnet=`print_subnet $ipaddress $netmask`
		    $IPTABLES -t nat -A static_privateroute -d $subnet/$masklen -j ACCEPT
		fi
		route add -net $ipaddress netmask $netmask metric $metric gw $gateway
		echo "route add -net $ipaddress netmask $netmask metric $metric gw $gateway (lan_rip=$rip)"
	done
}

start()
{
	#
	#1. Remove old routes
	#
	echo -e "\n Static routes setting:"
	if [ -f /tmp/configs/rm_old_route ]; then
		rm /tmp/configs/rm_old_route
		remove_routes "lan"
		remove_routes "wan0"
	fi

	nvram get lan_route > /tmp/configs/lan_route
	nvram get wan0_route > /tmp/configs/wan0_route

	#
	#2. add routes in lan_route, wan0_route
	# 
	wan0_ip=`nvram get wan$(nvram get wan_default_iface)_ipaddr`

	add_routes "lan"

	if [ -z $wan0_ip ]; then
		echo WAN is down
		exit 0
	fi

	add_routes "wan0"
}

#wan_only()
#{
#	wan0_ip=`nvram get wan$(nvram get wan_default_iface)_ipaddr`

#        if [ -z $wan0_ip ]; then
#                echo WAN is down
#                exit 0
#        fi

#        add_routes "wan0"
#}

case "$1" in
  start|restart)
        start
 	;;
#  wan)
#	wan_only
#	;;
  *)
        echo $"Usage: $0 {start|restart|wan}"
        exit 1
esac

