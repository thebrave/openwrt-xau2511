#!/bin/sh

NVRAM=/usr/sbin/nvram

change_dev_name=`nvram get change_dev_name`

if [ x"$change_dev_name" =  x1 ]; then
	mac=`nvram get lan_hwaddr`
	product=`nvram get product_id`
	
	mac6=`echo $mac | awk -F: '{ print $6 }'`
	sharename=$product-$mac6
	#echo "---------- sharename=$sharename ----------"
	${NVRAM} set usb_deviceName=$sharename
	${NVRAM} set plc_devicename=$sharename
	${NVRAM} set ap_netbiosname=$sharename
	${NVRAM} set netbiosname=$sharename
	${NVRAM} set change_dev_name=0
	${NVRAM} commit
fi
