#!/bin/sh

#[ -f /usr/local/sbin/ntpclient ] || exit 0
#[ -f /etc/resolv.conf ] || exit 0

RETVAL=0
set_by_GUI=$2
prog="ntpclient"
PID_FILE="/var/run/ntpclient.pid"
time_zone=`nvram get ntp_hidden_select`
ntp_server1=`nvram get ntpserver1`
ntp_server2=`nvram get ntpserver2`
dst_enabled=`nvram get ntpadjust`

adjust_time_zone() {
	time_zone=`nvram get ntp_hidden_select`
	case "$time_zone" in 
		0)
			time_zone="1"
			;;
		1)
			time_zone="2"
			;;
		2)
			time_zone="3"
			;;
		3)
			time_zone="4"
			;;
		4)
			time_zone="5"
			;;
		5)
			time_zone="7"
			;;
		6)
			time_zone="7"
			;;
		7)
			time_zone="8"
			;;
		8)
			time_zone="8"
			;;
		9)
			time_zone="12"
			;;
		10)
			time_zone="12"
			;;
		11)
			time_zone="15"
			;;
		12)
			time_zone="18"
			;;
		13)
			time_zone="20"
			;;
		14)
			time_zone="21"
			;;
		15)
			time_zone="23"
			;;
		16)
			time_zone="23"
			;;
		17)
			time_zone="25"
			;;
		18)
			time_zone="25"
			;;
		19)
			time_zone="25"
			;;
		20)
			time_zone="25"
			;;
		21)
			time_zone="31"
			;;
		22)
			time_zone="31"
			;;
		23)
			time_zone="31"
			;;
		24)
			time_zone="37"
			;;
		25)
			time_zone="38"
			;;
		26)
			time_zone="40"
			;;
		27)
			time_zone="42"
			;;
		28)
			time_zone="43"
			;;
		29)
			time_zone="44"
			;;
		30)
			time_zone="45"
			;;
		31)
			time_zone="46"
			;;
		32)
			time_zone="47"
			;;
		33)
			time_zone="48"
			;;
		34)
			time_zone="51"
			;;
		35)
			time_zone="52"
			;;
		36)
			time_zone="54"
			;;
		37)
			time_zone="55"
			;;
	esac
}


start() {

 	#local gw=$(/sbin/route -n | awk '/^0.0.0.0/ {print $2}')

	# Start daemons.
	echo $"Starting $prog: "
	
	# Waiting wan port connected 
	#while [ "$gw" = "" ]; do
	#	sleep 5
	#	gw=$(/sbin/route -n | awk '/^0.0.0.0/ {print $2}')
	#done
	adjust_time_zone

	#${prog} -d -z ${time_zone} -x ${dst_enabled} -h ${ntp_server1},${ntp_server2} -i 3 -l -s &
	if [ "$set_by_GUI" = "GUI" ]; then
		${prog} -z ${time_zone} -x ${dst_enabled} -h ${ntp_server1} -b ${ntp_server2} -u -i 15 -m 60 -p 123 -s 2 &
	else
		${prog} -z ${time_zone} -x ${dst_enabled} -h ${ntp_server1} -b ${ntp_server2} -i 15 -m 60 -p 123 -s 2 &
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	killall -9 ntpclient
	rm -f ${PID_FILE}
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

