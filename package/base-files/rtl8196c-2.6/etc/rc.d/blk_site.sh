#!/bin/sh

IPTABLES="iptables"

blk_sites_enabled=`nvram get block_skeyword`
#blk_sites_item variable is splited by space character, if any problem, we need handle this variable...
#blk_sites_item=`nvram get block_KeyWord_DomainList`

# block trusted ip
blk_trusted_ip_enable=`nvram get block_endis_Trusted_IP`
blk_trusted_ip=`nvram get block_trustedip`
BLOCK_SITE=fwd_block_site
BLOCK_SITE_NNTP=fwd_block_site_nntp
LAN_IF=`nvram get lan_ifname`
WAN_PROTO=`nvram get wan_proto`

start() 
{
	echo "run blk_site.sh start !!!"
	[ "`nvram get block_KeyWord_DomainList`" = "" ] && exit 0

	#nvram set blk_site_on=1
	#echo 0 > /proc/sys/net/ipv4/ct_fast_forward	#2:Block Sites doesn't work

# if $blk_sites_enabled=2, blk_sites service always running
	"$IPTABLES" -I $BLOCK_SITE 1 -m string --string "NETGEAR" --get-only 1 -j RETURN
	"$IPTABLES" -I $BLOCK_SITE_NNTP 1 -m string --string "NETGEAR" --get-only 1 -j RETURN

#   if [ "$blk_sites_enabled" -gt "0" ] && [ "$blk_trusted_ip_enable" -eq "1" ]; then
      for blk_str in `nvram get block_KeyWord_DomainList`;
      do
		echo "add $blk_str" > /proc/url_filter
      done
      if [ "$blk_sites_enabled" -gt "0" ] && [ "$blk_trusted_ip_enable" -eq "1" ]; then
      	echo "$blk_trusted_ip" > /proc/trusted_ip
      fi
#   else
#   fi
}

stop()
{
	echo "run blk_site.sh stop !!!"
	
	#nvram set blk_site_on=0
	#[ "`nvram get blk_svc_on`" = "0" ] && echo 1 > /proc/sys/net/ipv4/ct_fast_forward	#2:Block Sites doesn't work

	[ "$blk_sites_enabled" = "0" ] && exit 0
	
	"$IPTABLES" -F $BLOCK_SITE
	"$IPTABLES" -F $BLOCK_SITE_NNTP
	echo "0" > /proc/url_filter
	echo "0.0.0.0" > /proc/trusted_ip
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit 1