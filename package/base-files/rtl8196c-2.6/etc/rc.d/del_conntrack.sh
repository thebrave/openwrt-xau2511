#!/bin/sh

# Usage: del_conntrack.sh service_type entry_num
# Format: "Service_Type Src_IP Min_Port Max_Port"

sType=$1
sItem=$2
sArg_old=$3
sArg_new=$4
proc_fs_name=conntrack_killer

# Port Forwarding function
port_forwarding_func()
{
	entry=`nvram get "$sType""$sItem"`
	Src_IP=`echo $entry | awk '{print $5}'`
	Min_Port=`echo $entry | awk '{print $3}'`
	Max_Port=`echo $entry | awk '{print $4}'`
	echo $sType $Src_IP $Min_Port $Max_Port > /proc/$proc_fs_name
}
	
# Port Triggering function
port_triggering_func()
{
	entry=`nvram get "$sType""$sItem"`
	pTrigger_idv_en=`echo $entry | awk '{print $9}'`
	if [ "$pTrigger_idv_en" = "1" ]; then
		Src_IP=`echo $entry | awk '{print $3}'`
		Min_Port=`echo $entry | awk '{print $7}'`
		Max_Port=`echo $entry | awk '{print $8}'`
		if [ "$Src_IP" = "any" ]; then
			# if src_ip = any, then we set Src_IP=255.255.255.255. 
			# delete_conntrack kernel module will handle it.
			Src_IP=255.255.255.255
		fi

		echo $sType $Src_IP $Min_Port $Max_Port > /proc/$proc_fs_name
	fi
}

# pTrigger_en=`nvram get disable_port_trigger` # 0-> enable, 1-> disable
port_triggering_all_func()
{
	rule_num=`nvram show | grep ^triggering | wc -l`
	if [ "$rule_num" != "0" ]; then
		for item in `nvram show | grep ^triggering | awk -F"=" '{print $1}'`
		do
			entry=`nvram get $item`
			pTrigger_en=`echo $entry | awk '{print $9}'`
			if [ "$pTrigger_en" = "1" ]; then
				Src_IP=`echo $entry | awk '{print $3}'`
				Min_Port=`echo $entry | awk '{print $7}'`
				Max_Port=`echo $entry | awk '{print $8}'`
			if [ "$Src_IP" = "any" ]; then
				# if src_ip = any, then we set Src_IP=255.255.255.255. 
				# delete_conntrack kernel module will handle it.
				Src_IP=255.255.255.255
			fi
			echo $sType $Src_IP $Min_Port $Max_Port > /proc/$proc_fs_name
			fi
		done
	fi
	# disable trigger function, need flush lan port reservation table manually
	#echo "CLEAN" > /proc/lan_port_reserved_table
}

port_triggering_apply_func()
{
	num=1
	total_num=`nvram get port_trigger_num`
	while [ "$num" -le "$total_num" ];
	do
		x=`echo "$sArg_old" | cut -d ',' -f $num`
		y=`echo "$sArg_new" | cut -d ',' -f $num`

#		[ "$x" -eq 0 -a "$y" -eq 1 ] && lpr_for_enable_trigger $sType $num
		[ "$x" -eq 1 -a "$y" -eq 0 ] && sItem=$num && port_triggering_func $sType $sItem
		
                num=$(($num+1))
	done
}

dmz_func()
{
	dmz_ip=`nvram get dmz_ipaddr`
	echo $sType $dmz_ip 0 65535 > /proc/$proc_fs_name
}

# Main function define here
if [ "$sType" = "forwarding" ]; then
	port_forwarding_func
fi

if [ "$sType" = "triggering" ] && [ "$sItem" != "all" ]; then
	port_triggering_func
fi

if [ "$sType" = "triggering" ] && [ "$sItem" = "all" ]; then
	port_triggering_all_func
fi

if [ "$sType" = "triggering" ] && [ "$sItem" = "apply" ]; then
	port_triggering_apply_func
fi

if [ "$sType" = "dmz" ]; then
	dmz_func	
fi

