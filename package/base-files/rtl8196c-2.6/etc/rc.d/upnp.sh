#!/bin/sh

start() {
	#/etc/rc.d/mini_igd.sh start
	/etc/rc.d/mini_upnp.sh start
}

stop() {
	/etc/rc.d/mini_upnp.sh stop
	#/etc/rc.d/mini_igd.sh stop
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

