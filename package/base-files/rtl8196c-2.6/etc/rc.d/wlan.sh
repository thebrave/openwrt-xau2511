#!/bin/sh
#
# script file to start WLAN
#
#set -o xtrace
WLAN_IF="wlan0"
WLAN_VA_IF="wlan0-va"
WAN_IF="eth1"
BR="br0"
sys_uptime=$([ -f /proc/uptime ] && cat /proc/uptime | awk '{print $1}' | awk -F. '{print $1}')
MSSID_num=4
MSSID_ACT=0
MSSID_disable=1
lan_ifname=`nvram get lan_ifname`
lan_hwaddr=`nvram get lan_hwaddr`

Kill_proc()
{
    if [ -f $1 ] ; then
       	  PID=`cat $1`
	  if [ $PID != 0 ]; then
	       kill -9 $PID
	  fi
          rm -f $1
    fi
}

Applications()
{
   if [ "$1" = "start" ]; then
          echo "starting app"
          IF_LIST=$WLAN_IF
          #### MBSSID #### (2,3,4,5)
          num=0
          while [ $num -lt $MSSID_num ]
          do
                  if [ "$(nvram get wl_multi_ssid_endis_$(($num+2)))" = "1" ]; then
                       IF_LIST="$IF_LIST $WLAN_VA_IF$num"
                  fi
    
          num=$(($num+1))
          done
          
          iwcontrol $WLAN_IF
          #added by dvd.chen, to restart mini_upnpd. Cause setting wireless might enable<->disable WPS
          if [ "$(nvram get wsc_stat_change)" = "1" ]; then
                /etc/rc.d/mini_upnp.sh restart
                nvram set wsc_stat_change=0
          fi
          
   else
          echo "stopping app"
          Kill_proc "/var/run/iwcontrol.pid"
          Kill_proc "/var/run/auth-$WLAN_IF.pid"
   
   fi
}

Basic_settings()
{  
    
    #echo "Basic...."
    SSID=`nvram get wl_ssid`
    CH=`nvram get wl_channel`
    MODE=`nvram get wl_mode`
    COUN=`nvram get wl_country`
    WDS=`nvram get wds_endis_fun`
    APSD=`nvram get apsd_enable`
               
    ifconfig $WLAN_IF down
    #if [ "$SSID" = "any" -o "$SSID" = "Any" -o "$SSID" = "aNy" -o "$SSID" = "anY" -o "$SSID" = "ANy"  -o "$SSID" = "AnY"  -o "$SSID" = "aNY" -o "$SSID" = "ANY" ]; then
	#iwpriv $WLAN_IF set_mib deny_any=0
    #else
	#iwpriv $WLAN_IF set_mib deny_any=1
    #fi
    iwpriv $WLAN_IF set_mib ssid="$SSID"
    iwpriv $WLAN_IF set_mib channel=$CH
    iwpriv $WLAN_IF set_mib disable_protection=0
    iwpriv $WLAN_IF set_mib oprates=4095
    iwpriv $WLAN_IF set_mib basicrates=15
    iwpriv $WLAN_IF set_mib shortGI20M=1
    iwpriv $WLAN_IF set_mib shortGI40M=1
    iwpriv $WLAN_IF set_mib ampdu=1
    iwpriv $WLAN_IF set_mib amsdu=1
    iwpriv $WLAN_IF set_mib disable_brsc=0
    iwpriv $WLAN_IF set_mib wifi_specific=2
    iwpriv $WLAN_IF set_mib MIMO_TR_mode=3
    iwpriv $WLAN_IF set_mib ledBlinkingFreq=7
    # Fix DWA-160 issue
    # Default value is 2
    # lgyEncRstrct 	= 1, WEP don't support N rates
    #			= 2, TKIP don't support N rates
    #			= 3, WEP & TKIP don't support N rates
    #			= 11, WEP & TKIP support the rate under 54M
    iwpriv $WLAN_IF set_mib lgyEncRstrct=11
    iwpriv $WLAN_IF set_mib apsd_enable=$APSD ##WMM Power Save
        
    CLI_ASSO=`nvram get wds_endis_ip_client`
    if [ "$CLI_ASSO" = "1" -a "$WDS" = "1" ]; then ## WDS only
         iwpriv $WLAN_IF set_mib wds_pure=1
    else
	 iwpriv $WLAN_IF set_mib wds_pure=0
    fi
        
        
    case "$MODE" in
    
           1)
           iwpriv $WLAN_IF set_mib band=3
           iwpriv $WLAN_IF set_mib use40M=0
           iwpriv $WLAN_IF set_mib 2ndchoffset=0
           ;;
           2)    
           iwpriv $WLAN_IF set_mib band=11
           iwpriv $WLAN_IF set_mib use40M=0
           iwpriv $WLAN_IF set_mib 2ndchoffset=0
           iwpriv $WLAN_IF set_mib qos_enable=1
           ;;
           3)
           iwpriv $WLAN_IF set_mib band=11
           iwpriv $WLAN_IF set_mib use40M=1
           iwpriv $WLAN_IF set_mib qos_enable=1
           if [ $CH -gt 4 ]; then
                iwpriv $WLAN_IF set_mib 2ndchoffset=1
           else
                iwpriv $WLAN_IF set_mib 2ndchoffset=2
           fi           
           ;;
    esac    
    
    case "$COUN" in
         1)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         2)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         3)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         4)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         5)   
         iwpriv $WLAN_IF set_mib regdomain=1
         ;;
         6)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         7)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         8)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         9)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         10)   
         iwpriv $WLAN_IF set_mib regdomain=6
         ;;
         11)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         12)   
         iwpriv $WLAN_IF set_mib regdomain=1
         ;;
         13)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         14)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         15)   
         iwpriv $WLAN_IF set_mib regdomain=3
         ;;
         16)   
         iwpriv $WLAN_IF set_mib regdomain=1
         ;;
    esac
    
    
     num=0
     while [ "$num" -lt "$MSSID_num" ]
     do
     if [ "$(nvram get wl_multi_ssid_endis_$(($num+2)))" = "1" ]; then
           MSSID_disable=0
           MSSID_ACT=$(($MSSID_ACT+1))
     fi
     num=$(($num+1))
     done

    ############## set vap_enable=1  if any guest is enabled ################
    if [ "$MSSID_disable" = "0" ]; then
          iwpriv $WLAN_IF set_mib vap_enable=1
    else  
          iwpriv $WLAN_IF set_mib vap_enable=0
    fi
     
     ########### set wlan0 beacon #####################  
        case "$MSSID_ACT" in
           0)
           iwpriv $WLAN_IF set_mib bcnint=100
           iwpriv $WLAN_IF set_mib dtimperiod=3
           ;;
           1)
           iwpriv $WLAN_IF set_mib bcnint=200
           iwpriv $WLAN_IF set_mib dtimperiod=2
           ;;
           2)
           iwpriv $WLAN_IF set_mib bcnint=300
           iwpriv $WLAN_IF set_mib dtimperiod=2
           ;;
           3)
           iwpriv $WLAN_IF set_mib bcnint=400
           iwpriv $WLAN_IF set_mib dtimperiod=2
           ;;
           4)
           iwpriv $WLAN_IF set_mib bcnint=500
           iwpriv $WLAN_IF set_mib dtimperiod=2
           ;;
        esac
    
}

MSSID_settings()
{
    #echo "MSSID ..."
   
    
    #### MBSSID #### (2,3,4,5)
    num=0
    ac_num=0
    while [ $num -lt $MSSID_num ]
    do      

       if [ "$(nvram get wl_multi_ssid_endis_$(($num+2)))" = "1" ]; then
            SSID=$(nvram get wl_ssid_$(($num+2)))
            BROADCAST=$(nvram get endis_ssid_broadcast_$(($num+2)))
            GUEST_ACCESS=$(nvram get wl_guest_access_local_endis_$(($num+2)))
            
            ifconfig $WLAN_VA_IF$ac_num down
            iwpriv $WLAN_VA_IF$ac_num copy_mib 
            ifconfig $WLAN_VA_IF$ac_num hw ether $(nvram get hwaddr_$(($num+2)))
            iwpriv $WLAN_VA_IF$ac_num set_mib ssid="$SSID"
            iwpriv $WLAN_VA_IF$ac_num set_mib hiddenAP=$((1-$BROADCAST))
            iwpriv $WLAN_VA_IF$ac_num set_mib guest_access=$((1-$GUEST_ACCESS))
            iwpriv $WLAN_VA_IF$ac_num set_mib vap_enable=1
            iwpriv $WLAN_VA_IF$ac_num set_mib basicrates=0
            iwpriv $WLAN_VA_IF$ac_num set_mib oprates=0
            iwpriv $WLAN_VA_IF$ac_num set_mib wds_enable=0
            ac_num=$(($ac_num+1))

       fi
    
    num=$(($num+1))
    done
    
     

}

Adv_settings()
{
    #echo "ADV..."
    RADIO=`nvram get endis_wl_radio`
    BROADCAST=`nvram get endis_ssid_broadcast`
    WMM=`nvram get endis_wl_wmm`
    FRAG=`nvram get wl_frag`
    RTS=`nvram get wl_rts`
    PREAMBLE=`nvram get wl_plcphdr`
    
    iwpriv $WLAN_IF set_mib hiddenAP=$((1-$BROADCAST))
    
    MODE=`nvram get wl_mode`
    if [ "$MODE" = "1" ]; then  
          iwpriv $WLAN_IF set_mib qos_enable=$WMM
    fi
    iwpriv $WLAN_IF set_mib fragthres=$FRAG
    iwpriv $WLAN_IF set_mib rtsthres=$RTS
    iwpriv $WLAN_IF set_mib preamble=$PREAMBLE
}


Wpa_conf()
{
    SEC=`nvram get wl_sectype`
    case "$SEC" in
        1)
        ENC=0 
        ;;
        2)
        ENC=1
        ;;
        3)
        ENC=2
        ;;
        4)
        ENC=4
        ;;
        5)
        ENC=6
        ;;
        6)
        WPA_1X=`nvram get wl_radius_mode`
        case "$WPA_1X" in
              0)
              ENC=2
              UNI_CIPH=1
              WPA2_UNI_CIPH=1
              ;;
              1)
              ENC=4
              UNI_CIPH=1
              WPA2_UNI_CIPH=2  
              ;;
              2)
              ENC=6
              UNI_CIPH=3
              WPA2_UNI_CIPH=3
              ;;
        esac     
        ;;
    esac

    SSID=`nvram get wl_ssid`
    if [ "$SEC" = "0" -o "$SEC" = "1" ]; then
          ENABLE_1X=0
    else
          ENABLE_1X=1
    fi
    MAC_AUTH=0
    NON_WPA_CLIENT=0
    WEP_LEN=`nvram get key_length`
    if [ "$WEP_LEN" = "5" ]; then
          WEP_KEY=1
    else 
          WEP_KEY=2
    fi
    WEP_GROUP_KEY=""
    AUTH=1         
    PRE_AUTH=0
    USE_PASS=1
    PSK="12345678"
    G_REKEY=86400
    RS_PORT=`nvram get wl_radius_port`
    RS_IP=`nvram get wl_radius_ipaddr`
    RS_PW=`nvram get wl_radius_key`
    RS_MAX_REQ=3
    RS_AWHILE=5
    

cat <<EOF
encryption = $ENC
ssid = "$SSID"
enable1x = $ENABLE_1X
enableMacAuth = 0
supportNonWpaClient = 0
wepKey = $WEP_KEY
wepGroupKey = ""
authentication = $AUTH
unicastCipher = $UNI_CIPH
wpa2UnicastCipher = $WPA2_UNI_CIPH
enablePreAuth = 0
usePassphrase = 1
psk = "24b6ccbc76b3b9c667a498"
groupRekeyTime = 3600
rsPort = $RS_PORT
rsIP = $RS_IP
rsPassword = $RS_PW
rsMaxReq = 3
rsAWhile = 5
accountRsEnabled = 0
accountRsPort = 0
accountRsIP = 0.0.0.0
accountRsPassword = ""
accountRsUpdateEnabled = 0
accountRsUpdateTime = 0
accountRsMaxReq = 0
accountRsAWhile = 0

EOF

}

Security()
{
    #echo "Sec..."
    SEC=`nvram get wl_sectype$2`
    WEP_LEN=`nvram get key_length$2`
    WEP_KEY=`nvram get wl_key$2`
    AUTH=`nvram get wl_auth$2`
    WEP_KEY1=`nvram get wl_key1$2`
    WEP_KEY2=`nvram get wl_key2$2`
    WEP_KEY3=`nvram get wl_key3$2`
    WEP_KEY4=`nvram get wl_key4$2`
    WPA_PSK1=`nvram get wl_wpa1_psk$2`
    WPA_PSK2=`nvram get wl_wpa2_psk$2`
    WPA_PSKS=`nvram get wl_wpas_psk$2`

    iwpriv $WLAN_IF$1$3 set_mib 802_1x=0
    
    case "$SEC" in
    
          1)  ##none
          iwpriv $WLAN_IF$1$3 set_mib psk_enable=0
          iwpriv $WLAN_IF$1$3 set_mib encmode=0
          iwpriv $WLAN_IF$1$3 set_mib authtype=2
          ;;
          2)  ##wep
          iwpriv $WLAN_IF$1$3 set_mib psk_enable=0 
          
          if [ "$WEP_LEN" = "5" ]; then
                iwpriv $WLAN_IF$1$3 set_mib encmode=1	                
          else
                iwpriv $WLAN_IF$1$3 set_mib encmode=5
          fi
          
          iwpriv $WLAN_IF$1$3 set_mib authtype=$AUTH
          iwpriv $WLAN_IF$1$3 set_mib wepkey1=$WEP_KEY1
	  iwpriv $WLAN_IF$1$3 set_mib wepkey2=$WEP_KEY2
	  iwpriv $WLAN_IF$1$3 set_mib wepkey3=$WEP_KEY3
	  iwpriv $WLAN_IF$1$3 set_mib wepkey4=$WEP_KEY4
	  iwpriv $WLAN_IF$1$3 set_mib wepdkeyid=$(($WEP_KEY-1))       
          ;;
          3)  ##wpa-tkip
          iwpriv $WLAN_IF$1$3 set_mib authtype=2
          iwpriv $WLAN_IF$1$3 set_mib encmode=2
          iwpriv $WLAN_IF$1$3 set_mib psk_enable=1
          iwpriv $WLAN_IF$1$3 set_mib wpa_cipher=2
          iwpriv $WLAN_IF$1$3 set_mib passphrase="$WPA_PSK1"
          iwpriv $WLAN_IF$1$3 set_mib gk_rekey=3600
          ;;
          4)  ##wpa2-aes
          iwpriv $WLAN_IF$1$3 set_mib psk_enable=2
          iwpriv $WLAN_IF$1$3 set_mib encmode=2
          iwpriv $WLAN_IF$1$3 set_mib authtype=2
          iwpriv $WLAN_IF$1$3 set_mib wpa2_cipher=8
          iwpriv $WLAN_IF$1$3 set_mib passphrase="$WPA_PSK2"
          iwpriv $WLAN_IF$1$3 set_mib gk_rekey=3600
          ;;
          5)  ##wpa1 wpa2
          iwpriv $WLAN_IF$1$3 set_mib psk_enable=3
          iwpriv $WLAN_IF$1$3 set_mib encmode=2
          iwpriv $WLAN_IF$1$3 set_mib authtype=2
          iwpriv $WLAN_IF$1$3 set_mib wpa_cipher=10
          iwpriv $WLAN_IF$1$3 set_mib wpa2_cipher=10
          iwpriv $WLAN_IF$1$3 set_mib passphrase="$WPA_PSKS"
          iwpriv $WLAN_IF$1$3 set_mib gk_rekey=3600
          ;;
          6)  ##wpa 802_1x
          Wpa_conf > /var/wpa-$WLAN_IF.conf
          auth $WLAN_IF $BR auth /var/wpa-$WLAN_IF.conf
          iwpriv $WLAN_IF set_mib psk_enable=0
          iwpriv $WLAN_IF set_mib 802_1x=1
          iwpriv $WLAN_IF set_mib encmode=2
          iwpriv $WLAN_IF set_mib authtype=2        
          ;;
    esac
    
}


Security_MSSID()
{
    #### MBSSID #### (2,3,4,5)
    num=0
    ac_num=0
    while [ $num -lt $MSSID_num ]
    do
       if [ "$(nvram get wl_multi_ssid_endis_$(($num+2)))" = "1" ]; then
            Security "-va" "_$(($num+2))" "$ac_num"
            ac_num=$(($ac_num+1))
       fi
    
    num=$(($num+1))
    done



}

Wds()
{
    #echo "WDS......."
    WDS=`nvram get wds_endis_fun`
        
    if [ "$1" = "start" ]; then    
    
          IS_ROOTAP=`nvram get wds_repeater_basic`
          WDS_SEC=`nvram get wl_sectype`
          WEP_LEN=`nvram get key_length`
          WDS_KEY=`nvram get wl_key`
          WDS_WEPKEY=`nvram get wl_key$WDS_KEY`
          
          iwpriv $WLAN_IF set_mib wds_num=0       #reinitialize  
          
          if [ "$WDS_SEC" = "1" ]; then 
          #none
                iwpriv $WLAN_IF set_mib wds_encrypt=0
                iwpriv $WLAN_IF set_mib authtype=2
          elif [ "$WDS_SEC" = "2" -a "$WDS" = "1" ]; then 
          #wep
                if [ "$WEP_LEN" = "5" ]; then
                      iwpriv $WLAN_IF set_mib wds_encrypt=1
                      iwpriv $WLAN_IF set_mib wds_wepkey=$WDS_WEPKEY	                
                else
                      iwpriv $WLAN_IF set_mib wds_encrypt=5
                      iwpriv $WLAN_IF set_mib wds_wepkey=$WDS_WEPKEY
                fi
          fi      
          
          ####  repeater mode  ####
          REP_IP=`nvram get repeater_ip`
          TAR_ROOTAP=$(nvram get basic_station_mac | sed -n 's/://gp')   
          
          if [ "$WDS" = "1" ]; then                                    
                if [ "$IS_ROOTAP" = "0" ]; then
                      ifconfig $BR $REP_IP
                      brctl addif $BR $WAN_IF
                      iwpriv $WLAN_IF set_mib wds_add=$TAR_ROOTAP,0
	                                     
                else                      
                      num=1
                      while [ $num -lt 5 ]
                      do 
                          tmprep=$(nvram get repeater_mac$num | sed -n 's/://gp')
                          
                          if [ "$tmprep" != "" ]; then
                              iwpriv $WLAN_IF set_mib wds_add=$tmprep,0                                                 
                          fi
                          num=$(($num+1))
                      done
                                           
                fi                
                iwpriv $WLAN_IF set_mib wds_enable=1
                brctl stp $BR on
                brctl setfd $BR 4
                    
          else  ##wds disabled
                iwpriv $WLAN_IF set_mib wds_enable=0
                iwpriv $WLAN_IF set_mib wds_encrypt=0
                brctl stp $BR off
                brctl setfd $BR 0
          fi    
                   
    else  # stop
          iwpriv $WLAN_IF set_mib wds_enable=0
	      
    fi
}

Acl()
{
	#echo "ACL...."
	iwpriv $WLAN_IF set_mib aclnum=0
	MACAC_ENABLED=`nvram get wl_access_ctrl_on`
	iwpriv $WLAN_IF set_mib aclmode=$MACAC_ENABLED
	if [ "$MACAC_ENABLED" != "0" ]; then
		MACAC_NUM=`nvram get wl_acl_num`
		if [ "$MACAC_NUM" != "0" ]; then
			num=1
			while [ $num -le $MACAC_NUM ]
			do
				AC_TBL=`nvram get wlacl$num`
				addr_tmp=`echo $AC_TBL | awk '{print $2}'`
				addr=`echo $addr_tmp | sed -n 's/://gp'`
				iwpriv $WLAN_IF set_mib acladdr=$addr
				num=$(($num+1))
			done
		fi
	fi
}

Update_conf()
{
     DISABLE_PIN=`nvram get endis_pin`
     MODE=`nvram get wps_status`
     UPNP=1
     CONF=134  
     CONN=1
     MAN=0
     SSID=`nvram get wl_ssid`
     PIN=`nvram get wps_pin`
     #PIN="49424324"
     RF=1
     DEVICE=`cat /module_name`
     
     echo "disable_configured_by_exReg = $DISABLE_PIN" > /var/wsc.conf
     echo "mode = $MODE" >> /var/wsc.conf
     echo "upnp = $UPNP" >> /var/wsc.conf
     echo "config_method = $CONF" >> /var/wsc.conf
     echo "connection_type = $CONN" >> /var/wsc.conf
     echo "manual_config = $MAN" >> /var/wsc.conf     
          
     SEC=`nvram get wl_sectype`
     case "$SEC" in
     1)#none
       ENC=1
       AUTH=1
       KEY=""
     ;;
     2)#wep
       ENC=2
       WEP_KEY=`nvram get wl_key`
       if [ "$(nvram get wl_auth)" = "1" ]; then
             AUTH=4
       else
             AUTH=1
       fi
       KEY=`nvram get wl_key$WEP_KEY`
       WEP_KEY1=`nvram get wl_key1`
       WEP_KEY2=`nvram get wl_key2`
       WEP_KEY3=`nvram get wl_key3`
       WEP_KEY4=`nvram get wl_key4`

     ;;
     3)#wpa
       ENC=4
       AUTH=2
       KEY=`nvram get wl_wpa1_psk`
     ;;
     4)#wpa2
       ENC=8
       AUTH=32
       KEY=`nvram get wl_wpa2_psk`
     ;;
     5)#wpa+wpa2
       #ENC=12 %%% for netgear requirement  %%% ---> tell the client it's wpa2
       #AUTH=34
       ENC=8
       AUTH=32
       KEY=`nvram get wl_wpas_psk`
     ;;
     esac

     if [ "$ENC" = "2" ]; then
           echo "wep_transmit_key = $WEP_KEY" >> /var/wsc.conf
           echo "network_key = $KEY" >> /var/wsc.conf
           echo "wep_key2 = $WEP_KEY2" >> /var/wsc.conf
           echo "wep_key3 = $WEP_KEY3" >> /var/wsc.conf
           echo "wep_key4 = $WEP_KEY4" >> /var/wsc.conf
     else
           echo "network_key = $KEY" >> /var/wsc.conf
     fi
     
     echo "auth_type = $AUTH" >> /var/wsc.conf
     echo "encrypt_type = $ENC" >> /var/wsc.conf   
     echo "ssid = $SSID" >> /var/wsc.conf
     echo "pin_code = $PIN" >> /var/wsc.conf
     echo "rf_band = $RF" >> /var/wsc.conf
     echo "device_name = $DEVICE(Wireless AP)" >> /var/wsc.conf 
     echo "manufacturer = NETGEAR, Inc." >> /var/wsc.conf
     echo "manufacturerURL = http://www.netgear.com/products" >> /var/wsc.conf
     echo "modelDescription = $DEVICE PLC AP" >> /var/wsc.conf
     echo "model_name = $DEVICE(Wireless AP)" >> /var/wsc.conf
     echo "model_num = $DEVICE" >> /var/wsc.conf
     echo "modelURL = http://www.netgear.com/" >> /var/wsc.conf
     cat /var/wps/wscd.conf >> /var/wsc.conf
}

Wps()
{
     SEC=`nvram get wl_sectype`
     WPSOFF=`nvram get wsc_disable`
     nvram set wps_client_enable=0
     if [ "$1" = "start" -a "$WPSOFF" = "0" ]; then
           echo "wps start"
           Update_conf
	   wscd -start -c /var/wsc.conf -w $WLAN_IF -fi /var/wscd-$WLAN_IF.fifo -daemon
	   WAIT=1
	   while [ $WAIT != 0 ]		
	   do	
	        if [ -e /var/wscd-$WLAN_IF.fifo ]; then
		        WAIT=0
		else
			sleep 1
		fi
	   done
	   # Fix the WEP 64/128 with Share Key Authentication issue
	   iwpriv $WLAN_IF set_mib wsc_enable=3
     elif [ "$1" = "stop" ]; then
           echo "wps stop"
           #rm -rf /var/wps
           Kill_proc "/var/run/wscd-$WLAN_IF.pid"
           # Fix the WEP 64/128 with Share Key Authentication issue
           iwpriv $WLAN_IF set_mib wsc_enable=0
     fi

}

wps_client_process()
{
	client_status=`nvram get wps_client_enable`
	if [ "x$client_status" = "x3" ]; then
			echo "wps client mode timeout"
			killall wscd
			Wps start
			Applications start
			nvram set wps_client_enable=4
			/etc/rc.d/action.sh wlan_soapclient
	else
			echo "wps client mode start"
			killall wscd
			wscd -mode 2 -c /var/wsc.conf -w $WLAN_IF -fi /var/wscd-$WLAN_IF.fifo -daemon
			WAIT=1
			while [ $WAIT != 0 ]
				do	
				if [ -e /var/wscd-$WLAN_IF.fifo ]; then
					WAIT=0
				else
					sleep 1
				fi
				done
			iwcontrol $WLAN_IF
			sleep 1
			nvram set wps_client_enable=1
			#wscd -sig_pbc wlan0
	fi
}

IF_Handle()
{
    if [ "$1" = "start" ]; then
          echo "IF_handle start...."
          
          brctl addif $BR $WLAN_IF 2> /dev/null
          ifconfig $lan_ifname hw ether $lan_hwaddr
          iwpriv $WLAN_IF set_mib opmode=16
          ifconfig $WLAN_IF up  
                             
          #echo "wds interface...."          
          WDS_num=`iwpriv $WLAN_IF get_mib wds_num | cut -b 28`
          num=0
          while [ $num -lt $WDS_num ]
          do
                  brctl addif $BR $WLAN_IF-wds$num 2> /dev/null
                  ifconfig $WLAN_IF-wds$num 0.0.0.0
                  num=$(($num+1))
          done 
                   
          #### MBSSID #### (2,3,4,5)
          #echo "guest interface...."
          
          WDS=`nvram get wds_endis_fun`
          CLI_ASSO=`nvram get wds_endis_ip_client`
          if [ "$CLI_ASSO" = "1" -a "$WDS" = "1" ]; then ## WDS only
               echo "Pure wds. No guest."
          
          else
               num=0
               ac_num=0
               while [ $num -lt $MSSID_num ]
               do                 
                   if [ "$(nvram get wl_multi_ssid_endis_$(($num+2)))" = "1" ]; then
                       echo "initializing $WLAN_VA_IF$ac_num" 
                       sleep 1
                       brctl addif $BR $WLAN_VA_IF$ac_num 2> /dev/null
                       ifconfig $WLAN_VA_IF$ac_num 0.0.0.0
                       ac_num=$(($ac_num+1))                     
                   fi
               num=$(($num+1))
               done    
              
          fi  
         
          
    else    
          echo "IF_handle stop...."
          ifconfig $WLAN_IF down
                           
          num=0
          while [ $num -lt 4 ]
          do
                  ifconfig $WLAN_IF-wds$num down
                  brctl delif $BR $WLAN_IF-wds$num 2> /dev/null
                  num=$(($num+1))                  
          done
          
          #### MBSSID #### (2,3,4,5)
          num=0
          while [ $num -lt $MSSID_num ]
          do
                  ifconfig $WLAN_VA_IF$num down
                  brctl delif $BR $WLAN_VA_IF$num 2> /dev/null                   
    
          num=$(($num+1))
          done   
          
          brctl delif $BR $WAN_IF 2> /dev/null
          brctl delif $BR $WLAN_IF 2> /dev/null
    fi
}

main()
{
   if [ "$1" = "start" ]; then
          
          RADIO=`nvram get endis_wl_radio`
          if [ "$RADIO" = "1" ]; then 
                 
                 Acl
                 Basic_settings
                 Adv_settings
                 Security         
                 Wds start
                 MSSID_settings
                 Security_MSSID
                 IF_Handle start
                 Wps start
                 echo $sys_uptime > /tmp/WLAN_uptime
                 Applications start  
                 ledcontrol -n radio -s on              
          else
                 IF_Handle stop
                 ledcontrol -n radio -s off      
          fi
          
      
   else
          IF_Handle stop 
          Applications stop
          Wds stop
          Wps stop
          ledcontrol -n radio -s off         
   fi

}

case "$1" in 

       start)
            main start
            ;;  
       stop)
            main stop       
            ;;
       restart)
            main stop
            main start    
            ;;
       wsc)
            nvram set wps_status=5
            nvram commit
            echo 1 > /tmp/reinitwscd
            sleep 1
            nvram set action=99
            ;;
       wps_client)
       			wps_client_process
       			;;
esac            
            
             
