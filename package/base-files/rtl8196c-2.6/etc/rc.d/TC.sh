#!/bin/sh
#
#
TC="/usr/sbin/tc"
U_QOS_CHAIN="u_qos_chain"
D_QOS_CHAIN="d_qos_chain"
DOWNLINK_RATE=102400
qos_enable=`nvram get qos_endis_on`
total_num=`nvram get qos_rule_count`
WAN_IF=`nvram get wan_ifname`
LAN_IF=`nvram get lan_ifname`
#SU_IP="192.168.1.101/32"
SU_IP=`nvram get qos_su_ip`
SU_ENABLE=`nvram get qos_enable_su`
add="-A"
del="-D"

modify_DSCP()
{
	action=$1
	if [ "$SU_ENABLE" = "1" ]; then
		iptables -t mangle ${action} POSTROUTING -m mark --mark 10 -j DSCP --set-dscp 38
		iptables -t mangle ${action} POSTROUTING -m mark --mark 20 -j DSCP --set-dscp 28
		iptables -t mangle ${action} POSTROUTING -m mark --mark 30 -j DSCP --set-dscp 18
		iptables -t mangle ${action} POSTROUTING -m mark --mark 40 -j DSCP --set-dscp 10
		iptables -t mangle ${action} POSTROUTING -m mark --mark 5 -j DSCP --set-dscp 38
		iptables -t mangle ${action} POSTROUTING -m mark --mark 6 -j DSCP --set-dscp 28
		iptables -t mangle ${action} POSTROUTING -m mark --mark 7 -j DSCP --set-dscp 18
		iptables -t mangle ${action} POSTROUTING -m mark --mark 8 -j DSCP --set-dscp 10
	else
		iptables -t mangle ${action} POSTROUTING -m mark --mark 10 -j DSCP --set-dscp 38
		iptables -t mangle ${action} POSTROUTING -m mark --mark 20 -j DSCP --set-dscp 28
		iptables -t mangle ${action} POSTROUTING -m mark --mark 30 -j DSCP --set-dscp 18
		iptables -t mangle ${action} POSTROUTING -m mark --mark 40 -j DSCP --set-dscp 10
	fi
}

su_priority_switch()
{
	case "$1" in
	0)
		echo 5
		return
		;;
	1)
		echo 6
		return
		;;
	2)
		echo 7
		return
		;;
	3)
		echo 8
		return
		;;
	*)
		exit 1
	esac
}

priority_switch()
{
	case "$1" in
	0)
		echo 10
		return
		;;
	1)
		echo 20
		return
		;;
	2)
		echo 30
		return
		;;
	3)
		echo 40
		return
		;;
	*)
		exit 1
	esac
}

#port qos
do_rule_by_phy_port()
{
	action=$1
	qos_lan_port=`echo $2 | awk '{print $3}'`
	qos_pri=`echo $2 | awk '{print $4}'`
	priority=$(priority_switch $qos_pri)

	# change port number 1 
	case "$qos_lan_port" in
	1)
		port_nu=1
		;;
	2)
		port_nu=2
		;;
	3)
		port_nu=3
		;;
	4)
		port_nu=4
		;;
		*)
		exit 1
	esac
	#only for uplink
	iptables -t mangle ${action} $U_QOS_CHAIN -m ingressport --ingressport ${port_nu} -j MARK --set-mark $priority
}

do_rule_by_mac()
{
	action=$1
	qos_pri=`echo $2 | awk '{print $4}'`
	qos_mac=`echo $2 | awk '{print $9}'`
	priority=$(priority_switch $qos_pri)
	if [ "$SU_ENABLE" = "1" ]; then #super user mode enable
		#uplink
		iptables -t mangle ${action} $U_QOS_CHAIN -s ! $SU_IP -m mac --mac-source $qos_mac -j MARK --set-mark $priority
		#downlink
		iptables -t mangle ${action} $D_QOS_CHAIN -d ! $SU_IP -m mac --mac-destination $qos_mac -j MARK --set-mark $priority
	else
		#uplink
		iptables -t mangle ${action} $U_QOS_CHAIN -m mac --mac-source $qos_mac -j MARK --set-mark $priority
		#downlink
		iptables -t mangle ${action} $D_QOS_CHAIN -m mac --mac-destination $qos_mac -j MARK --set-mark $priority
	fi
}

	
do_rule_by_port()
{	
	
	action=$1
	qos_pri=`echo $2 | awk '{print $4}'`
	qos_proto=`echo $2 | awk '{print $5}'`
	qos_sport=`echo $2 | awk '{print $6}'`
	qos_dport=`echo $2 | awk '{print $7}'`
	priority=$(priority_switch $qos_pri)
	su_priority=$(su_priority_switch $qos_pri)
	
	if [ "$qos_proto" = "TCP/UDP" ]; then
		proto="tcp"
		proto_other="udp"
	else
		proto=$qos_proto
		proto_other=""
	fi
	num=1
	while [ "`echo $qos_sport | awk -F',' '{print $'$num'}'`" != "" ]; do
		start_port=`echo $qos_sport | awk -F',' '{print $'$num'}'`
		end_port=`echo $qos_dport | awk -F',' '{print $'$num'}'`
		
		if [ "$start_port" != "$end_port" ]; then
			port="$start_port:$end_port"
		else
			port="$start_port"
		fi
		
		if [ "$proto" != "" ] ; then
			if [ "$SU_ENABLE" = "1" ]; then #super user mode enable
				#uplink
				iptables -t mangle ${action} $U_QOS_CHAIN -s ! $SU_IP -p $proto --dport $port -j MARK --set-mark $priority
				iptables -t mangle ${action} $U_QOS_CHAIN -s $SU_IP -p $proto --dport $port -j MARK --set-mark $su_priority
				#downlink
				iptables -t mangle ${action} $D_QOS_CHAIN -d ! $SU_IP -p $proto --sport $port -j MARK --set-mark $priority
				iptables -t mangle ${action} $D_QOS_CHAIN -d $SU_IP -p $proto --sport $port -j MARK --set-mark $su_priority
			else
				#uplink
				iptables -t mangle ${action} $U_QOS_CHAIN -p $proto --dport $port -j MARK --set-mark $priority
				#downlink
				iptables -t mangle ${action} $D_QOS_CHAIN -p $proto --sport $port -j MARK --set-mark $priority
			fi
		fi
		if [ "$proto_other" != "" ]; then
			if [ "$SU_ENABLE" = "1" ]; then	#super user mode enable
				#uplink
				iptables -t mangle ${action} $U_QOS_CHAIN -s ! $SU_IP -p $proto_other --dport $port -j MARK --set-mark $priority
				iptables -t mangle ${action} $U_QOS_CHAIN -s $SU_IP -p $proto_other --dport $port -j MARK --set-mark $su_priority
				#downlink
				iptables -t mangle ${action} $D_QOS_CHAIN -d ! $SU_IP -p $proto_other --sport $port -j MARK --set-mark $priority
				iptables -t mangle ${action} $D_QOS_CHAIN -d $SU_IP -p $proto_other --sport $port -j MARK --set-mark $su_priority
			else
				#uplink
				iptables -t mangle ${action} $U_QOS_CHAIN -p $proto_other --dport $port -j MARK --set-mark $priority
				#downlink
				iptables -t mangle ${action} $D_QOS_CHAIN -p $proto_other --sport $port -j MARK --set-mark $priority
			fi
		fi
		num=$(($num+1))
	done
}

no_su_uplink_traffic_control_table()
{
	UPRATE=`nvram get qos_uprate`
	#Qos WAN $WAN_IF
   if [ "`nvram get router_disable`" = "1" ]; then
	   WAN_IF=`nvram get wan_hwifname`
   fi	
   
   #clean qdisc buffer
   #$TC qdisc del dev $WAN_IF root 2>/dev/null
   #add new qdisc
   $TC qdisc add dev $WAN_IF root handle 10: htb default 2
   # 4 level priority
   $TC class add dev $WAN_IF parent 10: classid 10:1 htb rate ${UPRATE}kbit ceil ${UPRATE}kbit > /dev/null
   $TC class add dev $WAN_IF parent 10:1 classid 10:2 htb rate 1kbit ceil ${UPRATE}kbit prio 2 > /dev/null
   $TC qdisc add dev $WAN_IF parent 10:2 handle 100: sfq perturb 10

   $TC class add dev $WAN_IF parent 10:1 classid 10:10 htb rate $((45*${UPRATE}/100))kbit ceil $((45*${UPRATE}/100))kbit prio 0
   $TC class add dev $WAN_IF parent 10:1 classid 10:20 htb rate $((30*${UPRATE}/100))kbit ceil $((30*${UPRATE}/100))kbit prio 1
   $TC class add dev $WAN_IF parent 10:1 classid 10:30 htb rate $((15*${UPRATE}/100))kbit ceil $((15*${UPRATE}/100))kbit prio 2
   $TC class add dev $WAN_IF parent 10:1 classid 10:40 htb rate $((10*${UPRATE}/100))kbit ceil $((10*${UPRATE}/100))kbit prio 3
   
   $TC qdisc add dev $WAN_IF parent 10:10 handle 101: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:20 handle 102: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:30 handle 103: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:40 handle 104: sfq perturb 10
   
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 10 fw classid 10:10
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 20 fw classid 10:20
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 30 fw classid 10:30
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 40 fw classid 10:40
}

no_su_downlink_traffic_control_table()
{
	# QoS LAN $LAN_IF
   #clean qdisc buffer
   #$TC qdisc del dev $LAN_IF root 2>/dev/null
   #add new qdisc
   $TC qdisc add dev $LAN_IF root handle 20: htb default 2
   # 4 level priority
   $TC class add dev $LAN_IF parent 20: classid 20:1 htb rate ${DOWNLINK_RATE}kbit ceil ${DOWNLINK_RATE}kbit > /dev/null
   $TC class add dev $LAN_IF parent 20:1 classid 20:2 htb rate 1kbit ceil ${DOWNLINK_RATE}kbit prio 2 > /dev/null
   $TC qdisc add dev $LAN_IF parent 20:2 handle 200: sfq perturb 10

   $TC class add dev $LAN_IF parent 20:1 classid 20:10 htb rate $((45*${DOWNLINK_RATE}/100))kbit ceil ${DOWNLINK_RATE}kbit prio 0
   $TC class add dev $LAN_IF parent 20:1 classid 20:20 htb rate $((30*${DOWNLINK_RATE}/100))kbit ceil ${DOWNLINK_RATE}kbit prio 1
   $TC class add dev $LAN_IF parent 20:1 classid 20:30 htb rate $((15*${DOWNLINK_RATE}/100))kbit ceil ${DOWNLINK_RATE}kbit prio 2
   $TC class add dev $LAN_IF parent 20:1 classid 20:40 htb rate $((10*${DOWNLINK_RATE}/100))kbit ceil ${DOWNLINK_RATE}kbit prio 3
   
   $TC qdisc add dev $LAN_IF parent 20:10 handle 201: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:20 handle 202: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:30 handle 203: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:40 handle 204: sfq perturb 10
   
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 10 fw classid 20:10
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 20 fw classid 20:20
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 30 fw classid 20:30
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 40 fw classid 20:40   
}

su_uplink_traffic_control_table()
{
	 UPRATE=`nvram get qos_uprate`
   SU_URATE=$(($UPRATE/2))  
   QQ_URATE=$(($UPRATE-$SU_URATE)) 
		#Qos WAN $WAN_IF
   if [ "`nvram get router_disable`" = "1" ]; then
	   WAN_IF=`nvram get wan_hwifname`
   fi	
   
	 $TC qdisc add dev $WAN_IF root handle 10: htb default 35
   # 4 level priority
   $TC class add dev $WAN_IF parent 10: classid 10:1 htb rate ${UPRATE}kbit ceil ${UPRATE}kbit > /dev/null
 
   ###SUPA user###
   $TC class add dev $WAN_IF parent 10:1 classid 10:2 htb rate 1kbit ceil ${SU_URATE}kbit  > /dev/null
     #packets marked#
   $TC class add dev $WAN_IF parent 10:2 classid 10:21 htb rate $((45*${SU_URATE}/100))kbit ceil $((45*${SU_URATE}/100))kbit prio 0
   $TC class add dev $WAN_IF parent 10:2 classid 10:22 htb rate $((30*${SU_URATE}/100))kbit ceil $((30*${SU_URATE}/100))kbit prio 1
   $TC class add dev $WAN_IF parent 10:2 classid 10:23 htb rate $((15*${SU_URATE}/100))kbit ceil $((15*${SU_URATE}/100))kbit prio 2
   $TC class add dev $WAN_IF parent 10:2 classid 10:24 htb rate $((10*${SU_URATE}/100))kbit ceil $((10*${SU_URATE}/100))kbit prio 3
     #default(unmarked)
   $TC class add dev $WAN_IF parent 10:2 classid 10:25 htb rate 1kbit ceil ${SU_URATE}kbit prio 2

   
   $TC qdisc add dev $WAN_IF parent 10:21 handle 210: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:22 handle 220: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:23 handle 230: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:24 handle 240: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:25 handle 250: sfq perturb 10
   
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 5 fw classid 10:21
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 6 fw classid 10:22
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 7 fw classid 10:23
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 8 fw classid 10:24
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 9 fw classid 10:25
   ###non super user###
     
   ###non super user###
   $TC class add dev $WAN_IF parent 10:1 classid 10:3 htb rate 1kbit ceil ${QQ_URATE}kbit  > /dev/null
     #packets marked#
   $TC class add dev $WAN_IF parent 10:3 classid 10:31 htb rate $((45*${QQ_URATE}/100))kbit ceil $((45*${QQ_URATE}/100))kbit prio 0
   $TC class add dev $WAN_IF parent 10:3 classid 10:32 htb rate $((30*${QQ_URATE}/100))kbit ceil $((30*${QQ_URATE}/100))kbit prio 1
   $TC class add dev $WAN_IF parent 10:3 classid 10:33 htb rate $((15*${QQ_URATE}/100))kbit ceil $((15*${QQ_URATE}/100))kbit prio 2
   $TC class add dev $WAN_IF parent 10:3 classid 10:34 htb rate $((10*${QQ_URATE}/100))kbit ceil $((10*${QQ_URATE}/100))kbit prio 3
     #default(unmarked)
   $TC class add dev $WAN_IF parent 10:3 classid 10:35 htb rate 1kbit ceil ${QQ_URATE}kbit prio 2
   
   $TC qdisc add dev $WAN_IF parent 10:31 handle 310: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:32 handle 320: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:33 handle 330: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:34 handle 340: sfq perturb 10
   $TC qdisc add dev $WAN_IF parent 10:35 handle 350: sfq perturb 10
   
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 10 fw classid 10:31
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 20 fw classid 10:32
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 30 fw classid 10:33
   $TC filter add dev $WAN_IF parent 10: protocol ip prio 100 handle 40 fw classid 10:34
   ###super user###
}

su_downlink_traffic_control_table()
{
   SU_DRATE=$(($DOWNLINK_RATE/2))  
   QQ_DRATE=$(($DOWNLINK_RATE-$SU_DRATE)) 
#add new qdisc
   $TC qdisc add dev $LAN_IF root handle 20: htb default 35
   # 4 level priority
   $TC class add dev $LAN_IF parent 20: classid 20:1 htb rate ${DOWNLINK_RATE}kbit ceil ${DOWNLINK_RATE}kbit > /dev/null
 
   ###SUPA user###
   $TC class add dev $LAN_IF parent 20:1 classid 20:2 htb rate 1kbit ceil ${SU_DRATE}kbit  > /dev/null
     #packets marked#
   $TC class add dev $LAN_IF parent 20:2 classid 20:21 htb rate $((45*${SU_DRATE}/100))kbit ceil $((45*${SU_DRATE}/100))kbit prio 0
   $TC class add dev $LAN_IF parent 20:2 classid 20:22 htb rate $((30*${SU_DRATE}/100))kbit ceil $((30*${SU_DRATE}/100))kbit prio 1
   $TC class add dev $LAN_IF parent 20:2 classid 20:23 htb rate $((15*${SU_DRATE}/100))kbit ceil $((15*${SU_DRATE}/100))kbit prio 2
   $TC class add dev $LAN_IF parent 20:2 classid 20:24 htb rate $((10*${SU_DRATE}/100))kbit ceil $((10*${SU_DRATE}/100))kbit prio 3
     #default(unmarked)
   $TC class add dev $LAN_IF parent 20:2 classid 20:25 htb rate 1kbit ceil ${SU_DRATE}kbit prio 2

   
   $TC qdisc add dev $LAN_IF parent 20:21 handle 210: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:22 handle 220: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:23 handle 230: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:24 handle 240: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:25 handle 250: sfq perturb 10
   
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 5 fw classid 20:21
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 6 fw classid 20:22
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 7 fw classid 20:23
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 8 fw classid 20:24
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 9 fw classid 20:25
   ###non super user###
  
   
   ###non super user###
   $TC class add dev $LAN_IF parent 20:1 classid 20:3 htb rate 1kbit ceil ${QQ_DRATE}kbit  > /dev/null
     #packets marked#
   $TC class add dev $LAN_IF parent 20:3 classid 20:31 htb rate $((45*${QQ_DRATE}/100))kbit ceil $((45*${QQ_DRATE}/100))kbit prio 0
   $TC class add dev $LAN_IF parent 20:3 classid 20:32 htb rate $((30*${QQ_DRATE}/100))kbit ceil $((30*${QQ_DRATE}/100))kbit prio 1
   $TC class add dev $LAN_IF parent 20:3 classid 20:33 htb rate $((15*${QQ_DRATE}/100))kbit ceil $((15*${QQ_DRATE}/100))kbit prio 2
   $TC class add dev $LAN_IF parent 20:3 classid 20:34 htb rate $((10*${QQ_DRATE}/100))kbit ceil $((10*${QQ_DRATE}/100))kbit prio 3
     #default(unmarked)
   $TC class add dev $LAN_IF parent 20:3 classid 20:35 htb rate 1kbit ceil ${QQ_DRATE}kbit prio 2
   
   $TC qdisc add dev $LAN_IF parent 20:31 handle 310: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:32 handle 320: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:33 handle 330: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:34 handle 340: sfq perturb 10
   $TC qdisc add dev $LAN_IF parent 20:35 handle 350: sfq perturb 10
   
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 10 fw classid 20:31
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 20 fw classid 20:32
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 30 fw classid 20:33
   $TC filter add dev $LAN_IF parent 20: protocol ip prio 100 handle 40 fw classid 20:34
   ###super user###
   

}

traffic_control_rule()
{
	#qos_entry=MSN_messenger 0 MSN_messenger 1 TCP 443 443 ---- ----
	action=$1
	qos_entry=`nvram get qos_list$2`
	qos_mode=`echo $qos_entry | awk '{print $2}'`
	
	case "$qos_mode" in
	0)
		do_rule_by_port "$action" "$qos_entry"
		;;
	1)
		do_rule_by_port "$action" "$qos_entry"
		;;
	2)
		do_rule_by_phy_port "$action" "$qos_entry"
		;;
	3)
		do_rule_by_mac "$action" "$qos_entry"
		;;
	*)
		;;
	esac
	
}	

tc_table_clear()
{
	if [ "`nvram get router_disable`" = "1" ]; then
		WAN_IF="$($NVRAM get wan_hwifname)"
	fi	
	$TC qdisc del dev $WAN_IF root 2>/dev/null
	$TC qdisc del dev $LAN_IF root 2>/dev/null
}

start()
{
echo "TC.sh start:"  > /dev/console
	if [ "$qos_enable" = "1" ]; then
		#for super user no priority 
		if [ "$SU_ENABLE" = "1" ]; then	#super user mode enable
			echo "$SU_IP" > /proc/qos_su_ip
			iptables -A PREROUTING -t mangle -s $SU_IP -j MARK --set-mark 9
			iptables -A POSTROUTING -t mangle -d $SU_IP -j MARK --set-mark 9
			su_uplink_traffic_control_table
			su_downlink_traffic_control_table
		else
			no_su_uplink_traffic_control_table
			no_su_downlink_traffic_control_table
		fi
		iptables -A PREROUTING -t mangle -j $U_QOS_CHAIN
		iptables -A POSTROUTING -t mangle -j $D_QOS_CHAIN
		count=1
		while [ "$count" -le "$total_num" ]; do
			traffic_control_rule $add $count
			count=$(($count+1))
		done
		modify_DSCP $add
		echo "1," > /proc/qos
	fi
}

stop()
{
echo "TC.sh stop:" > /dev/console
#	[ "$qos_enable" = "0" ] && exit 0
	iptables -D PREROUTING -t mangle -j $U_QOS_CHAIN
	iptables -D POSTROUTING -t mangle -j $D_QOS_CHAIN
	iptables -t mangle -F $U_QOS_CHAIN
	iptables -t mangle -F $D_QOS_CHAIN
	tc_table_clear
	modify_DSCP $del
	echo "" > /proc/qos_su_ip
	echo "0," > /proc/qos
}

add_one()
{
	[ "$1" = "" ] && return
	if [ "$qos_enable" = "1" ]; then
	     n=$1
	     traffic_control_rule $add "${n}"
    fi
	
}

delete_one()
{
	[ "$1" = "" ] && return
	if [ "$qos_enable" = "1" ]; then
	     n=$1
	     traffic_control_rule $del "${n}"
	fi
}
delete_all()
{    
    #count=1
	#	while [ "$count" -le "$total_num" ]; do
	#		traffic_control_rule $del "$count"
	#		count=$(($count+1))
	#	done
	iptables -t mangle -F $U_QOS_CHAIN
	iptables -t mangle -F $D_QOS_CHAIN
}
status()
{
	echo "show qdisc ............ "
	$TC -d -s qdisc
#	echo "show filter ............ "
#		$TC -d -s filter ls dev $WAN_IF
	echo "show class ............ "
	if [ "x$WAN_IF" != "x" ];then
		$TC -d -s class ls dev $WAN_IF
		$TC -d -s class ls dev $LAN_IF
	fi
}

case "$1" in
  start)
   start;
   ;;
  stop)
   stop;
   ;;
  restart)
   stop;
   start;
   ;;
  add_one)
   add_one $2;
   ;;
  delete_one)
   delete_one $2;
   ;;
  del_all)
   delete_all;
   ;;
  status)
	status;
	;;
  *)
   echo $"Usage: $0 {start|stop|restart|status}"
   exit 1
esac

exit $RETVAL
