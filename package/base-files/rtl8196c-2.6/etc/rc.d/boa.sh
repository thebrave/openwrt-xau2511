#!/bin/sh

#[ -f /usr/local/sbin/boa ] || exit 0
#[ -f /etc/resolv.conf ] || exit 0

RETVAL=0
prog="boa"
PID_FILE="/var/run/boa.pid"

start() {
	# Start daemons.
	echo $"Starting $prog: "
	${prog}
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	if [ -e ${PID_FILE} ]; then
		kill `cat ${PID_FILE}`
		rm -f ${PID_FILE}
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

