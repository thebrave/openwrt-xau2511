#!/bin/sh

. /etc/rc.d/tools.sh

lan_ipaddr=`nvram get lan_ipaddr`
lan_subnet=`nvram get lan_netmask`
masklen=`print_masklen $lan_subnet`
subnet=`print_subnet $lan_ipaddr $lan_subnet`

iptables="iptables"

start() {
	if [ "`nvram get dns_hijack`" = "0" -a "`nvram get wan_lan_ip_conflict`" = "1" ]; then

		$iptables -t nat -A nat_local_server -p tcp -s $subnet/$masklen --dport 80 -j DNAT --to-destination $lan_ipaddr:80
	fi
}

stop() {
	$iptables -t nat -D nat_local_server -p tcp -s $subnet/$masklen --dport 80 -j DNAT --to-destination $lan_ipaddr:80
}
	
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart|reload)
        stop
        start
        RETVAL=$?
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $RETVAL

