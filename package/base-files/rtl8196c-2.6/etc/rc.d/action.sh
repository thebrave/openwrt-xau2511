#!/bin/sh

RETVAL=0

UDHCPD_INIT_PROG="/etc/rc.d/udhcpd.sh"
DHCPRELAY_INIT_PROG="/etc/rc.d/dhcprelay.sh"
DNSMASQ_INIT_PROG="/etc/rc.d/dnsmasq.sh"
NTPCLIENT_INIT_PROG="/etc/rc.d/ntpclient.sh"
UPNP_INIT_PROG="/etc/rc.d/upnp.sh"
SYSLOGD_INIT_PROG="/etc/rc.d/syslogd.sh"
DROPBEAR_INIT_PROG="/etc/rc.d/dropbear.sh"
TELNETD_INIT_PROG="/etc/rc.d/telnetd.sh"
SNMPD_INIT_PROG="/etc/rc.d/snmpd.sh"
RIPD_INIT_PROG="/etc/rc.d/ripd.sh"
TR069_AGENT_PROG="/etc/rc.d/tr069-agent.sh"
SIP_ALG_PROG="/etc/rc.d/sip_alg.sh"
WAN_START_PROG="/etc/rc.d/wan_start.sh"
WAN_DDNS_PROG="/etc/rc.d/wan_ddns.sh"
LAN_START_PROG="/etc/rc.d/lan_start.sh"
IGMPPROXY_PROG="/etc/rc.d/igmpproxy.sh"
ROUTING_PROG="/etc/rc.d/routing.sh"
log_file="/var/log/messages"
PLCCMD_PROG="/usr/sbin/dni-plccmd"
MTDCMD_PROG="/usr/sbin/mtdcmd"
LEDCONTROL_PROG="/sbin/ledcontrol"
NETBIOS_PROG="/etc/rc.d/netbios.sh"
WLAN_PROG="/etc/rc.d/wlan.sh"
SOAPCLIENT_PROG="/usr/sbin/soapclient"

RM=/bin/rm
CP=/bin/cp
MV=/bin/mv
DD=/bin/dd
CAT=/bin/cat
GREP=/bin/grep
MKDIR=/bin/mkdir
SLEEP=/bin/sleep
PS=/bin/ps
ECHO=/bin/echo
REBOOT=/sbin/reboot
NVRAM=/usr/sbin/nvram
KILLALL=/usr/bin/killall
AWK=/usr/bin/awk
LOGGER=/usr/bin/logger

action_reboot()
{
				${ECHO} "Reboot..........."
				#${SLEEP} 2 
				#auth_reboot=1 means an authorized host reboots this machine.a
				#auth_reboot will be use in boa
				
				${PLCCMD_PROG} reboot
				
				if [ `${NVRAM} get router_disable` = 0 ]; then 
						if [ "`${NVRAM} get auth_reboot`" != "xxxx" ]; then
                                	${NVRAM} set auth_reboot=1
										${ECHO} "auth_reboot=1"
						else
										${NVRAM} unset auth_ip
                                	${NVRAM} unset auth_reboot
						fi
						${WAN_START_PROG} stop 
				else 
						if [ `${NVRAM} get lan_dhcpclient` = 1 ]; then 
								${KILLALL} -SIGUSR2 udhcpc 
						fi 
				fi 
                        
              # Turn off WLAN LED 
              ${LEDCONTROL_PROG} -n wlan -c green -s off 

				#reboot
				[ -f /tmp/reboot ] && /tmp/reboot || ${REBOOT}
}

action_restart()
{
				${ECHO} "Restart..........."
				${ECHO} 0 > /proc/sys/net/ipv4/ip_forward
				${LAN_START_PROG}
				if [ `${NVRAM} get router_disable` = 0 ]; then
				${WAN_START_PROG} stop
				${UDHCPD_INIT_PROG} stop
				${DNSMASQ_INIT_PROG} stop
				#${SYSLOGD_INIT_PROG} stop
				${UPNP_INIT_PROG} stop
				#${RIPD_INIT_PROG} stop
				#${NTPCLIENT_INIT_PROG} stop
				${SIP_ALG_PROG} stop
				${IGMPPROXY_PROG} stop
				fi
				if [ "`${NVRAM} get router_disable`" = "0" ]; then	# not AP mode
					if [ "`${NVRAM} get nat_disable`" = "0" ]; then	# not AP mode
						#${DNSMASQ_INIT_PROG} start
						#${UPNP_INIT_PROG} start
						${ECHO} "nat disable "
					fi
					#${UDHCPD_INIT_PROG} start
					${WAN_START_PROG} start
					${RIPD_INIT_PROG} start
				else	#AP mode
					iptables -F
					iptables -X
					iptables -Z
					iptables -F -t nat
					iptables -X -t nat
					iptables -Z -t nat
					iptables -P INPUT ACCEPT
					iptables -P FORWARD ACCEPT
					${ECHO} ">>>>>>>>>>>>>>> AP mode  <<<<<<<<<<<<<"
				fi
				#/etc/rc.d/resetbutton.sh restart
				if [ `${NVRAM} get router_disable` = 0 ]; then
				${ROUTING_PROG} start
				fi
				if [ "`${NVRAM} get router_disable`" = "0" ]; then
					${RIPD_INIT_PROG} start
				fi

				#if [ `${NVRAM} get pwd_changed` = "1" ]; then
				#	${ECHO} "encrypt /etc/passwd password field"
				#	userlogin
				#	${NVRAM} unset pwd_changed
				#fi
				${NVRAM} commit
}

action_ip_up()
{
				blk_site_enable=`${NVRAM} get block_skeyword`
				blk_svc_enable=`${NVRAM} get blockserv_ctrl`
				wan_ip=`${NVRAM} get wan0_ipaddr`
				iface="0"
				if [ "`${NVRAM} get wan${iface}_ipaddr`" != "" ]; then
					if [ `${NVRAM} get router_disable` = 0 ]; then
							${LOGGER} -- "[Internet connected] IP address: $wan_ip,"
							${DNSMASQ_INIT_PROG} restart
							/etc/rc.d/start_firewall_1.sh
							/etc/rc.d/start_firewall_2.sh
							# this script first clean all ebtables rull and then do the pppoe passthrough and selective bridging traffic
							${NTPCLIENT_INIT_PROG} restart
							if [ -e /tmp/upnp_portmap ]; then
        	   						rm -f /tmp/upnp_portmap
	        				fi
							${UPNP_INIT_PROG} restart
							#/etc/rc.d/resetbutton.sh restart
							if [ "$blk_site_enable" -eq "2" ]; then
									/etc/rc.d/blk_site.sh restart
							fi
							if [ "$blk_svc_enable" -eq "2" ]; then
									/etc/rc.d/blk_svc.sh restart
							fi
							#/etc/rc.d/urlfilter.sh restart
							${WAN_DDNS_PROG} restart
							${ROUTING_PROG} restart
               			# RIP function support WAN port
							if [ "`nvram get router_disable`" = "0" ]; then   # not AP mode
									${RIPD_INIT_PROG} restart
							fi
							${SIP_ALG_PROG} start
							${IGMPPROXY_PROG} restart
							# scheduler for blk_site/blk_svc
							/etc/rc.d/cmdsched.sh start both
							# WAN LED indicator
							#${LEDCONTROL_PROG} -n wan -c green -s on
							#${LEDCONTROL_PROG} -n wan -c amber -s off
					else
							${DNSMASQ_INIT_PROG} restart
							${NETBIOS_PROG} restart
							${TELNETD_INIT_PROG} reload
							${PLCCMD_PROG} discoverd_restart
					fi
			  	else 
				# this script first clean all ebtables rull and then do the pppoe passthrough and selective bridging traffic
					${SIP_ALG_PROG} start
   		  		fi
				#${NVRAM} commit
}

action_restore()
{
		${ECHO} "Restore Default settings..."
		${NVRAM} unset restore_defaults
		
		${MTDCMD_PROG} default
		${REBOOT}
}

action_script()
{
				action_script=`${NVRAM} get action_script`
				${NVRAM} unset action_script
				if [ ! $action_script = "" ]; then
					$action_script
				fi
}

action_commit()
{
				${NVRAM} commit
}


action_wlan_restart()
{
	       ${WLAN_PROG} restart
	       ${PLCCMD_PROG} discoverd_restart
}

action_wps_restart()
{
#wscd config ap,  restart wlan	
	if [ -e /tmp/reinitwscd ]; then
	       ${RM} /tmp/reinitwscd
	       action_wlan_restart
	fi
}

action_wlan_onoff()
{
			endis_wl_radio=`${NVRAM} get endis_wl_radio`
			if [ "$endis_wl_radio" = "1" ]; then
				${ECHO} $"Turn OFF the WLAN ..."
				${NVRAM} set endis_wl_radio=0
				${WLAN_PROG} stop
			else
				${ECHO} $"Turn ON the WLAN ..."
				${NVRAM} set endis_wl_radio=1
				${WLAN_PROG} start	
			fi
			${NVRAM} commit
}

action_wlan_soapclient()
{
	client_status=`nvram get wps_client_enable`
	if [ "x$client_status" = "x0" ]; then
		/etc/rc.d/wlan.sh wps_client
		${ECHO} 2 > /proc/gpio
	elif [ "x$client_status" = "x4" ]; then
			${ECHO} $"Trigger the SOAP client function ..."
			${SOAPCLIENT_PROG}
			nvram set wps_client_enable=0
	fi
}

action_upnp_wan_restart()
{
#upnp, request connection, start wan
		if [ -e /tmp/upnp_wan_restart ]; then
	       ${RM} /tmp/upnp_wan_restart
		   ${SLEEP} 1
		   ${ECHO} "upnp request connection!!" > /dev/console
	       /sbin/cmdwan.sh start
	       
	fi
}

action_ui_proc_stop()
{
	plc_supp=`${NVRAM} get plc_supp`
	if [ "$plc_supp" = "1" ]; then
		${PLCCMD_PROG} poll_stop
	fi
}

action_ui_proc_restart()
{
	plc_supp=`${NVRAM} get plc_supp`
	if [ "$plc_supp" = "1" ]; then
		${PLCCMD_PROG} poll_start
	fi
}

action_others()
{
				#check 6:script
				local act=`${ECHO} $1 | ${AWK} -F\: '{print $1}'`
				local script=`${ECHO} $1 | ${AWK} -F\: '{print $2}'`
				local commit=`${ECHO} $1 | ${AWK} -F\: '{print $3}'`
 
            	if [ "$act" = "6" -a -n $script ]; then
               			if [ "$script" = "tftpdstart" ]; then
                  				${ECHO} "start tftpd"
                  				#/usr/sbin/in.tftpd -v -i /etc/tftpserver.ini &
                  				#${NVRAM} set action=6:tftpdstop
               			elif [ "$script" = "tftpdstop" ]; then
                  				${ECHO} "stop tftpd"
                  				${KILLALL} in.tftpd
                  		#kenny.w added
	                  	elif [ "$script" = "snmpfwupgradestart" ]; then   
                  				${ECHO} "snmp firmware upgrade start"
                  				/etc/rc.d/snmp_fw_upgrade.sh
                  				#end of added
               			else 
		  						if [ "x$commit" != "x" -a "$commit" != "0" ]; then	
                  					${NVRAM} commit
		  						fi	
                  				${script} restart	
               			fi
				fi	
}

action_ui_reset()
{
			${SLEEP} 1
			${KILLALL} setobject.cgi
}

action_reload_lan()
{
	/bin/echo $1 > /proc/sys/kernel/hostname 

	${PLCCMD_PROG} discoverd_restart
	/etc/rc.d/netbios.sh restart
}

action_reload_netusb()
{
	KVER=`uname -r | head -c 6`

	killall kcairplay
	rmmod NetUSB.ko
	#echo ">>>> insmod /lib/modules/$KVER/NetUSB.ko localID=\"$1\"<<<<"
	insmod /lib/modules/$KVER/NetUSB.ko localID=\"$1\"
	
	tmp_buff=`echo $1 | sed 's/XAU2511-//')`
	kcairplay $tmp_buff &
}

# See how we were called.
case "$1" in
  reboot)
	action_reboot
	;;
  restart)
	action_restart
	;;
  ip_up)
	action_ip_up
	;;
  restore)
	action_restore
	;;
  script)
	action_script
	;;
  commit)
	action_commit
	;;
  wlan_restart)
	action_wlan_restart
	;;
	wps_restart)
	action_wps_restart
	;;
  wlan_onoff)
	action_wlan_onoff
	;;
  wlan_soapclient)
	action_wlan_soapclient
	;;		
  upnp_wan_restart)
	action_upnp_wan_restart
	;;
  ui_proc_stop)
  	action_ui_proc_stop
  	;;
  ui_proc_restart)
  	action_ui_proc_restart
  	;;  	
  others)
	action_others "$2"
	;;
  ui_reset)
	action_ui_reset
	;;
  reload_lan)
	action_reload_lan $2
	;;
  reload_netusb)
	action_reload_netusb $2
	;;
  *)
	echo $"Usage: $0 reboot|restart|ip_up|restore|script|commit|wlan_restart|upnp_wan_restart|others"
	exit 1
esac

exit $RETVAL