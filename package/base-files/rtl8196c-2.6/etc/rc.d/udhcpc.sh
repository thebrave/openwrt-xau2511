#!/bin/sh

#[ -f /sbin/udhcpc ] || exit 0
[ -f /etc/udhcpc/udhcpc.script ] || exit 0

RETVAL=0
prog="udhcpc"
PID_FILE="/var/run/udhcpc.pid"
SCRIPT_FILE="/etc/udhcpc/udhcpc.script"
#INTERFACE=`nvram get wan_hwifname`
wan_ifname=`nvram get wan_hwifname`
BIGPOND_INIT_PROG="/etc/rc.d/heartbeat.sh"
DYNAMIC_PPTP_PROG="/etc/rc.d/dyn_pptp.sh"
DNSMASQ_INIT_PROG="/etc/rc.d/dnsmasq.sh"
wan_proto=`nvram get wan_proto`
lan_dhcp_ipaddr_file=/tmp/lan_dhcp_ipaddr

vendorid=`nvram get dhcp_vendorid`
if [ -z $vendorid ]; then
	vendorid=
else
	vendorid="-d ${vendorid}"
fi

vendorinfo=`nvram get dhcp_vendorinfo`
if [ -z $vendorinfo ]; then
	vendorinfo=
else
	vendorinfo="-e ${vendorinfo}"
fi

clientid=`nvram get dhcp_clientid` 	 
if [ -z $clientid ]; then
	clientid=
else
	clientid="-c ${clientid}"
fi

hostname=`nvram get wan_hostname`
if [ -z $hostname ]; then
	hostname=
else
	hostname="-h ${hostname}"
fi

if [ "`nvram get router_disable`" = "1" ]; then
	hostname=`nvram get wan_hostname`
	lan_ifname=`nvram get lan_ifname`
	mac=`ifconfig $lan_ifname | sed -n 1p | awk '{ print $5 }'`
 	mac4=`echo $mac | awk -F: '{ print $4 }'`
 	mac5=`echo $mac | awk -F: '{ print $5 }'`
 	mac6=`echo $mac | awk -F: '{ print $6 }'`
 	hostname="${hostname}-${mac4}${mac5}${mac6}"
fi

manual=$2

route_dns_renew()
{
		router=`/usr/sbin/nvram get wan_default_gateway`
		interface=`nvram get lan_ifname`
		if [ "x$router" != "x" ]; then
			route del default gw $router dev $interface
		fi
		router=`/usr/sbin/nvram get wan_gateway`
		if [ "x$router" != "x" ]; then
			route add default gw $router dev $interface
		fi
		${DNSMASQ_INIT_PROG} restart
}

start() {
	# Start daemons.
	echo $"Starting $prog: "
	if [ "`nvram get router_disable`" = "0" ]; then
		nvram set wan_ifname=$wan_ifname
		ifconfig $wan_ifname mtu $(nvram get wan_dhcp_mtu) 
		ifconfig $wan_ifname 0.0.0.0 up
	fi

	if [ "$wan_proto" = "pptp" ]; then
		if [ "`nvram get dy_pptp`" = "1" ]; then
			${prog} -R -i ${wan_ifname} -p ${PID_FILE} -s ${DYNAMIC_PPTP_PROG} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &
		fi
	else
		if [ "`nvram get router_disable`" = "0" ]; then
			${prog} -R -i ${wan_ifname} -p ${PID_FILE} -s ${SCRIPT_FILE} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &
			echo "${prog} -R -i ${wan_ifname} -p ${PID_FILE} -s ${SCRIPT_FILE} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &"
		else
			if [ -f $lan_dhcp_ipaddr_file ]; then
				lan_dhcp_ipaddr=`cat $lan_dhcp_ipaddr_file`
				lan_ipaddr=`ifconfig $lan_ifname | sed -n 2p | awk -F: '{print $2}' | awk '{print $1}'`
				dhcp_flag=0
				if [ "$lan_ipaddr" = "$lan_dhcp_ipaddr" ]; then
					ifconfig $lan_ifname 199.99.199.99
					dhcp_flag=1
				fi
				${prog} -R -i ${lan_ifname} -r ${lan_dhcp_ipaddr} -p ${PID_FILE} -s ${SCRIPT_FILE} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &
				echo "${prog} -R -i ${lan_ifname} -r ${lan_dhcp_ipaddr} -p ${PID_FILE} -s ${SCRIPT_FILE} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &"
				if [ "$dhcp_flag" = "1" ]; then
					cnt=1
					while [ $cnt -le 167 ]
					do
	    					cnt=$(($cnt+1))
	    					ipaddr=`ifconfig $lan_ifname | sed -n 2p | awk -F: '{print $2}' | awk '{print $1}'`
	    					if [ "$ipaddr" != "199.99.199.99" ]; then
	    						cnt=168
	    					fi
	    					#echo "cnt=$cnt"
					done
					ipaddr=`ifconfig $lan_ifname | sed -n 2p | awk -F: '{print $2}' | awk '{print $1}'`
					if [ "$ipaddr" = "199.99.199.99" ]; then
						ifconfig $lan_ifname $lan_ipaddr
					fi
				fi
			else
				${prog} -R -i ${lan_ifname} -p ${PID_FILE} -s ${SCRIPT_FILE} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &
				echo "${prog} -R -i ${lan_ifname} -p ${PID_FILE} -s ${SCRIPT_FILE} ${clientid} ${vendorid} ${vendorinfo} ${hostname} &"
			fi
		fi
	fi
	
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	/sbin/ifconfig $INTERFACE 0.0.0.0 
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	killall -17 udhcpc	#for release
	
	if [ "`nvram get router_disable`" = "0" ]; then	
		ifconfig $wan_ifname 0.0.0.0
	else
		lan_ipaddr=`nvram get lan_ipaddr`
		killall -SIGUSR2 udhcpc
		if [ `nvram get lan_dhcpclient` = 1 ]; then
			lan_dhcp_ipaddr=`ifconfig $lan_ifname | sed -n 2p | awk -F: '{print $2}' | awk '{print $1}'`
			if [ "$lan_ipaddr" != "$lan_dhcp_ipaddr" ]; then
				echo "$lan_dhcp_ipaddr" > $lan_dhcp_ipaddr_file
				ifconfig $lan_ifname $lan_ipaddr
			fi
		else
			echo "$lan_ipaddr" > $lan_dhcp_ipaddr_file
		fi
		route_dns_renew
	fi
	killall -9 udhcpc
	killall -9 bpalogin
	rm -f ${PID_FILE}
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

