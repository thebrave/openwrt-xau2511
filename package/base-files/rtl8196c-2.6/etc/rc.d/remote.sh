#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

. /etc/rc.d/tools.sh

accept="ACCEPT"
log="LOG"
drop="DROP"
reject="REJECT"

RETVAL=0

iptables="iptables"

LAN_IPADDR=`nvram get lan_ipaddr`
LAN_SUBNET=`nvram get lan_netmask`
masklen=`print_masklen $LAN_SUBNET`
subnet=`print_subnet $LAN_IPADDR $LAN_SUBNET`
wan_ip=`nvram get wan0_ipaddr`
mark_flag=3

start() {
	local remote_endis=`nvram get remote_endis`
	local remote_access=`nvram get remote_access`
	local remote_iplist=`nvram get remote_iplist`
	local remote_port=`nvram get remote_port`
	local wan_ifname=`nvram get wan_ifname`


	if [ "$remote_endis" = "1" ]; then
		#$iptables -t nat -A nat_local_server -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
		if [ "$remote_access" = "2" ]; then	# every one
			#The rule in mangle table mark packet which will be REDIRECT. 
			#This can prevent remote host access DUT by port 80 directly.
			$iptables -t mangle -A PREROUTING -p tcp --dport $remote_port -j MARK --set-mark $mark_flag 
			$iptables -t nat -A nat_local_server -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
			$iptables -A local_server -m mark --mark $mark_flag -d $wan_ip -p tcp --dport 80 -j ACCEPT	
		else
			if [ "$remote_access" = "0" ]; then	# only this ip
				$iptables -t mangle -A PREROUTING -s $remote_iplist -p tcp --dport $remote_port -j MARK --set-mark $mark_flag
				$iptables -t nat -A nat_local_server -s $remote_iplist -p tcp --dport $remote_port -j REDIRECT --to-ports 80
				$iptables -A local_server -m mark --mark $mark_flag -d $wan_ip -s $remote_iplist -p tcp --dport 80 -j ACCEPT
			else
				$iptables -t mangle -A PREROUTING -m iprange --src-range $remote_iplist -p tcp --dport $remote_port -j MARK --set-mark $mark_flag
				$iptables -t nat -A nat_local_server -m iprange --src-range $remote_iplist -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
				$iptables -A local_server -m mark --mark $mark_flag -d $wan_ip -m iprange --src-range $remote_iplist -p tcp --dport 80 -j ACCEPT
			fi
		fi
	fi	
	#$iptables -t nat -A nat_local_server -p tcp --dport 80 -s $subnet/$masklen -j DROP	
}

stop() {

	local remote_endis=`nvram get remote_endis`
	local remote_access=`nvram get remote_access`
	local remote_iplist=`nvram get remote_iplist`
	local remote_port=`nvram get remote_port`
	local wan_ifname=`nvram get wan_ifname`


	if [ "$remote_endis" = "1" ]; then
		#$iptables -t nat -D nat_local_server -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
		if [ "$remote_access" = "2" ]; then	# every one 
			$iptables -t mangle -D PREROUTING -p tcp --dport $remote_port -j MARK --set-mark $mark_flag 
			$iptables -t nat -D nat_local_server -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
			$iptables -D local_server -m mark --mark $mark_flag -d $wan_ip -p tcp --dport 80 -j ACCEPT	
		else
			if [ "$remote_access" = "0" ]; then	# only this ip
				$iptables -t mangle -A PREROUTING -s $remote_iplist -p tcp --dport $remote_port -j MARK --set-mark $mark_flag
				$iptables -t nat -D nat_local_server -s $remote_iplist -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
				iptables -D local_server -m mark --mark $mark_flag -d $wan_ip -s $remote_iplist -p tcp --dport 80 -j ACCEPT
			else
				$iptables -t mangle -A PREROUTING -m iprange --src-range $remote_iplist -p tcp --dport $remote_port -j MARK --set-mark $mark_flag
				$iptables -t nat -D nat_local_server -m iprange --src-range $remote_iplist -p tcp --dport $remote_port -j REDIRECT --to-ports 80	
				iptables -D local_server -m mark --mark $mark_flag -d $wan_ip -m iprange --src-range $remote_iplist -p tcp --dport 80 -j ACCEPT
			fi
		fi
	fi
	#$iptables -t nat -D nat_local_server -p tcp --dport 80 -s $subnet/$masklen -j DROP	
	
}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL
