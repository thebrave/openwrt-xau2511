#!/bin/sh

[ -f /usr/sbin/ez-ipupdate ] || exit 0

prog="/usr/sbin/ez-ipupdate"
pid="/var/run/ez-ipupd.pid"

ORAY_SERVER=phservice5.oray.net
ORAY_PORT=6060

ddns_enable=`nvram get endis_ddns`
usr_name=`nvram get sysDNSUser`
usr_password=`nvram get sysDNSPassword`
host_name=`nvram get sysDNSHost`
wan_ifname=`nvram get wan_ifname`
wan_ipaddr=`nvram get wan_default_ipaddr`
ddns_wildcard=`nvram get endis_wildcards`
ddns_provider=`nvram get sysDNSProviderlist`

# Not used parameter: sysDNSProviderlist,  change_wan_type

#return 0 for check pass, 1 for check fail.
returnValue=0
check()	
{
	if [ "$ddns_enable" != 1 ]; then	#ddns disable, do nothing
		returnValue=1
		return 1
	fi
	if [ "$usr_name" = "" ]; then
		echo "No user name for DDNS!"
		returnValue=1
		return 1
	fi
	if [ "$usr_password" = "" ]; then
		echo "No password for DDNS!"
		returnValue=1
		return 1
	fi
	if [ "$host_name" = "" ]; then
		echo "No host name for DDNS!"
		returnValue=1
		return 1
	fi
	if [ "$wan_ipaddr" = "" ]; then
		echo "No WAN IP address for DDNS!"
		returnValue=1
		return 1
	fi

	#ddns will not process when current ip and host are the same as last update.
	if [ -f /tmp/ddns_lastip ] && [ -f /tmp/ddns_lasthost ]; then
		lastip=`cat /tmp/ddns_lastip`
		lasthost=`cat /tmp/ddns_lasthost`
		if [ "$lastip" = "$wan_ipaddr" ] && [ "$lasthost" = "$host_name" ]; then
			echo "== Your IP address and hostname have not changed since the last DDNS update =="
			returnValue=1
			return 1
		fi
	fi

	echo $wan_ipaddr > /tmp/ddns_lastip
	echo $host_name > /tmp/ddns_lasthost

	returnValue=0
	return 0
}

start()
{
	check

	if [ "$returnValue" = "0" ] ; then
		ddns_update
	fi
}

ddns_update()
{
	killall -9 ez-ipupdate
	killall -9 updatednsip
	rm -f ${pid}
	
	if [ "$ddns_enable" = "1" ]; then        #ddns disable, do nothing
		local DDNS_WILDCARD
		if [ "$ddns_wildcard" = 1 ]; then
			DDNS_WILDCARD="-w"
		fi
		if [ "$ddns_provider" = "0" ]; then
			service_type="dyndns"
		elif [ "$ddns_provider" = "1" ]; then
			service_type="qdns"
		elif [ "$ddns_provider" = "2" ]; then
			service_type="dtdns"
		elif [ "$ddns_provider" = "3" ]; then
			service_type="oray"
		elif [ "$ddns_provider" = "4" ]; then
			service_type="hdns"
		fi

		if [ "$service_type" != "oray" ]; then
			echo "$prog -S $service_type -u $usr_name:$usr_password -h $host_name -i $wan_ifname $DDNS_WILDCARD -M 86400 -p 30 -P 10 -r 7 -F $pid -d"
			$prog -S $service_type -u $usr_name:$usr_password -h $host_name -i $wan_ifname $DDNS_WILDCARD -M 86400 -p 30 -P 10 -r 7 -F $pid -d
		else
			echo "/usr/sbin/updatednsip $ORAY_SERVER $ORAY_PORT $usr_name $usr_password"
			/usr/sbin/updatednsip $ORAY_SERVER $ORAY_PORT $usr_name $usr_password &
		fi
		#if file is not exist, create it. This file will be used in monitor.sh
		echo `cat /proc/uptime | cut -d "." -f 1` >/tmp/ddns_force_update
	fi
}
		

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	;;
  restart|reload)
	start
	;;
  ddns_update)
	ddns_update	
	;;
  *)
	echo $"Usage: $0 {start|stop|restart|ddns_update}"
	exit 1
esac

exit 1

