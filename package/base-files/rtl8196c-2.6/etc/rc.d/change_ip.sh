#!/bin/sh

. /etc/rc.d/tools.sh

lan_ifname=`nvram get lan_ifname`

#Use as Access Point
# To avoid to change mac address on br0, there are 2 ways as the followings,
# 1. changed eth1 hwaddr equal to eth0 
# 2. brctl addif br0 eth1, brctl addif br0 eth0

LAN_IPADDR=`ifconfig br0 | grep "inet addr" |  sed 's/:/ /g' | awk '{print $3}'`
LAN_SUBNET=`nvram get lan_netmask`

FIXED_INIT_PROG="/etc/rc.d/static.sh"
DNS_INIT_PROG="/etc/rc.d/dnsmasq.sh"
# NETBIOS_INIT_PROG="/etc/rc.d/netbios.sh"

WAN_TYPE=`nvram get wan_ether_wan_assign`
WAN_NAME=`nvram get wan_ifname`
WAN_IPADDR=`nvram get wan_ipaddr`
#WAN_SUBNET=`nvram get wan_netmask`
WAN_SUBNET=`ifconfig br0 | grep "inet addr" |  sed 's/:/ /g' | awk '{print $7}'`
udhcpc_start()
{
	UDHCPC_PROG="/usr/sbin/udhcpc"
	NETBIOS_NAME=`nvram get ap_netbiosname`
	lan_ifname=`nvram get lan_ifname`
	WAN_IPADDR=`nvram get wan_ipaddr`
	#WAN_SUBNET=`nvram get wan_netmask`
	WAN_SUBNET=`ifconfig br0 | grep "inet addr" |  sed 's/:/ /g' | awk '{print $7}'`
	if [ "$WAN_TYPE" = "0" ]; then
		# ${UDHCPC_PROG} -b -i $lan_ifname -h $NETBIOS_NAME -r 0.0.0.0
		${UDHCPC_PROG} -b -i $lan_ifname -r 0.0.0.0
		sleep 4
		if [ "`nvram get wan_dhcp_ipaddr`" = "0.0.0.0" ]; then
			# ifconfig $lan_ifname $LAN_IPADDR
			/etc/rc.d/lan_start_tmp.sh $LAN_IPADDR $WAN_SUBNET
		fi
	else
		if [ "$WAN_TYPE" = "1" ]; then
			ifconfig $lan_ifname $WAN_IPADDR
			killall smbd
			/etc/init.d/samba start		
		fi
	fi
	
}

echo "Configuring LAN , lan_ifname = $lan_ifname ........"
/etc/rc.d/netbios.sh stop

if [ "$WAN_TYPE" = "1" ]; then
	ifconfig $lan_ifname $WAN_IPADDR netmask $WAN_SUBNET
	${FIXED_INIT_PROG} start
else
	#ifconfig $lan_ifname $LAN_IPADDR netmask $LAN_SUBNET
	killall udhcpc
	sleep 1
	udhcpc_start
fi

# ${NETBIOS_INIT_PROG} restart
# /etc/init.d/samba restart
#killall smbd
#/etc/init.d/samba start
#/sbin/cmddlna restart