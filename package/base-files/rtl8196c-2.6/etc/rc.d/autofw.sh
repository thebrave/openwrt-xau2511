#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

RETVAL=0

iptables="iptables"

# autofw_port format : outbound-proto(tcp/udp):outbound-port(d-d),inbound-proto(tdp/udp):inbound-port(d-d)>private-port(d-d),on/off,desc
add_autofw_port()
{
	local trigger_timeout=`nvram get porttrigger_timeout`
	local trigger="`nvram get autofw_port$1`"
	if [ -z $trigger ]; then
		return 0
	fi

	local outbound_proto=`echo $trigger | awk -F: '{print $1}'`	
	local tmpstr1=`echo $trigger | awk -F: '{print $2}'`
	local outbound_port=`echo $tmpstr1 | awk -F, '{print $1}'`
	local outbound_port1=`echo $outbound_port | awk -F- '{print $1}'`
	local inbound_proto=`echo $tmpstr1 | awk -F, '{print $2}'`
	local tmpstr2=`echo $trigger | awk -F: '{print $3}'`
	local inbound_port=`echo $tmpstr2 | awk -F\> '{print $1}'`
	local inbound_port1=`echo $inbound_port | awk -F- '{print $1}'`
	local inbound_port2=`echo $inbound_port | awk -F- '{print $2}'`
	local private_port=`echo $tmpstr2 | awk -F\> '{print $2}' | awk -F, '{print $1}'`
	local trigger_enable=`echo $trigger | awk -F, '{print $3}'`
	local trigger_ipaddr=`echo $trigger | awk -F, '{print $5}'`

	if [ $trigger_enable = "off" ]; then
		return 0
	fi

	if [ "$trigger_ipaddr" != "any" ]; then
		local single_ipaddr="-s `echo $trigger_ipaddr`"
	fi

	if [ "$inbound_proto" = "*" ]; then
		$iptables -A fwd_port_trigger $single_ipaddr -i br0 -p $outbound_proto --dport $outbound_port1 -j TRIGGER \
--trigger-type out --trigger-proto all --trigger-match $outbound_port1-$outbound_port1 --trigger-relate $inbound_port1-$inbound_port2 --trigger-timeout $trigger_timeout
		inbound_proto="tcp"
		$iptables -t nat -A nat_port_trigger_inbound -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type dnat --trigger-timeout $trigger_timeout
		$iptables -A fwd_port_trigger -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type in --trigger-timeout $trigger_timeout
		inbound_proto="udp"
		$iptables -t nat -A nat_port_trigger_inbound -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type dnat --trigger-timeout $trigger_timeout
		$iptables -A fwd_port_trigger -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type in --trigger-timeout $trigger_timeout
	else
	$iptables -t nat -A nat_port_trigger_inbound -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type dnat --trigger-timeout $trigger_timeout
	$iptables -A fwd_port_trigger -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type in --trigger-timeout $trigger_timeout
	$iptables -A fwd_port_trigger $single_ipaddr -i br0 -p $outbound_proto --dport $outbound_port1 -j TRIGGER \
--trigger-type out --trigger-proto $inbound_proto --trigger-match $outbound_port1-$outbound_port1 --trigger-relate $inbound_port1-$inbound_port2 --trigger-timeout $trigger_timeout
	fi
}

# autofw_port format : outbound-proto(tcp/udp):outbound-port(d-d),inbound-proto(tdp/udp):inbound-port(d-d)>private-port(d-d),on/off,desc
del_autofw_port()
{
	local trigger_timeout=`nvram get porttrigger_timeout`
	local trigger="`nvram get autofw_port$1`"
	if [ -z $trigger ]; then
		return 0
	fi

	local outbound_proto=`echo $trigger | awk -F: '{print $1}'`	
	local tmpstr1=`echo $trigger | awk -F: '{print $2}'`
	local outbound_port=`echo $tmpstr1 | awk -F, '{print $1}'`
	local outbound_port1=`echo $outbound_port | awk -F- '{print $1}'`
	local inbound_proto=`echo $tmpstr1 | awk -F, '{print $2}'`
	local tmpstr2=`echo $trigger | awk -F: '{print $3}'`
	local inbound_port=`echo $tmpstr2 | awk -F\> '{print $1}'`
	local inbound_port1=`echo $inbound_port | awk -F- '{print $1}'`
	local inbound_port2=`echo $inbound_port | awk -F- '{print $2}'`
	local private_port=`echo $tmpstr2 | awk -F\> '{print $2}' | awk -F, '{print $1}'`
	local trigger_enable=`echo $trigger | awk -F, '{print $3}'`
	local trigger_ipaddr=`echo $trigger | awk -F, '{print $5}'`

	if [ $trigger_enable = "off" ]; then
		return 0
	fi

	if [ "$trigger_ipaddr" != "any" ]; then
		local single_ipaddr="-s `echo $trigger_ipaddr`"
	fi

	if [ "$inbound_proto" = "*" ]; then
		$iptables -D fwd_port_trigger $single_ipaddr -i br0 -p $outbound_proto --dport $outbound_port1 -j TRIGGER \
--trigger-type out --trigger-proto all --trigger-match $outbound_port1-$outbound_port1 --trigger-relate $inbound_port1-$inbound_port2 --trigger-timeout $trigger_timeout
		inbound_proto="tcp"
		$iptables -t nat -D nat_port_trigger_inbound -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type dnat --trigger-timeout $trigger_timeout
		$iptables -D fwd_port_trigger -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type in --trigger-timeout $trigger_timeout
		inbound_proto="udp"
		$iptables -t nat -D nat_port_trigger_inbound -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type dnat --trigger-timeout $trigger_timeout
		$iptables -D fwd_port_trigger -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type in --trigger-timeout $trigger_timeout
	else	
	$iptables -t nat -D nat_port_trigger_inbound -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type dnat --trigger-timeout $trigger_timeout
	$iptables -D fwd_port_trigger -p $inbound_proto --dport $inbound_port1:$inbound_port2 -j TRIGGER --trigger-type in --trigger-timeout $trigger_timeout
	$iptables -D fwd_port_trigger $single_ipaddr -i br0 -p $outbound_proto --dport $outbound_port1 -j TRIGGER \
--trigger-type out --trigger-proto $inbound_proto --trigger-match $outbound_port1-$outbound_port1 --trigger-relate $inbound_port1-$inbound_port2 --trigger-timeout $trigger_timeout
	fi
}

add_one() {
	[ "$1" = "" ] && return
	add_autofw_port $1
}

delete_one() {
	[ "$1" = "" ] && return
	del_autofw_port $1
}

start() {
	PT_disable=`nvram get disable_port_trigger`
	if [ $PT_disable -eq 0 ]; then
        num=1
		entry="$(nvram get autofw_port${num})"
		while [ "$entry" != "" ]; 
        do
			add_autofw_port $num
                num=$(($num+1))
			entry="`nvram get autofw_port${num}`"
        done
	fi
}

stop() {
	$iptables -F fwd_port_trigger
	$iptables -F nat_port_trigger_inbound -t nat
}

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  add_one)
	add_one $2
	;;
  delete_one)
	delete_one $2
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|add_one|delete_one|restart}"
	exit 1
esac

exit $RETVAL
