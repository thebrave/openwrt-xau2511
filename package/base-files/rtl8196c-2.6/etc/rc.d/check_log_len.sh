#!/bin/sh

LOG_FILE=/var/log/messages

length=$(wc -l $LOG_FILE | awk '{ print $1 }')
email_get_notify=`nvram get email_notify`
email_send_flag=`nvram get email_cfAlert_Select`

if [ -e /tmp/full_send_mail ]; then
	if [ "$length" -gt 300 ]; then
		start_len=$((length - 300))
		sed -i "1,$start_len d" $LOG_FILE
		rm /tmp/full_send_mail
	fi
else
	if [ "$length" -gt 256 ] && [ "$email_get_notify" -eq 1 ] && [ "$email_send_flag" -eq 0 ]; then
		touch /tmp/full_send_mail
		/etc/email/email_full_log &
	else
		if [ "$length" -gt 300 ]; then
			start_len=$((length - 300))
			sed -i "1,$start_len d" $LOG_FILE
		fi
	fi
fi
	