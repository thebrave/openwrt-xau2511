#!/bin/sh

RETVAL=0

NVRAM=/usr/sbin/nvram
lan_ifname=`${NVRAM} get lan_ifname`

TELNET_NAME="utelnetd"
TELNET_PROG="/usr/sbin/${TELNET_NAME} -d -i ${lan_ifname} -l /bin/sh"

TELNETENABLE_NAME="telnetenable"
TELNETENABLE_PROG="/usr/sbin/${TELNETENABLE_NAME}"

tmp_telnet_proc="/tmp/telnet_proc"
old_telnet_proc="/tmp/telnet_old_proc"

RM=/bin/rm
CP=/bin/cp
CAT=/bin/cat
GREP=/bin/grep
PS=/bin/ps
ECHO=/bin/echo
NVRAM=/usr/sbin/nvram
KILLALL=/usr/bin/killall
AWK=/usr/bin/awk

check_telnet_proc()
{
				if [ ! -f ${old_telnet_proc} ]; then
					echo "${TELNETENABLE_NAME}" > ${old_telnet_proc}
				fi
				${PS} >  ${tmp_telnet_proc}

				telnet_state=`${CAT} ${tmp_telnet_proc} | ${GREP}  ${TELNET_NAME}`
				if [ "x${telnet_state}" != "x" ]; then
					echo "${TELNET_NAME}" > ${tmp_telnet_proc}
				else
					telnet_state=`${CAT} ${tmp_telnet_proc} | ${GREP}  ${TELNETENABLE_NAME}`
					if [ "x${telnet_state}" != "x" ]; then
						echo "${TELNETENABLE_NAME}" > ${tmp_telnet_proc}
					else
						${CP} -f ${old_telnet_proc} ${tmp_telnet_proc} 
					fi
				fi
				
				${CP} -f ${tmp_telnet_proc} ${old_telnet_proc}
}

telnet_start()
{
				${TELNETENABLE_PROG} &
				echo "${TELNETENABLE_NAME}" > ${tmp_telnet_proc}
}

telnet_stop()
{
				check_telnet_proc

				telnet_state=`${CAT} ${tmp_telnet_proc}`
				if [ "${telnet_state}" = "${TELNET_NAME}" ]; then
					${KILLALL}  ${TELNET_NAME}
				fi
				
				if [ "${telnet_state}" = "${TELNETENABLE_NAME}" ]; then
					${KILLALL}  ${TELNETENABLE_NAME}
				fi
}

telnet_reload()
{
				telnet_stop

				telnet_state=`${CAT} ${tmp_telnet_proc}`
				if [ "${telnet_state}" = "${TELNET_NAME}" ]; then
					${TELNET_PROG} &
				fi
				
				if [ "${telnet_state}" = "${TELNETENABLE_NAME}" ]; then
					${TELNETENABLE_PROG} &
				fi
}

# See how we were called.
case "$1" in
  reload)
  	telnet_reload
	;;
  stop)
	telnet_stop
	;;
  start)
	telnet_start
	;;
  *)
	echo $"Usage: $0 reload|stop|start"
	exit 1
esac

exit $RETVAL