#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

. /etc/rc.d/tools.sh
. /etc/rc.d/firewall_setup_function.sh

RETVAL=0

iptables="iptables"
wan_ifname=`nvram get wan_ifname`
fw_disable=`nvram get fw_disable`

start() {
	#for f in /proc/sys/net/ipv4/conf/*/rp_filter; do
	#	echo 1 > $f
	#done

	if [ "$fw_disable" = "0" ]; then
        	#####################################################################
		# Enable support of dynamic IP addresses by kernel      
		echo 1 > /proc/sys/net/ipv4/ip_dynaddr
		# Enable broadcast echo Protection
		echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts
		# Disable ECN routing 
		echo 0 > /proc/sys/net/ipv4/tcp_ecn
		# Enable tcp SYN Cookie Protection
		# echo 1 > /proc/sys/net/ipv4/tcp_syncookies
		# Disable Source Routed Packets
		for f in /proc/sys/net/ipv4/conf/*/accept_source_route; do
			echo 1 >$f
		done
		# Disable ICMP Redirect Acceptance
		for f in /proc/sys/net/ipv4/conf/*/accept_redirects; do
			echo 0 > $f
		done
		# Log packets with impossible addressses
		for f in /proc/sys/net/ipv4/conf/*/log_martians; do
			echo 1 >$f
		done
		#########################################################################

		#add_dos_firewall_chain
		#iptables -A INPUT -i $wan_ifname -j input_dos
		iptables -I FORWARD 4 -i $wan_ifname -j fwd_dos_input
		iptables -I FORWARD 10 -i $wan_ifname -j fwd_dos_EchoChargen
		iptables -I FORWARD -t mangle -o $wan_ifname -j fwd_dos_mangle_out
		iptables -I FORWARD -t mangle -i $wan_ifname -j fwd_dos_mangle_in
		#dos_firewall_advance $lan_ifname
		iptables -t nat -I PREROUTING 2 -i $wan_ifname -j nat_dos		
	else
		#To protect route itself
		#iptables -A INPUT -i $wan_ifname -j input_dos
		iptables -t nat -I PREROUTING 2 -i $wan_ifname -j nat_dos		
	fi
}

stop() {


	if [ "$fw_disable" = "0" ]; then
		iptables -D FORWARD -i $wan_ifname -j fwd_dos_input
		iptables -D FORWARD -i $wan_ifname -j fwd_dos_EchoChargen
		iptables -D FORWARD -t mangle -o $wan_ifname -j fwd_dos_mangle_out
		iptables -D FORWARD -t mangle -i $wan_ifname -j fwd_dos_mangle_in
		iptables -t nat -D PREROUTING -i $wan_ifname -j nat_dos		
	else
		iptables -t nat -D PREROUTING -i $wan_ifname -j nat_dos		
	fi
}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL
