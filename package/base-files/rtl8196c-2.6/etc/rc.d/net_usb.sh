#!/bin/sh

MODULE_PATH="/lib/modules/2.6.30"
HOSTNAME=`nvram get wan_hostname`
PRODUCT=`nvram get product_id`
NET_IF=`nvram get lan_ifname`

/usr/sbin/insmod $MODULE_PATH/sxuptp.ko netif=$NET_IF
/usr/sbin/insmod $MODULE_PATH/sxuptp_driver.ko
#/usr/sbin/insmod $MODULE_PATH/sxuptp_devfilter.ko
/usr/sbin/insmod $MODULE_PATH/jcp.ko
/usr/sbin/insmod $MODULE_PATH/jcp_cmd.ko product=$PRODUCT hostname=$HOSTNAME
 
