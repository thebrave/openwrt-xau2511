#!/bin/sh

#[ -f /sbin/syslogd ] || exit 0

RETVAL=0
prog="syslogd"
PID_FILE="/var/run/syslogd.pid"
klogd="klogd"

start() {
	# Start daemons.
	echo $"Starting $prog: "
	${prog} -m -1
	${klogd}
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	killall -9 syslogd
	rm -f ${PID_FILE}
	killall -9 klogd
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

