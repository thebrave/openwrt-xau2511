#!/bin/sh

RETVAL=0
wan_ifname=`nvram get wan_hwifname`
WAN_IPADDR=`nvram get wan_ipaddr`
WAN_SUBNET=`nvram get wan_netmask`
WAN_GATEWAY=`nvram get wan_gateway`
RESOLV_CONF="/etc/resolv.conf"
dns=`nvram get wan_dns`
wan_domain=`nvram get wan_domain`

start() {
	/usr/sbin/nvram set wan_ifname=$wan_ifname
	ifconfig $wan_ifname mtu $(nvram get wan_dhcp_mtu)
	ifconfig $wan_ifname $WAN_IPADDR netmask $WAN_SUBNET
	route add default gw $WAN_GATEWAY
	echo search $wan_domain > $RESOLV_CONF
	for i in $dns; do
		echo adding dns $i
		echo nameserver	$i >> $RESOLV_CONF
	done
	/usr/sbin/nvram set wan0_ipaddr=$WAN_IPADDR
	/usr/sbin/nvram set wan0_netmask=$WAN_SUBNET
	/usr/sbin/nvram set wan0_gateway=$WAN_GATEWAY
	/usr/sbin/nvram set wan0_dns="$dns"
	/usr/sbin/nvram set wan_default_ipaddr=$WAN_IPADDR
	/usr/sbin/nvram set wan_default_netmask=$WAN_SUBNET
	/usr/sbin/nvram set wan_default_gateway=$WAN_GATEWAY
	/usr/sbin/nvram set action=3
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	ifconfig $wan_ifname 0.0.0.0
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

