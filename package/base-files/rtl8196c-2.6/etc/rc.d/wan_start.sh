#!/bin/sh

RETVAL=0
UDHCPC_INIT_PROG="/etc/rc.d/udhcpc.sh"
PPPOE_INIT_PROG="/etc/rc.d/pppoe.sh"
PPTP_INIT_PROG="/etc/rc.d/pptp.sh"
FIXED_INIT_PROG="/etc/rc.d/static.sh"

NVRAM="/usr/sbin/nvram"
wan_proto=`nvram get wan_proto`
manual=$2

start() {
	# Start daemons.
	echo $"Starting WAN: "
	#${UDHCPC_INIT_PROG} stop
	#${PPPOE_INIT_PROG} stop
	#${PPTP_INIT_PROG} stop

	wanif=`nvram get wan_hwifname`
	wanhwaddr=`nvram get wan_hwaddr`
	lan_ifname=`nvram get lan_hwifname`
	if [ -z "$wanhwaddr" ]; then
		wanhwaddr=`nvram get wan_factory_mac`
		nvram set wan_hwaddr=$wanhwaddr
		nvram set wan0_hwaddr=$wanhwaddr
		#nvram commit
	fi
	nvram set wan0_hwaddr=$wanhwaddr
	ifconfig $wanif down hw ether $wanhwaddr
	ifconfig $wanif up
	nvram unset wan0_ipaddr
	nvram unset wan0_netmask
	nvram unset wan0_gateway
	nvram unset wan0_dns
	nvram unset wan_default_ipaddr
	nvram unset wan_default_netmask
	nvram unset wan_default_gateway
	
	echo "0" > /proc/fast_pptp
	echo "1" > /proc/fast_nat
	echo "2" > /proc/fast_nat

	case "$wan_proto" in	
		static)
#			nvram set DHCPRelayEnabled=0
			${FIXED_INIT_PROG} start
			;;
		dhcp)		
			#hostname `nvram get wan_hostname`
#			nvram set DHCPRelayEnabled=1	# scenario DUT(WAN)->Relay->Server 
			${UDHCPC_INIT_PROG} start $manual
			;;
		pppoe)
#			nvram set DHCPRelayEnabled=0
			${PPPOE_INIT_PROG} start $manual
			;;
		pptp)
			echo "1" > /proc/fast_pptp
			echo "3" > /proc/pptp_conn_ck
#			nvram set DHCPRelayEnabled=0
			if [ `$NVRAM get dy_pptp` = "1" ];then
				${UDHCPC_INIT_PROG} start $manual
			else
			${PPTP_INIT_PROG} start $manual
			fi
			;;
		*)

			echo $"Usage: $0 {start|stop|restart}"
	esac

	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting WAN: "
	
	# Delete default route
	#/sbin/ip route delete default
	#ip route delete default
	route del default
	/etc/rc.d/igmpproxy.sh stop
	#${UDHCPC_INIT_PROG} stop
	${PPPOE_INIT_PROG} stop
	${PPTP_INIT_PROG} stop
	${UDHCPC_INIT_PROG} stop
	${FIXED_INIT_PROG} stop
	iptables -F -t nat
	echo 2 > /proc/fast_nat
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

