#!/bin/sh

lan_ifname=`nvram get lan_ifname`
lan_ipaddr=`nvram get lan_ipaddr`

start()
{
        if [ "`nvram get wan_lan_ip_conflict`" = "1" ]; then
		iptables -A PREROUTING -t mangle -i $lan_ifname -d ! $lan_ipaddr -p udp --dport 53 -j DNS_HIJACK --url .as.xboxlive.com,.tgs.xboxlive.com,.macs.xboxlive.com --state blank
	else
		dns_hijack=$(nvram get dns_hijack)

		if [ "$dns_hijack" = "1" ];then
			iptables -A PREROUTING -t mangle -i $lan_ifname -d ! $lan_ipaddr -p udp --dport 53 -j DNS_HIJACK --url .as.xboxlive.com,.tgs.xboxlive.com,.macs.xboxlive.com --state blank
		elif [ "$dns_hijack" = "0" ];then
			iptables -A PREROUTING -t mangle -i $lan_ifname -d ! $lan_ipaddr -p udp --dport 53 -j DNS_HIJACK --url .www.routerlogin.com,.routerlogin.com,.www.routerlogin.net,.routerlogin.net --state normal
		fi
	fi
}

stop()
{

	if [ "`nvram get wan_lan_ip_conflict`" = "1" ]; then
                iptables -D PREROUTING -t mangle -i $lan_ifname -d ! $lan_ipaddr -p udp --dport 53 -j DNS_HIJACK --url .as.xboxlive.com,.tgs.xboxlive.com,.macs.xboxlive.com --state blank
        else
                dns_hijack=$(nvram get dns_hijack)

                if [ "$dns_hijack" = "1" ];then
                        iptables -D PREROUTING -t mangle -i $lan_ifname -d ! $lan_ipaddr -p udp --dport 53 -j DNS_HIJACK --url .as.xboxlive.com,.tgs.xboxlive.com,.macs.xboxlive.com --state blank
                elif [ "$dns_hijack" = "0" ];then
                        iptables -D PREROUTING -t mangle -i $lan_ifname -d ! $lan_ipaddr -p udp --dport 53 -j DNS_HIJACK --url .www.routerlogin.com,.routerlogin.com,.www.routerlogin.net,.routerlogin.net --state normal
                fi
        fi

}

# See how we were called.
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
	stop
	start
	;;
  *)
        echo $"Usage: $0 {start|stop|restart|AddChain|restartChain}"
        exit 1
esac

exit $RETVAL

