#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

RETVAL=0

#iptables="iptables"
lan_ifname=`nvram get lan_ifname`
wan_ifname=`nvram get wan_ifname`
blk_svc_enable=`nvram get blockserv_ctrl`
BLOCK_SVC=fwd_block_svc
DEL_PROC_ENTRY=/proc/conntrack_killer

# filter_client0=ip-ip:dp-dp,tcp[udp][both],0-6-0-23,on/off,block(0->always)
#---------------------------------------------------------------------------
#block_services1=FTP TCP 20 21 FTP 2 all
#block_services2=HTTP TCP 80 80 HTTP 1 192.168.1.10-192.168.1.20
#block_services3=Telnet TCP 23 23 Telnet 0 192.168.1.100
#---------------------------------------------------------------------------

start() 
{
	[ "`nvram get block_services1`" = "" ] && exit 0

	#nvram set blk_svc_on=1				#used by blk_site.sh
	#echo 0 > /proc/sys/net/ipv4/ct_fast_forward	#4:Block Services doesn't work

if [ "$blk_svc_enable" -gt "0" ]; then
   for item in `nvram show | grep ^block_services | awk -F"=" '{print $1}'`
   do
	svc_entry=`nvram get $item`
#	svc_type=`echo $svc_entry | awk -F" " '{print $1}'`
	svc_proto=`echo $svc_entry | awk -F" " '{print $2}'`
	svc_dport_s=`echo $svc_entry | awk -F" " '{print $3}'`
	svc_dport_e=`echo $svc_entry | awk -F" " '{print $4}'`
	svc_desc=`echo $svc_entry | awk -F" " '{print $5}'`
	svc_desc=`echo $svc_desc | sed 's/\&harr\;/ /g'`
	svc_filter=`echo $svc_entry | awk -F" " '{print $6}'`
	src_ip=`echo $svc_entry | awk -F" " '{print $7}'`
	src_ip_s=`echo $src_ip | awk -F"-" '{print $1}'`
	src_ip_e=`echo $src_ip | awk -F"-" '{print $2}'`
	
	# protocal handler
	if [ "$svc_proto" = "TCP/UDP" ]; then
		proto="tcp"
		proto_other="udp"
	else
		proto=$svc_proto
		proto_other=""
	fi

	# destination port handler
	if [ "$svc_dport_s" != "$svc_dport_e" ]; then
		dport="$svc_dport_s:$svc_dport_e"
	else
		dport="$svc_dport_s"
	fi

	if [ "$svc_filter" -eq "0" ]; then
		if [ "$proto" != "" ]; then
			iptables -A $BLOCK_SVC -i $lan_ifname -s $src_ip -p $proto --dport $dport  -j LOG --log-level info --log-prefix "[service blocked: $svc_desc]"
			iptables -A $BLOCK_SVC -i $lan_ifname -s $src_ip -p $proto --dport $dport  -j DROP
			echo "block_lan $src_ip $svc_dport_s $svc_dport_e $proto" > $DEL_PROC_ENTRY
		fi
		if [ "$proto_other" != "" ]; then
			iptables -A $BLOCK_SVC -i $lan_ifname -s $src_ip -p $proto_other --dport $dport  -j LOG --log-level info --log-prefix "[service blocked: $svc_desc]"
			iptables -A $BLOCK_SVC -i $lan_ifname -s $src_ip -p $proto_other --dport $dport  -j DROP
			echo "block_lan $src_ip $svc_dport_s $svc_dport_e $proto_other" > $DEL_PROC_ENTRY
		fi
	elif [ "$svc_filter" -eq "1" ]; then
		if [ "$proto" != "" ]; then
			iptables -A $BLOCK_SVC -i $lan_ifname -m iprange --src-range $src_ip -p $proto --dport $dport  -j LOG --log-level info --log-prefix "[service blocked: $svc_desc]"
			iptables -A $BLOCK_SVC -i $lan_ifname -m iprange --src-range $src_ip -p $proto --dport $dport  -j DROP
			echo "block_lan $src_ip_s $svc_dport_s $svc_dport_e $proto $src_ip_e" > $DEL_PROC_ENTRY
		fi
		if [ "$proto_other" != "" ]; then
			iptables -A $BLOCK_SVC -i $lan_ifname -m iprange --src-range $src_ip -p $proto_other --dport $dport  -j LOG --log-level info --log-prefix "[service blocked: $svc_desc]"
			iptables -A $BLOCK_SVC -i $lan_ifname -m iprange --src-range $src_ip -p $proto_other --dport $dport  -j DROP
			echo "block_lan $src_ip_s $svc_dport_s $svc_dport_e $proto_other $src_ip_e" > $DEL_PROC_ENTRY
		fi

	elif [ "$svc_filter" -eq "2" ]; then
		if [ "$proto" != "" ]; then
			iptables -A $BLOCK_SVC -i $lan_ifname -p $proto --dport $dport  -j LOG --log-level info --log-prefix "[service blocked: $svc_desc]"
			iptables -A $BLOCK_SVC -i $lan_ifname -p $proto --dport $dport  -j DROP
			echo "block_lan $src_ip $svc_dport_s $svc_dport_e $proto all_ip" > $DEL_PROC_ENTRY
		fi
		if [ "$proto_other" != "" ]; then
			iptables -A $BLOCK_SVC -i $lan_ifname -p $proto_other --dport $dport  -j LOG --log-level info --log-prefix "[service blocked: $svc_desc]"
			iptables -A $BLOCK_SVC -i $lan_ifname -p $proto_other --dport $dport  -j DROP
			echo "block_lan $src_ip $svc_dport_s $svc_dport_e $proto_other all_ip" > $DEL_PROC_ENTRY
		fi
	fi	
   done
fi

}


stop() {

	#nvram set blk_svc_on=0				#used by blk_site.sh
	#[ "`nvram get blk_site_on`" = "0" ] && echo 1 > /proc/sys/net/ipv4/ct_fast_forward #4:Block Services doesn't work

	[ "$blk_svc_enable" = "0" ] && exit 0
	iptables -F $BLOCK_SVC

}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

