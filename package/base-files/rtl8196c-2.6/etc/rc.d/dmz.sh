#!/bin/sh
# this script is used to control management control list function

#[ -f /bin/iptables ] || exit 0

. /etc/rc.d/tools.sh
RETVAL=0
iptables="iptables"

start() {

	local lan_ipaddr=`nvram get dmz_ipaddr`
	local enable=`nvram get wan_endis_dmz`
	if [ "$enable" = "1" ]; then
		$iptables -t nat -A nat_dmz -p icmp -j DROP
		$iptables -t nat -A nat_dmz -j DNAT --to-destination $lan_ipaddr --dnat-type dmz
		$iptables -A fwd_dmz -d $lan_ipaddr -m log --log-prefix "[LAN access from remote]" -j ACCEPT
	fi
}


stop() {

	local lan_ipaddr=`nvram get dmz_ipaddr`
	local enable=`nvram get wan_endis_dmz`
	if [ "$enable" = "1" ]; then
		$iptables -t nat -D nat_dmz -p icmp -j DROP
		$iptables -t nat -D nat_dmz -j DNAT --to-destination $lan_ipaddr --dnat-type dmz
		$iptables -D fwd_dmz -d $lan_ipaddr -m log --log-prefix "[LAN access from remote]" -j ACCEPT
	fi
}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL
