#!/bin/sh
. /etc/rc.d/tools.sh
iptables="iptables"
drop="DROP"
accept="ACCEPT"
reject="REJECT"
Added="-A"

lan_ifname=`nvram get lan_ifname`
lan_ifname=`nvram get wan_ifname`

# Attack 1 ---> Land Attack 
# arg1 : chain name
land_attack()
{
	chain=$1
	$iptables $Added $chain -s $wan_ipaddr -d $wan_ipaddr -m log --log-prefix "[DoS Attack: Land Attack]" -j DROP
}


# Attack 6 ---> TCP/UDP Echo/Chargen Attack
##### Drop all the UDP/TCP packet, with source port 7/19 and destination port 19/7, maybe need to be modified! ####
# arg1 : chain name
echo_chargen_attack()
{
	chain=$1
        $iptables $Added $chain -p udp --sport 7 --dport 19 -m log --log-prefix "[DoS Attack: Echo/Chargen Attack]" -j DROP

        $iptables $Added $chain -p udp --sport 19 --dport 7 -m log --log-prefix "[DoS Attack: Echo/Chargen Attack]" -j DROP

        $iptables $Added $chain -p tcp --sport 7 --dport 19 -m log --log-prefix "[DoS Attack: Echo/Chargen Attack]" -j DROP

        $iptables $Added $chain -p tcp --sport 19 --dport 7 -m log --log-prefix "[DoS Attack: Echo/Chargen Attack]" -j DROP
}

# Attack 11 ---> ICMP Scan, ICMP Sweep/Ping Sweep
# Refer to the feature "respond to Ping on Internet Port"
# arg1 : chain name
icmp_scan_attack()
{
	chain=$1
	$iptables $Added $chain -p icmp --icmp-type 10 -m log --log-prefix "[DoS Attack: ICMP Scan Attack]" -j DROP
	$iptables $Added $chain -p icmp --icmp-type 13 -m log --log-prefix "[DoS Attack: ICMP Scan Attack]" -j DROP
	$iptables $Added $chain -p icmp --icmp-type 15 -m log --log-prefix "[DoS Attack: ICMP Scan Attack]" -j DROP
	$iptables $Added $chain -p icmp --icmp-type 17 -m log --log-prefix "[DoS Attack: ICMP Scan Attack]" -j DROP
	$iptables $Added $chain -p icmp --icmp-type 37 -m log --log-prefix "[DoS Attack: ICMP Scan Attack]" -j DROP
}


#Attack 13 tcp port scan
#arg1 : chain name
tcp_scan_dos()
{
	chain=$1
	if [ "$chain" = "fwd_dos_input" ]; then
		CHAIN_="fwd_dos_mangle_in -t mangle"
	else
		CHAIN_=$chain
	fi
	# Xmas Tress Scan FIN/URG/PSH 
	$iptables $Added $CHAIN_ -p tcp --tcp-flags ALL FIN,URG,PSH -m log --log-prefix "[DoS Attack: Xmas Tress Scan]" -j DROP
	# FIN Scan
	$iptables $Added $chain -p tcp --tcp-flags ALL FIN -m state --state NEW -m log --log-prefix "[DoS Attack: FIN Scan]" -j DROP
	# Null Scan
	$iptables $Added $CHAIN_ -p tcp --tcp-flags ALL NONE -m log --log-prefix "[DoS Attack: NULL Scan]" -j DROP
	# SYN/FIN Scan	(IMAP SCAN)
	$iptables $Added $CHAIN_ -p tcp --tcp-flags SYN,FIN SYN,FIN -m log --log-prefix "[DoS Attack: IMAP Scan]" -j DROP
	#ACK Scan
	$iptables $Added $chain -p tcp --tcp-flags ALL ACK -m state --state NEW -m log --log-prefix "[DoS Attack: ACK Scan]" -j DROP
	#RST Scan
	$iptables $Added $chain -p tcp --tcp-flags ALL RST -m state --state NEW -m log --log-prefix "[DoS Attack: RST Scan]" -j DROP
	# SYN/RST Scan
	$iptables $Added $chain -p tcp --tcp-flags SYN,RST SYN,RST -m state --state NEW -m log --log-prefix "[DoS Attack: SYN/RST Scan]" -j DROP
}

dos_firewall_advance()
{

	local wan_default_iface="0"
	local wan_ipaddr=`nvram get wan${wan_default_iface}_ipaddr`

	$iptables $Added syn-flood -m limit --limit 50/s --limit-burst 50 -j RETURN
	$iptables $Added syn-flood -m log --log-prefix "[DoS Attack : Syn-Flood]" -j DROP

	$iptables $Added ping-death -m length ! --length 0:65535 -m log --log-prefix "[DoS Attack : Ping Of Death]" -j DROP
	$iptables $Added ping-death -m length --length 0:65535 -j RETURN


#-----------------FORWARD CHAIN----------------------------------
	$iptables $Added fwd_dos_input -p tcp --syn -j syn-flood
	#$iptables $Added FORWARD -i $1 -p icmp --icmp-typ echo-request -j ping-death
	# Attack 1 ---> Land Attack 
	land_attack fwd_dos_input

	#Attack 2,4 ---> Ping of Death Attack, Jolt2 Attack
	$iptables $Added fwd_dos_input -j ping-death
	#Attack 3 Teardrop
	#Attack	4 Jolt2
	# Attack 5 ---> Ascend Kill Attack
	#$iptables $Added fwd_dos_input -p udp --dport 9 -j DROP
	# Attack 6 ---> TCP/UDP Echo/Chargen Attack
	#echo_chargen_attack fwd_dos_input
	echo_chargen_attack fwd_dos_EchoChargen
	# Attack 10 ---> WinNuke Attack
	$iptables $Added fwd_dos_input -p tcp --tcp-flags ALL URG -m log --log-prefix "[DoS Attack: WinNuke Attack]" -j DROP
	# Attack 11 ---> ICMP Scan, ICMP Sweep/Ping Sweep
	# Refer to the feature "respond to Ping on Internet Port"
	icmp_scan_attack fwd_dos_input
	# Attack 12,13 ---> UDP Port Scan, TCP Port Scan/Vanilla TCP Connect Scan
	$iptables $Added fwd_dos_mangle_out -t mangle -p icmp --icmp-type 3 -m log --log-prefix "[DoS Attack: ICMP destination unreachable]" -j DROP #icmp-type 3 -> Destination Unreachable
	$iptables $Added fwd_dos_mangle_out -t mangle -p tcp --tcp-flags RST RST -m state --state NEW -j DROP
	$iptables $Added fwd_dos_mangle_out -t mangle -p tcp --tcp-flags ACK ACK -m state --state NEW -j DROP
	$iptables $Added fwd_dos_mangle_out -t mangle -p tcp --tcp-flags FIN FIN -m state --state NEW -j DROP
	tcp_scan_dos fwd_dos_input
	
#-----------------INPUT CHAIN----------------------------------
	$iptables $Added input_dos -p tcp --syn -j syn-flood
	#$iptables $Added INPUT -i $1 -p icmp --icmp-typ echo-request -j ping-death
	# Attack 1 ---> Land Attack 
	land_attack input_dos

	#Attack 2,4 ---> Ping of Death Attack, Jolt2 Attack
	$iptables $Added input_dos -j ping-death
	#$iptables $Added INPUT -i $1 -m length ! --length 0:65535 -j DROP
	#$iptables $Added FORWARD -i $1 -m length ! --length 0:65535 -j DROP
	#The length will be ip total length, for ICMP the sizeof(IP_HEADER) and sizeof(ICMP_HEADER) will be added

	#Attack 3 Teardrop
	#Attack 4 Jolt2

	# Attack 5 ---> Ascend Kill Attack
	##### Drop all the UDP packet to the port 9(Discard port), maybe need to be modified! ##### 
	$iptables $Added input_dos -p udp --dport 9 -m log --log-prefix "[DoS Attack: Ascend Kill Attack]" -j DROP

	# Attack 6 ---> TCP/UDP Echo/Chargen Attack
	echo_chargen_attack input_dos

	# Attack 10 ---> WinNuke Attack
	$iptables $Added input_dos -p tcp --tcp-flags ALL URG -m log --log-prefix "[DoS Attack: WinNuke Attack]" -j DROP
	# Attack 11 ---> ICMP Scan, ICMP Sweep/Ping Sweep
	# Refer to the feature "respond to Ping on Internet Port"
	icmp_scan_attack input_dos
	# Attack 12,13 ---> UDP Port Scan, TCP Port Scan/Vanilla TCP Connect Scan
	$iptables $Added output_dos -p icmp --icmp-type 3 -j DROP #icmp-type 3 -> Destination Unreachable
	tcp_scan_dos input_dos
}

dos_ip_spoofing()
{

	local LAN_IPADDR=`nvram get lan_ipaddr`
	local LAN_SUBNET=`nvram get lan_netmask`
	local LAN_masklen=`print_masklen $LAN_SUBNET`
	local LAN_subnet=`print_subnet $LAN_IPADDR $LAN_SUBNET`

	# Attack 8 ---> IP Spoofling Attack 
	# Drop the packet to the wan interface, with the source address at the subnet of the router's Lan
	# WAN IP spoofing
	$iptables -t nat $Added nat_dos -s $LAN_subnet/$LAN_masklen -m log --log-prefix "[DoS Attack: IP Spoofing]" -j DROP
}
