#!/bin/sh

RETVAL=0

router_disable=`nvram get router_disable`
if [ "$router_disable" = "1" ]; then
	echo "The SIP ALG of DUT is unsuppored!!"
	exit 1
fi

SIP_ALG_disable=`nvram get wan_endis_sipalg`
sip_ports=`nvram get sip_ports`
if [ "$sip_ports" = "" ]; then
	sip_ports="5060,5061"
fi
MODULE_PATH="/lib/modules/2.4.27"

MODULE_NAME_1="ip_conntrack_sip"
MODULE_NAME_2="ip_nat_sip"

start() {
	echo "Set SIP_ALG function enable ..."

	if [ "$SIP_ALG_disable" = "0" ]; then
		for module in $MODULE_NAME_1 $MODULE_NAME_2;
		do
			echo "insert $module kernel module"
			if ! lsmod |grep $module > /dev/null; then \
				insmod $module; \
			fi
		done
	fi

	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	echo "Set SIP_ALG function disable ..."

	#if [ "$SIP_ALG_disable" = "0" ]; then
		for module in $MODULE_NAME_2 $MODULE_NAME_1;
		do
			echo "remove $module kernel module"
			rmmod $module
		done
	#fi
	
	RETVAL=$?
	echo
	return $RETVAL
}


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

