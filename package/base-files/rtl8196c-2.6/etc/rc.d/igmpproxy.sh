#!/bin/sh

#[ -f /usr/local/sbin/igmpproxy ] || exit 0

RETVAL=0
prog="igmpproxy"
PID_FILE="/var/run/igmpproxy.pid" #if the path/file changes, remember to modify snmp checking function
CONFIG_FILE="/var/igmpproxy.conf"
lan_ifname=`nvram get lan_ifname`
wan_ifname=`nvram get wan_ifname`
wan_hwifname=`nvram get wan_hwifname`
wan_proto=`nvram get wan_proto`

start() {
	# Start daemons.
	local igmp_enable=`nvram get igmp_enable`
	if [ "$igmp_enable" != "1" ]; then	
		return $RETVAL
	fi
	echo $"Starting $prog: "
	echo "quickleave" > $CONFIG_FILE
	echo "" >> $CONFIG_FILE

        case "$wan_proto" in
                static|dhcp)
                        wan_ifname_upstream=$wan_ifname
                        ;;
                pppoe)
			if [ "$(nvram get wan_pppoe_wan_assign)" = "1" ] && [ "$(nvram get wan_pppoe_netmask)" != "0.0.0.0" ]; then                
							
                        	wan_ifname_upstream=$wan_hwifname
			else
                        	wan_ifname_upstream=$wan_ifname
			fi
                        ;;
                pptp)
                        wan_ifname_upstream=$wan_hwifname
                        ;;
                *)
                        wan_ifname_upstream=$wan_ifname
        esac
	
	echo "phyint $wan_ifname_upstream upstream  ratelimit 0  threshold 1" >> $CONFIG_FILE
	echo "" >> $CONFIG_FILE

	echo "phyint $lan_ifname downstream  ratelimit 0  threshold 1" >> $CONFIG_FILE
	echo "" >> $CONFIG_FILE

	if [ "$wan_ifname_upstream" = "$wan_hwifname" ]; then
		echo "phyint ppp0 disabled" >> $CONFIG_FILE
	echo "" >> $CONFIG_FILE
	fi

	touch $PID_FILE
	iptables -D INPUT -i $wan_ifname_upstream -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT > /dev/null 2>&1
	iptables -D FORWARD -i $wan_ifname_upstream -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT > /dev/null 2>&1

	iptables -I INPUT -i $wan_ifname_upstream -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT
	iptables -I FORWARD -i $wan_ifname_upstream -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT

	echo "1" > /proc/br_igmpsnoop
	echo "1" > /proc/br_igmpProxy

	${prog} -c $CONFIG_FILE
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	echo "0" > /proc/br_igmpsnoop
	echo "0" > /proc/br_igmpProxy

	killall -2 igmpproxy
	rm -f ${PID_FILE}
	iptables -D INPUT -i $wan_ifname -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT > /dev/null 2>&1
	iptables -D FORWARD -i $wan_ifname -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT > /dev/null 2>&1
	iptables -D INPUT -i $wan_hwifname -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT > /dev/null 2>&1
	iptables -D FORWARD -i $wan_hwifname -m iprange --dst-range 224.0.0.0-239.255.255.255 -j ACCEPT > /dev/null 2>&1
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

