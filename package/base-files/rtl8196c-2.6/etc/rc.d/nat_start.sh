#!/bin/sh

. /etc/rc.d/tools.sh

INTERFACE=`nvram get lan_ifname`
LAN_IPADDR=`nvram get lan_ipaddr`
LAN_SUBNET=`nvram get lan_netmask`
masklen=`print_masklen $LAN_SUBNET`
subnet=`print_subnet $LAN_IPADDR $LAN_SUBNET`
NAT_TYPE_FULLCONE=`nvram get nat_type_fullcone`
wan_ifname=`nvram get wan_ifname`
NVRAM="/usr/sbin/nvram"

lan1_enable=`nvram get lan1_enable`
lan2_enable=`nvram get lan2_enable`
lan3_enable=`nvram get lan3_enable`

iptables="iptables"

#echo "Enable IP forwarding"
#echo 1 > /proc/sys/net/ipv4/ip_forward || echo "/proc not available"

echo "Enable SNATP2P and CONENAT Modules"
echo 0 > /proc/sys/net/ipv4/ip_forward || echo "/proc not available"
$iptables -F -t nat
#$iptables -X -t nat
$iptables -Z -t nat
$iptables -t nat -P PREROUTING ACCEPT
$iptables -t nat -P POSTROUTING ACCEPT
$iptables -t nat -P OUTPUT ACCEPT

#$iptables -t nat -A POSTROUTING -s $subnet/$masklen -j MASQUERADE

#
# Add SNATP2P rule for each wan interface
#

lan_ifname=`nvram get lan_ifname`
wan_ifname=`nvram get wan_ifname`
wan_ipaddr=`nvram get wan0_ipaddr`
if [ "`$NVRAM get wan_proto`" = "pptp" ]; then
	if [ "`$NVRAM get dy_pptp`" = "1" ]; then
		dhcp_if=`$NVRAM get wan_dhcp_ifname`
		dhcp_wan=`$NVRAM get wan_dhcp_ipaddr`
		$iptables -t nat -A POSTROUTING -o $dhcp_if -j SNATP2P --to-source $dhcp_wan
	else 
		PPTP_MY_IP=`$NVRAM get wan_pptp_local_ip`
		wan_hwifname=`$NVRAM get wan_hwifname`
		$iptables -t nat -A POSTROUTING -o $wan_hwifname -j SNATP2P --to-source ${PPTP_MY_IP}
	fi
	/etc/rc.d/igmpproxy.sh restart
fi

if [ "`$NVRAM get wan_proto`" = "pppoe" ]; then
	if [ "`$NVRAM get wan_pppoe_wan_assign`" = "1" ] && [ "`$NVRAM get wan_pppoe_netmask`" != "0.0.0.0" ]; then
		dyn_wan_ip=`$NVRAM get wan_pppoe_ip`
		pppoe_netmask=`$NVRAM get wan_pppoe_netmask`
		pppoe_wan_hwifname=`$NVRAM get wan_hwifname`
		ifconfig $pppoe_wan_hwifname $dyn_wan_ip netmask $pppoe_netmask up
		iptables -t nat -D POSTROUTING -o $pppoe_wan_hwifname -j SNATP2P --to-source $dyn_wan_ip
		iptables -t nat -A POSTROUTING -o $pppoe_wan_hwifname -j SNATP2P --to-source $dyn_wan_ip
		/etc/rc.d/igmpproxy.sh restart
	fi
fi

$iptables -t nat -A PREROUTING -i $wan_ifname -j dnat
#$iptables -t nat -A PREROUTING -i $wan_ifname -j nat_dos
#$iptables -t nat -A PREROUTING -i $wan_ifname -d $wan_ipaddr -j nat_local_server
$iptables -t nat -A PREROUTING -d $wan_ipaddr -j nat_local_server
$iptables -t nat -A PREROUTING -d $wan_ipaddr -j nat_port_forward
$iptables -t nat -A PREROUTING -d $wan_ipaddr -j nat_port_trigger_inbound
if [ "`$NVRAM get wan_proto`" = "pptp" ]; then
	if [ "`$NVRAM get dy_pptp`" = "1" ]; then
		pptp_static_wan=`$NVRAM get wan_dhcp_ipaddr`
	else 
		pptp_static_wan=`$NVRAM get wan_pptp_local_ip`
	fi
	iptables -t nat -A PREROUTING -d $pptp_static_wan -j nat_port_forward
	iptables -t nat -A PREROUTING -d $pptp_static_wan -j nat_port_trigger_inbound
fi
if [ "`$NVRAM get wan_proto`" = "pppoe" ]; then
	if [ "`$NVRAM get wan_pppoe_wan_assign`" = "1" ] && [ "`$NVRAM get wan_pppoe_netmask`" != "0.0.0.0" ]; then
		pppoe_static_wan=`$NVRAM get wan_pppoe_ip`
		iptables -t nat -A PREROUTING -d $pppoe_static_wan -j nat_port_forward
		iptables -t nat -A PREROUTING -d $pppoe_static_wan -j nat_port_trigger_inbound
	fi
fi
		
$iptables -t nat -A PREROUTING -i $wan_ifname -d $wan_ipaddr -j nat_upnp
$iptables -t nat -A PREROUTING -d $wan_ipaddr -j nat_dmz

#$iptables -t nat -A POSTROUTING -o $wan_ifname -j SNATP2P --to-source $wan_ipaddr
$iptables -t nat -A POSTROUTING -o $lan_ifname -s $LAN_IPADDR -j ACCEPT
#for static routing in LAN domain.
$iptables -t nat -A POSTROUTING -o $lan_ifname -j static_privateroute
#for RIP routing in LAN domain.
$iptables -t nat -A POSTROUTING -o $lan_ifname -j RIP_privateroute

$iptables -t nat -A POSTROUTING -s $subnet/$masklen -j SNATP2P --to-source $wan_ipaddr
$iptables -t nat -I nat_local_server -p icmp -s $subnet/$masklen -j ACCEPT


/etc/rc.d/nat_type.sh start

echo "Enable IP forwarding"
echo 1 > /proc/sys/net/ipv4/ip_forward || echo "/proc not available"

/etc/rc.d/conflict.sh start
