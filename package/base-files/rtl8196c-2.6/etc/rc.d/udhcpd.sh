#!/bin/sh


# Configure /etc/udhcpd.conf via nvram

#[ -f /sbin/udhcpd ] || exit 0
#[ -f /etc/udhcpd.conf ] || exit 0
#dhcprelay=`nvram get DHCPRelayEnabled`

RETVAL=0
prog="udhcpd"
PID_FILE="/var/run/udhcpd.pid"
CONFIG_FILE="/var/udhcpd.conf"

lan_ifname=`nvram get lan_ifname`

lan_proto=`nvram get lan_proto`
dhcp_start=`nvram get dhcp_start`
dhcp_end=`nvram get dhcp_end`
subnet=`nvram get lan_netmask`
lan_ipaddr=`nvram get lan_ipaddr`
domain=`nvram get wan_domain`
leasetime=`nvram get dhcp_lease`
dhcp_route=`nvram get dhcp_route`
dhcp_dns=`nvram get dhcp_dns`
wds_enable=`nvram get wds_endis_fun`
wds_mode=`nvram get wds_repeater_basic`

manual_ip() {
        num=1
	entry=`nvram get reservation$num`
	while [ "$entry" != "" ];
        do
		ip=`echo $entry | awk -F" " '{print $1}'`
		mac=`echo $entry | awk -F" " '{print $2}'`
		echo "static_lease $mac $ip" >> $CONFIG_FILE
		num=$(($num+1))
		entry=`nvram get reservation$num`
        done
}

start() {
#   	[ "$dhcprelay" = "1" ] && exit 0
	[ -e ${PID_FILE} ] && exit 0

	# Start daemons.
	echo $"Starting $prog: "
	echo "start		$dhcp_start" > $CONFIG_FILE
	echo "end		$dhcp_end" >> $CONFIG_FILE
	echo "interface		$lan_ifname" >> $CONFIG_FILE
	echo "remaining		yes" >> $CONFIG_FILE
	echo "lease_file	/tmp/udhcpd.leases" >> $CONFIG_FILE
	#echo "auto_time		5" >> $CONFIG_FILE
	echo "pidfile		$PID_FILE" >> $CONFIG_FILE
	echo "option	subnet	$subnet" >> $CONFIG_FILE
   if [ "$dhcp_route" = "" ]; then
      echo "option	router	$lan_ipaddr" >> $CONFIG_FILE
   else
      echo "option	router	$dhcp_route" >> $CONFIG_FILE
   fi
   if [ "$dhcp_dns" = "" ]; then
      echo "option	dns	$lan_ipaddr" >> $CONFIG_FILE
   else
      echo "option	dns	$dhcp_dns" >> $CONFIG_FILE
   fi
	echo "option	domain	$domain" >> $CONFIG_FILE
	if [ "$leasetime" = "0" ]; then		#lease time = forever
		echo "option	lease	0xFFFFFFFF" >> $CONFIG_FILE	
	else	
		echo "option	lease	$(($leasetime*1800))" >> $CONFIG_FILE	#half hours to seconds	
	fi
	
	manual_ip
	
	if [ "$lan_proto" = "dhcp" ]; then
		${prog} ${CONFIG_FILE} &
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	#if [ -e ${PID_FILE} ]; then
	#	kill `cat ${PID_FILE}`
	#	rm -f ${PID_FILE}
	#fi
	killall -9 udhcpd
	if [ -e ${PID_FILE} ]; then
		rm -f ${PID_FILE}
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
  if [ "$wds_enable" = "1" -a "$wds_mode" = "0" ]; then
  	echo "wds in repeater mode, no dhcpd on"
  else
  	start
  fi
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

