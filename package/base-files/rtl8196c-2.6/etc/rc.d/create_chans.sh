#!/bin/sh

iptables -t nat -N dnat
iptables -t nat -N nat_dos
iptables -t nat -N nat_port_forward
iptables -t nat -N nat_port_trigger_inbound
iptables -t nat -N nat_dmz
iptables -t nat -N nat_local_server
iptables -t nat -N nat_upnp
iptables -t nat -N static_privateroute
iptables -t nat -N RIP_privateroute

iptables -N input_init
iptables -N input_dos
iptables -N input_local_server

iptables -N forward_init
iptables -N wan_forward
iptables -N lan_forward
iptables -N fwd_port_forward
iptables -N fwd_port_trigger
iptables -N fwd_dmz
iptables -N fwd_block_site
iptables -N fwd_block_site_nntp
iptables -N fwd_block_svc
iptables -N fwd_dos_input
iptables -N fwd_dos_EchoChargen
iptables -t mangle -N fwd_dos_mangle_out
iptables -t mangle -N fwd_dos_mangle_in
iptables -t mangle -N u_qos_chain
iptables -t mangle -N d_qos_chain

iptables -N fwd_local_server
iptables -N fwd_upnp


iptables -N output_init
iptables -N output_dos
iptables -N local_server

iptables -N syn-flood
iptables -N ping-death


