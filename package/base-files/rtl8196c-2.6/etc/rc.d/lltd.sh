#!/bin/sh

#

RETVAL=0
prog="/usr/sbin/lld2d"

lan_ifname=`nvram get lan_ifname`
wan_ifname=`nvram get wan_ifname`

PID_FILE="/var/run/lld2d-$lan_ifname.pid"

start() {
	# Start daemons.
	local lltd_enable=`nvram get lltd_enable`

	if [ "$lltd_enable" = "1" ]; then
		echo $"Starting $prog: "
		${prog} $lan_ifname
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	killall -9 lld2d
	if [ -f "$PID_FILE" ]; then
		rm -f ${PID_FILE}
	fi
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL



