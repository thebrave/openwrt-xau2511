#!/bin/sh

#'flash set hw_params' will set all needed hw_setting vars from HW_SETTING_AREA and
#write then into various files. Then only lan_mac and wan_mac will be set to nvram,
#other wlan params will stay in files located under /tmp/hw_oaram for wlan script to follow
mkdir -p /tmp/hw_param
flash set hw_params


#echo "##HW related nvram"
#echo "lan_hwaddr: $(nvram get lan_hwaddr)"
#echo "wan_factory_mac: $(nvram get wan_factory_mac)"

#NVRAM_UPDATED=0


#set lan_mac
#if [ "$(nvram get lan_hwaddr)" = "" ];then
        WAIT=1
	while [ $WAIT != 0 ]		
	do	
	        if [ -e /tmp/hw_param/HW_NIC0_ADDR ]; then
		        WAIT=0
		else
			sleep 1
		fi
	done
	
	lan_hwaddr=`cat /tmp/hw_param/HW_NIC0_ADDR`
	#echo "nvram set lan_hwaddr=$lan_hwaddr"
	nvram set lan_hwaddr=$lan_hwaddr
	#NVRAM_UPDATED=1
#fi

##set wan_mac
##if [ "$(nvram get wan_factory_mac)" = "" ];then
#	wan_hwaddr=`cat /tmp/hw_param/HW_NIC1_ADDR`
#	#nvram set wan_hwaddr=$wan_hwaddr
#	nvram set wan_factory_mac=$wan_hwaddr
#	#NVRAM_UPDATED=1
##fi

flash gen_mac $(nvram get wan_factory_mac)

#set wps_pin
#if [ "$(nvram get wps_pin)" = "" ];then
	wps_pin=`cat /tmp/hw_param/HW_WLAN0_WSC_PIN`
	nvram set wps_pin=$wps_pin
	#NVRAM_UPDATED=1
#fi

#set wlan_mac
WLAN_IF=wlan0
WLAN_VA_IF=wlan0-va
BR=br0
WL_MAC=`nvram get lan_hwaddr`
ifconfig $WLAN_IF hw ether $WL_MAC

num=0
while [ $num -lt 4 ]
do 
    ifconfig $WLAN_IF-wds$num hw ether $WL_MAC
    num=$(($num+1))     
done

#if [ "$NVRAM_UPDATED" = "1" ];then
#	nvram commit
#fi


##wireless hw settings
#SET="iwpriv $WLAN_IF set_mib"
#
#$SET regdomain=`cat /tmp/hw_param/HW_WLAN0_REG_DOMAIN`
##$SET led_type=`cat /tmp/hw_param/HW_WLAN0_LED_TYPE`
#$SET led_type=7
## The 8196b support EEPROM,but the 8196c have not the EEPROM and it is the cost down version of 8196b 
##$SET RFChipID=`cat /tmp/hw_param/HW_WLAN0_RF_TYPE`
#
##2G
#$SET pwrlevelCCK_A=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_CCK_A`
#$SET pwrlevelCCK_B=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_CCK_B`
#sleep 1
#$SET pwrlevelHT40_1S_A=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_HT40_1S_A`
#$SET pwrlevelHT40_1S_B=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_HT40_1S_B`
#sleep 1
#$SET pwrdiffHT40_2S=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_DIFF_HT40_2S`
#sleep 1
#$SET pwrdiffHT20=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_DIFF_HT20`  
#$SET pwrdiffOFDM=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_DIFF_OFDM`
#sleep 1
#
##5G
##$SET pwrlevel5GHT40_1S_A=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_5G_HT40_1S_A`
##$SET pwrlevel5GHT40_1S_B=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_5G_HT40_1S_B`
##sleep 1
##$SET pwrdiff5GHT40_2S=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_DIFF_5G_HT40_2S`
##sleep 1
##$SET pwrdiff5GHT20=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_DIFF_5G_HT20`
##$SET pwrdiff5GOFDM=`cat /tmp/hw_param/HW_WLAN0_TX_POWER_DIFF_5G_OFDM`  
##sleep 1
#
#$SET tssi1=`cat /tmp/hw_param/HW_WLAN0_11N_TSSI1`
#$SET tssi2=`cat /tmp/hw_param/HW_WLAN0_11N_TSSI2`
#$SET xcap=`cat /tmp/hw_param/HW_WLAN0_11N_XCAP`
#$SET ther=`cat /tmp/hw_param/HW_WLAN0_11N_THER`
#
#
#
##delete tem files written in flash set ...
##rm -rf /tmp/hw_param
