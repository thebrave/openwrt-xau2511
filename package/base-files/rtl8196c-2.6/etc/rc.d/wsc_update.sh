#!/bin/sh

cat=/bin/cat

ssid=$($cat /tmp/wps/update_ssid)
if [ "x$ssid" != "x" ]; then
	#echo "===>wl_ssid $ssid"
	nvram set wl_ssid="$ssid"
fi

pass=$($cat /tmp/wps/update_wpa_key)
if [ "x$pass" != "x" ]; then
	#echo "===>wl_wpa1_psk $pass"
	nvram set wl_wpa1_psk="$pass"
fi

rm /tmp/wps/update_ssid
rm /tmp/wps/update_wpa_key

exit $RETVAL

