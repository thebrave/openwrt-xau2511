#!/bin/sh
nvram="/usr/sbin/nvram"

set_lan() #$1 is is the first 3 field of lan ip
{	
	ip=`$nvram get lan_ipaddr | sed 's/\([0-9]*.[0-9]*.[0-9]*\).*/\1/'`	

   # for port forward web setting change
	for file in `nvram show | grep ^forwarding | cut -d "=" -f1`
	do
      new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
   # for port forward iptables setting change
   for file in `nvram show | grep ^forward_port | cut -d "=" -f1`
	do
      new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
   # for port triggering web setting change
	for file in ` nvram show | grep ^triggering | cut -d "=" -f1`     
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
   # for port triggering iptables setting change
   for file in ` nvram show | grep ^autofw_port | cut -d "=" -f1`     
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
	for file in `nvram show | grep ^reservation | cut -d "=" -f1`   
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
	for file in `nvram show | grep ^block_services | cut -d "=" -f1`   
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		new_info=`echo $new_info | sed "s/-[0-9]*\.[0-9]*\.[0-9]*/-$ip/"`
		$nvram set $file="$new_info"
	done

	new_info=`$nvram get dmz_ipaddr | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
	$nvram set dmz_ipaddr="$new_info"
	new_info=`$nvram get block_trustedip | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
	$nvram set block_trustedip="$new_info"	
	
	/sbin/cmdlan.sh restart
	$nvram set action="3"
}

case "$1" in
        start)

         # add http_hijack services, here means not in blankstate, need to run http_hijack when WAN/LAN IP conflict
		set_lan 
        	;;
        stop)
#		echo -n 7 > /proc/switch_phy
		;;
	restart)
		set_lan
		;;
        *)
        	echo "Usage: /sbin/ip_conflict.sh start|stop|restart"
        	;;
esac
