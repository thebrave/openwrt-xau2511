#!/bin/sh

RETVAL=0
UDHCPC_INIT_PROG="/etc/rc.d/udhcpc.sh"
PPPOE_INIT_PROG="/etc/rc.d/pppoe.sh"
PPTP_INIT_PROG="/etc/rc.d/pptp.sh"
FIXED_INIT_PROG="/etc/rc.d/static.sh"

wan_proto=`nvram get wan_proto`
manual=$2
nvram set manual_start=$manual

start() {
	# Start daemons.
	echo $"Starting WAN: "
	#${UDHCPC_INIT_PROG} stop
	#${PPPOE_INIT_PROG} stop
	#${PPTP_INIT_PROG} stop
        wl_radio=`nvram get endis_wl_radio`
        router_disable=`nvram get router_disable`
	if [ "$router_disable" -ne "0" ]; then
		return $RETVAL
	fi	

	/etc/rc.d/set_wan_ifname.sh
	#/etc/rc.d/upnp.sh start
	#/etc/rc.d/mini_igd.sh start
	wanif=`nvram get wan_hwifname`
	wanhwaddr=`nvram get wan_hwaddr`
	if [ "`nvram get wan_enable_ipv6_passthrough`" = "1" ];then
			echo 1 > /proc/custom_Passthru
	else
			echo 0 > /proc/custom_Passthru
	fi
	nvram set wan0_hwaddr=$wanhwaddr
	ifconfig $wanif down hw ether $wanhwaddr
	ifconfig $wanif up
	nvram unset wan0_ipaddr
	nvram unset wan0_netmask
	nvram unset wan0_gateway
	nvram unset wan0_dns
	nvram unset wan_default_ipaddr
	nvram unset wan_default_netmask
	nvram unset wan_default_gateway
	
	
	
#for miniigd
if [ "$wan_proto" = "pptp" -o "$wan_proto" = "pppoe" ]; then
     /bin/sed -e 's/WANIPConnection/WANPPPConnection/g' /var/linuxigd/picsdesc.skl > /var/linuxigd/picsdesc.skl.2
	 /bin/sed -e 's/WANIPConn1/WANPPPConn1/g' /var/linuxigd/picsdesc.skl.2 > /var/linuxigd/picsdesc.skl.1
	 /bin/sed -e 's/wanipcn.xml/wanpppcn.xml/g' /var/linuxigd/picsdesc.skl.1 > /var/linuxigd/picsdesc.skl
	 /bin/sed -e 's/WANIPConnection/WANPPPConnection/g' /var/linuxigd/picsdesc.xml > /var/linuxigd/picsdesc.xml.2
	 /bin/sed -e 's/WANIPConn1/WANPPPConn1/g' /var/linuxigd/picsdesc.xml.2 > /var/linuxigd/picsdesc.xml.1
	 /bin/sed -e 's/wanipcn.xml/wanpppcn.xml/g' /var/linuxigd/picsdesc.xml.1 > /var/linuxigd/picsdesc.xml
else
    /bin/sed -e 's/WANPPPConnection/WANIPConnection/g' /var/linuxigd/picsdesc.skl > /var/linuxigd/picsdesc.skl.2
	/bin/sed -e 's/WANPPPConn1/WANIPConn1/g' /var/linuxigd/picsdesc.skl.2 > /var/linuxigd/picsdesc.skl.1
	/bin/sed -e 's/wanpppcn.xml/wanipcn.xml/g' /var/linuxigd/picsdesc.skl.1 > /var/linuxigd/picsdesc.skl
	/bin/sed -e 's/WANPPPConnection/WANIPConnection/g' /var/linuxigd/picsdesc.xml > /var/linuxigd/picsdesc.xml.2
	/bin/sed -e 's/WANPPPConn1/WANIPConn1/g' /var/linuxigd/picsdesc.xml.2 > /var/linuxigd/picsdesc.xml.1
	/bin/sed -e 's/wanpppcn.xml/wanipcn.xml/g' /var/linuxigd/picsdesc.xml.1 > /var/linuxigd/picsdesc.xml 
fi
	
	echo "0" > /proc/fast_pptp
	echo "1" > /proc/fast_nat
        cat /proc/uptime | sed 's/ .*//' > /tmp/WAN_RE_uptime
	case "$wan_proto" in	
		static)
#			nvram set DHCPRelayEnabled=0
			${FIXED_INIT_PROG} start
			;;
		dhcp|bigpond)		
#			nvram set DHCPRelayEnabled=1
			echo `nvram get wan_hostname` > /proc/sys/kernel/hostname
#			hostname `nvram get wan_hostname`
			${UDHCPC_INIT_PROG} start $manual
			;;
		pppoe)
#			nvram set DHCPRelayEnabled=0
			if [ "$manual" = "manual" ]; then
				touch /tmp/ppp/trying
			fi
			${PPPOE_INIT_PROG} start $manual
			;;
		pptp)
			echo "1" > /proc/fast_pptp
			echo "3" > /proc/pptp_conn_ck
			
			if [ "$manual" = "manual" ]; then
				touch /tmp/ppp/trying
			fi
#			nvram set DHCPRelayEnabled=0
                        if [ "`nvram get dy_pptp`" = "1" ];then
                                ${UDHCPC_INIT_PROG} start $manual
                        else
				${PPTP_INIT_PROG} start $manual
			fi
			;;
		*)

			echo $"Usage: $0 {start|stop|restart}"
	esac

	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	# Stop daemons.
	echo $"Shutting WAN: "
	
	# Delete default route
	#/sbin/ip route delete default
	#ip route delete default
	route del default
	#/etc/rc.d/upnp.sh stop
	#/etc/rc.d/mini_igd.sh stop
	/etc/rc.d/ntpclient.sh stop
	/etc/rc.d/spi_dos.sh stop
	#/etc/rc.d/TC.sh stop
	/etc/rc.d/igmpproxy.sh stop
	${UDHCPC_INIT_PROG} stop
	${PPPOE_INIT_PROG} stop
	${PPTP_INIT_PROG} stop
	${FIXED_INIT_PROG} stop
	iptables -F -t nat
	iptables -F
	iptables -P INPUT ACCEPT 
	iptables -P FORWARD ACCEPT 
	if [ -f /tmp/ppp/trying ]; then
		rm /tmp/ppp/trying
	fi
	echo 2 > /proc/fast_nat
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	sleep 1
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

