#!/bin/sh

RETVAL=0

echo "running cmdlan.sh"

#wl_radio=`nvram get endis_wl_radio`
#repeater_basic=`nvram get wds_repeater_basic`
#wds_endis=`nvram get wds_endis_fun`
router_disable=`nvram get router_disable`
#enet_flag=`nvram get reinstall_enet`
#nvram unset reinstall_enet
phy_restart=$2

#INITFILE=/tmp/bridge_init
#lan_ifname=`nvram get lan_ifname`
#lan_hwifname=`nvram get lan_hwifname`
#wan_hwifname=`nvram get wan_hwifname`
#wl_ifname=`nvram get wl_ifname`

start() {

	/etc/rc.d/lan_start.sh
	#echo "2" > /proc/fast_nat
	/etc/init.d/samba stop
	
	sleep 1
	if [ "x$phy_restart" != "x" -a "$phy_restart" -gt "0" ]; then
		sleep 4
		/usr/bin/lan_phy up         # phy up 
	fi

	#/etc/rc.d/netbios.sh restart 2> /dev/null
	#sleep 1
	/etc/rc.d/service_start.sh udhcpc_start
	
	RETVAL=$?
	echo
	return $RETVAL
}

stop() {
	if [ "x$phy_restart" != "x" -a "$phy_restart" -gt "0" ]; then 
		sleep 1
		/usr/bin/lan_phy down         # phy down 
	fi
#	/etc/rc.d/service_start.sh stop_ui
#	if [ "$router_disable" = "1" ]; then
#		/etc/rc.d/wan_start.sh stop	
#		/etc/rc.d/ntpclient.sh stop	
#	fi
  /etc/rc.d/service_start.sh udhcpc_stop
	sleep 4
        
	RETVAL=$?
	echo
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $RETVAL

