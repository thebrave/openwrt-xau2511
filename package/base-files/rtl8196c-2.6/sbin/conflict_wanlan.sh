#!/bin/sh

nvram="/usr/sbin/nvram"

echo "running conflict_wanlan.sh"

local ret=0
$nvram set old_lan_ipaddr="`$nvram get lan_ipaddr`"
ip0=$(nvram get lan_ipaddr | sed 's/\([0-9]*.[0-9]*.[0-9]*\).*/\1/')

if [ "$ip0" = "192.168.1" ] ; then
	ret=1
fi

if [ "$ip0" = "10.0.0" ] ; then
	ret=2
fi

if [ "$ip0" = "172.16.0" ] ; then
	ret=0
fi
	
case "$ret" in
	1)
   $nvram set lan_ipaddr=10.0.0.1
   $nvram set dhcp_start=10.0.0.2
   $nvram set dhcp_end=10.0.0.254
	;;
	2)
	$nvram set lan_ipaddr=172.16.0.1
	$nvram set dhcp_start=172.16.0.2
   $nvram set dhcp_end=172.16.0.254
	;;
	0)
	$nvram set lan_ipaddr=192.168.1.1
	$nvram set dhcp_start=192.168.1.2
   $nvram set dhcp_end=192.168.1.254
	;;
esac

$nvram set lan_netmask=255.255.255.0



