#!/bin/sh
nvram="/usr/sbin/nvram"

lan_ipaddr=$($nvram get lan_ipaddr)

echo "running ip_conflict.sh"

set_lan() #$1 is is the first 3 field of lan ip
{	
	echo -n "1" > /tmp/wan_lan_ip_conflict	
	#$nvram set wan_lan_ip_conflict=1
	
	ip=`$nvram get lan_ipaddr | sed 's/\([0-9]*.[0-9]*.[0-9]*\).*/\1/'`	

   # for port forward web setting change
	for file in `nvram show | grep ^forwarding | cut -d "=" -f1`
	do
      new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
   # for port forward iptables setting change
   for file in `nvram show | grep ^forward_port | cut -d "=" -f1`
	do
      new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
   # for port triggering web setting change
	for file in ` nvram show | grep ^triggering | cut -d "=" -f1`     
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
   # for port triggering iptables setting change
   for file in ` nvram show | grep ^autofw_port | cut -d "=" -f1`     
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
	for file in `nvram show | grep ^reservation | cut -d "=" -f1`   
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		$nvram set $file="$new_info"
	done
	for file in `nvram show | grep ^block_services | cut -d "=" -f1` 
	do
		new_info=`nvram get $file | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
		new_info=`echo $new_info | sed "s/-[0-9]*\.[0-9]*\.[0-9]*/-$ip/"`
		$nvram set $file="$new_info"
	done

	new_info=`$nvram get dmz_ipaddr | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
	$nvram set dmz_ipaddr="$new_info"
	new_info=`$nvram get block_trustedip | sed "s/[0-9]*\.[0-9]*\.[0-9]*/$ip/"`
	$nvram set block_trustedip="$new_info"	

                index=1;
                old_ip=`$nvram get old_lan_ipaddr | sed 's/\([0-9]*.[0-9]*.[0-9]*\).*/\1/'`
                for file in `nvram show | grep static_router | cut -d "=" -f1`
                do
                        new_info=`nvram get $file | awk '{print$6}' | sed 's/\([0-9]*.[0-9]*.[0-9]*\).*/\1/'`
                        if [ "$old_ip" = "$new_info" -a "$ip" != "$new_info" ]; then
                                $nvram unset $file
                        else
                                nvram set static_router$index="`nvram get $file`"
                                if [ "static_router$index" != "$file" ]; then
                                        nvram unset $file
                                fi
                                #let "index += 1"
                                index=$(($index+1))
                        fi
                done
		. /www/cgi-bin/st_router.sh
		config_for_sw2
	
	if [ -f /tmp/static_conflict ]; then
		rm -rf /tmp/static_conflict
		/sbin/cmdlan.sh start
	else
		#/www/cgi-bin/firewall.sh stop
		/sbin/cmdlan.sh restart
		#/www/cgi-bin/firewall.sh start
		$nvram commit
	fi
}

case "$1" in
        start)

         # add http_hijack services, here means not in blankstate, need to run http_hijack when WAN/LAN IP conflict
         sleep 1
         /usr/bin/lan_phy down
         set_lan
         sleep 2
         /usr/bin/lan_phy up
         echo "set wan_lan_ip_conflict=1"
         /usr/sbin/nvram set wan_lan_ip_conflict=1
         echo 2 > /proc/fast_nat
         /etc/rc.d/conflict.sh restart
        	;;
        stop)
#		echo -n 7 > /proc/switch_phy
		;;
        *)
        	echo "Usage: /sbin/ip_conflict.sh start|stop"
        	;;
esac
